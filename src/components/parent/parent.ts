import { Component } from '@angular/core';
import { Events } from 'ionic-angular';

/**
 * Generated class for the ParentComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'parent',
  templateUrl: 'parent.html'
})
export class ParentComponent {

  text: string;

  constructor(public events: Events) {
    console.log('Hello ParentComponent Component');
    // this.text = 'Hello World';
    this.text="";
    // events.subscribe('star-rating:changed', (starRating) => {console.log(starRating)});
  }

}
