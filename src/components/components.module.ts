import { NgModule } from '@angular/core';
import { ParentComponent } from './parent/parent';
@NgModule({
	declarations: [ParentComponent],
	imports: [],
	exports: [ParentComponent]
})
export class ComponentsModule {}
