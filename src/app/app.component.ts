import { Component ,ViewChild} from '@angular/core';
import { Platform ,Events,Nav,AlertController} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AfterSplashPage } from '../pages/after-splash/after-splash';
import { TranslateService } from '@ngx-translate/core';
import { HelperProvider } from '../providers/helper/helper';
import { TabsPage } from '../pages/tabs/tabs';
import { Storage } from '@ionic/storage';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { CompanypostedjobPage } from '../pages/companypostedjob/companypostedjob';
import { AccountsForCompPage } from '../pages/accounts-for-comp/accounts-for-comp';
import { CompanyHomePage } from '../pages/company-home/company-home';
import { CompanyprofilePage } from '../pages/companyprofile/companyprofile';
import { ServicesProvider } from '../providers/services/services';
import { AccountsPage } from '../pages/accounts/accounts'; 
import { InAppBrowser } from '@ionic-native/in-app-browser';


@Component({
  templateUrl: 'app.html',
  providers:[Push]

})
export class MyApp {

  @ViewChild(Nav) nav: Nav;
  rootPage:any ;
  menuSide;
  userLoged = false;//false
 
  userImageUrl: string = "assets/imgs/default-avatar.png";
  userName: string = "";
  userJob: string ="UI/UX";
  menu:any
  constructor( private iab: InAppBrowser,public alertCtrl: AlertController,public srv:ServicesProvider, private push: Push,public events: Events, public storage:Storage,public translate: TranslateService,public helper:HelperProvider,
    public platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
      
      //changed min sdk from 19 to 21  966000000000
      //comp login acount 966896587412 , js account 96600.. ,9666..
      //apple account js 123456789 , pass 123123
      //apple comp 987654321 , pass 123123 
      
      events.subscribe('menu', (val) => {
       if(val=="js")
       {
        this.menu=true
       }
       else{
        this.menu=false

       }
     
      });

      // events.subscribe('changeLang', () => {
      //   console.log(" subscribe changeLang",this.platform.dir())
      //   // this.initializeLang();
      // });
      events.subscribe('user:userLoginSucceeded', (userData) => {
        console.log("suscribe userLoginSucceeded",userData)
        

        this.userLoged=true;
        if(userData.type=="js")
        this.menu=true;
       else
        this.menu=false;


        if(userData.type=="js")
        {
          console.log("js",userData.FirstName)
          this.userName = userData.FirstName;
          this.userImageUrl =userData.ProfileImage;
        } 
        else
        {console.log("comp",userData)
        // userData = JSON.parse(userData);
          console.log("comp",userData.CompanyName);
          console.log("comp",userData.CompanyName);

          this.userName = userData.CompanyName;
          this.userImageUrl =userData.CompanyLogo;

        }

       
        // console.log("userLoginSucceeded: " + JSON.stringify(userData))
        // this.userName = userData.FirstName;
        
        // this.userImageUrl = "assets/imgs/default-avatar.png"; //userData.ProfileImage;
      });

      events.subscribe('user:userLogedout', () => {
        this.userLoged = false;
        console.log("user loged out")
      });

      events.subscribe('profileImageChanged', (data) => {
       this.userImageUrl = data
      });

      events.subscribe('profileChanged', (data) => {
        this.userName = data
       });

       
      events.subscribe('user:userLogedout2', () => {
        this.userLoged = false;
        console.log("user loged out2");
        this.Logout2();
      });

      platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      statusBar.backgroundColorByHexString("#696767");
      splashScreen.hide();
      this.initializeLang();
      this.initPage();
      this.pushnotification();
    });

    if(platform.is('ios'))
    {
      this.helper.device_type="0";
      console.log("ios device type",this.helper.device_type);
    }else{
      this.helper.device_type="1";
      console.log("android device type",this.helper.device_type);
    }

  }

  initPage(){
    
      this.storage.get('js_info').then((val) => {
        console.log("val from get js info",val);
        if(val)
        {
          // if(val.type=="js")
          // {
          //   this.menu=true;
          //   this.userLoged=true;
          //   this.rootPage = TabsPage;
          //   this.userName = val.FirstName;
          //   this.userImageUrl ="assets/imgs/default-avatar.png"; //val.ProfileImage;
          
          // } 
          // else
          // {
          //   this.menu=false;
          //   this.userName = val.CompanyName;
          //   this.userImageUrl ="assets/imgs/default-avatar.png"; //val.CompanyLogo;
          // }
            
          if(val.type=="js")
          {
            this.menu=true;
            this.userLoged=true;
            this.rootPage = TabsPage;
            this.userName = val.FirstName;
            this.userImageUrl = val.ProfileImage;
          } 
          else
          {
            this.menu=false;
            this.userLoged=true;
            this.rootPage = TabsPage;
            this.userName = val.CompanyName;
            this.userImageUrl = val.CompanyLogo;

          }
          // this.userLoged=true;
          //   this.rootPage = TabsPage;
          //   this.userName = val.FirstName;
          //   this.userImageUrl ="assets/imgs/default-avatar.png"; //val.ProfileImage;

          
          
        }  
        else
          this.rootPage = AfterSplashPage; 
      }).catch(err=>{
        console.log("catch from get js info",err);
      });
  }
  initializeLang() {
   
    this.storage.get('language').then((val) => {
      if(val){
        console.log("lang value detected",val);
        if(val == 'ar'){
          this.translate.setDefaultLang('ar');  
          this.translate.use('ar');    
          this.platform.setDir('rtl', true);
          this.helper.lang_direction = "rtl";   
          this.menuSide = "right";

        }else{
          console.log("form initializeLang: ",val);
          this.translate.setDefaultLang('en');
          this.translate.use('en');
          this.platform.setDir('ltr', true);
          this.helper.lang_direction = "ltr";
          this.menuSide = "left";
        }
      }else{
        console.log("lang value not detected",val);
        var userLang = navigator.language.split('-')[0];
        console.log("navigator.language",navigator.language);
        console.log("userlang",userLang);
        
      //  userLang = 'ar';
    
        if (userLang == 'ar') {
          this.translate.setDefaultLang('ar');  
          this.translate.use('ar');    
          this.platform.setDir('rtl', true);
          this.helper.lang_direction = "rtl";   
          this.menuSide = "right";
        }else {
              this.translate.setDefaultLang('en');
              this.translate.use('en');
              this.platform.setDir('ltr', true);
              this.helper.lang_direction = "ltr";
              this.menuSide = "left";
        }

      }
    });



  }

openProfileImg(){

}
openProfile(){
  // this.nav.push('JsProfilePage');
  this.nav.getActiveChildNav().select(3)
}
openActiveJobs(){
  this.nav.push('ActiveJobsPage');
}
openjobsHistory(){
  // this.nav.push('JobHistoryPage');
  this.nav.getActiveChildNav().select(2)
}
openSettings(){
// this.changelang();
this.nav.push('SettingsPage');
  
}
openAppliedJobs(){
  this.nav.push('JsAppliedJobsPage');
}
Logout(){
  console.log("logout");

  let alert = this.alertCtrl.create({
    // title: this.translate.instant(""),
    message: this.translate.instant("logoutconfirm"),
    buttons: [
      {
        text: this.translate.instant("no2"),
        role: 'cancel',
        handler: () => {
          console.log('disagree clicked');
        }
      },
      {
        text: this.translate.instant("yes2"),
        handler: () => {
          console.log('agree clicked');
          this.storage.remove('js_info').then(()=>{
            console.log("js_info removed");
            this.userLoged = false;
            this.nav.setRoot(AfterSplashPage); 
          });
   
        }
      }
    ]
  });
  alert.present();


  
}
Logout2(){
  console.log("logout");
  this.storage.remove('js_info').then(()=>{
    console.log("js_info removed");
    this.userLoged = false;
    this.nav.setRoot(AfterSplashPage); 
  });
}

openpostedJobs(){
  // this.nav.push(CompanypostedjobPage);
  
  // this.nav.parent.select(1);
  this.nav.getActiveChildNav().select(1)
 
}
opencomphome(){
  // this.nav.push(  CompanyHomePage);
  this.nav.getActiveChildNav().select(0)
}
opencompbalance(){
  this.nav.push(AccountsForCompPage);
  // this.nav.getActiveChildNav().select(2)
}
openjsbalance(){
  this.nav.push(AccountsPage);
}
opencompProfile(){
  // this.nav.push(CompanyprofilePage);
  //this.nav.setRoot(TabsPage);
  // this.nav.select(3);
  console.log("nav",this.nav)
  this.nav.getActiveChildNav().select(3)
}
openmonytransfer(){
  console.log("open money transfer history");
  this.nav.push('MoneytransferPage',{from:"js"});
}
openmonytransferforcomapny(){
  this.nav.push('MoneytransferPage',{from:"company"});
}
opencompjobsHistory(){
  console.log("open opencompjobsHistory");
  // this.nav.push('CompanyJobHistoryPage');
  this.nav.getActiveChildNav().select(2)
  
}
openAboutApp(){
  // window.open("http://perhour-sa.com/");
  const browser = this.iab.create("http://perhour-sa.com/",'_system',"location=yes");
}

changepass(){
  console.log("changepass ");
  var idforcomporjs;
  this.storage.get('js_info').then((val)=>{
    console.log("js_info from storage: " ,JSON.stringify(val))
    if(val)
    {
      idforcomporjs=val.SID 
      this.nav.push('ChangepassPage',{type:"compchangepass",mobile:"",id:idforcomporjs });
    }
  });
  
}


changepassforjs(){
  console.log("changepassforjs ");
  var idforcomporjs;
  this.storage.get('js_info').then((val)=>{
    console.log("js_info from storage: " ,JSON.stringify(val))
    if(val)
    {
      idforcomporjs=val.SID 
      this.nav.push('ChangepassPage',{type:"jschangepass",mobile:"",id:idforcomporjs });
    }
  });
  
}
openTermsAndConditions(){
  console.log("showConditions: ");
  this.srv.getLicense(resp=>{
    console.log("getLicense resp",resp);
    var respData = JSON.parse(resp);
    var msgTxt = "" ;
    console.log("this.helper.lang_direction",this.helper.lang_direction)
    console.log("respData.LicenseAgreement_En : ",respData.LicenseAgreement_En)
    if(this.helper.lang_direction == "rtl")
     msgTxt =  respData.LicenseAgreement_Ar;
    else if(this.helper.lang_direction == "ltr")
      msgTxt =  respData.LicenseAgreement_En;

    // this.presentAlert(msgTxt);
    this.nav.push('ConditionsPage',{data:msgTxt});
  },err=>{
    console.log("getLicense err",err);
    this.helper.presentToast(this.translate.instant("ServerError"))
  })

}
openordercetificate(){
  console.log("openordercetificate")
  
  let alert = this.alertCtrl.create({
    // title: this.translate.instant(""),
    message: this.translate.instant("requestcertificateconfirm"),
    buttons: [
      {
        text: this.translate.instant("tryagain"),
        role: 'cancel',
        handler: () => {
          console.log('disagree clicked');
        }
      },
      {
        text: this.translate.instant("aggree"),
        handler: () => {
          console.log('agree clicked');
        
          this.srv.companylistjobcategories(resp=>{
            console.log("resp from companylistjobcategories",resp)
            this.categoriesAlert(JSON.parse(resp))
          },err=>{
            console.log("err from companylistjobcategories",err)
          })
          
        }
      }
    ]
  });
  alert.present();
}
categoriesAlert(allData){
  var alertInput = []
  var labeltxt="";
  for(var i=0;i<allData.length;i++){
    if(this.helper.lang_direction == "rtl")
      labeltxt = allData[i].CategoryNameAr
    else if(this.helper.lang_direction == "ltr")
    {
      labeltxt = allData[i].CategoryNameEn
console.log("from rtl labeltxt : ",labeltxt)
    }
     
//       console.log("this.helper.lang_direction: ",this.helper.lang_direction)
//     console.log("allData[i].CategoryNameEn : ",allData[i].CategoryNameEn)
// console.log("labeltxt : ",labeltxt)
    alertInput.push({type:'radio',
    label:labeltxt,
    value:allData[i].SID})
  }

  console.log("alertInput : ",alertInput)
  let alert = this.alertCtrl.create({
    title: this.translate.instant("Job_category"),
    // message: this.translate.instant(""),
    inputs : alertInput,
    buttons: [
      {
        text: this.translate.instant("cancel"),
        role: 'cancel',
        handler: (data) => {
          console.log('disagree clicked',data);
        }
      },
      {
        text: this.translate.instant("done"),
        handler: (catid) => {
          console.log('agree clicked',catid);
          var jsid
          this.storage.get('js_info').then((val) => {
            console.log("val from get js info",val);
            if(val)
            {
              jsid = val.SID
              console.log("user id : ",jsid," cat id: ",catid)
              this.srv.requestExperienceCertificate(jsid,catid,resp=>{
                console.log("resp from requestExperienceCertificate",resp);
                if(JSON.parse(resp).result == "-1"){
                  this.helper.presentToast(this.translate.instant("requestErr"))
                }else if(JSON.parse(resp).result == "1"){
                  this.helper.presentToast(this.translate.instant("donesuccessfully"))
                }
              },err=>{
                console.log("err from requestExperienceCertificate",err);
              })
            }
          });
        
        }
      }
    ]
  });
  alert.present();
}
pushnotification() {
  let options: PushOptions
    options = {
      android: {
        forceShow:true
      },
      ios: {
        alert: 'true',
        badge: true,
        sound: 'true'
      }
    }
  const pushObject: PushObject = this.push.init(options);
  pushObject.on('notification').subscribe((notification: any) => {
    console.log("notification " + JSON.stringify(notification))
    
    if (this.platform.is('ios')) {
      console.log("ios notification",notification);
      
              if(notification.additionalData["gcm.notification.redirectTo"] == "3")
      {
        //js hire goto active jobs

        // this.nav.setRoot(TabsPage);
        // this.nav.push('ActiveJobsPage');
        // this.nav.setRoot(TabsPage,{tab: 2});
      //  / this.nav.setRoot(TabsPage);
      // this.nav.parent.select(2);
      this.nav.getActiveChildNav().select(2)

      }else if(notification.additionalData["gcm.notification.redirectTo"] == "1")
      {
        //comp  js checkin apear view to accept or reject
        this.nav.setRoot(TabsPage);
        this.nav.push('CompanyCheckInNotificationPage',{notificationType:"checkIn",jsid:notification.additionalData["gcm.notification.seeker"],jobId:notification.additionalData["gcm.notification.jobID"]});
  
      }else if(notification.additionalData["gcm.notification.redirectTo"] == "2")
      {
        //comp  js checkout apear view to accept or reject
        this.nav.setRoot(TabsPage);
        this.nav.push('CompanyCheckInNotificationPage',{notificationType:"checkout",jsid:notification.additionalData.seeker,jobId:notification.additionalData.jobID});
  
      }else if(notification.additionalData["gcm.notification.redirectTo"] == "5")
      {
        //js  comp accept checkin 
        // this.nav.setRoot(TabsPage);
        // this.nav.setRoot(TabsPage);
        // this.nav.push('ActiveJobsPage');
        // this.nav.setRoot(TabsPage,{tab: 2});
        // this.nav.setRoot(TabsPage);
        // this.nav.parent.select(2);
        this.nav.getActiveChildNav().select(2)
  
      }else if(notification.additionalData["gcm.notification.redirectTo"] == "6")
      {
        //js  comp reject checkin 
        // this.nav.setRoot(TabsPage);
        // this.nav.setRoot(TabsPage);
        //     this.nav.push('ActiveJobsPage');
        // this.nav.setRoot(TabsPage,{tab: 2});
        // this.nav.setRoot(TabsPage);
        // this.nav.parent.select(2);
        this.nav.getActiveChildNav().select(2)
      }else if(notification.additionalData["gcm.notification.redirectTo"] == "4")
      {
        //comp  js cancelJOb 
        this.nav.setRoot(TabsPage);
        this.nav.push('CompanyCheckInNotificationPage',{notificationType:"cancel",jsid:notification.additionalData["gcm.notification.seeker"],jobId:notification.additionalData["gcm.notification.jobID"]});
  
      }else if(notification.additionalData["gcm.notification.redirectTo"] == "7" || notification.additionalData["gcm.notification.redirectTo"] == "8")
      {
        //confirm company for js late or ontime
        this.nav.setRoot(TabsPage);
    //     var newjobname = "";
    // if(this.langDirection == "rtl")
    // newjobname = item.JobTitleAr;
    // else if(this.langDirection == "ltr")
    // newjobname = item.JobTitleEn;
    this.nav.push("CompanyselectedusersPage",{jobId:notification.additionalData["gcm.notification.jobID"],jobname:""})

      }else if(notification.additionalData["gcm.notification.redirectTo"] == "9" )
      {
       //comp  js apply open postedjobs
       this.nav.setRoot(TabsPage);
       // this.nav.push('CompanyCheckInNotificationPage',{notificationType:"checkIn",jsid:notification.additionalData.seeker,jobId:notification.additionalData.jobID});
       this.nav.push(CompanypostedjobPage,{from:"notification"});
      }else if(notification.additionalData["gcm.notification.redirectTo"] == "10" )
      {
       //js open job details
       this.nav.setRoot(TabsPage);
   
       this.nav.push("JobDetailsPage",{'jobDetails':"","jobId":notification.additionalData["gcm.notification.jobID"],'from':"notification"})
      }

      
    }else {
      console.log("notification from android",notification);
      if(notification.additionalData.redirectTo == "3")
      {
        //js hire goto active jobs
        console.log("from redirectTo 3")
        // this.nav.setRoot(TabsPage);
        // this.nav.push('ActiveJobsPage');
        // this.nav.setRoot(TabsPage,{tab: 2});
        // this.nav.setRoot(TabsPage);
        // this.nav.parent.select(2);
        this.nav.getActiveChildNav().select(2)

      }else if(notification.additionalData.redirectTo == "1")
      {
        //comp  js checkin apear view to accept or reject
        this.nav.setRoot(TabsPage);
        this.nav.push('CompanyCheckInNotificationPage',{notificationType:"checkIn",jsid:notification.additionalData.seeker,jobId:notification.additionalData.jobID});
  
      }else if(notification.additionalData.redirectTo == "2")
      {
        //comp  js checkout apear view to accept or reject
        this.nav.setRoot(TabsPage);
        this.nav.push('CompanyCheckInNotificationPage',{notificationType:"checkout",jsid:notification.additionalData.seeker,jobId:notification.additionalData.jobID});
  
      }else if(notification.additionalData.redirectTo == "5")
      {
        //js  comp accept checkin 
        // this.nav.setRoot(TabsPage);
        // this.nav.setRoot(TabsPage);
        // this.nav.push('ActiveJobsPage');
        // this.nav.setRoot(TabsPage,{tab: 2});
        // this.nav.setRoot(TabsPage);
        // this.nav.parent.select(2);
        this.nav.getActiveChildNav().select(2)
  
      }else if(notification.additionalData.redirectTo == "6")
      {
        //js  comp reject checkin 
        // this.nav.setRoot(TabsPage);
        // this.nav.setRoot(TabsPage);
        //     this.nav.push('ActiveJobsPage');
        // this.nav.setRoot(TabsPage,{tab: 2});
        // this.nav.setRoot(TabsPage);
        // this.nav.parent.select(2);
        this.nav.getActiveChildNav().select(2)
  
      }else if(notification.additionalData.redirectTo == "4")
      {
        //comp  js cancelJOb 
        this.nav.setRoot(TabsPage);
        this.nav.push('CompanyCheckInNotificationPage',{notificationType:"cancel",jsid:notification.additionalData.seeker,jobId:notification.additionalData.jobID});
  
      }else if(notification.additionalData.redirectTo == "7" || notification.additionalData.redirectTo == "8")
      {
        //confirm company for js late or ontime
        this.nav.setRoot(TabsPage);
    //     var newjobname = "";
    // if(this.langDirection == "rtl")
    // newjobname = item.JobTitleAr;
    // else if(this.langDirection == "ltr")
    // newjobname = item.JobTitleEn;
    this.nav.push("CompanyselectedusersPage",{jobId:notification.additionalData.jobID,jobname:""})

      }else if(notification.additionalData.redirectTo == "9")
      {

  // this.nav.push('CompanyCheckInNotificationPage',{notificationType:"checkIn",jsid:notification.additionalData.seeker,jobId:notification.additionalData.jobID});        //comp  js apply open postedjobs
     
       this.nav.setRoot(TabsPage);
     
        this.nav.push(CompanypostedjobPage,{from:"notification"});
        //this.nav.setRoot(TabsPage,{tab: 1,from:"notification"});
      }else if(notification.additionalData.redirectTo == "10" )
      {
       //js open job details
       this.nav.setRoot(TabsPage);
   
       this.nav.push("JobDetailsPage",{'jobDetails':"","jobId":notification.additionalData.jobID,'from':"notification"})
      }

      // 1,2,4 com \n 3 js \n 5,6 js accept , reject 
      // if(notification.additionalData.type == "js")
      // {
      //   // hire =>1
      //   if(notification.additionalData.status == "1")
      //   {
      //     this.nav.setRoot(TabsPage);
      //     this.nav.push('ActiveJobsPage');
      //   }




      // }else if(notification.additionalData.type == "comp"){
      //   //candidtate check in => 1
      //   //candidtate check out => 2
      //   if(notification.additionalData.status == "1"){
      //     this.nav.setRoot(TabsPage);
      //     this.nav.push('CompanyCheckInNotificationPage',{jsid:1047,jobId:notification.additionalData.jobID});

          
      //   }else if(notification.additionalData.status == "1"){

      //   }




      // }
          
    }
 
  });
  pushObject.on('registration').subscribe((registration: any) => {
    console.log("registraion : ",registration);
    console.log("registrationId " + registration.registrationId)
    this.helper.registration = registration.registrationId;


    if(localStorage.getItem("firebaseRegNoti")){
      if(localStorage.getItem("firebaseRegNoti") == registration.registrationId){
        localStorage.setItem("regChanged","0")
      }
      else{
        localStorage.setItem("regChanged","1")
        localStorage.setItem("firebaseRegNoti",registration.registrationId)
      
      }
    }


  });

  pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));
}

changelang(){
console.log("this.platform.dir(): ",this.platform.dir());
  if (this.helper.lang_direction == 'ltr') {
    console.log("stroe ar lang");
    this.helper.storeLanguage('ar');
    this.translate.setDefaultLang('ar');  
    this.translate.use('ar');    
    this.platform.setDir('rtl', true);
    this.helper.lang_direction = "rtl"; 
   // this.lang_direction = "rtl";  
    this.menuSide = "right";
   
  }else {
    console.log("stroe en lang");
    this.helper.storeLanguage('en');
        this.translate.setDefaultLang('en');
        this.translate.use('en');
        this.platform.setDir('ltr', true);
        this.helper.lang_direction = "ltr";
//        this.lang_direction = "ltr";

          this.menuSide = "left";

  }

}

}
