import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler, IonicPageModule } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { AfterSplashPage } from '../pages/after-splash/after-splash';
import { JobListPage } from '../pages/job-list/job-list';
import { MapPage } from '../pages/map/map';
import { JsProfilePage } from '../pages/js-profile/js-profile';
import { CompanyHomePage } from '../pages/company-home/company-home';
import { AccountsPage } from '../pages/accounts/accounts'; 
import { CompanyJobHistoryPage } from "../pages/company-job-history/company-job-history";
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpModule, Http } from '@angular/http';
import { HttpClient,HttpClientModule } from '@angular/common/http';
import { HelperProvider } from '../providers/helper/helper';
import { ServicesProvider } from '../providers/services/services';
import { CompanycheckinviewPage } from '../pages/companycheckinview/companycheckinview';
import { CompanyprofilePage } from '../pages/companyprofile/companyprofile';
import { CompanyjophistoryPage } from '../pages/companyjophistory/companyjophistory';
import { IonicStorageModule } from '@ionic/storage';

import { CompanypostajobPage } from '../pages/companypostajob/companypostajob';
import { CompanypostedjobPage } from '../pages/companypostedjob/companypostedjob';
import { DatePipe } from '@angular/common';

import { AccountsForCompPage } from '../pages/accounts-for-comp/accounts-for-comp';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Diagnostic } from '@ionic-native/diagnostic';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Geolocation } from '@ionic-native/geolocation';
import { StreamingMedia} from '@ionic-native/streaming-media';
import { ChartsModule } from 'ng2-charts';
import { Ionic2RatingModule } from "ionic2-rating";
import { JobHistoryPage } from '../pages/job-history/job-history';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { ActiveJobsPage } from '../pages/active-jobs/active-jobs';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}



@NgModule({
  declarations: [
    CompanyprofilePage,
    CompanyjophistoryPage,
    CompanycheckinviewPage,
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    AfterSplashPage,
    JobListPage,
    MapPage,
    CompanyHomePage,
    CompanypostajobPage,
    CompanypostedjobPage,
    AccountsPage,
    AccountsForCompPage,
    JsProfilePage,
    JobHistoryPage,
    CompanyJobHistoryPage,
    ActiveJobsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    // TranslateModule.forRoot(),
    HttpModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    IonicStorageModule.forRoot(),
    ChartsModule,
    Ionic2RatingModule
    
    // Ionic2RatingModule 
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    CompanyprofilePage,
    CompanyjophistoryPage,
    CompanycheckinviewPage,
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    AfterSplashPage,
    JobListPage,
    MapPage,
    CompanyHomePage,
    CompanypostajobPage,
    CompanypostedjobPage,
    AccountsPage,
    AccountsForCompPage,
    JsProfilePage,
    JobHistoryPage,
    CompanyJobHistoryPage,
    ActiveJobsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    HttpClient,
    HelperProvider,
    ServicesProvider,
    [DatePipe],
    SocialSharing,
    Diagnostic, 
    LocationAccuracy,
    Geolocation,
    StreamingMedia,
    InAppBrowser
  ]
})
export class AppModule {}
