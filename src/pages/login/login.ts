import { Component ,ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams ,Slides,Platform,Events} from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { TabsPage } from '../tabs/tabs';
import { TranslateService } from '@ngx-translate/core';
import { HelperProvider } from '../../providers/helper/helper';
import { ServicesProvider } from '../../providers/services/services';


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  @ViewChild(Slides) slides: Slides;

  jobSeeker = true;
  
  private logInForm : FormGroup;
  email;
  password;
  submitAttempt = false;
  errors = {
    emailErr:"",
    passwordErr:""
  };

  placeholder = {email:"",password:""};
  langDirection = "";
  scaleClass="";

  constructor( public events:Events,public service:ServicesProvider, public platform: Platform,public helper:HelperProvider,
    private formBuilder: FormBuilder,public translate: TranslateService,
    public navCtrl: NavController, public navParams: NavParams) {
    
      this.langDirection = this.helper.lang_direction;

      if(this.langDirection == "rtl")
      this.scaleClass="scaleClass";

      console.log("this.langDirection from login: ",this.langDirection);

      this.logInForm = this.formBuilder.group({
        email: ['', Validators.required],
        password: ['', Validators.required],
      });

      this.errors.emailErr = this.translate.instant("mobileErr");
      this.errors.passwordErr = this.translate.instant("passwordErr");

      var loginType = this.navParams.get('type');
      console.log("login type",loginType);
      if(loginType == "jobSeeker")
        this.jobSeeker = true;
      else if(loginType == "company")
        this.jobSeeker = false;

     
    

        console.log("this.js",this.jobSeeker);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
    this.placeholder.email = this.translate.instant("enterMobileNumber");
    this.placeholder.password = this.translate.instant("enterPassword");
  }


  login(){
    console.log("form valid",this.logInForm.valid);
   
    var loginData = {
      "email":this.email,
      "password":this.password
    };
    console.log("login data: ",loginData);
    if(!this.logInForm.valid)
      this.submitAttempt = true;
    else
    {
      console.log("x.charAt(0)", this.email.charAt(0))
      console.log("substr: ",this.email.substr(1))
    console.log("966+mobile","966"+this.email)
    if(this.email.charAt(0) == "0" || this.email.charAt(0) == "٠")
    {
      this.email =  this.email.substr(1);
      // this.email = "966"+this.email;
      console.log("from if =0 this.mobile: ",this.email);
    }else{
      // this.email = "966"+this.email;
      console.log("from else =0 this.mobile: ",this.email);
    }
  
      if(this.jobSeeker == true)
      {
        this.service.jsLogin(this.email,this.password,resp=>{
          console.log("resp from login",resp);
          
          var respData = JSON.parse(resp);
  
          if(respData["SID"])
          {
            respData.type = "js";
            this.helper.storeJSInfo(respData);
            this.helper.storePass(this.password)
            this.events.publish('user:userLoginSucceeded', respData);
            this.navCtrl.setRoot(TabsPage); 
          }
          else 
            this.helper.presentToast(this.translate.instant("invalidData"))
          /*
          {"SID":1013,"FirstName":"ss","MiddleName":"","SirName":"","Email":"ss@ss.ssss","Mobile":"01023456785","ProfileImage":"2002159.jpg","CreationDate":"2018-11-17T10:56:43.473"}
  
           */
  
        },err=>{
          console.log("err from login",err);
        });
      }else{

        this.service.companyLogin(this.email,this.password,(data)=>{

          console.log(JSON.stringify(data))
          // this.navCtrl.setRoot(TabsPage); 
          let DataParse = JSON.parse(data);
          if(DataParse["SID"])
          {
          DataParse.type = "company";
            this.helper.storeJSInfo(DataParse);
            this.helper.storePass(this.password)
            this.events.publish('menu',"com");
            
            this.events.publish('user:userLoginSucceeded',DataParse);
            this.navCtrl.setRoot(TabsPage); 
          }else 
            this.helper.presentToast(this.translate.instant("invalidData"))

        },(data)=>{
          console.log(JSON.stringify(data))
  
          
        })
        

        
      }

    } 
      
  }
  signup(){
    this.navCtrl.push('SignupPage',{jobSeeker:this.jobSeeker});
  }
  forgetPassword(){
    this.navCtrl.push('ForgetPasswordPage',{jobSeeker:this.jobSeeker});
  }

  dismiss(){
    console.log("pop login")
    this.navCtrl.pop();
  }

  changeTxt(){
    console.log("mobile before replacement  ...",this.email);
    this.email = this.textArabicNumbersReplacment(this.email);
    console.log("phone after replacement: ",this.email); 
  
  }

  textArabicNumbersReplacment(strText) {
    var strTextFiltered = strText.trim();
    strTextFiltered = strText;
    strTextFiltered = strTextFiltered.replace(/[\٩]/g, '9');
    strTextFiltered = strTextFiltered.replace(/[\٨]/g, '8');
    strTextFiltered = strTextFiltered.replace(/[\٧]/g, '7');
    strTextFiltered = strTextFiltered.replace(/[\٦]/g, '6');
    strTextFiltered = strTextFiltered.replace(/[\٥]/g, '5');
    strTextFiltered = strTextFiltered.replace(/[\٤]/g, '4');
    strTextFiltered = strTextFiltered.replace(/[\٣]/g, '3');
    strTextFiltered = strTextFiltered.replace(/[\٢]/g, '2');
    strTextFiltered = strTextFiltered.replace(/[\١]/g, '1');
    strTextFiltered = strTextFiltered.replace(/[\٠]/g, '0');
    return strTextFiltered;
  }

}
