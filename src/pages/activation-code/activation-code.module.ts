import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ActivationCodePage } from './activation-code';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';

@NgModule({
  declarations: [
    ActivationCodePage,
  ],
  imports: [
    IonicPageModule.forChild(ActivationCodePage),
    TranslateModule.forChild()
  ],
})
export class ActivationCodePageModule {}
