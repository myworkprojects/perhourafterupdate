import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams ,Events} from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
import { TabsPage } from '../tabs/tabs';
import { ServicesProvider } from '../../providers/services/services';
import { TranslateService } from '@ngx-translate/core';
import { HomePage } from '../home/home';
import { CompanyHomePage } from '../company-home/company-home';

@IonicPage()
@Component({
  selector: 'page-activation-code',
  templateUrl: 'activation-code.html',
})
export class ActivationCodePage {

  // @ViewChild('focusInput0') myInput0 ;
  
  @ViewChild('focusInput1') myInput1 ;
  @ViewChild('focusInput2') myInput2 ;
  @ViewChild('focusInput3') myInput3 ;
  @ViewChild('focusInput3') myInput4 ;

  // @ViewChild('focusInput4') myInput4 ;

  // @ViewChild('focusInput0') focusInput0 ;
  // @ViewChild('focusInput1') myInput2 ;
  // @ViewChild('focusInput2') myInput3 ;
  // @ViewChild('focusInput3') myInput4 ;

  input1:any
  input2:any
  input3:any
  input4:any
  scaleClass="";
  langDirection;
  JsSID;
  userDataFromRegister;

  type;
  ver;
  timer;
  time=60;
  borderColor="#e19419";
  hideMark = true;
  flag = false;

  constructor(public  events:Events,public translate: TranslateService,public service:ServicesProvider, public helper:HelperProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.langDirection = this.helper.lang_direction;
    if(this.langDirection == "rtl")
    this.scaleClass="scaleClass";

    this.JsSID =  this.navParams.get('JsSID');
    console.log("this.JsSID",this.JsSID,": ",this.navParams.get('JsSID'),":",this.navParams.get('data'))
    //  this.JsSID = "1013";

    this.userDataFromRegister = this.navParams.get("userdata");
    console.log("this.userDataFromRegister",this.userDataFromRegister);

    this.type = this.navParams.get("type");

   this.enableTimer();
   this.borderColor="#e19419";
   this.hideMark = true;
   

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ActivationCodePage');
    // this.input1 = "";
    //this.focusInput0.setFocus();
    
  }

  home(){
    console.log("activation this.type",this.type);
    if(this.type == "js")
    {
      //this.ver
      this.service.jsActivationCode(this.JsSID,this.ver,resp=>{
        console.log("activation resp",resp);
        var respData = JSON.parse( resp);
        if(respData["Result"] == "1")
        {
          this.helper.presentToast(this.translate.instant("activationDone"));
          clearTimeout(this.timer);
  
          this.service.jsLogin(this.userDataFromRegister.mobile,this.userDataFromRegister.pass,resp=>{
            console.log("resp from login activation",resp);
            
            var respData = JSON.parse(resp);
    
            if(respData["SID"])
            {
              respData.type="js";
              this.helper.storeJSInfo(respData);
              this.helper.storePass(this.userDataFromRegister.pass)
              this.events.publish('user:userLoginSucceeded', respData);
             this.events.publish('menu',"js")
              // this.navCtrl.parent.select(0)
              // this.navCtrl.setRoot(HomePage);
              this.navCtrl.setRoot(TabsPage); 
            }
            
    
          },err=>{
            console.log("err from login",err);
            this.flag=false;
          });
  
  
  
          //this.navCtrl.setRoot(TabsPage);
        } 
        else if( respData["Result"] == "-1"){
          this.helper.presentToast(this.translate.instant("activationWrongCode"));
          this.input1="";
          this.input2="";
          this.input3="";
          this.input4="";
          this.hideMark = true; 
          this.flag=false;
        }
          
  
  
      },err=>{
        console.log("activation err",err);
        this.flag=false;
      })

    }else if( this.type == "company"){

  
      this.service.comverification(this.JsSID,this.ver,(data)=>{
        let DataParse = JSON.parse(data);
        
        console.log("comverification parse da'ta",DataParse);
        // if(DataParse.Result==-1)  
        //  this.helper.presentToast(this.translate.instant('activationWrongCode'));
        // else
        // {
        //    DataParse.type = "company";
        //     this.helper.storeJSInfo(DataParse);
        //     this.events.publish('menu',"com")
        //     this.navCtrl.setRoot(TabsPage);     
        //      }
        if(DataParse["Result"] == "1")
        {
          this.helper.presentToast(this.translate.instant("activationDone"));
          clearTimeout(this.timer);
  
          this.service.companyLogin(this.userDataFromRegister.mobile,this.userDataFromRegister.pass,resp=>{
            console.log("resp from login activation",resp);
            
            var respData = JSON.parse(resp);
    
            if(respData["SID"])
            {
              respData.type="company";
              this.helper.storeJSInfo(respData);
              this.helper.storePass(this.userDataFromRegister.pass)
              this.events.publish('user:userLoginSucceeded', respData);
             this.events.publish('menu',"com")
              // this.navCtrl.parent.select(0)
              // this.navCtrl.setRoot(HomePage);
              this.navCtrl.setRoot(TabsPage); 
            }
            
    
          },err=>{
            console.log("err from login",err);
            this.flag=false;
          });
  
  
  
          //this.navCtrl.setRoot(TabsPage);
        } 
        else if( DataParse["Result"] == "-1")
        {
          this.helper.presentToast(this.translate.instant("activationWrongCode"));
          this.input1="";
          this.input2="";
          this.input3="";
          this.input4="";
          this.hideMark = true; 
          this.flag=false;
        }  
  
        
  
      },
  
       (data)=>{
        console.log(JSON.stringify(data));
        this.flag=false;
       }
     )                         


    }else if (this.type == "js-forget"){

      this.service.jsActivationCodeForForget(this.JsSID,this.ver,resp=>{
        console.log("activation resp",resp);
        var respData = JSON.parse( resp);
        if(respData["Result"] == "1")
        {
          this.helper.presentToast(this.translate.instant("activationDone"));
          clearTimeout(this.timer);
          this.navCtrl.setRoot('ConfirmPasswordPage',{type:"jsActivationForget",mobile:this.userDataFromRegister,id:this.JsSID});
          
        } 
        else if( respData["Result"] == "-1"){
          this.helper.presentToast(this.translate.instant("activationWrongCode"));
          this.input1="";
          this.input2="";
          this.input3="";
          this.input4="";
          this.hideMark = true; 
          this.flag=false;
        }
          
  
  
      },err=>{
        console.log("activation err",err);
        this.flag=false;
      })

    }else if(this.type == "comp-forget"){


      this.service.compActivationCodeForForget(this.JsSID,this.ver,resp=>{
        console.log("activation resp",resp);
        var respData = JSON.parse( resp);
        if(respData["Result"] == "1")
        {
          this.helper.presentToast(this.translate.instant("activationDone"));
          clearTimeout(this.timer);
  
          this.navCtrl.setRoot('ConfirmPasswordPage',{type:"compActivationForget",mobile:this.userDataFromRegister,id:this.JsSID});
          
        } 
        else if( respData["Result"] == "-1"){
          this.helper.presentToast(this.translate.instant("activationWrongCode"));
          this.input1="";
          this.input2="";
          this.input3="";
          this.input4="";
          this.hideMark = true; 
         this.flag=false;
        }
          
  
  
      },err=>{
        console.log("activation err",err);
        this.flag=false;
      })

      
    }
    
    
  }
  dismiss(){
    this.navCtrl.pop();
  }


onInputTime(ev)
{
  console.log("onInputTime")
  this.borderColor="#afc4d3";
  this.myInput1.setFocus();
    // setTimeout(() => {
    //   // Keyboard.show() // for android
    //   this.myInput1.setFocus();
    // },500); //a least 5000ms.
}
onInputTime1(ev)
{
  console.log("onInputTime1")
  this.myInput2.setFocus();
        // setTimeout(() => {
        //   // Keyboard.show() // for android
        //   this.myInput2.setFocus();
        // },500); //a least 5000ms.
}
    onInputTime2(ev)
    {
      console.log("onInputTime2")
                this.myInput3.setFocus();
                // setTimeout(() => {
                //   // Keyboard.show() // for android
                //   this.myInput3.setFocus();
                // },500); //a least 5000ms.
      }
                  onInputTime3()
                  { console.log("onInputTime3");
                  
                    this.ver=this.input1+this.input2+this.input3+this.input4;                    
                    console.log("ver ",this.ver);

                // setTimeout(() => {
                //   // Keyboard.show() // for android
                //   this.ver=this.input1+this.input2+this.input3+this.input4                    
                //   this.home();
                //   },500); //a least 5000ms.    
                         
                  }
                  onInputTime4()
                  { console.log("onInputTime4");
                  
                    this.ver=this.input1+this.input2+this.input3+this.input4;                    
                    console.log("ver 4: ",this.ver);
                    if(this.flag == false && this.input1 && this.input2 && this.input3 && this.input4)
                    {
                      this.flag = true;
                      console.log("complete");
                      this.hideMark = false;
                      this.home();
                    } 

                // setTimeout(() => {
                //   // Keyboard.show() // for android
                //   this.ver=this.input1+this.input2+this.input3+this.input4                    
                //   this.home();
                //   },500); //a least 5000ms.    
                         
                  }

resend(){
  if(this.time>0){
    this.helper.presentToast(this.translate.instant("wait"));
  }else if(this.time == 0){
    this.time = 60;
    this.enableTimer();  
    this.input1="";
    this.input2="";
    this.input3="";
    this.input4="";
    this.hideMark = true;  
    if(this.type =="js")
    {
      this.service.resendActivation(this.JsSID,resp=>{
        console.log("resend activation resp",resp);
        if(JSON.parse(resp).Result == "1")
        {
          this.helper.presentToast(this.translate.instant("resendding"));
          this.flag=false;
        }  
        else
        {
          this.flag=false;
          this.helper.presentToast(this.translate.instant("ServerError"));
        }
          
      },err=>{
        console.log("resend activation err",err);
        this.flag=false;
      });
      
    }else if( this.type == "company"){
      this.service.comresendActivation(this.JsSID,resp=>{
        console.log("resend comresendActivation resp",resp);
        if(JSON.parse(resp).Result == "1")
        {
          this.helper.presentToast(this.translate.instant("resendding"));
          this.flag=false;
        }
        else
        {
          this.flag=false;
          this.helper.presentToast(this.translate.instant("ServerError"));
        }
          
      },err=>{
        console.log("resend comresendActivation err",err);
        this.flag=false;
      });
      
    }else if (this.type == "js-forget"){
      
      this.service.jsresendActivationForForget(this.JsSID,resp=>{
        console.log("resend jsresendActivationForForget resp",resp);
        if(JSON.parse(resp).Result == "1")
        {
          this.helper.presentToast(this.translate.instant("resendding"));
          this.flag=false;
        }
        else
        {
          this.flag=false;
          this.helper.presentToast(this.translate.instant("ServerError"));
        }
          
      },err=>{
        console.log("resend comresendActivation err",err);
        this.flag=false;
      });

    }else if(this.type == "comp-forget"){

      this.service.compresendActivationForForget(this.JsSID,resp=>{
        console.log("resend compresendActivationForForget resp",resp);
        if(JSON.parse(resp).Result == "1")
        {
          this.helper.presentToast(this.translate.instant("resendding"));
          this.flag=false;
        }
        else
        {
          this.flag=false;
          this.helper.presentToast(this.translate.instant("ServerError"));
        }
          
      },err=>{
        console.log("resend comresendActivation err",err);
        this.flag=false;
      });

    }
    
    
  }
}

enableTimer(){
  this.timer =setInterval(()=>{
    this.time--;
      if(this.time <= 0){
        console.log("timer off");
     
        clearTimeout(this.timer);
      }
  },1000);
}


}
