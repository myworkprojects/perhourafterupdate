import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
import { TranslateService } from '@ngx-translate/core';

/**
 * Generated class for the HoursPopoverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-hours-popover',
  templateUrl: 'hours-popover.html',
})
export class HoursPopoverPage {
  fromTime: any;
  toTime: any;
  fromuser
  touser
  constructor(public navCtrl: NavController, public viewCtrl:ViewController, public navParams: NavParams,
    public helper:HelperProvider, public translate: TranslateService) {
      this.fromuser =  this.navParams.get('from')
      this.touser =  this.navParams.get('to')

      console.log("this.fromuser : ",this.fromuser , " : touser : ",this.touser)
      this.fromTime = this.fromuser
      this.toTime = this.touser
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HoursPopoverPage');
  }
  DismissHourss(){
   
    let hours = {from: this.fromTime,to:this.toTime}
    console.log("hours :from pop ",hours);
    if(!this.fromTime || !this.toTime){
      this.helper.presentToast(this.translate.instant("enterFromTime"))
    }else  if(this.fromTime > this.toTime){
      this.helper.presentToast(this.translate.instant("fromgreaterthanto"))
    }
    else{
    //   this.fromTime = this.convertTime24to12(this.fromTime)
    //   this.toTime = this.convertTime24to12(this.toTime)

    // let hours = {from: this.fromTime,to:this.toTime}
    // console.log("hours 2:  ",hours);
      this.viewCtrl.dismiss(hours)
    }
    
  }

  convertTime24to12(time24){
    var tmpArr = time24.split(':'), time12;
    if(+tmpArr[0] == 12) {
    time12 = tmpArr[0] + ':' + tmpArr[1] + ' pm';
    } else {
    if(+tmpArr[0] == 0) {
    time12 = '12:' + tmpArr[1] + ' am';
    } else {
    if(+tmpArr[0] > 12) {
    time12 = (+tmpArr[0]-12) + ':' + tmpArr[1] + ' pm';
    } else {
    time12 = (+tmpArr[0]) + ':' + tmpArr[1] + ' am';
    }
    }
    }
    return time12;
    }
}
