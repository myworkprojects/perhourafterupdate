import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HoursPopoverPage } from './hours-popover';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    HoursPopoverPage,
  ],
  imports: [
    IonicPageModule.forChild(HoursPopoverPage),
    TranslateModule.forChild(),
  ],
})
export class HoursPopoverPageModule {}
