import { Component, ViewChild,ElementRef,NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams  ,Events} from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
import { TranslateService } from '@ngx-translate/core';
import { TabsPage } from '../tabs/tabs';
import { ServicesProvider } from '../../providers/services/services';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Storage } from '@ionic/storage';
declare var google;
var infoWindow;

@Component({
  selector: 'page-job-list',
  templateUrl: 'job-list.html',
})
export class JobListPage {
  @ViewChild('map') mymap:ElementRef;
  map:any
  scaleClass="";
  langDirection="";
  searchjobs;
  morertl="";
  catnames;
  values;
  AllJobs;
  allMarkers = [] ; 
  lat:any
  markers:any=[]
  long:any
  displayBlock = 1;
  check1 = true;
  check2 = false;
  check3 = false;
  recommendedList
  rvalues
  hiderecommendednodata
  userlat;
  userlng
  nearestJobs=[]

  hideallnodata
  searchWord=""

  constructor(private zone:NgZone,public events:Events, public storage:Storage,public socialSharing: SocialSharing,public service:ServicesProvider, public translate: TranslateService,public helper:HelperProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.langDirection = this.helper.lang_direction;
    
    console.log("this.navParams.get(from)",this.navParams.get("from"));
    this.hiderecommendednodata = false;
    this.hideallnodata = false

    if(this.langDirection == "rtl")
    {
      this.scaleClass="scaleClass";
      this.morertl ="left";
    } 
    else{
      
      this.morertl ="right";
    }

    this.searchjobs = this.translate.instant("searchjobs");

   
    this.events.subscribe('detectUserLocation', (data) => {
      console.log("suscribe detectUserLocation",data)

      this.getnearestjob();      
    });

    (window as any).angularComponent = { GoDetail: this.GoDetail, zone: zone };

  }
  GoDetail = (x) => {
    this.zone.run(() => {
       //Navigate To New Page
       console.log("item from jobDetails: ",x);
       console.log("item[0",x[0])
       this.service.getJobDetails(x[0],resp=>{
        console.log("resp from getJobDetails ",resp)
        if(JSON.parse(resp).Result == "1"){
          this.navCtrl.push("JobDetailsPage",{'jobDetails':JSON.parse(resp).details,'from':"apply"})
        }
        
       },err=>{
         console.log("err from getJobDetails ",err)
       })
      // this.navCtrl.push('DetailsFromMapInfoPage', { 'id':x,"gateName":this.gateInfo.name,allTraps:this.trapsData});
      
    }); 
       
 } 

  openJobDetails(newitem){
    this.navCtrl.push("JobDetailsPage",{'jobDetails':newitem,'from':"apply"})
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad JobListPage');
    console.log("this.helper.choosetabs",this.helper.choosetabs);
    if(this.helper.choosetabs == "RecommendedJobs")
    {
      console.log("RecommendedJobs1");
      this.displayBlock = 3;
      this.check1 = false;
      this.check2 = false;
      this.check3 = true;
      this.helper.choosetabs = "";
      this.getrecommended();
      // this.recommendedList = this.helper.recommendedArr

    }else{
      this.displayBlock = 1;
      this.check1 = true;
      this.check2 = false;
      this.check3 = false;
    }

    this.service.jsListAllJobs(resp=>{
      console.log("resp from list all jobs",resp);
     
      var respData = JSON.parse(resp);
      
      // var catIds = Object.keys(respData);
      //  let res = Object.keys(respData).map(key => respData[key]);
      //  res.forEach(element => {
      //    element.hide = 1
      //  });
      // this.values = res
      if(respData != "No Data")
      {

      for(var x=0;x<respData.length;x++){
        if(respData[x].SalaryType == "3" || respData[x].SalaryType == "2"){
          respData[x].PricePerHour = respData[x].MonthlySalary
        }
      }
      this.values = respData
       this.AllJobs = this.values
       this.hideallnodata=false
      console.log("values",this.values);
      console.log("all jobs",this.AllJobs)
    }else{
      this.values = []
       this.AllJobs = []
       this.hideallnodata=true
    }
    },err=>{
      console.log("err from list all jobs",err);
    });
  
  }
  changDispaly(index){
    console.log("changDispaly index: ",index)
    this.displayBlock = index
    console.log("this.displayBlock : ",this.displayBlock)
    if(index == 2){
      this.initMap();
      // this.getnearestjob();
      this.helper.geoLoc(()=>{})
      console.log("after call detectUserLoctaion"); 
 
      this.check1 = false;
      this.check2 = true;
      this.check3 = false;
    }else if(index == 1){
      this.check1 = true;
      this.check2 = false;
      this.check3 = false;
    }else if (index == 3)
  {
    this.check1 = false;
    this.check2 = false;
    this.check3 = true;
    this.getrecommended();
  }

  }
  ionViewWillEnter(){
    console.log("ionViewWillEnter");
    // console.log("this.helper.choosetabs",this.helper.choosetabs);
    // if(this.helper.choosetabs == "RecommendedJobs")
    // {
    //   console.log("RecommendedJobs1");
    //   this.displayBlock = 3;
    //   this.check1 = false;
    //   this.check2 = false;
    //   this.check3 = true;
    //   this.helper.choosetabs = "";
    //   this.getrecommended();
    //   // this.recommendedList = this.helper.recommendedArr

    // }else{
    //   this.displayBlock = 1;
    //   this.check1 = true;
    //   this.check2 = false;
    //   this.check3 = false;
    // }

    // this.service.jsListAllJobs(resp=>{
    //   console.log("resp from list all jobs",resp);
     
    //   var respData = JSON.parse(resp);
      
    //   // var catIds = Object.keys(respData);
    //   //  let res = Object.keys(respData).map(key => respData[key]);
    //   //  res.forEach(element => {
    //   //    element.hide = 1
    //   //  });
    //   // this.values = res
    //   this.values = respData
    //    this.AllJobs = this.values
    //   console.log("values",this.values);
    //   console.log("all jobs",this.AllJobs)

    // },err=>{
    //   console.log("err from list all jobs",err);
    // });
  }
 
  dismiss(){
  
    this.navCtrl.parent.select(0);
  }


  addMarker(item,position, title, companyName, logo,PricePerHour,HourNumber) {
console.log("marker add" + '<table><tr><td><img  class="rounded-circle" '+
'item-start style="width: 40px;height: 40px; border-radius: 50%;"  src='+ logo +'/> '+ 
 ' <h2 style="color:#4a84ac;" text-wrap>'+ title + '</h2></td></tr>'+
 '<tr><td><p style="color:#e19419" >'+ companyName+'</p></td></tr>' +
 '<tr><td> <ion-icon name="mycurrencey" style="font-size:15px"></ion-icon>'+ PricePerHour +'</td></tr>'+
 '<tr><td><ion-icon name="myclock" style="font-size:15px"></ion-icon>'+HourNumber+'</td></tr></table>')

 
  

    let marker = new google.maps.Marker({
     position: position,
     map: this.map,
     draggable: false
   })
   //marker.setIcon('assets/imgs/bus.png');
   this.markers.push(marker)

   google.maps.event.addListener(marker,'click', (evt) => {
     if(infoWindow){
       infoWindow.close()
     }
     infoWindow = new google.maps.InfoWindow({
     content: '<table style="text-align:center;" onclick="window.angularComponent.GoDetail(' +"['"+item+"']"+')"><tr><td><img  class="rounded-circle" '+
     'item-start style="width: 40px;height: 40px; border-radius: 50%;" src='+ logo +' onerror="this.onerror=null;this.src='+"'assets/imgs/default-avatar.png'"+';" > '+ 
      ' <h2 style="color:#4a84ac;font-size: 1em; margin: 4px;" text-wrap>'+ title + '</h2></td></tr>'+
      '<tr><td><p style="color:#e19419;margin:4px" >'+ companyName+'</p></td></tr>' +'</table>'
   });
   infoWindow.open(this.map,marker)
   })
  //  <tr><td> <img src="assets/icon/money.png" style="width:10px;margin:0 4px">'+HourNumber+'</td></tr>
   return marker;
 }


  initMap(){
    console.log("initMap his.helper.lat,this.helper.lng :",this.helper.lat,this.helper.lng)
    const locations =new google.maps.LatLng(this.helper.lat,this.helper.lng)
    let mapOptions = {
        center:locations,
        zoom:15,
        streetViewControl:false
        };
        this. map =new google.maps.Map(this.mymap.nativeElement,mapOptions)
      //  if(!(this.AllJobs==null))
      //  {
      //    for(let i=0;i<this.AllJobs.length;i++){
      //   for(let x=0;x<this.AllJobs[i].length ;x++){
      //     let jobTilte = ""
      //     if(this.helper.lang_direction == 'rtl'){
      //       jobTilte = this.AllJobs[i][x].JobTitleAr
      //     }
      //     else{
      //       jobTilte = this.AllJobs[i][x].JobTitleEn
      //     }
      //     this.addMarker( new google.maps.LatLng(this.AllJobs[i][x].Latitude, this.AllJobs[i][x].Longitude),jobTilte,this.AllJobs[i][x].CompanyName,this.AllJobs[i][x].CompanyLogo,this.AllJobs[i][x].PricePerHour,this.AllJobs[i][x].HourNumber)
      //   }
      //  }
      // }
      let url = "http://maps.google.com/mapfiles/ms/icons/blue-dot.png";
      let marker = new google.maps.Marker({
        position: locations,
        map: this.map,
        draggable: false,
        icon: {
          url: url
        }
      })
      
      this.markers.push(marker)

     
      for(let x=0;x<this.nearestJobs.length ;x++){
        console.log("this.nearestJobs[x] : ",this.nearestJobs[x])
            let jobTilte = ""
            if(this.helper.lang_direction == 'rtl'){
              jobTilte = this.nearestJobs[x].JobTitleAr
            }
            else{
              jobTilte = this.nearestJobs[x].JobTitleEn
            }
            this.addMarker(this.nearestJobs[x].SID, new google.maps.LatLng(this.nearestJobs[x].Latitude, this.nearestJobs[x].Longitude),jobTilte,this.nearestJobs[x].CompanyName,this.nearestJobs[x].CompanyLogo,this.nearestJobs[x].PricePerHour,this.nearestJobs[x].HourNumber)
          }
  }
  getItems(ev: any) {
   console.log("search getItems : ")
      if (ev.target.value == undefined) {
        ev.target.value = "";
      }
      
      if(ev.target.value){
        console.log("target val: "+ev.target.value)
        if(this.displayBlock == 1){
          console.log("search fro all target:",ev.target.value)
      this.filterLoadedData(ev.target.value)
        }else if(this.displayBlock == 3){
          console.log("search fro recommended target:",ev.target.value)
          this.filterRecommendedLoadedData(ev.target.value)
        }
      }else{
        if(this.displayBlock == 1){
          console.log("search fro all")
        //   if(this.values.length > 0){
        //   for(let i = 0; i < this.values.length ; i++){
         
            
        //     this.values[i].hide = 1
          
           
        //   }
        // }

        this.service.jsListAllJobs(resp=>{
          console.log("resp from list all jobs",resp);
          var respData = JSON.parse(resp);
          
          // var catIds = Object.keys(respData);
          // let res = Object.keys(respData).map(key => respData[key]);
          //  res.forEach(element => {
          //    element.hide = 1
          //  });
          // this.values = res
          //  this.AllJobs = this.values
    
          if(respData != "No Data")
          {

          for(var x=0;x<respData.length;x++){
            if(respData[x].SalaryType == "3" || respData[x].SalaryType == "2"){
              respData[x].PricePerHour = respData[x].MonthlySalary
            }
          }
    
    
          this.values = respData
           this.AllJobs = this.values
           this.hideallnodata = false
          console.log("values",this.values);
        }else{
          this.values = []
           this.AllJobs = []
           this.hideallnodata = true
        }
          if(this.refresher){
            this.refresher.complete();
          }
        },err=>{
          console.log("err from list all jobs",err);
          if(this.refresher){
            this.refresher.complete();
          }
        });

        }
        else if(this.displayBlock == 3){
          console.log("search fro recommended")
          if(this.rvalues.length > 0){
          for(let i = 0; i < this.rvalues.length ; i++){
           
           
              this.rvalues[i].hide = 1
           
          }
        }

        }
       

        
      }
  }
  refresher;

  doRefresh(ev){
    this.refresher = ev;
    if(this.displayBlock == 1){
    this.service.jsListAllJobs(resp=>{
      console.log("resp from list all jobs",resp);
      var respData = JSON.parse(resp);
      
      // var catIds = Object.keys(respData);
      // let res = Object.keys(respData).map(key => respData[key]);
      //  res.forEach(element => {
      //    element.hide = 1
      //  });
      // this.values = res
      //  this.AllJobs = this.values

      if(respData != "No Data")
      {

      for(var x=0;x<respData.length;x++){
        if(respData[x].SalaryType == "3" || respData[x].SalaryType == "2"){
          respData[x].PricePerHour = respData[x].MonthlySalary
        }
      }


      this.values = respData
       this.AllJobs = this.values
       this.hideallnodata = false
    }else{
      this.values = []
      this.AllJobs = []
      this.hideallnodata = true

    }
      console.log("values",this.values);
      
      if(this.refresher){
        this.refresher.complete();
      }
    },err=>{
      console.log("err from list all jobs",err);
      if(this.refresher){
        this.refresher.complete();
      }
    });

  }else if(this.displayBlock == 3){
    this.getrecommended();
  }else if(this.displayBlock == 2){
    // this.getnearestjob();
    if(this.refresher){
      this.refresher.complete();
    }
  }


  }

  shareThisJob(){
    this.socialSharing.share("Per Hour Jobs", "22", null, null).then(() => {
      

    }).catch(() => {
      console.log("not available");

    });

  }
 
  filterLoadedData(txtToFilter){
    var searchString = "";
    this.values = this.AllJobs;
    if (this.helper.lang_direction == "rtl") {
      searchString = this.textArabicNumbersReplacment(txtToFilter);
      // if (searchString != undefined) {
        if (searchString && searchString.trim() != '') {
  
          console.log("from search values :",this.values)
        //   for(let i = 0; i < this.AllJobs.length ; i++){
        //     for(let x = 0;x<this.AllJobs[i].length;x++){
        //       if(this.AllJobs[i][x].JobTitleAr.toLowerCase().indexOf(searchString.toLowerCase()) > -1){
        //         this.AllJobs[i][x].hide = 1
        //       }
        //       else{
        //         this.AllJobs[i][x].hide = 0
        //       }
        //     }
                 
        // }
        this.service.searchApi(searchString,resp=>{
          console.log("resp from searchApi : ",resp)
          var respData = JSON.parse(resp);
      
            
      // console.log("search respData.length : ",respData.length)
      if(respData != "No Data")
      {
            for(var x=0;x<respData.length;x++){
              if(respData[x].SalaryType == "3" || respData[x].SalaryType == "2"){
                respData[x].PricePerHour = respData[x].MonthlySalary
              }
            }
      
      
            this.values = respData
             this.AllJobs = this.values
             this.hideallnodata = false
            console.log("values",this.values);
          }else{
            this.values = []
            this.AllJobs = []
            this.hideallnodata = true
          }

        },err=>{
          console.log("err from searchApi : ",err)
          this.hideallnodata = false
        })
        // for(let i = 0; i < this.values.length ; i++){
        //     if(this.values[i].JobTitleAr.toLowerCase().indexOf(searchString.toLowerCase()) > -1){
        //       this.values[i].hide = 1
        //     }
        //     else{
        //       this.values[i].hide = 0
        //     } 
            
            

      // }
      console.log("after search values  :",this.values)


      }
      // }
    } else {
      searchString = this.textArabicNumbersReplacment(txtToFilter);
      if (searchString && searchString.trim() != '') {
      //   for(let i = 0; i < this.AllJobs.length ; i++){
      //     for(let x = 0;x<this.AllJobs[i].length;x++){
      //       if(this.AllJobs[i][x].JobTitleEn.toLowerCase().indexOf(searchString.toLowerCase()) > -1){
      //         this.AllJobs[i][x].hide = 1
      //       }
      //       else{
      //         this.AllJobs[i][x].hide = 0
      //       }
      //     }
               
      // }

      this.service.searchApi(searchString,resp=>{
        console.log("resp from searchApi : ",resp)
        var respData = JSON.parse(resp);
    
          
    // console.log("search respData.length : ",respData.length)
    if(respData != "No Data")
    {
          for(var x=0;x<respData.length;x++){
            if(respData[x].SalaryType == "3" || respData[x].SalaryType == "2"){
              respData[x].PricePerHour = respData[x].MonthlySalary
            }
          }
    
    
          this.values = respData
           this.AllJobs = this.values
           this.hideallnodata = false
          console.log("values",this.values);
        }else{
          this.values = []
          this.AllJobs = []
          this.hideallnodata = true
        }

      },err=>{
        console.log("err from searchApi : ",err)
        this.hideallnodata = false
      })



  //     for(let i = 0; i < this.values.length ; i++){
  //       if(this.values[i].JobTitleEn.toLowerCase().indexOf(searchString.toLowerCase()) > -1){
  //         this.values[i].hide = 1
  //         console.log("this.values[i].JobTitleEn: ",this.values[i].JobTitleEn,"hide : ",this.values[i].hide," i: ",i)
  //       }
  //       else{
  //         this.values[i].hide = 0
  //         console.log("this.values[i].JobTitleEn: ",this.values[i].JobTitleEn,"hide : ",this.values[i].hide," i: ",i)
  //       }    
  // }

    }
    }

    // for(let v=0;v<this.AllJobs.length;v++){
    //   let arr = []
    //   for(let x=0;x<this.AllJobs[v].length;x++){
    //     if(this.AllJobs[v][x].hide == 1){
    //       arr.push(this.AllJobs[v][x].SID)
    //     }
    //   }
    //   if(arr.length > 0){
    //     this.AllJobs[v].hide = 1
    //   }
    //   else{
    //     this.AllJobs[v].hide = 0
    //   }
    // }
    
    console.log(JSON.stringify(this.AllJobs))
  }


  filterRecommendedLoadedData(txtToFilter){
    var searchString = "";
    // this.rvalues = this.AllJobs;
    if (this.helper.lang_direction == "rtl") {
      searchString = this.textArabicNumbersReplacment(txtToFilter);
      if (searchString != undefined) {
        if (searchString && searchString.trim() != '') {
  
          console.log("from search rvalues :",this.rvalues)
  
        for(let i = 0; i < this.rvalues.length ; i++){
            if(this.rvalues[i].JobTitleAr.toLowerCase().indexOf(searchString.toLowerCase()) > -1){
              this.rvalues[i].hide = 1
            }
            else{
              this.rvalues[i].hide = 0
            }    
      }
      console.log("after search rvalues  :",this.rvalues)


      }
      }
    } else {
      searchString = this.textArabicNumbersReplacment(txtToFilter);
      if (searchString && searchString.trim() != '') {
  
      for(let i = 0; i < this.rvalues.length ; i++){
        if(this.rvalues[i].JobTitleEn.toLowerCase().indexOf(searchString.toLowerCase()) > -1){
          this.rvalues[i].hide = 1
          console.log("this.values[i].JobTitleEn: ",this.values[i].JobTitleEn,"hide : ",this.rvalues[i].hide," i: ",i)
        }
        else{
          this.rvalues[i].hide = 0
          console.log("this.rvalues[i].JobTitleEn: ",this.rvalues[i].JobTitleEn,"hide : ",this.rvalues[i].hide," i: ",i)
        }    
  }

    }
    }

    
    console.log(JSON.stringify(this.AllJobs))
  }



  textArabicNumbersReplacment(strText) {
    // var strTextFiltered = strText.Trim().replace(" ", "");
   
    var strTextFiltered = strText.trim();
    var strTextFiltered = strText;
    //
    // strTextFiltered = strTextFiltered.replace('ي', 'ى');
    strTextFiltered = strTextFiltered.replace(/[\ي]/g, 'ى');
    // strTextFiltered = strTextFiltered.replace('ئ', 'ى');
    strTextFiltered = strTextFiltered.replace(/[\ئ]/g, 'ى');
    //
    // strTextFiltered = strTextFiltered.replace('أ', 'ا');
    strTextFiltered = strTextFiltered.replace(/[\أ]/g, 'ا');
    // strTextFiltered = strTextFiltered.replace('إ', 'ا');
    strTextFiltered = strTextFiltered.replace(/[\إ]/g, 'ا');
    // strTextFiltered = strTextFiltered.replace('آ', 'ا');
    strTextFiltered = strTextFiltered.replace(/[\آ]/g, 'ا');
    // strTextFiltered = strTextFiltered.replace('ء', 'ا');
    strTextFiltered = strTextFiltered.replace(/[\ء]/g, 'ا');
    //كاشيده
    strTextFiltered = strTextFiltered.replace(/[\u0640]/g, '');
    // التنوين  Unicode Position              
    strTextFiltered = strTextFiltered.replace(/[\u064B\u064C\u064D\u064E\u064F\u0650\u0651\u0652]/g, '');
    // چ
    strTextFiltered = strTextFiltered.replace(/[\u0686]/g, 'ج');
    // ڤ
    strTextFiltered = strTextFiltered.replace(/[\u06A4]/g, 'ف');
    //                
    // strTextFiltered = strTextFiltered.replace('ة', 'ه');
    strTextFiltered = strTextFiltered.replace(/[\ة]/g, 'ه');
    // strTextFiltered = strTextFiltered.replace('ؤ', 'و');
    strTextFiltered = strTextFiltered.replace(/[\ؤ]/g, 'و');
    //
    strTextFiltered = strTextFiltered.replace(/[\٩]/g, '9');
    strTextFiltered = strTextFiltered.replace(/[\٨]/g, '8');
    strTextFiltered = strTextFiltered.replace(/[\٧]/g, '7');
    strTextFiltered = strTextFiltered.replace(/[\٦]/g, '6');
    strTextFiltered = strTextFiltered.replace(/[\٥]/g, '5');
    strTextFiltered = strTextFiltered.replace(/[\٤]/g, '4');
    strTextFiltered = strTextFiltered.replace(/[\٣]/g, '3');
    strTextFiltered = strTextFiltered.replace(/[\٢]/g, '2');
    strTextFiltered = strTextFiltered.replace(/[\١]/g, '1');
    strTextFiltered = strTextFiltered.replace(/[\٠]/g, '0');
    //
    return strTextFiltered;
    //
  }

  jsid
  getrecommended(){

    this.storage.get('js_info').then((val) => {
      console.log("val from get js info",val);
      if(val)
        this.jsid = val.SID; 
      

  //   this.service.jsgetRecomendedJobs(this.jsid,resp=>{
  //     console.log("resp from jsgetRecomendedJobs",resp);
  //     if(JSON.parse(resp).result == "1"){
  //       this.hiderecommendednodata= false
  //       // this.rvalues = JSON.parse(resp).jobsList.slice(0,3);
  //       var respData = JSON.parse(resp).jobsList;
      
  //       var catIds = Object.keys(respData);
  //       let res = Object.keys(respData).map(key => respData[key]);
  //        res.forEach(element => {
  //          element.hide = 1
  //        });
  //       this.rvalues = res
  //       //  this.AllJobs = this.values
  //       console.log("values",this.values);

  //     }else{
  //         this.hiderecommendednodata= true  
             
  //     }
        

  // if(this.refresher){
  //   this.refresher.complete();
  // }

  //   },err=>{
  //     console.log("err from jsgetLatestJobs",err);

  // if(this.refresher){
  //   this.refresher.complete();
  // }
  //   })


  this.service.jsgetRecomendedJobs(this.jsid,resp=>{
    console.log("resp from jsgetRecomendedJobs",resp);
    if(JSON.parse(resp).result == "1"){
      console.log("JSON.parse(resp).jobsList.length : ",JSON.parse(resp).jobsList.length);
            this.hiderecommendednodata= false

var  respData = JSON.parse(resp).jobsList
            for(var x=0;x<respData.length;x++){
              if(respData[x].SalaryType == "3" || respData[x].SalaryType == "2"){
                respData[x].PricePerHour = respData[x].MonthlySalary
              }
            }

        this.rvalues = respData;

        console.log("this.recommendedJobs : ",this.rvalues);
        // for(var j=0;j<this.rvalues.length;j++)
        // {
        //   var dt1 = new Date();
        //   var dt2 = new Date(this.rvalues[j].CreationDate);//"2018-11-16T12:50:53.466Z"
        //   // console.log("dateDiff: ",this.DateDiff(dt1, dt2));
        //   console.log("this.newJobsList[j].SID",this.rvalues[j].SID);
        //   this.rvalues[j].dateConversion=this.DateDiff(dt1, dt2);
          
        // }

        
        
    }
    else  if(JSON.parse(resp).result == "-1"){
      console.log("from else recommendedJobs: ");
      this.hiderecommendednodata= true
      this.rvalues = [];
    }

    console.log("recommendedJobs.length  before  <= 0 condition: ",this.rvalues.length )
      // if(this.rvalues.length <= 0)
      //     this.hideRecommendedTitle = true;
      //   else
      //     this.hideRecommendedTitle = false;

if(this.refresher){
  this.refresher.complete();
}

  },err=>{
    console.log("err from jsgetLatestJobs",err);

if(this.refresher){
  this.refresher.complete();
}
  })


  });

  }

  getnearestjob(){
    this.service.jsnearestJobs(this.helper.lat,this.helper.lng,resp=>{
      console.log("resp from jsnearestJobs",resp);
      if(JSON.parse(resp).result == "1")
        this.nearestJobs = JSON.parse(resp).jobsList;
      else
        this.nearestJobs = [];

        if(this.refresher){
          this.refresher.complete();
        }
        
      this.initMap();
    },err=>{
      console.log("err from jsnearestJobs",err);
      if(this.refresher){
        this.refresher.complete();
      }

    });

  }

  locateUser(){
    //this.helper.detectUserLoctaion(); 
    console.log("locateUser :")
    this.helper.geoLoc(()=>{})
  }


  openXdetails(){
    this.navCtrl.push("JobDetailsPage",{'jobDetails':"","jobId": 268,'from':"notification"})
  }

  searchForJob(){
    console.log("searchForJob ")
    if (this.searchWord  == undefined) {
      this.searchWord = "";
    }
    
    if(this.searchWord){
      console.log("target val: "+this.searchWord)
      if(this.displayBlock == 1){
        console.log("search fro all target:",this.searchWord)
    this.filterLoadedData(this.searchWord)
      }else if(this.displayBlock == 3){
        console.log("search fro recommended target:",this.searchWord)
        this.filterRecommendedLoadedData(this.searchWord)
      }
    }else{
      if(this.displayBlock == 1){
        console.log("search fro all")
     

      this.service.jsListAllJobs(resp=>{
        console.log("resp from list all jobs",resp);
        var respData = JSON.parse(resp);
        
        
        if(respData != "No Data")
        {
        for(var x=0;x<respData.length;x++){
          if(respData[x].SalaryType == "3" || respData[x].SalaryType == "2"){
            respData[x].PricePerHour = respData[x].MonthlySalary
          }
        }
  
  
        this.values = respData
         this.AllJobs = this.values
         this.hideallnodata = false
        console.log("values",this.values);
      }else{
        this.values = []
        this.AllJobs = []
        this.hideallnodata = true
      }
        
        if(this.refresher){
          this.refresher.complete();
        }
      },err=>{
        console.log("err from list all jobs",err);
        if(this.refresher){
          this.refresher.complete();
        }
      });

      }
      else if(this.displayBlock == 3){
        console.log("search fro recommended")
        if(this.rvalues.length > 0){
        for(let i = 0; i < this.rvalues.length ; i++){
         
         
            this.rvalues[i].hide = 1
         
        }
      }

      }
     

      
    }

  }

  checkEmptyToLoadAll(){
    console.log("checkEmptyToLoadAll")
    if(this.searchWord == ""){
      this.service.jsListAllJobs(resp=>{
        console.log("resp from list all jobs",resp);
        var respData = JSON.parse(resp);
        if(respData != "No Data")
      {
      
        
        for(var x=0;x<respData.length;x++){
          if(respData[x].SalaryType == "3" || respData[x].SalaryType == "2"){
            respData[x].PricePerHour = respData[x].MonthlySalary
          }
        }
        this.values = respData
         this.AllJobs = this.values
         this.hideallnodata=false
        console.log("values",this.values);
        console.log("all jobs",this.AllJobs)
      }
      else{
        console.log("else")
        this.values = []
        this.AllJobs = []
        this.hideallnodata=true
      }
        if(this.refresher){
          this.refresher.complete();
        }
  
      },err=>{
        console.log("err from list all jobs",err);
        if(this.refresher){
          this.refresher.complete();
        }
      });
    }
    
  }
}

