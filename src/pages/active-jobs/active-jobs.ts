import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,AlertController,Platform} from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { HelperProvider } from '../../providers/helper/helper';
import { Storage } from '@ionic/storage';
import { ServicesProvider } from '../../providers/services/services';
import { TranslateService } from '@ngx-translate/core';

// @IonicPage()
@Component({
  selector: 'page-active-jobs',
  templateUrl: 'active-jobs.html',
})
export class ActiveJobsPage {

  scaleClass="";
  langDirection;
  jsSID;
  sprint1Hidden = true;

  myActiveJobs;
  refresher;
  hideNoDataTxt = true;

  constructor(public Platform: Platform,public alertCtrl:AlertController, public translate: TranslateService,public service:ServicesProvider,
    public storage:Storage,public helper:HelperProvider,
    public navCtrl: NavController, public navParams: NavParams) {
    this.langDirection = this.helper.lang_direction;
    if(this.langDirection == "rtl")
      this.scaleClass="scaleClass";
    this.sprint1Hidden = true;

    this.hideNoDataTxt = true;
  }
  openDetails(item){
    this.navCtrl.push("JobDetailsPage",{'jobDetails':item,'applied':1})
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ActiveJobsPage');

    this.storage.get('js_info').then((val) => {
      console.log("val from get js info",val);
      if(val)
        this.jsSID = val.SID;  
      this.getAllActiveJob();
    }).catch(err=>{
      console.log("catch from get js info",err);
      
    });

  }
  dismiss(){
    this.navCtrl.pop();
    // this.navCtrl.setRoot(TabsPage)
  }


  getAllActiveJob(){

    this.service.jsGetActiveJobs(this.jsSID,resp=>{
      console.log("resp from jsGetActiveJobs",resp);
      var respData = JSON.parse(resp);
      var CurrentJobId ;
      if(respData.WorkStatus == "1")
      {
        CurrentJobId= respData.CurrentJobID;
        if(respData.activeJobs.length !=0){
          this.hideNoDataTxt = true;
        for(var j=0;j<respData.activeJobs.length;j++){
          if(respData.activeJobs[j].SalaryType == "1")
          respData.activeJobs[j].jobtypetxt = this.translate.instant("perhour")
          else if(respData.activeJobs[j].SalaryType == "2")
          respData.activeJobs[j].jobtypetxt = this.translate.instant("permonth")
          else if(respData.activeJobs[j].SalaryType == "3")
          respData.activeJobs[j].jobtypetxt = this.translate.instant("peryear")

          if(CurrentJobId == respData.activeJobs[j].SID)
          {
            respData.activeJobs[j].checkInOutBtnTxt = this.translate.instant("Checkout");
            respData.activeJobs[j].hideCancelBtn = true;
            respData.activeJobs[j].hideLateOrOntime = true;
          }
            
          else
          {
            respData.activeJobs[j].checkInOutBtnTxt = this.translate.instant("checkIN");
            respData.activeJobs[j].hideCancelBtn = false;
            respData.activeJobs[j].hideLateOrOntime = false;
          }
            
        }
      }else{
        console.log("respData.activeJobs.length != 0 else" )
        this.hideNoDataTxt = false;
      }

      }else{
        if(respData.activeJobs.length !=0){
          this.hideNoDataTxt = true;
        for(var j=0;j<respData.activeJobs.length;j++){

          if(respData.activeJobs[j].SalaryType == "1")
          respData.activeJobs[j].jobtypetxt = this.translate.instant("perhour")
          else if(respData.activeJobs[j].SalaryType == "2")
          respData.activeJobs[j].jobtypetxt = this.translate.instant("permonth")
          else if(respData.activeJobs[j].SalaryType == "3")
          respData.activeJobs[j].jobtypetxt = this.translate.instant("peryear")

          respData.activeJobs[j].checkInOutBtnTxt = this.translate.instant("checkIN");
          respData.activeJobs[j].hideCancelBtn = false;
          respData.activeJobs[j].hideLateOrOntime = false;
        }}else{
          console.log("respData.activeJobs.length != 0 else2" )
          this.hideNoDataTxt = false;          
        }
      }
      this.myActiveJobs = respData.activeJobs;

for(var j=0;j<this.myActiveJobs.length;j++){
  if(!(respData.activeJobs[j].SalaryType == "3")){
    var dt1 = new Date();
    // var dt2 = new Date(this.myActiveJobs[j].StartDate);//"2018-11-16T12:50:53.466Z"
    var a = []
    a = this.myActiveJobs[j].StartDate.split(/[^0-9]/);
    console.log("a : ",a)
    var d=new Date (a[0],a[1]-1,a[2],a[3],a[4],a[5] );
    console.log("d : ",new Date(d))
    // this.jobDetails.Starttime=  this.datepipe.transform(new Date(d), 'h:m a')
    var dt2 = new Date(d)
    this.myActiveJobs[j].hideLateOrOntime=this.DateDiff2(dt1, dt2);
  
  }else{
    this.myActiveJobs[j].hideLateOrOntime = true
    this.myActiveJobs[j].hidecheckin = true
    this.myActiveJobs[j].hideCancelBtn = this.DateDiff2(dt1, dt2);
  }
 
}


      if(this.refresher){
        this.refresher.complete();
      }

      
    },err=>{
      console.log("err from jsGetActiveJobs",err);

      if(this.refresher){
        this.refresher.complete();
      }
      
    });


  }
  checkINOut(item){
    
    console.log("item from checkINOut",item);
    var type;

    if(item.checkInOutBtnTxt == this.translate.instant("checkIN"))

    {
      
      type=1;
      this.service.jsCheckInOut(this.jsSID,item.SID,type,resp=>{
        console.log("resp from jsCheckInOut",resp);
        
        var respData = JSON.parse(resp);

        if(respData.result == "1"){
          item.checkInOutBtnTxt = this.translate.instant("Checkout");
          item.hideCancelBtn = true;
          item.hideLateOrOntime = true;
          this.helper.presentToast(this.translate.instant("Sucheckin"));
        }
        else if(respData.result == "-2")
          this.helper.presentToast(this.translate.instant("checkINBefore"));
        else if (respData.result == "-4")
          this.helper.presentToast(this.translate.instant("timeOut"));
        else if (respData.result == "3")
          this.helper.presentToast(this.translate.instant("checkinbeforestarttime"));
        else if (respData.result == "-3")
        this.helper.presentToast(this.translate.instant("jobnotstarted"));
        else
          this.helper.presentToast(this.translate.instant("ServerError"))

      },err=>{
        console.log("err from jsCheckInOut",err);
        this.helper.presentToast(this.translate.instant("ServerError"))
      });
    }else{
      
      type=2;
      this.service.jsCheckInOut(this.jsSID,item.SID,type,resp=>{
        console.log("resp from jsCheckInOut",resp);
        var respData = JSON.parse(resp);
        if(respData.result == "1"){
          item.hideCancelBtn = true;
          item.hideLateOrOntime = false;
          this.helper.presentToast(this.translate.instant("Sucheckout"));
          this.getAllActiveJob();
        }
        else if(respData.result == "-2")
          this.helper.presentToast(this.translate.instant("checkOutBefore"));
        else
          this.helper.presentToast(this.translate.instant("ServerError"))
      },err=>{
        console.log("err from jsCheckInOut",err);
        this.helper.presentToast(this.translate.instant("ServerError"))
      });
    }
    

    
  }

  canceljob(item){
    this.service.cancelActiveJob(this.jsSID,item.SID,resp=>{
      console.log("resp from canceljob ",resp);
      var respData =  JSON.parse(resp);
      if(respData.Result == "1")
      {
        this.helper.presentToast(this.translate.instant("cancelledsu"))
        this.getAllActiveJob();
      }
        
    },err=>{
      console.log("err from canceljob ",err);
    });
  }

  cancelconfirm(item)
  {

  let alert = this.alertCtrl.create({
    title: this.translate.instant('cancelActiveJobTitle'),
    buttons: [
      {
        text: this.translate.instant('no2'),
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: this.translate.instant('yes2'),
        handler: () => {
       this.canceljob(item);

        }
      }
    
  ]
})
alert.present()
  }

  doRefresh(ev){
    this.refresher = ev;
    this.getAllActiveJob();
  }

  DateDiff(dt2, dt1) 
{
  var hideLateOrOntime ;
  // dt2 today
  // var ss =Math.abs(Math.round((dt2.getTime() - dt1.getTime()) / 1000));
  console.log("current time : ",dt2)
  console.log("job time : ",dt1)
 

  // if(dt1 <= dt2){
  //   console.log("if dt2 < dt1 :")
    
  //   console.log("dt2.getTime() : ",dt2.getTime())
  //   console.log("dt1.getTime() : ",dt1.getTime())
  //   var ss =Math.abs(Math.round((dt2.getTime() - dt1.getTime()) / 1000));
  //       var newss = ss-(2*60*60);
  //       console.log("ss: ",ss," newss: ",newss);
  //       // var h = Math.floor(ss/3600);
  //       var m = Math.floor(ss % 3600 /60);
  // console.log("m : ",m)
  
  // if(m <= 60 &&  m >=0)
  //   hideLateOrOntime = false;
  // else
  //   hideLateOrOntime = true;
  
  // return hideLateOrOntime;
  // }else{
  //   console.log("else dt2 < dt1 :")
    
  //   return true; 
  // }


//  var jobtime =   dt1.getTime() + (60*60*1000)
//  var currentTime =  dt2.getTime()
//   if(jobtime <= currentTime){
//     var ss =Math.abs(Math.round((dt2.getTime() - dt1.getTime()) / 1000));
//     var newss = ss-(2*60*60);
//     console.log("ss: ",ss," newss: ",newss);
//     // var h = Math.floor(ss/3600);
//     var m = Math.floor(ss % 3600 /60);
// console.log("m : ",m)

// if(m <= 60 &&  m >=0)
// hideLateOrOntime = false;
// else
// hideLateOrOntime = true;

// return hideLateOrOntime;
//   }else{
//     console.log("else dt2 < dt1 :")
    
//     return true; 
//   }

var jobtime =   dt1.getTime()
var currentTime =  dt2.getTime() + (60*60*1000)
 console.log("job.getTime() : ",dt1.getTime())
 console.log("current.getTime() : ",dt2.getTime())
var ssafteraddhour =Math.abs(Math.round((currentTime - jobtime) / 1000));

var mm = Math.floor(ssafteraddhour % 3600 /60);
console.log("mm after add hour : ",mm)


   var ss =Math.abs(Math.round((dt2.getTime() - dt1.getTime()) / 1000));

  //  var newss = ss-(2*60*60);
  //  console.log("ss: ",ss," newss: ",newss);
   // var h = Math.floor(ss/3600);
   
   
   var m = Math.floor(ss % 3600 /60);
console.log("m : ",m)
if(dt1 > dt2){
  console.log("if job > cuurent , m : ",m)
  if( m>=0 && m<=60 )
    hideLateOrOntime = false;
  else
    hideLateOrOntime = true;
}else{
  hideLateOrOntime = true;
}
// if(m <= 60 &&  m >=0)
// hideLateOrOntime = false;
// else
// hideLateOrOntime = true;

return hideLateOrOntime;



//  var currentDate = dt2.toLocaleString()
//  var startDate = dt1.toLocaleString()
 
//  var hours = Math.abs(currentDate - startDate) / 36e5;
//  var mins = Math.abs(dt2 - dt1) / (60*1000);
 
//  console.log("mins : ",mins)
// if(Date.parse(x3) > Date.parse('12/30/2018, 1:26:24 PM'))

//   console.log("dt2.getTime() : ",dt2.getTime());
//   console.log("dt1.getTime() : ",dt1.getTime());

//   //edit dt1
//   var returnTime;
//   var timeDiff = Math.abs(dt2.getTime() - dt1.getTime());
//   var diffDays = Math.floor(timeDiff / (1000 * 3600 * 24)); 
//   console.log("diff day",diffDays);
// console.log("dt1.getTime(): ",dt1.getTime());

//   if(diffDays <= 0)
//   {
//     var ss =Math.abs(Math.round((dt2.getTime() - dt1.getTime()) / 1000));
//     var newss = ss-(2*60*60);
//     console.log("ss: ",ss," newss: ",newss);
//     // var h = Math.floor(ss/3600);
//     // var m = Math.floor(ss % 3600 /60);
//     // var s = Math.floor(ss % 3600 % 60);

//     // console.log("h ", h,"m: ",m,"s: ",s);
//     var h = Math.floor(newss/3600);
//     var m = Math.floor(newss % 3600 /60);
//     var s = Math.floor(newss % 3600 % 60);

//     console.log("h ", h,"m: ",m,"s: ",s);
    
//     // var newh = Math.floor(newss/3600);
//     // var newm = Math.floor(newss % 3600 /60);
//     // var news = Math.floor(newss % 3600 % 60);
//     // console.log("newh ", newh,"newm: ",newm,"news: ",news); 

//     var hdisplay = h > 0 ? h + (h == 1 ? this.translate.instant("hour"):this.translate.instant("hours")):"";
//     var mdisplay = m > 0 ? m + (m == 1 ? this.translate.instant("min"):this.translate.instant("mins")):"";
//     // var sdisplay = s > 0 ? s + (s == 1 ? this.translate.instant("sec"):this.translate.instant("secs")):"";
//     // returnTime = s+this.translate.instant("sec")+":" + min++":"+hour+;
  
//     if(h>0 && m >0)
//     {
//       console.log("1");
//       returnTime =hdisplay+this.translate.instant(",")+mdisplay;
//     }
//     else if(h<=0)
//     {
//       returnTime =mdisplay;
//       console.log("2");
//     }
//     else if (m<=0)
//     {
//       returnTime = hdisplay;
//       console.log("3")
//     }
    
//    if (h<=0 && m<=0){
//       returnTime = this.translate.instant("fewSeconds");
//       console.log("4")
//     }
    
//     return returnTime;
      
//   }else{
//     returnTime = diffDays+this.translate.instant("day");
//     return returnTime;
//   }

 }

late(item){
  console.log("late ",item)
  // 1: Ontime           2: Late
this.service.jsLateOrOntime(this.jsSID,item.SID,2,resp=>{
  console.log("resp from jsLateOrOntime : ",resp)
  if(JSON.parse(resp).result)
  this.helper.presentToast(this.translate.instant("donesuccessfully"))
else
  this.helper.presentToast(this.translate.instant("ServerError"))
},err=>{
  console.log("err from jsLateOrOntime : ",err)
  this.helper.presentToast(this.translate.instant("ServerError"))
});
}

ontime(item){
  console.log("ontime ",item)
this.service.jsLateOrOntime(this.jsSID,item.SID,1,resp=>{
  console.log("resp from jsLateOrOntime : ",resp)
  if(JSON.parse(resp).result)
    this.helper.presentToast(this.translate.instant("donesuccessfully"))
  else
    this.helper.presentToast(this.translate.instant("ServerError"))
},err=>{
  console.log("err from jsLateOrOntime : ",err)
  this.helper.presentToast(this.translate.instant("ServerError"))
});
}


lateorontimeconfirm(item,lateorontime)
{
  var msg
if(lateorontime == "1")
  msg = this.translate.instant("ontimetitle") //ontime
else if (lateorontime == "2")
  msg = this.translate.instant("latetitle") //late

let alert = this.alertCtrl.create({
  title: msg,
  buttons: [
    {
      text: this.translate.instant('no2'),
      role: 'cancel',
      handler: () => {
        console.log('Cancel clicked');
      }
    },
    {
      text: this.translate.instant('yes2'),
      handler: () => {
     
        if(lateorontime == "1")
        this.ontime(item)
      else if (lateorontime == "2")
        this.late(item)
      }
    }
  
]
})
alert.present()
}


DateDiff2(dt2, dt1) 
{
  var returnTime;

  var hideLateOrOntime ;
  
  console.log("current time : ",dt2)
  console.log("job time : ",dt1)
  
  var timeDiff = Math.abs(dt2.getTime() - dt1.getTime());
  var diffDays = Math.floor(timeDiff / (1000 * 3600 * 24)); 
  console.log("diff day",diffDays);

  if(diffDays <= 0)
  {
    var ss =Math.abs(Math.round((dt2.getTime() - dt1.getTime()) / 1000));
    var newss = ss
    // if (this.Platform.is('android')) {
    //   newss = ss-(2*60*60);
    // }else  if (this.Platform.is('ios')) {
    //   newss = ss
    // }
    
    console.log("ss: ",ss," newss: ",newss);

   

    var h = Math.floor(newss/3600);
    var m = Math.floor(newss % 3600 /60);
    var s = Math.floor(newss % 3600 % 60);

    console.log("newss h ", h,"m: ",m,"s: ",s);  
    
    // var hdisplay = h > 0 ? h + (h == 1 ? this.translate.instant("hour"):this.translate.instant("hours")):"";
    // var mdisplay = m > 0 ? m + (m == 1 ? this.translate.instant("min"):this.translate.instant("mins")):"";
    // if(h>0 && m >0)
    // returnTime =hdisplay+this.translate.instant(",")+mdisplay;
    // else if(h<=0)
    // returnTime =mdisplay;
    // else if (m<=0)
    // returnTime = hdisplay;
    
    // if (h<=0 && m<=0){
    //   returnTime = this.translate.instant("fewSeconds");
    //   console.log("else s time")
    // }
    // return returnTime;
    if(dt2>dt1)
    return true
    else{
      if(h == 1 && m == 0)
      return false
      else if (h==0 && (m>=0 && m<=60))
      return false
      else 
      return true
    }
  
      
  }else{
    // if(diffDays == 1)
    // returnTime = diffDays+this.translate.instant("day");
    // else 
    // returnTime = diffDays+this.translate.instant("days");


    
    // return returnTime;
    return true
  }
}

}
