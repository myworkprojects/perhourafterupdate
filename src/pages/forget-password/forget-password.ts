import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ServicesProvider } from '../../providers/services/services';

@IonicPage()
@Component({
  selector: 'page-forget-password',
  templateUrl: 'forget-password.html',
})
export class ForgetPasswordPage {

  scaleClass="";
  langDirection;
  mobile;
  private registerForm : FormGroup;
  errors = {
       mobileErr:""
  };

  placeholder = {mobileNumber:""};
  submitAttempt = false;
  type = "";
  constructor(public service:ServicesProvider, public translate: TranslateService,private formBuilder: FormBuilder,public helper:HelperProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.langDirection = this.helper.lang_direction;
    if(this.langDirection == "rtl")
    this.scaleClass="scaleClass";

    this.registerForm = this.formBuilder.group({
           mobile: ['', Validators.compose([Validators.required,Validators.pattern("[0-9]{9,10}")])],
     

    });

    this.errors.mobileErr = this.translate.instant("mobileErr");
    this.placeholder.mobileNumber = this.translate.instant("enterMobileNumber");
  console.log("this.navParams.get('jobSeeker'): ",this.navParams.get('jobSeeker'));
    if(this.navParams.get('jobSeeker') == true)
      this.type = "js";
    else
      this.type = "comp";

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgetPasswordPage');
  }
  dismiss(){
    this.navCtrl.pop();
  }
send(){
  if(! this.registerForm.valid ){
    this.submitAttempt=true;
    if(this.registerForm.controls["mobile"].errors){
      if(this.registerForm.controls["mobile"].errors['required'])  
        this.errors.mobileErr = this.translate.instant("mobileErr");
      else if(this.registerForm.controls["mobile"].errors['pattern']) 
        this.errors.mobileErr = this.translate.instant("phoneErr");
      // else if (this.mobile.includes('-') || this.mobile.includes('.'))
      //   this.errors.mobileErr = this.translate.instant("invalidPhone");
      else
        console.log("mobile errors:",this.registerForm.controls["mobile"].errors);
    }
  }else{
    console.log("forget pass this.type ",this.type);

    console.log("x.charAt(0)", this.mobile.charAt(0))
    console.log("substr: ",this.mobile.substr(1))
  console.log("966+mobile","966"+this.mobile)
  if(this.mobile.charAt(0) == "0" || this.mobile.charAt(0) == "٠")
  {
    this.mobile =  this.mobile.substr(1);
    // this.mobile = "966"+this.mobile;
    console.log("from if =0 this.mobile: ",this.mobile);
  }else{
    // this.mobile = "966"+this.mobile;
    console.log("from else =0 this.mobile: ",this.mobile);
  }


    if(this.type == "js")
    {
      this.service.jsForgetPassMobile(this.mobile,resp=>{
        console.log("resp from jsForgetPassMobile",resp);
        var respData = JSON.parse(resp);
        if(respData["Result"] == "-1")
          this.helper.presentToast(this.translate.instant("mobileNotRegistered"));
        else 
        {
        this.navCtrl.setRoot('ActivationCodePage',{JsSID:respData["Result"],userdata:this.mobile,type:"js-forget"}); 
        }

       
      },err=>{
        console.log("err from jsForgetPassMobile",err);
      })
     
    }
    else if (this.type == "comp")
    {
      this.service.compForgetPassMobile(this.mobile,resp=>{
        console.log("resp from compForgetPassMobile",resp);
       
        var respData = JSON.parse(resp);
        if(respData["Result"] == "-1")
          this.helper.presentToast(this.translate.instant("mobileNotRegistered"));
        else 
        {
          this.navCtrl.setRoot('ActivationCodePage',{JsSID:respData["Result"],userdata:this.mobile,type:"comp-forget"}); 
        }

        
      },err=>{
        console.log("err from compForgetPassMobile",err);
      })

      
    }
      
  }

}


changeTxt(){
  console.log("mobile before replacement  ...",this.mobile);
  this.mobile = this.textArabicNumbersReplacment(this.mobile);
  console.log("phone after replacement: ",this.mobile); 

}

textArabicNumbersReplacment(strText) {
  var strTextFiltered = strText.trim();
  strTextFiltered = strText;
  strTextFiltered = strTextFiltered.replace(/[\٩]/g, '9');
  strTextFiltered = strTextFiltered.replace(/[\٨]/g, '8');
  strTextFiltered = strTextFiltered.replace(/[\٧]/g, '7');
  strTextFiltered = strTextFiltered.replace(/[\٦]/g, '6');
  strTextFiltered = strTextFiltered.replace(/[\٥]/g, '5');
  strTextFiltered = strTextFiltered.replace(/[\٤]/g, '4');
  strTextFiltered = strTextFiltered.replace(/[\٣]/g, '3');
  strTextFiltered = strTextFiltered.replace(/[\٢]/g, '2');
  strTextFiltered = strTextFiltered.replace(/[\١]/g, '1');
  strTextFiltered = strTextFiltered.replace(/[\٠]/g, '0');
  return strTextFiltered;
}

}
