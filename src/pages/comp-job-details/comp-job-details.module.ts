import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CompJobDetailsPage } from './comp-job-details';

import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    CompJobDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(CompJobDetailsPage),
    TranslateModule.forChild(),
  ],
})
export class CompJobDetailsPageModule {}
