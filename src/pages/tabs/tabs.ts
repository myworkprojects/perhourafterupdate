import { Component } from '@angular/core';

// import { AboutPage } from '../about/about';
// import { ContactPage } from '../contact/contact';
import { ContactPage } from '../contact/contact';
import { AccountsPage } from '../accounts/accounts';
import { HomePage } from '../home/home';
import { JobListPage } from '../job-list/job-list';
import { MapPage } from '../map/map';
import { CompanyHomePage } from '../company-home/company-home';

import { Storage } from '@ionic/storage';
import { CompanycheckinviewPage } from '../companycheckinview/companycheckinview';
import { CompanyprofilePage } from '../companyprofile/companyprofile';
import { CompanyjophistoryPage } from '../companyjophistory/companyjophistory';
import { CompanypostajobPage } from '../companypostajob/companypostajob';
import { CompanypostedjobPage } from '../companypostedjob/companypostedjob';
import { AccountsForCompPage } from '../accounts-for-comp/accounts-for-comp';
import { JobHistoryPage } from '../job-history/job-history';
import { CompanyJobHistoryPage } from '../company-job-history/company-job-history';
import { JsProfilePage } from '../js-profile/js-profile';
import { HelperProvider } from '../../providers/helper/helper';
import { ActiveJobsPage } from '../active-jobs/active-jobs';


@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage ;
  tab2Root = JobListPage;
  // tab3Root = AccountsPage;
  tab3Root = JobHistoryPage;
  // tab4Root = MapPage;
  tab4Root = JsProfilePage;
  tab5Root = CompanyHomePage;
  // tab6Root = CompanycheckinviewPage;
  // tab6Root = CompanypostajobPage;
  tab6Root = CompanypostedjobPage;
  // tab7Root = AccountsForCompPage;
  tab7Root = CompanyJobHistoryPage;
  tab8Root = CompanyprofilePage ;
  tab9Root = ActiveJobsPage
  com:any;
  hometitle;
  postedjobstitle
  acctitle
  profiletitle
  jobstitle

  activeJobstitle

  constructor( public helper:HelperProvider,public storage:Storage) {

    console.log("this.helper.lang_directiont: ",this.helper.lang_direction);
  var lang =this.helper.lang_direction;
    if( lang == "rtl"){
      this.hometitle = "الرئيسية";
      // this.postedjobstitle = "الوظائف المعلن عنها"
      this.postedjobstitle = "الوظائف المعلنة"
      // this.acctitle = "الحسابات"
      this.acctitle = "سجل الوظائف"
      this.profiletitle = "الحساب الشخصى"
      this.jobstitle = "قائمة الوظائف"
      this.activeJobstitle = "الوظائف الحالية"

    }else if ( lang == "ltr"){
      this.hometitle = "Home";
      this.postedjobstitle = "Posted Jobs";
      // this.acctitle = "Accounts"
      this.acctitle = "Jobs History"
      this.profiletitle = "Profile"
      this.jobstitle = "Jobs List"
      this.activeJobstitle = "Active Jobs"
    }
    
    this.storage.get('js_info').then((val) => {
      console.log("val from get js info",val);
      if(val.type == "js")
        this.com = false;  
      else
        this.com = true;
       
    }).catch(err=>{
      console.log("catch from get js info",err);
      
    });

    
  }
  
    
    
  
}
