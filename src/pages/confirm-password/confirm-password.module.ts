import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConfirmPasswordPage } from './confirm-password';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';

@NgModule({
  declarations: [
    ConfirmPasswordPage,
  ],
  imports: [
    IonicPageModule.forChild(ConfirmPasswordPage),
    TranslateModule.forChild()
  ],
})
export class ConfirmPasswordPageModule {}
