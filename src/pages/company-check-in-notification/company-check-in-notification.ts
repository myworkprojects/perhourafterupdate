import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { HelperProvider } from '../../providers/helper/helper';
import { ServicesProvider } from '../../providers/services/services';
import { TranslateService } from '@ngx-translate/core';

@IonicPage()
@Component({
  selector: 'page-company-check-in-notification',
  templateUrl: 'company-check-in-notification.html',
})
export class CompanyCheckInNotificationPage {

  jsShortDescription = "";
  jsFirstName="";
  scaleClass="";
  langDirection;

  jobId;
  jsId;
  pageType;
  hide2Btns;
  jobName;
  jsName;
  txt;

  constructor(public translate: TranslateService,public service:ServicesProvider,public helper:HelperProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.langDirection = this.helper.lang_direction;
    if(this.langDirection == "rtl")
      this.scaleClass="scaleClass";

    this.jobId = this.navParams.get('jobId');
    this.jsId = this.navParams.get('jsid');
    this.pageType = this.navParams.get('notificationType');
    console.log("this.pageType: ",this.pageType);
    if(this.pageType == "cancel")
    {
      this.hide2Btns = true;
      this.txt = this.translate.instant("Cancelled");
    }
    else if (this.pageType == "checkout")
      {
        this.hide2Btns = true;
        this.txt = this.translate.instant("Checkout2");
      }
    else if (this.pageType == "checkIn")
      {
        this.hide2Btns = false;
        this.txt = this.translate.instant("Checkin2");
      }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CompanyCheckInNotificationPage');
    this.service.getUserDetails(this.jsId,resp=>{
      console.log("resp from getUserDetails",resp);
      var respData = JSON.parse(resp);
      if(respData.Result == "1")
      {
        
        this.jsName = respData.details.FirstName;
        
   var xdesc  = respData.details.ShortDescription
      if(xdesc == "1")
      this.jsShortDescription = this.translate.instant("student")
        else if(xdesc == "2")
        this.jsShortDescription = this.translate.instant("employee")
        else if(xdesc == "3")
        this.jsShortDescription = this.translate.instant("unemployed")



      }else{
        console.log("else respData.Result == 1");
        this.navCtrl.pop();
      }

    },err=>{
      console.log("err from getUserDetails",err);
    });

    this.service.getJobDetails(this.jobId,resp=>{
      console.log("resp from getJobDetails",resp);
      var respData = JSON.parse(resp);
      if(respData.Result == "1")
      {
        if(this.helper.lang_direction == "ltr")
        this.jobName = respData.details.JobTitleEn;
        else if(this.helper.lang_direction == "rtl")
        this.jobName = respData.details.JobTitleAr;
      


      }else{
        console.log("else respData.Result == 1");
        this.navCtrl.pop();
      }
    },err=>{
      console.log("err from getJobDetails",err);
    });

  }

  
   dismiss(){
    this.navCtrl.pop();
  }
  accept(){
    this.service.acceptorrejectAfterNotificationJSCheckin(this.jobId,this.jsId,1,resp=>{
     // if(JSON.parse(resp).Result ==  "-1")
      //  this.helper.presentToast(this.translate.instant("acceptBefore"));
     // else
        this.helper.presentToast(this.translate.instant("acceptsu"));
      this.navCtrl.pop();
    },err=>{
      this.navCtrl.pop();
    });
    
  }
  reject(){
   
    this.service.acceptorrejectAfterNotificationJSCheckin(this.jobId,this.jsId,-1,resp=>{
      this.helper.presentToast(this.translate.instant("rejectsu"))
      this.navCtrl.pop();
    },err=>{
      this.navCtrl.pop();
    });
  }
}
