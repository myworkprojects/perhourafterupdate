import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CompanyCheckInNotificationPage } from './company-check-in-notification';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';

@NgModule({
  declarations: [
    CompanyCheckInNotificationPage,
  ],
  imports: [
    IonicPageModule.forChild(CompanyCheckInNotificationPage),
    TranslateModule.forChild()
  ],
})
export class CompanyCheckInNotificationPageModule {}
