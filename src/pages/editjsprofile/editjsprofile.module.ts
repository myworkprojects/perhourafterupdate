import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditjsprofilePage } from './editjsprofile';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [
    EditjsprofilePage,
  ],
  imports: [
    IonicPageModule.forChild(EditjsprofilePage),
    TranslateModule.forChild(),
  ],
})
export class EditjsprofilePageModule {}
