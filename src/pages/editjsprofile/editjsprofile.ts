import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Events,AlertController ,PopoverController} from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ServicesProvider } from '../../providers/services/services';
import { HelperProvider } from '../../providers/helper/helper';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';


@IonicPage()
@Component({
  selector: 'page-editjsprofile',
  templateUrl: 'editjsprofile.html',
})
export class EditjsprofilePage {

  private registerForm : FormGroup;
  name;
  email;
  IBANtxt;
  jobtitletxt;
  videourl="";
  submitAttempt = false;
  errors = {
    nameErr:"",
    emailErr:"",
    IBANtxtErr: "",
    videourlErr:"",
    jobtitletxt:""
  };
  langDirection = "";
  placeholder = {name:"",email:"", IBANtxt: "" ,videourl:"",jobtitletxt:""};
  scaleClass="";
  hideemailerr = true;
  userData;
  userid;
  canceltext
  oktext
  categories
  optionsArr 
  cities = []
  city_id = ""
  gender = ""
  dob = ""
  date=new Date().toISOString();
  SelectedDayStrings
  SelectedDaysIDs
  daysArr = []
  days
  workingtime=""
  workingDaysInWeekend =""
  hoursperweek = ""
  
  malechecked
  anotherdata
  hoursCount

  sunamTime = false
  sunpmTime = false

  monamTime = false
  monpmTime = false

  tueamTime = false
  tuepmTime = false

  wedamTime = false
  wedpmTime = false

  thuamTime = false
  thupmTime = false

  friamTime = false
  fripmTime = false

  satamTime = false
  satpmTime = false


  nat_id
  natsArr
  natString
  jobscatsarr
  jobcatid

  constructor(public srv:ServicesProvider, public storage: Storage,public events:Events,
    public translate: TranslateService,public helper:HelperProvider,public alertCtrl: AlertController,
    private formBuilder: FormBuilder,public navCtrl: NavController,public popoverCtrl: PopoverController,
    public navParams: NavParams) {

      // this.gender = "1"
      
      this.jobscatsarr=[{SID:1,labeltxt:this.translate.instant("student")},{SID:2,labeltxt:this.translate.instant("employee")},{SID:3,labeltxt:this.translate.instant("unemployed")}]

    this.name = this.navParams.get("name");
    this.email = this.navParams.get("email");
    this.userid = this.navParams.get("userid");
    this.jobtitletxt = this.navParams.get("desc");
    this.jobcatid = this.navParams.get("desc");
    this.anotherdata = this.navParams.get("anotherData");
    console.log("anotherdata from js edit : ",this.anotherdata)

    if(this.navParams.get("iban"))
      this.IBANtxt = this.navParams.get("iban");

    if(this.navParams.get("videourl"))
      this.videourl = this.navParams.get("videourl");

    this.langDirection = this.helper.lang_direction;

    if(this.langDirection == "rtl")
    this.scaleClass="scaleClass";
    


    this.registerForm = this.formBuilder.group({
      
            name :['',Validators.required],
            email:['', Validators.compose([Validators.required,Validators.email])],
            // IBANtxt: ['', Validators.compose([Validators.required, Validators.pattern("^(SA)([0-9]{22})")])],
            IBANtxt: ['',Validators.pattern("^(SA)([0-9]{22})")],
            // jobtitletxt :['',Validators.required],
            jobtitletxt :['',''],
            categories:['',''],
            city_id:['',''],
            gender:['',''],
            dob:['',''],
            // days:['',''],
            // workingtime:['',''],
            workingDaysInWeekend:['',''],
            hoursperweek:['',''],
            nat_id:['',''],
            jobcatid:['','']

            //,
          //  videourl:['',Validators.pattern('(?:(?:(?:ht|f)tp)s?://)?[\\w_-]+(?:\\.[\\w_-]+)+([\\w.,@?^=%&:/~+#-]*[\\w@?^=%&/~+#-])?')]
     

    });

    this.placeholder.name = this.translate.instant("name");
    this.errors.nameErr = this.translate.instant("nameErr");

    this.placeholder.email = this.translate.instant("email");
    this.placeholder.IBANtxt = this.translate.instant("IBAN");
    this.placeholder.videourl = this.translate.instant("videourl");
    this.errors.videourlErr = this.translate.instant("videourlErr");

    this.placeholder.jobtitletxt = this.translate.instant("jobtitletxt1")
    this.errors.jobtitletxt = this.translate.instant("jobtitleErr")
    

    
    this.hideemailerr = true;
  //  this.natsArr=[{SID:1,Nationality:this.translate.instant("student")},{SID:2,Nationality:this.translate.instant("employee")},{SID:3,Nationality:this.translate.instant("unemployed")}]

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditjsprofilePage');
    this.oktext = this.translate.instant("done")
    this.canceltext = this.translate.instant("cancel")
   
    this.daysArr = [{SID:1,labeltxt:this.translate.instant("saturday"),checked:true},
    {SID:2,labeltxt:this.translate.instant("sunday"),checked:false},
    {SID:3,labeltxt:this.translate.instant("monday"),checked:true},
    {SID:4,labeltxt:this.translate.instant("tuesday"),checked:false},
    {SID:5,labeltxt:this.translate.instant("Wednesday"),checked:false},
    {SID:6,labeltxt:this.translate.instant("Thursday"),checked:false},
    {SID:7,labeltxt:this.translate.instant("friday"),checked:false}]

    this.hoursCount =[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24]

    if(this.anotherdata.joinday)
      this.days = this.anotherdata.joinday.split(",")

     if(this.anotherdata.amorpmTxt){
      if(this.anotherdata.amorpmTxt == this.translate.instant("am"))
        this.workingtime = "1"
      else if(this.anotherdata.amorpmTxt == this.translate.instant("pm"))
        this.workingtime = "0"
     } 

     if(this.anotherdata.natid)
      this.nat_id = this.anotherdata.natid
     else
      this.nat_id = ""

     if(this.anotherdata.gender)
      this.gender = this.anotherdata.gender

    if(this.anotherdata.dob)
     this.dob = this.anotherdata.dob

     if(this.anotherdata.hoursperweek)
     this.hoursperweek = this.anotherdata.hoursperweek

     if(this.anotherdata.workingDaysInWeekend)
      this.workingDaysInWeekend = this.anotherdata.workingDaysInWeekend
// this.categories=["1","2","3"]
    this.date=new Date().toISOString();
    this.listCategoriesAndCities();
    this.loadNationality();



    // this.workingDaysAndTimes = respData.details.DaysAvailability
    //         if(respData.details.DaysAvailability){
    //           this.hideAll = false
    //           if(respData.details.DaysAvailability.length >=3){
    //             this.hideAll = false
    //             var tmp
    //             var arr = respData.details.DaysAvailability.split(",")
    //             for(var j=0;j<arr.length;j++){
    //               tmp = arr[j].split("x")


console.log("this.anotherdata.workingDaysAndTimes : ",this.anotherdata.workingDaysAndTimes)
    if(this.anotherdata.workingDaysAndTimes){

      if(this.anotherdata.workingDaysAndTimes){
      //  this.hideAll = false
        // if(this.anotherdata.workingDaysAndTimes.length >=1){
          if(this.anotherdata.workingDaysAndTimes.length >=3){
         // this.hideAll = false
          var tmp
          var arr
          console.log("typeof(this.anotherdata.workingDaysAndTimes) : ",typeof(this.anotherdata.workingDaysAndTimes))
          if(typeof(this.anotherdata.workingDaysAndTimes) == "string")
           arr = this.anotherdata.workingDaysAndTimes.split(",")
          else
           arr = this.anotherdata.workingDaysAndTimes//.split(",")
          console.log("arr : ",arr)
          for(var j=0;j<arr.length;j++){
            tmp = arr[j].split("x")
            if(tmp[0] == "1"){
            //  this.sunHide = false
              if(tmp[1] == "0"){
                this.sunamTime  = true
                this.sunpmTime = true
              }else if(tmp[1] == "1"){
                this.sunamTime  = true
                this.sunpmTime = false
              }else if(tmp[1] == "2"){
                this.sunamTime  = false
                this.sunpmTime = true
              }
            }
            if(tmp[0] == "2"){
              //this.monHide = false
              if(tmp[1] == "0"){
                this.monamTime  = true
                this.monpmTime = true
              }else if(tmp[1] == "1"){
                this.monamTime  = true
                this.monpmTime = false
              }else if(tmp[1] == "2"){
                this.monamTime  = false
                this.monpmTime = true
              }
              
            }
            if(tmp[0] == "3"){
              //this.tueHide = false
              if(tmp[1] == "0"){
                this.tueamTime  = true
                this.tuepmTime = true
              }else if(tmp[1] == "1"){
                this.tueamTime  = true
                this.tuepmTime = false
              }else if(tmp[1] == "2"){
                this.tueamTime  = false
                this.tuepmTime = true
              }
            }
            if(tmp[0] == "4"){
              //this.wedHide = false
              if(tmp[1] == "0"){
                this.wedamTime  = true
                this.wedpmTime = true
              }else if(tmp[1] == "1"){
                this.wedamTime  = true
                this.wedpmTime = false
              }else if(tmp[1] == "2"){
                this.wedamTime  = false
                this.wedpmTime = true
              }
            }
            if(tmp[0] == "5"){
              //this.thuHide = false
              if(tmp[1] == "0"){
                this.thuamTime  = true
                this.thupmTime = true
              }else if(tmp[1] == "1"){
                this.thuamTime  = true
                this.thupmTime = false
              }else if(tmp[1] == "2"){
                this.thuamTime  = false
                this.thupmTime = true
              }
            }
            if(tmp[0] == "6"){
              //this.friHide = false
              if(tmp[1] == "0"){
                this.friamTime  = true
                this.fripmTime = true
              }else if(tmp[1] == "1"){
                this.friamTime  = true
                this.fripmTime = false
              }else if(tmp[1] == "2"){
                this.friamTime  = false
                this.fripmTime = true
              }
            }
            if(tmp[0] == "7"){
              //this.satHide = false
              if(tmp[1] == "0"){
                this.satamTime  = true
                this.satpmTime = true
              }else if(tmp[1] == "1"){
                this.satamTime  = true
                this.satpmTime = false
              }else if(tmp[1] == "2"){
                this.satamTime  = false
                this.satpmTime = true
              }
            }
      
      
      
      
      
      
      
          }
        }else{
         // this.hideAll = true  
         console.log("this.hideAll = true 2")
        }
      }else{
        //this.hideAll = true
        console.log("this.hideAll = true 1")
      }


    }





  }

  save(){
    if(! this.registerForm.valid ){
      this.submitAttempt=true;

      // if(this.registerForm.controls["videourl"].errors){
      //   console.log("videourl errors",this.registerForm.controls["videourl"].errors)
      // }
      if(this.registerForm.controls["email"].errors){
        if(this.registerForm.controls["email"].errors['required'])
        {
          this.hideemailerr = false;
          this.errors.emailErr = this.translate.instant("emailErr");
        }
        else if(this.registerForm.controls["email"].errors['email']) //invalidChars 
        {
          this.hideemailerr = false;
          this.errors.emailErr = this.translate.instant("invalidEmailAddress");
        }   
        else
        {
          this.errors.emailErr="";
          this.hideemailerr = true;
          console.log("phone errors:",this.registerForm.controls["email"].errors);
        }
          
      }


      if (this.registerForm.controls["IBANtxt"].errors) {
        if (this.registerForm.controls["IBANtxt"].errors['required'])
          this.errors.IBANtxtErr = this.translate.instant("IBANreq");
        else if (this.registerForm.controls["IBANtxt"].errors['pattern'])
          this.errors.IBANtxtErr = this.translate.instant("ibanpatt");
        // else if (this.mobile.includes('-') || this.mobile.includes('.'))
        //   this.errors.mobileErr = this.translate.instant("invalidPhone");
        else
          console.log("IBANtxt errors:", this.registerForm.controls["IBANtxt"].errors);
      }




    }else{

      if(! ( this.email.includes('.com') || this.email.includes('.net')) ) 
      {
        console.log("not contain  .net .com");
        this.errors.emailErr = this.translate.instant("invalidEmailAddress");
        this.hideemailerr = false;
      }else{
        this.errors.emailErr = "";
        this.hideemailerr = true;

        console.log("save");
        var joinCat ="";
        if(this.categories && this.categories.length > 1)
          joinCat = this.categories.join(',')
        else if(this.categories && this.categories.length  == 1)
          joinCat = this.categories[0]
        else 
          joinCat = ""

          var joinday ="";
          var daysStrings = []
          var daysJoin = "";
          if(this.days && this.days.length > 1){
            joinday = this.days.join(',')

            for(var x=0;x<this.days.length;x++){
              for(var y=0;y<this.daysArr.length;y++){
                if(this.days[x] == this.daysArr[y].SID)
                {
                  daysStrings.push(this.daysArr[y].labeltxt)
                  break
                }
              }
            }
            daysJoin = daysStrings.join(',')

          }else if(this.days && this.days.length  == 1)
          {
            joinday = this.days
            for(var x=0;x<this.days.length;x++){
              for(var y=0;y<this.daysArr.length;y++){
                if(this.days[x] == this.daysArr[y].SID)
                {
                  daysStrings.push(this.daysArr[y].labeltxt)
                  break
                }
              }
            }
            daysJoin = daysStrings.join(',')
          }
          else {
            joinday = ""  
            daysJoin = ""
          }
          
//day number , 0 both . 1 am .2 pm
// 1 sunday
        var sunTime = ""
        var monTime = ""
        var tueTime = ""
        var wedTime = ""
        var thuTime = ""
        var friTime = ""
        var satTime = ""

        
        if(this.sunamTime == true && this.sunpmTime == true)
          sunTime = "0"
        else if(this.sunamTime == true && this.sunpmTime == false)
          sunTime = "1"
        else if(this.sunamTime == false && this.sunpmTime == true)
          sunTime = "2"
        else
          sunTime = ""

          console.log("sunTime : ",sunTime , " , sunamTime ",this.sunamTime ," , sunpmTime ",this.sunpmTime )

        if(this.monamTime == true && this.monpmTime == true)
          monTime = "0"
        else if(this.monamTime == true && this.monpmTime == false)
          monTime = "1"
        else if(this.monamTime == false && this.monpmTime == true)
          monTime = "2"
        else
          monTime = ""


          console.log("x sunTime : ",sunTime )

        if(this.tueamTime == true && this.tuepmTime == true)
          tueTime = "0"
        else if(this.tueamTime == true && this.tuepmTime == false)
          tueTime = "1"
        else if(this.tueamTime == false && this.tuepmTime == true)
          tueTime = "2"
        else
          tueTime = ""

          console.log("a sunTime : ",sunTime )

        if(this.wedamTime == true && this.wedpmTime == true)
          wedTime = "0"
        else if(this.wedamTime == true && this.wedpmTime == false)
          wedTime = "1"
        else if(this.wedamTime == false && this.wedpmTime == true)
          wedTime = "2"
        else
          wedTime = ""

        if(this.thuamTime == true && this.thupmTime == true)
          thuTime = "0"
        else if(this.thuamTime == true && this.thupmTime == false)
          thuTime = "1"
        else if(this.thuamTime == false && this.thupmTime == true)
          thuTime = "2"
        else
          thuTime = ""

          console.log("b sunTime : ",sunTime )
        if(this.friamTime == true && this.fripmTime == true)
          friTime = "0"
        else if(this.friamTime == true && this.fripmTime == false)
          friTime = "1"
        else if(this.friamTime == false && this.fripmTime == true)
          friTime = "2"
        else
          friTime = ""

        if(this.satamTime == true && this.satpmTime == true)
          satTime = "0"
        else if(this.satamTime == true && this.satpmTime == false)
          satTime = "1"
        else if(this.satamTime == false && this.satpmTime == true)
          satTime = "2"
        else
          satTime = ""


          console.log("c sunTime : ",sunTime )
          
          var workingDaysAndTimes = []
          workingDaysAndTimes.push("0x0")
          if(sunTime)
            workingDaysAndTimes.push("1x"+sunTime)
          
            if(monTime)
            workingDaysAndTimes.push("2x"+monTime)

            if(tueTime)
            workingDaysAndTimes.push("3x"+tueTime)


            if(wedTime)
            workingDaysAndTimes.push("4x"+wedTime)

            if(thuTime)
            workingDaysAndTimes.push("5x"+thuTime)

            if(friTime)
            workingDaysAndTimes.push("6x"+friTime)

            if(satTime)
            workingDaysAndTimes.push("7x"+satTime)

console.log("workingDaysAndTimes: ",workingDaysAndTimes)
for(var j=0;j<this.natsArr.length;j++){
  if(this.nat_id == this.natsArr[j].SID){
    if(this.helper.lang_direction == "rtl")
    this.natString = this.natsArr[j].Nationality_Ar
    else if(this.helper.lang_direction == "ltr")
    this.natString = this.natsArr[j].Nationality_En

    break;
  }
    
}

var data = {
  'Seeker' : this.userid,
  'FullName':this.name,
  'Mail':this.email,
  'IBAN':this.IBANtxt,
  'VideoURL':this.videourl,
  'ShortDescription':this.jobtitletxt,
  "interestedJobs":joinCat,
  "city_id":this.city_id,
  "gender":this.gender,
  "dob":this.dob,
  "days":joinday,
  "amorpm":this.workingtime,
  "workingDaysInWeekend":this.workingDaysInWeekend,
  "WeeklyHoursCount":this.hoursperweek,
  "workingDaysAndTimes":workingDaysAndTimes.toString(),
  "Nationality":this.nat_id

}


  console.log("saved data : ",data)
this.srv.jsEditProfile(data,resp=>{
  console.log("resp from jsEditProfile",resp);
  if(JSON.parse(resp).OperationResult == "1")
  {
    this.storage.get('js_info').then((val) => {
          console.log("val from get js info from edit", val);
          if (val) {
            this.userData = val;
        this.userData.IBAN = this.IBANtxt;
        this.userData.FirstName = this.name;
        this.userData.Email = this.email;
        this.userData.VideoURL = this.videourl;
        // this.userData.ShortDescription = this.jobtitletxt;
        this.userData.ShortDescription = this.jobcatid;
        this.userData.Workingdays = daysJoin  //for storage and profile check
        this.userData.joinday  = joinday // , 
        var amorpmTxt = ""
        if(this.workingtime == "1")
          amorpmTxt = this.translate.instant("am")
        else if(this.workingtime == "0")
          amorpmTxt = this.translate.instant("pm")
        
        this.userData.amorpmTxt = amorpmTxt
        
        this.userData.city_id = this.city_id
        this.userData.interestedJobs = joinCat
        this.userData.gender = this.gender
        this.userData.dob = this.dob
        this.userData.hoursperweek = this.hoursperweek
        this.userData.workingDaysInWeekend = this.workingDaysInWeekend

        this.userData.workingDaysAndTimes = workingDaysAndTimes
        this.userData.Nationality = this.nat_id
        this.userData.natString = this.natString
        this.events.publish("profileChanged",this.name)
        this.events.publish("profileChangedall",this.userData)
        this.helper.storeJSInfo(this.userData);
          }});

    this.helper.presentToast(this.translate.instant("suedit"));
    this.navCtrl.pop();

    
  }else{
    this.helper.presentToast(this.translate.instant("ServerError"));
    this.navCtrl.pop();
  }
    
    

},err=>{
  console.log("err from jsEditProfile",err);
  this.helper.presentToast(this.translate.instant("ServerError"));
})

        

      }
      
    }

  }



  onInputTime() {
    console.log("onInputTime");
    this.IBANtxt = this.IBANtxt.replace(/\s/g, "");
  }

  dismiss(){
    this.navCtrl.pop();
  }

  listCategoriesAndCities(){
    
   if(this.anotherdata.city_id)
    this.city_id = this.anotherdata.city_id

    if(this.anotherdata.interestedJobs)
      this.categories = this.anotherdata.interestedJobs

    console.log("city_id: ",this.city_id)
      this.srv.listCities((data)=>{
      data = JSON.parse(data)
      this.cities = data
      console.log(data)
      if(this.anotherdata.city_id)
      this.city_id = this.anotherdata.city_id

      console.log("city_id: ",this.city_id)

    },
    ()=> this.helper.presentToast(this.translate.instant("ServerError")))


    this.srv.companylistjobcategories(resp=>{
      console.log("resp from companylistjobcategories",resp)
    //  this.categoriesAlert(JSON.parse(resp))
      this.optionsArr = JSON.parse(resp)
      for(var i=0;i<this.optionsArr.length;i++){
        if(this.helper.lang_direction == "rtl")
          this.optionsArr[i].labeltxt = this.optionsArr[i].CategoryNameAr
        else if(this.helper.lang_direction == "ltr")
        {
          this.optionsArr[i].labeltxt = this.optionsArr[i].CategoryNameEn
    
        }
      }

      if(this.anotherdata.interestedJobs)
      this.categories = this.anotherdata.interestedJobs
    },err=>{
      console.log("err from companylistjobcategories",err)
    })

  }
  categoriesAlert(allData){
    var alertInput = []
    var labeltxt="";
    for(var i=0;i<allData.length;i++){
      if(this.helper.lang_direction == "rtl")
        labeltxt = allData[i].CategoryNameAr
      else if(this.helper.lang_direction == "ltr")
      {
        labeltxt = allData[i].CategoryNameEn
  console.log("from rtl labeltxt : ",labeltxt)
      }
       
  //       console.log("this.helper.lang_direction: ",this.helper.lang_direction)
  //     console.log("allData[i].CategoryNameEn : ",allData[i].CategoryNameEn)
  // console.log("labeltxt : ",labeltxt)
  // checked:true
      alertInput.push({type:'checkbox',
      label:labeltxt,
      value:allData[i].SID})
    }
  
    console.log("alertInput : ",alertInput)
    let alert = this.alertCtrl.create({
      title: this.translate.instant("Job_category"),
      // message: this.translate.instant(""),
      inputs : alertInput,
      buttons: [
        {
          text: this.translate.instant("cancel"),
          role: 'cancel',
          handler: (data) => {
            console.log('disagree clicked',data);
          }
        },
        {
          text: this.translate.instant("done"),
          handler: (catid) => {
            console.log('agree clicked',catid);
            var jsid
            this.storage.get('js_info').then((val) => {
              console.log("val from get js info",val);
              if(val)
              {
                jsid = val.SID
                console.log("user id : ",jsid," cat id: ",catid)
              }
            });
          
          }
        }
      ]
    });
    alert.present();
  }

  choosedCategories(){
    console.log("choosedCategories : ",this.categories)
  }
  daysPopover(event) {
    let popover = this.popoverCtrl.create('DaysPopoverPage');
    popover.present({
      ev: event
    });
    popover.onDidDismiss(data => {
      console.log("days popover data : ",data);
      if (data != null) {
        // this.selectedDays = data
        let DaysIDs = []
        let DaysStrings = []
        for(let x=0;x<data.length;x++){
          DaysStrings.push(data[x].day)
          DaysIDs.push(data[x].value)
        }
        this.SelectedDayStrings = DaysStrings.toString()
        this.SelectedDaysIDs = DaysIDs.toString()
        // console.log("this.SelectedDaysIDs : ",this.SelectedDaysIDs)
      }
    })
  
  }

  loadNationality(){
    this.srv.listNationalities(1,resp=>{
      console.log("resp from listNationalities : ",resp);
      
      this.natsArr = JSON.parse(resp);
    },err=>{
      console.log("err from listNationalities : ",err);
    })
  }

  
}
