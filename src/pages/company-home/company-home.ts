import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController,ViewController ,AlertController} from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { HelperProvider } from '../../providers/helper/helper';

import { ServicesProvider } from '../../providers/services/services';
import { Storage } from '@ionic/storage';
import { DatePipe } from '@angular/common';
import { CompanyselectedcandidatesPage } from '../companyselectedcandidates/companyselectedcandidates';
import { Chart } from 'chart.js';

@Component({
  selector: 'page-company-home',
  templateUrl: 'company-home.html',
})
export class CompanyHomePage {
  CompanyLogo
  langDirection:any
  comsid:any
  jobdata:any=[]
  date:any
  disable:any=false;
  refresher;
  hideNoDataTxt = true;
  activeJobsForStatistics = "";
  AppliedForStatistics = "";
  percentForStatistics = ""

  promisedDeliveryChart
   
  constructor( public alertCtrl:AlertController,public datepipe:DatePipe,public storage:Storage,public service:ServicesProvider,public ViewCtrl:ViewController,public helper:HelperProvider,public translate:TranslateService,public menu:MenuController,public navCtrl: NavController, public navParams: NavParams) {
    this.hideNoDataTxt = true;
    
  }

  ionViewDidLoad() {
    
    this.langDirection = this.helper.lang_direction;

    this.date=this.datepipe.transform(new Date(), 'MM-dd-yyyy')
    console.log(this.date)
    this.langDirection = this.helper.lang_direction;
    this.storage.get('js_info').then((val)=>{
      // if(this.refresher){
      //   this.data=[];
      // }

      console.log(JSON.stringify(val))
      if(val)
      {
        this.CompanyLogo = val.CompanyLogo
        this.comsid=val.SID
        console.log(this.comsid)

        this.service.companyStatistics(this.comsid,resp=>{
          console.log("resp from companyStatistics",resp);
          var respData = JSON.parse(resp);
          if(respData.Result == "1")
          {
            if(respData.List.ActiveJobsCount)
              this.activeJobsForStatistics = respData.List.ActiveJobsCount
            else
              this.activeJobsForStatistics = "0"

            if(respData.List.CandidatesCount)
              this.AppliedForStatistics = respData.List.CandidatesCount
            else
              this.AppliedForStatistics = "0"

            console.log("respData.List.JobsHasCandidatesPercentage : ",respData.List.JobsHasCandidatesPercentage)
            if(respData.List.JobsHasCandidatesPercentage)
              this.percentForStatistics = respData.List.JobsHasCandidatesPercentage
            else
              this.percentForStatistics = "0"


            console.log("befor call stat : ",this.percentForStatistics)
            this.loadChart(this.percentForStatistics,this.translate.instant('jobresp'),this.helper.lang_direction);
          }else{
            this.activeJobsForStatistics = "0"
            this.AppliedForStatistics = "0"
            this.percentForStatistics = "0"
            this.loadChart(this.percentForStatistics,this.translate.instant('jobresp'),this.helper.lang_direction);
          }
           
        },err=>{
          console.log("err from companyStatistics",err);
        });


        this.service.companypostedjob(this.comsid,(data)=>{
          console.log("resp from companypostedjob : ",data)
          let Dataparse=JSON.parse(data)
          if(Dataparse.length == 0)
          {
            this.hideNoDataTxt = false;
            this.jobdata=[]
           // this.helper.presentToast(this.translate.instant("noPostedjobs"))
          }else{
            this.hideNoDataTxt = true;
          this.jobdata=Dataparse
          // this.jobdata.forEach(element => {
           
          // });
          this.jobdata.forEach(element => {
            element.CreationDate= this.datepipe.transform(element.CreationDate.split('T')[0], 'MM-dd-yyyy')
           // element.CreationDate= (this.date - parseInt(element.CreationDate))/ (24 * 3600 * 1000);
           if(element.Status == "1")
           {
            element. jobbordercolor ="2px solid #2783c3";
            element.hideviewDEtailsForUNposted = false;
           }
          else if(element.Status == "0")
          {
            element. jobbordercolor ="2px solid red";
            element.hideviewDEtailsForUNposted = true;
          }
          

          });
          console.log("this.jobdata : ",this.jobdata)

        }

        },(data)=>{
    
        })
      }
  
    })
   
    console.log('ionViewDidLoad CompanypostedjobPage');


  }
  openmenu()
  {
    this.menu.open()
  }
  showdetails(name,id,count)
  {
    if(count>0)
    {
    this.navCtrl.push('CompanyselectedcandidatesPage',{name: name,ID:id})
    }
    else
    {
      this.helper.presentToast(this.translate.instant('nocandidate'))
    }
  }
  doRefresh(ev){
    this.refresher = ev;
    this.date=this.datepipe.transform(new Date(), 'MM-dd-yyyy')
    console.log(this.date)
    this.langDirection = this.helper.lang_direction;
    console.log("for refresh2")
    this.storage.get('js_info').then((val)=>{
      console.log(JSON.stringify(val))
      if(val)
      {
        this.comsid=val.SID
        console.log(this.comsid)
        this.service.companypostedjob(this.comsid,(data)=>{
          console.log("for refresh1")
          if(this.refresher){
            this.refresher.complete();
          }
          console.log("resp from companypostedjob : ",data)
          let Dataparse=JSON.parse(data)
          console.log("Dataparse : ",Dataparse , "Dataparse.length :",Dataparse.length)
          if(Dataparse.length == 0)
          {
            this.hideNoDataTxt = false;
            this.jobdata=[]
            //this.helper.presentToast(this.translate.instant("noPostedjobs"))
          }else{
            this.hideNoDataTxt = true;
          this.jobdata=Dataparse
          // this.jobdata.forEach(element => {
           
          // });
          this.jobdata.forEach(element => {
            element.CreationDate= this.datepipe.transform(element.CreationDate.split('T')[0], 'MM-dd-yyyy')
           // element.CreationDate= (this.date - parseInt(element.CreationDate))/ (24 * 3600 * 1000);
          
           if(element.Status == "1")
           {
            element. jobbordercolor ="2px solid #2783c3";
            element.hideviewDEtailsForUNposted = false;
           }
          else if(element.Status == "0")
          {
            element. jobbordercolor ="2px solid red";
            element.hideviewDEtailsForUNposted = true;
          }

          });
          
          console.log("this.jobdata : ", this.jobdata)
        }

          if(this.refresher){
            this.refresher.complete();
          }

        },(data)=>{
          if(this.refresher){
            this.refresher.complete();
          }
        })
      }
  
    })
   
    
  }

  activationCode(){
    this.navCtrl.setRoot('ActivationCodePage',{"JsSID":"1025",type:"js"}); 

  }

  canceluppostedjob(item){
    console.log("item from canceluppostedjob",item);
   
    let alert = this.alertCtrl.create({
      title: this.translate.instant('cancelJob'),
      buttons: [
        {
          text: this.translate.instant('no2'),
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: this.translate.instant('yes2'),
          handler: () => {
        this.service.compCacelNotPostedJob(this.comsid ,item.SID,resp=>{
          console.log("resp from compCacelNotPostedJob",resp);
           // Cancel Status: when -2: it means this job have selected JS and cannot be canceled

          if(JSON.parse(resp).Result == "1")
            this.helper.presentToast(this.translate.instant("jobcanceled"));
          else if( JSON.parse(resp).Result == "-2")
            this.helper.presentToast(this.translate.instant("can'tcancelThisJob"));
          else 
          this.helper.presentToast(this.translate.instant("ServerError"));

          this.doRefresh2();
        },err=>{
          console.log("err from compCacelNotPostedJob",err);
          this.helper.presentToast(this.translate.instant("ServerError"));
          this.doRefresh2();
        })
          }
        }
      
    ]
  })
  alert.present()
  }
  
  doRefresh2(){
   
    this.date=this.datepipe.transform(new Date(), 'MM-dd-yyyy')
    console.log(this.date)
    this.langDirection = this.helper.lang_direction;
    console.log("for refresh2")
    this.storage.get('js_info').then((val)=>{
      console.log(JSON.stringify(val))
      if(val)
      {
        this.comsid=val.SID
        console.log(this.comsid)



        this.service.companyStatistics(this.comsid,resp=>{
          console.log("resp from companyStatistics",resp);
          var respData = JSON.parse(resp);
          if(respData.Result == "1")
          {
            if(respData.List.ActiveJobsCount)
              this.activeJobsForStatistics = respData.List.ActiveJobsCount
            else
              this.activeJobsForStatistics = "0"

            if(respData.List.CandidatesCount)
              this.AppliedForStatistics = respData.List.CandidatesCount
            else
              this.AppliedForStatistics = "0"

            console.log("respData.List.JobsHasCandidatesPercentage : ",respData.List.JobsHasCandidatesPercentage)
            if(respData.List.JobsHasCandidatesPercentage)
              this.percentForStatistics = respData.List.JobsHasCandidatesPercentage
            else
              this.percentForStatistics = "0"

              
            console.log("befor call stat : ",this.percentForStatistics)
            this.loadChart(this.percentForStatistics,this.translate.instant('jobresp'),this.helper.lang_direction);
          }else{
            this.activeJobsForStatistics = "0"
            this.AppliedForStatistics = "0"
            this.percentForStatistics = "0"
            this.loadChart(this.percentForStatistics,this.translate.instant('jobresp'),this.helper.lang_direction);
          }
           
        },err=>{
          console.log("err from companyStatistics",err);
        });


        this.service.companypostedjob(this.comsid,(data)=>{
          console.log("for refresh1")
          if(this.refresher){
            this.refresher.complete();
          }
          console.log("resp from companypostedjob : ",data)
          let Dataparse=JSON.parse(data)
          console.log("Dataparse : ",Dataparse , "Dataparse.length :",Dataparse.length)
          if(Dataparse.length == 0)
          {
            this.hideNoDataTxt = false;
            this.jobdata=[]
            //this.helper.presentToast(this.translate.instant("noPostedjobs"))
          }else{
            this.hideNoDataTxt = true;
          this.jobdata=Dataparse
          // this.jobdata.forEach(element => {
           
          // });
          this.jobdata.forEach(element => {
            element.CreationDate= this.datepipe.transform(element.CreationDate.split('T')[0], 'MM-dd-yyyy')
           // element.CreationDate= (this.date - parseInt(element.CreationDate))/ (24 * 3600 * 1000);
          
           if(element.Status == "1")
           {
            element. jobbordercolor ="2px solid #2783c3";
            element.hideviewDEtailsForUNposted = false;
           }
          else if(element.Status == "0")
          {
            element. jobbordercolor ="2px solid red";
            element.hideviewDEtailsForUNposted = true;
          }

          });
          
          console.log("this.jobdata : ",this.jobdata)
        }

          

        },(data)=>{
          
        })
      }
  
    })
   
    
  }

  jsprofile(){
    console.log("js profile")
    this.navCtrl.push('JsProfilePage');
  }


  details(item){
  console.log("details item: ",item);
    this.navCtrl.push("CompJobDetailsPage",{jobId:item.SID})
  }
  selected(item){
    console.log("selected item: ",item);
    var newjobname = "";
    if(this.langDirection == "rtl")
    newjobname = item.JobTitleAr;
    else if(this.langDirection == "ltr")
    newjobname = item.JobTitleEn;
    this.navCtrl.push("CompanyselectedusersPage",{jobId:item.SID,jobname:newjobname})
  }
  public chartClicked(e:any):void {
    console.log(e);
  }
  
  public chartHovered(e:any):void {
    console.log(e);
  }
  public doughnutChartLabels:string[] = ['Download Sales', 'In-Store Sales', 'Mail-Order Sales'];
  public doughnutChartData:number[] = [80, 20];
  public doughnutChartType:string = 'doughnut';
  public doughnutColors:any[] = [
    { backgroundColor: ["#0a7f0a","rgb(210, 210, 210)"] },
    { borderColor: ["#AEEBF2", "#FEFFC9"] }];
  
    barChartColors: any [] =[
      {
          backgroundColor:["#0a7f0a","rgb(210, 210, 210)"] ,
          fontSize:'10'
          // borderColor: "rgba(10,150,132,1)",
          // borderWidth: 1
      }
  ];
  public options:any={
    cutoutPercentage:90,
      percentageInnerCutout: 95,
 
  };
  
  loadChart(txtval,newtxt,lang){
    console.log("from load chart : ",this.percentForStatistics)
    console.log("txtval = ",txtval);
    var data = {
      labels: [
        "",
        ""
      ],
      datasets: [
        {
          data: [txtval,100-txtval],
          backgroundColor: [
            "#0a7f0a","rgb(210, 210, 210)"
          ]
          //,
          // hoverBackgroundColor: [
          //   "#FF6384",
          //   "#36A2EB",
          //   "#FFCE56"
          // ]
        }]
    };

    // this.promisedDeliveryChart = new Chart(document.getElementById('myChart')
    // this.promisedDeliveryChart.distroy()
     this.promisedDeliveryChart = new Chart(document.getElementById('myChart'), {
      type: 'doughnut',
      data: data,
      options: {
        responsive: true,
        legend: {
          display: false
        },
        percentageInnerCutout: 80,
        cutoutPercentage:80
      }
    });
    
    Chart.pluginService.register({
      beforeDraw: function(chart) {
        var width = chart.chart.width,
            height = chart.chart.height,
            ctx = chart.chart.ctx;
    
        ctx.restore();
        var fontSize = ((height / 114)-2).toFixed(2);
        ctx.font = fontSize + "em sans-serif"; //font = "20px Roboto";
        ctx.textBaseline = "middle";
        ctx.textColor="#0a7f0a";
        ctx.fillStyle = '#0a7f0a'

        console.log("before text : ",this.percentForStatistics )
        console.log("txtval before assign : ",txtval)
        var text = txtval+ "%" ,
            textX = Math.round((width - ctx.measureText(text).width) / 2),
            textY = height / 2;
            console.log("text = ",text )
            if(lang == "rtl")
              textX  = textX + 20 //25

            console.log("lang",lang , "textx",textX)
        ctx.fillText(text, textX, textY-20);
        //ctx.fillText((distributionChartData[0] + distributionChartData[1] + distributionChartData[2]) + " Responses", 135, 160);
        // var fontSize2 = ((height / 114)-5).toFixed(2);
        ctx.font = fontSize  + "em sans-serif";
        ctx.fillStyle = '#868484';
        //ctx.textAlign = "center";
        var text2 = newtxt,
            textX2 = Math.round((width - ctx.measureText(text2).width) / 2),
            textY2 = height / 2;

            if(lang == "rtl")
            textX2  = textX2 + 65
            console.log("lang",lang , "textx2",textX2)
        ctx.fillText(text2, textX2, textY2+5);

    

        ctx.save();
      }
    });
  }

  
}
