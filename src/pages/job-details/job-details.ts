import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams , Platform,Events} from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
import { TranslateService } from '@ngx-translate/core';
import { ServicesProvider } from '../../providers/services/services';
import { Storage } from '@ionic/storage';
import { SocialSharing } from '@ionic-native/social-sharing';
import { DatePipe } from '@angular/common';
// declare var google;

declare var google: any;
// import {google } from '@ionic-native/google-maps';

@IonicPage()
@Component({
  selector: 'page-job-details',
  templateUrl: 'job-details.html',
})
export class JobDetailsPage {

  
  langDirection;
  
  lat;
  lng;
  jobDetails;
  applied
  from
  btntxt
  hideadd
  
  @ViewChild('map') mapElement;
  map: any;
  scaleClass="";
  myLongAddress="";
  jobdaysString 


//   SeekerAgeFrom: "",
// SeekerAgeTo: ""

// Nationality_Ar: "سعودي"
// Nationality_En: "Saudi"
// SID: 1

// Nationality_Ar: "غير سعودي"
// Nationality_En: "Not Saudi"
// SID: 2


// Nationality_Ar: "كل الجنسيات"
// Nationality_En: "All Nationalities"
// SID: 3

  constructor(public events:Events, public datepipe: DatePipe,public socialSharing: SocialSharing,public storage:Storage,public service:ServicesProvider,
    public Platform: Platform, public translate: TranslateService,public helper:HelperProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.langDirection = this.helper.lang_direction;
    if(this.langDirection == "rtl")
      this.scaleClass="scaleClass";
    this.jobDetails = this.navParams.get('jobDetails');
    

    if (this.jobDetails.NationalitySID == 1){
      this.jobDetails.NationalityTXt = this.translate.instant("Saudi")
    }else if (this.jobDetails.NationalitySID == 2){
      this.jobDetails.NationalityTXt = this.translate.instant("NotSaudi")
    }else if (this.jobDetails.NationalitySID == 3){
      this.jobDetails.NationalityTXt = this.translate.instant("AllNationalities")
    }
 

    if(this.jobDetails.SalaryType == "2" || this.jobDetails.SalaryType == "3"){

      var days = this.jobDetails.WorkingDays.split(",")
      var daysString = []
      for(var x=0;x<days.length;x++){
        if(days[x] == "1")
        daysString.push(this.translate.instant("saturday"))
        else if(days[x] == "2")
        daysString.push(this.translate.instant("sunday"))
        else if(days[x] == "3")
        daysString.push(this.translate.instant("monday"))
        else if(days[x] == "4")
        daysString.push(this.translate.instant("tuesday"))
        else if(days[x] == "5")
        daysString.push(this.translate.instant("Wednesday"))
        else if(days[x] == "6")
        daysString.push(this.translate.instant("Thursday"))
        else if(days[x] == "7")
        daysString.push(this.translate.instant("friday"))
      }
      this.jobdaysString = daysString.join(",")
    }
   
    this.from = this.navParams.get('from');
    if(this.from == "unapply"){
      this.btntxt = this.translate.instant('unapply');
      this.hideadd = true;
    }
    else if(this.from == "apply"){
      this.btntxt = this.translate.instant('Apply');
      this.hideadd = false;
    }else if(this.from =="notification"){
      this.btntxt = this.translate.instant('Apply');
      this.hideadd = false;
      var jobId = this.navParams.get("jobId")
      this.service.getjobdetailsforcomp(jobId,resp=>{
        console.log("resp from getjobdetailsforcomp",resp);
        if(JSON.parse(resp).Result == "1"){
          this.jobDetails = JSON.parse(resp).details;

          // Nationality_Ar: "سعودي"
// Nationality_En: "Saudi"
// SID: 1

// Nationality_Ar: "غير سعودي"
// Nationality_En: "Not Saudi"
// SID: 2


// Nationality_Ar: "كل الجنسيات"
// Nationality_En: "All Nationalities"
// SID: 3

          if (this.jobDetails.NationalitySID == 1){
            this.jobDetails.NationalityTXt = this.translate.instant("Saudi")
          }else if (this.jobDetails.NationalitySID == 2){
            this.jobDetails.NationalityTXt = this.translate.instant("NotSaudi")
          }else if (this.jobDetails.NationalitySID == 3){
            this.jobDetails.NationalityTXt = this.translate.instant("AllNationalities")
          }
       
          

          this.lat = this.jobDetails.Latitude;
        this.lng = this.jobDetails.Longitude;
    
        // let startDat = new Date(this.jobDetails.StartDate+".000Z")
        console.log("this.jobDetails.StartDate : ",this.jobDetails.StartDate)
      //  (new Date(x)).toUTCString()
      var a = []
      a = this.jobDetails.StartDate.split(/[^0-9]/);
      console.log("a : ",a)
      var d=new Date (a[0],a[1]-1,a[2],a[3],a[4],a[5] );
      console.log("d : ",new Date(d))
      this.jobDetails.Starttime=  this.datepipe.transform(new Date(d), 'h:m a')
     
        let startDat = new Date(this.jobDetails.StartDate)
        console.log(" : ",startDat)
        
    
        this.jobDetails.StartDate2 = startDat.getDate()+"/"+(startDat.getMonth()+1) + "/" +startDat.getFullYear()
        // this.jobDetails.Starttime= startDat.getUTCHours()+":"+startDat.getUTCMinutes()
        // this.jobDetails.Starttime = this.datepipe.transform(startDat, 'h:m a')
    
        
      //  new Date(this.jobDetails.StartDate).toUTCString()
        //this.jobDetails.Starttime = this.datepipe.transform(new Date(this.jobDetails.StartDate).toUTCString(), 'h:m a')
        // let endDat = new Date(this.jobDetails.EndDate+".000Z")
        console.log("this.jobDetails.EndDate : ",this.jobDetails.EndDate)
        var a2 = []
      a2 = this.jobDetails.EndDate.split(/[^0-9]/);
      console.log("a : ",a2)
      var d2=new Date (a2[0],a2[1]-1,a2[2],a2[3],a2[4],a2[5] );
      console.log("d : ",new Date(d2))
      this.jobDetails.Endtime=  this.datepipe.transform(new Date(d2), 'h:m a')
     
        let endDat = new Date(this.jobDetails.EndDate)
        console.log("endDat : ",endDat)
        this.jobDetails.EndDate2 = endDat.getDate()+"/"+(endDat.getMonth()+1) + "/" +endDat.getFullYear()
        // this.jobDetails.Endtime = endDat.getHours()+":"+endDat.getMinutes()
        // this.jobDetails.Endtime = this.datepipe.transform(endDat, 'h:m a')
    
        if(this.jobDetails.Gender == 0)
          this.jobDetails.jobGender = this.translate.instant("female");
        else if (this.jobDetails.Gender == 1)
          this.jobDetails.jobGender = this.translate.instant("male");
        else 
          this.jobDetails.jobGender = this.translate.instant("any2");
    
        var dt1 = new Date();
        console.log("this.jobDetails.CreationDate : ",this.jobDetails.CreationDate)
        console.log("today : ",dt1)
        var dt2 = new Date(this.jobDetails.CreationDate);
        console.log("today : ",dt1)
        console.log("job date : ",dt2)
        this.jobDetails.dateConversion=this.DateDiff(dt1, dt2);
          
    
        if(this.jobDetails.SalaryType == "2" || this.jobDetails.SalaryType == "3"){
    
          var days = this.jobDetails.WorkingDays.split(",")
          var daysString = []
          for(var x=0;x<days.length;x++){
            if(days[x] == "1")
            daysString.push(this.translate.instant("saturday"))
            else if(days[x] == "2")
            daysString.push(this.translate.instant("sunday"))
            else if(days[x] == "3")
            daysString.push(this.translate.instant("monday"))
            else if(days[x] == "4")
            daysString.push(this.translate.instant("tuesday"))
            else if(days[x] == "5")
            daysString.push(this.translate.instant("Wednesday"))
            else if(days[x] == "6")
            daysString.push(this.translate.instant("Thursday"))
            else if(days[x] == "7")
            daysString.push(this.translate.instant("friday"))
          }
          this.jobdaysString = daysString.join(",")
        }
    
    
    
    
    
        this.lat = this.jobDetails.Latitude;
        this.lng = this.jobDetails.Longitude;
        
        this.service.getaddress(this.jobDetails.Latitude,this.jobDetails.Longitude).subscribe(
            resp=>{
              console.log("resp from get address1",resp);
               this.myLongAddress =  JSON.parse(JSON.stringify(resp)).results[0].formatted_address;
        },err=>{
          console.log("err from get address1",err);
          this.myLongAddress = "";
        })      
        this.initMap();
        }
      },err=>{
        console.log("err from getjobdetailsforcomp",err);
      });
    }
      

    if(this.from !="notification"){

    console.log("this.jobDetails"+ this.jobDetails)
    var a = []
  a = this.jobDetails.StartDate.split(/[^0-9]/);
  console.log("a : ",a)
  var d=new Date (a[0],a[1]-1,a[2],a[3],a[4],a[5] );
  console.log("d : ",new Date(d))
  this.jobDetails.Starttime=  this.datepipe.transform(new Date(d), 'h:m a')
    let startDat = new Date(this.jobDetails.StartDate)
    // let startDat = new Date(this.jobDetails.StartDate.toGMTString())
    // console.log("gmt start date : ",startDat)
    // let startDat2 = new Date(this.jobDetails.StartDate.toUTCString())
  //  console.log("utc start date : ",startDat2)
    //  this.jobDetails.StartDate = startDat.getDate()+"/"+(startDat.getMonth()+1) + "/" +startDat.getFullYear()+" "+startDat.getHours()+":"+startDat.getMinutes()
    this.jobDetails.StartDate2 = startDat.getDate()+"/"+(startDat.getMonth()+1) + "/" +startDat.getFullYear()
    // this.jobDetails.Starttime= startDat.getHours()+":"+startDat.getMinutes()
    // this.jobDetails.Starttime = this.datepipe.transform(startDat, 'h:m a')
    // let endDat = new Date(this.jobDetails.EndDate+".000Z")
    
    var a2 = []
    a2 = this.jobDetails.EndDate.split(/[^0-9]/);
    console.log("a : ",a2)
    var d2=new Date (a2[0],a2[1]-1,a2[2],a2[3],a2[4],a2[5] );
    console.log("d : ",new Date(d2))
    this.jobDetails.Endtime=  this.datepipe.transform(new Date(d2), 'h:m a')
    
    let endDat = new Date(this.jobDetails.EndDate)
    // this.jobDetails.EndDate = endDat.getDate()+"/"+(endDat.getMonth()+1) + "/" +endDat.getFullYear()+" "+endDat.getHours()+":"+endDat.getMinutes()
    this.jobDetails.EndDate2 = endDat.getDate()+"/"+(endDat.getMonth()+1) + "/" +endDat.getFullYear()
    // this.jobDetails.Endtime = endDat.getHours()+":"+endDat.getMinutes()
    // this.jobDetails.Endtime = this.datepipe.transform(endDat, 'h:m a')

    this.applied = this.navParams.get('applied')
    console.log("this.jobDetails",this.jobDetails);
    this.lat = this.jobDetails.Latitude;
    this.lng = this.jobDetails.Longitude;
    
    
    if(this.jobDetails.Gender == 0)
      this.jobDetails.jobGender = this.translate.instant("female");
    else if (this.jobDetails.Gender == 1)
      this.jobDetails.jobGender = this.translate.instant("male");
    else 
      this.jobDetails.jobGender = this.translate.instant("any2");

      console.log("this.jobDetails.CreationDate : ",this.jobDetails.CreationDate)
    var dt1 = new Date();
    var dt2 = new Date(this.jobDetails.CreationDate);
    console.log("current date dt1 : ",dt1," , job date dt2 : ",dt2)
    this.jobDetails.dateConversion=this.DateDiff(dt1, dt2);
      
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad JobDetailsPage');

    if(this.from !="notification"){
console.log("from  != notifications")
      this.lat = this.jobDetails.Latitude;
      this.lng = this.jobDetails.Longitude;
      
      this.service.getaddress(this.jobDetails.Latitude,this.jobDetails.Longitude).subscribe(
          resp=>{
            console.log("resp from get address1",resp);
             this.myLongAddress =  JSON.parse(JSON.stringify(resp)).results[0].formatted_address;
      },err=>{
        console.log("err from get address1",err);
        this.myLongAddress = "";
      })      
      this.initMap();

    }
   


  }
  dismiss(){
    this.navCtrl.pop();
  }
initMap(){

    console.log("init map");
    console.log("this.lat,this.lng: ",this.lat,this.lng);
    let latlng = new google.maps.LatLng(this.lat,this.lng);
    var mapOptions={
     center:latlng,
      zoom:15,
      streetViewControl:false,
      mapTypeId:google.maps.MapTypeId.ROADMAP,
  
    };
    this.map=  new google.maps.Map(this.mapElement.nativeElement,mapOptions);

    let marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: new google.maps.LatLng(this.lat, this.lng),
      icon: { 
        url : 'assets/icon/place.png',
        // size: new google.maps.Size(71, 71),
         scaledSize: new google.maps.Size(20, 25) 
      }     
    });
}
applyForJob(){
  this.storage.get('js_info').then((val) => {
    console.log("val from get js info",val);
    if(val)
    {
      console.log("val.SID ",val.SID);
      if(this.from == "apply"){

console.log("this.jobDetails.Gender  : ",this.jobDetails.Gender  ,"val.gender : ",val.gender )
this.service.getUserDetails(val.SID,resp=>{
  console.log("resp from getUserDetails",JSON.parse(resp));
  var respData = JSON.parse(resp)
  console.log("resp data : ",respData)
  if(respData.Result == "1"){
    console.log("result 1 : ",respData.Result,"respData.details.Gender : " ,respData.details.Gender)
    var mygender 
    // if(respData.details.Gender)
    if(respData.details)
      mygender = respData.details.Gender
    console.log("mygender : ",mygender)
      // if(this.gender == "1")
      //   this.gendertxt = this.translate.instant("male")
      // else if(this.gender == "0")
      //   this.gendertxt = this.translate.instant("female")

//     }

//   }
// },err=>{
//     console.log("err from getUserDetails : ",err);

//   });
        // val.gender: "1"
        // val.gender
        if(this.jobDetails.Gender == 0 && mygender == "1"){
        // {this.jobDetails.jobGender = this.translate.instant("female");
          this.helper.presentToast(this.translate.instant("youCantapplaygenderNotCompaiable"))
          return 
        }
    else if (this.jobDetails.Gender == 1 &&   mygender == "0"){
      // this.jobDetails.jobGender = this.translate.instant("male");
      this.helper.presentToast(this.translate.instant("youCantapplaygenderNotCompaiable"))
          return
    }
      
   


      this.service.jsApplayForJob(val.SID,this.jobDetails.SID,resp=>{
        console.log("jsApplayForJob resp",resp);
        var respData = JSON.parse(resp);
        if(respData.OperationResult == "1")
        {
          this.helper.presentToast(this.translate.instant("AppliedSuccessfully"));
          this.events.publish('refreshAppliedJobs');
          this.navCtrl.pop();
        }
        else if(respData.OperationResult == "-2"){
          this.helper.presentToast(this.translate.instant("AppliedBefore"));
          this.events.publish('refreshAppliedJobs');
          this.navCtrl.pop();
        }else
          this.helper.presentToast(this.translate.instant("AppliedError"));
          
      },err=>{
        console.log("jsApplayForJob err",err);
        
      });



    

  }
},err=>{
    console.log("err from getUserDetails : ",err);

  });


    }else{

      this.service.jsUnapply(val.SID,this.jobDetails.SID,resp=>{
        console.log("resp from jsUnapply ",resp);
        if(JSON.parse(resp).OperationResult == "1")
        {
          this.helper.presentToast(this.translate.instant("unappliedsu"))
          
        }
        this.events.publish('refreshAppliedJobs');
        this.navCtrl.pop();
          
      },err=>{
        console.log("err from jsUnapply ",err);
      });

    }


    }
    
     
  }).catch(err=>{
    console.log("catch from get js info",err);
    
  });


  
}
DateDiff(dt2, dt1) 
{
  var returnTime;
  console.log("dt2 : ",dt2)
  console.log("dt1 : ",dt1)
  var timeDiff = Math.abs(dt2.getTime() - dt1.getTime());
  var diffDays = Math.floor(timeDiff / (1000 * 3600 * 24)); 
  console.log("diff day",diffDays);

  if(diffDays <= 0)
  {
    var ss =Math.abs(Math.round((dt2.getTime() - dt1.getTime()) / 1000));
    var newss
    if (this.Platform.is('android')) {
      newss = ss-(2*60*60);
    }else  if (this.Platform.is('ios')) {
      newss = ss
    }
    // var newss = ss;
    console.log("ss: ",ss," newss: ",newss);

    // var h = Math.floor(ss/3600);
    // var m = Math.floor(ss % 3600 /60);
    // var s = Math.floor(ss % 3600 % 60);
    // console.log("ss h ", h,"m: ",m,"s: ",s);

    var h = Math.floor(newss/3600);
    var m = Math.floor(newss % 3600 /60);
    var s = Math.floor(newss % 3600 % 60);

    console.log("newss h ", h,"m: ",m,"s: ",s);  
    
    var hdisplay = h > 0 ? h + (h == 1 ? this.translate.instant("hour"):this.translate.instant("hours")):"";
    var mdisplay = m > 0 ? m + (m == 1 ? this.translate.instant("min"):this.translate.instant("mins")):"";
    if(h>0 && m >0)
    returnTime =hdisplay+this.translate.instant(",")+mdisplay;
    else if(h<=0)
    returnTime =mdisplay;
    else if (m<=0)
    returnTime = hdisplay;
    
    if (h<=0 && m<=0){
      returnTime = this.translate.instant("fewSeconds");
      console.log("else s time")
    }
    return returnTime;
      
  }else{
    if(diffDays == 1)
    returnTime = diffDays+this.translate.instant("day");
    else 
    returnTime = diffDays+this.translate.instant("days");


    // returnTime = diffDays+this.translate.instant("day");
    return returnTime;
  }
}

shareThisJob(){
  var sharetTxt ="";
  var shareLink;
    if (this.Platform.is('ios')) {

      // shareLink = "https://itunes.apple.com/us/app/id1445041257";
      shareLink =  "http://onelink.to/vc3mnz" ;

    } else {
      // shareLink = " https://play.google.com/store/apps/details?id=net.ITRoots.perHOURjobs";
      // shareLink = "http://perhour-sa.com/DownloadApp.html";
      shareLink =  "http://onelink.to/vc3mnz" ;
    }

var sal
//     if(this.jobDetails.SalaryType == '1'){

// if(this.helper.lang_direction == "rtl")
// sal = this.jobDetails.PricePerHour + "ريال فى الساعة"
// else
// sal = this.jobDetails.PricePerHour +"SAR Per Hour"
//     }else if(this.jobDetails.SalaryType == '3' || this.jobDetails.SalaryType == '2'){
//       if(this.helper.lang_direction == "rtl")
// sal = this.jobDetails.MonthlySalary + "ريال"
// else
// sal = this.jobDetails.MonthlySalary + "SAR"
//     }

   if(this.jobDetails.SalaryType == '1'){

      sal = this.jobDetails.PricePerHour 
    }else if(this.jobDetails.SalaryType == '3' || this.jobDetails.SalaryType == '2'){

      sal = this.jobDetails.MonthlySalary 
    }

 if(this.helper.lang_direction == "rtl")
 {
  sharetTxt = "Per Hour Jobs \n Job Title: "+ this.jobDetails.JobTitleAr +" \n Job Category: "+this.jobDetails.CategoryNameAr+" \n Job Description: " +this.jobDetails.JobDescription+"\n Working fees: "+sal+" ريال "+"\n Gender: "+this.jobDetails.jobGender+"\n Location: "+this.myLongAddress+"\n";
 } else{
  sharetTxt = "Per Hour Jobs \n المسمى الوظيفي: "+ this.jobDetails.JobTitleEn +" \n تصنيف الوظيفة: "+this.jobDetails.CategoryNameEn+" \n وصف الوظيفة: "+ this.jobDetails.JobDescription+"\n تكلفة العمل: "+sal+" SAR "+"\n الجنس: "+this.jobDetails.jobGender+"\n الموقع: "+this.myLongAddress+"\n";
 }
  console.log("shareTXT ",sharetTxt)
  this.socialSharing.share(sharetTxt, "Per Hour Jobs", null, shareLink).then(() => {
    

  }).catch(() => {
    console.log("not available");

  });

}

}
