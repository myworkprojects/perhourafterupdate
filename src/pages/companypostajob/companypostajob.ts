import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController ,Content,Events, PopoverController} from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { HelperProvider } from '../../providers/helper/helper';
import { ServicesProvider } from '../../providers/services/services';
import { Storage } from '@ionic/storage';
import { DatePicker } from '@ionic-native/date-picker';
import { DatePipe } from '@angular/common';
// import {Keyboard} from 'ionic-native';

declare var google:any

/**
 * Generated class for the CompanypostajobPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-companypostajob',
  templateUrl: 'companypostajob.html',
  providers: [DatePicker]
})
export class CompanypostajobPage {
  @ViewChild('map') mymap:ElementRef;
  @ViewChild(Content) content: Content;
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;

  langDirection = "";
  jobdesc:any
  job:any
  startdate:any
  startDateDisplay
  description:any
  reward:any
  places:any
  lat:any
  markers:any=[]
  long:any
  numberofhour:any
  salary:any
  enddate:any
  endDateDisplay
  map:any
  category:any
  jobs:any=[]
  categorys:any=[]
  type:any
  city:any
  comsid:any
  // date=new Date().toISOString();
  date
  scaleClass="";
  clickForLocation = false;
  jobTypeSelected = 1
  selectedDays:any
  selecteHours: any;
  SelectedDayStrings = ""
  SelectedDaysIDs = ""
  cities = []
  city_id: any;
  monthlySalary:any
  selecedJS

  cssClass = "classRemoveUpToShow";
  selectedStratHours
  selectedEndHours 

  natsarr
  natId
  locationdesc

  agefrom1
  ageto1

  currentTime

  myendDate;
myendTime;
// myendTime = new Date().getHours() + ":"+new Date().getMinutes()
myenddate


myDate;
// myTime="15:07"
myTime

// myTime = new Date().getHours() + ":"+new Date().getMinutes()
mystartdate


  constructor(public datepipe: DatePipe,public events: Events,public ViewCtrl:ViewController,public storage:Storage,public popoverCtrl: PopoverController,
    public service:ServicesProvider,public helper:HelperProvider,public translate:TranslateService,
    public navCtrl: NavController,private datePicker: DatePicker, public navParams: NavParams) {
    this.langDirection = this.helper.lang_direction;
    if(this.langDirection == "rtl")
    this.scaleClass="scaleClass";

    this.cssClass = "classRemoveUpToShow";
    this.date=new Date().toISOString();
    this.currentTime = new Date()

    this.myendTime = new Date().getHours() + ":"+new Date().getMinutes()
console.log(" this.myendTime : ", this.myendTime)
this.myTime = new Date().getHours() + ":"+new Date().getMinutes()
console.log("this.myTime : ",this.myTime)
    // toLocaleString(); 
    //2019-02-19T12:31:50.551Z
    // "2/19/2019, 2:38:40 PM"
    // var part1 = this.date.split(",")[0]
    // var part2 = this.date.split(",")[1]
  

    // console.log("date x : ",this.date)
    // var endDat = new Date();
    // var newdate = endDat.getFullYear()+"-"+(endDat.getMonth()+1) + "-" +endDat.getDate()+"T"+endDat.getHours()+":"+endDat.getMinutes()+".000Z"
    // console.log("newdate : ",newdate)
    //var a = []
    //a = this.date.split(/[^0-9]/);
   // console.log("a : ",a)
    //var d=new Date (a[0],a[1]-1,a[2],a[3],a[4],a[5] );
    //console.log("d date : ",new Date(d))
    // this.jobDetails.Starttime=  this.datepipe.transform(new Date(d), 'h:m a')
   
    //   let startDat = new Date(this.jobDetails.StartDate)
    //   console.log(" : ",startDat)
      
  
    //   this.jobDetails.StartDate = startDat.getDate()+"/"+(startDat.getMonth()+1) + "/" +startDat.getFullYear()
      console.log("data to check hours difference: ",this.date)//"2019-02-19T11:29:45.000Z"
  }
  scrollToBottom(): void {
    // method used to enable scrolling
    this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
}

// ngAfterViewChecked() {
//   console.log("ngAfterViewChecked")
//   this.scrollToBottom();
// }

  scrollup(){
    console.log("scrollup");
    this.cssClass = "classUpToShow";
    // this.scrollToBottom();
    // if(this.cssClass == "classRemoveUpToShow")
    // this.cssClass = "classUpToShow";
    // else  if(this.cssClass == "classUpToShow")
    // this.cssClass = "classRemoveUpToShow";
    // this.content.scrollTo(1,,3000);
    // console.log("ev.target.scrollup: ",ev.target);
  }
  ionViewDidLoad() {
    if(navigator.onLine){
      this.service.listCities((data)=>{
        data = JSON.parse(data)
        this.cities = data
        console.log(data)

      },
      ()=> this.helper.presentToast(this.translate.instant("ServerError")))
      
      this.service.listNationalities(0,resp=>{
        console.log("resp from listNationalities : ",resp);
        
        this.natsarr = JSON.parse(resp);
      },err=>{
        console.log("err from listNationalities : ",err);
      })

    }
    else{
      this.helper.presentToast(this.translate.instant("InternetErr"))
    }
    this.langDirection = this.helper.lang_direction;
    console.log("before call detectUserLoctaion");
   // this.helper.detectUserLoctaion(); 
   this.helper.geoLoc(()=>{})
    console.log("after call detectUserLoctaion"); 
    this.events.subscribe('detectUserLocation', () => {
      console.log("suscribe detectUserLocation")
      this.initMap();      
    });

    this.lat = this.helper.lat;
    this.long = this.helper.lng;

     this.storage.get('js_info').then((val)=>{
       this.comsid=val.SID
       console.log(this.comsid)

     });
     
    this.service.companylistjobcategories((data)=>{
      let DataParse = JSON.parse(data);
      this.categorys=DataParse
    console.log(JSON.stringify(DataParse))
    },(data)=>{
 
    })
this.initMap();
    console.log('ionViewDidLoad CompanyprofilePage');
    console.log('ionViewDidLoad CompanypostajobPage');
  }
  selectionTypeChanged(){

  }
  chooseDate(type) {

    var minmDate;
    var maxDate;
    let userLang = this.helper.lang_direction;
    // let x = new Date()
    // let d = x.getFullYear() + '-' + (x.getMonth() + 1) + '-' + (x.geminDatetDate()) + " 00:00:00.000z"
    // let v = (x.getFullYear() + 1) + '-' + (x.getMonth() + 1) + '-' + (x.getDate() + 1) + "T00:00:00.000z"
    // if (this.plt.is('ios')) {
    //   minmDate = new Date().toISOString()
    //   //maxDate = new Date(v).toISOString()
    // }
    // else {
      minmDate = new Date().valueOf()
      //maxDate = new Date(v).valueOf()
    //}
    console.log("userLang: ",userLang," helper.lang :",this.helper.lang_direction)
    let localLang = 'en_us';
    let nowTxt = 'Today';
    let okTxt = 'Done';
    let cancelTxt = 'Cancel';
    if (userLang == 'rtl') {
      localLang = 'ar_eg';
      nowTxt = 'اليوم';
      okTxt = 'تم';
      cancelTxt = 'إلغاء'
    }

    console.log("localLang : ",localLang)

    this.datePicker.show({
      date: new Date(),
      mode: 'datetime',
      minDate: minmDate,
      okText: okTxt,
      cancelText: cancelTxt,
      todayText: nowTxt,
       locale: localLang,
      // locale: 'en_us',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_LIGHT
    }).then(
      date => {
        console.log("date from datepic : ",date)
       if(type == 1){
         this.startdate = date.toISOString()
        //  this.startDateDisplay = date.getFullYear() + '-' + (date.getMonth()+1) + '-' + date.getDate() + " " + date.getHours() + ":" + date.getMinutes()
         this.startDateDisplay = date.getFullYear() + '-' + (date.getMonth()+1) + '-' + date.getDate() + " " +  this.datepipe.transform(new Date(this.startdate) , 'h:m a')
       }
       else{
         this.enddate = date.toISOString()
       //  this.endDateDisplay = date.getFullYear() + '-' + (date.getMonth()+1) + '-' + date.getDate() + " " + date.getHours() + ":" + date.getMinutes()
         this.endDateDisplay = date.getFullYear() + '-' + (date.getMonth()+1) + '-' + date.getDate() + " " + this.datepipe.transform(new Date(this.enddate) , 'h:m a')
       }
        },
      err => {
        console.log('Error occurred while getting date: ', err);

      }
    );
  }
  // dismiss(){
  //   this.navCtrl.pop();
  // }

  locateUser(){
    //this.helper.detectUserLoctaion(); 
    this.helper.geoLoc(()=>{})
  }
  initMap(){
    let page=this
    const locations =new google.maps.LatLng(this.helper.lat,this.helper.lng)
    let mapOptions = {
        center:locations,
        zoom:15,//8
        fullscreenControl:false,
        streetViewControl:false
        // controls: {
        //   myLocationButton: true         
        // }
        // mapTypeControl: true,
        //   mapTypeControlOptions: {
        //     style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
        //     mapTypeIds: ['roadmap', 'terrain']
        //   }
        };
        this. map =new google.maps.Map(this.mymap.nativeElement,mapOptions) 
        var marker = new google.maps.Marker({
          position: locations,
          draggable:true,
          map: this.map
        });
        this.markers.push(marker)
        google.maps.event.addListener(this.map, 'click', function(event) {
          page.lat=event.latLng.lat()
          page.long=event.latLng.lng()
          page.clickForLocation = true;
          console.log("this.clickForLocation:",this.clickForLocation)
console.log("lat lng: ",page.lat,page.long);
          for(var i=0;i<page.markers.length;i++)
          {
            page.markers[i].setMap(null);

          }

          page.placeMarker(event.latLng);
      });

  }

  choosejob(){
    console.log("from choose job",this.category);
    if(!this.category)
    this.helper.presentToast(this.translate.instant("chooseCatFirst"))
  }
getjob()
{
  console.log(this.category)
 

 
  this.service.companylistjobtitles(this.category,(data)=>{
    let DataParse = JSON.parse(data);
    if(DataParse.length == 0)
      // this.helper.presentToast(this.translate.instant("noDataincat"));
      this.helper.presentToast(this.translate.instant("contactAdmin"));
    else
    this.jobs=DataParse
  console.log(JSON.stringify(DataParse))
  },(data)=>{

  })
  
}
publish()
{
  // var type1LimitSalary = 15
  // var type2LimitSalary = 800
  // var type3LimitSalary = 3000
  var type1LimitSalary 
  var type2LimitSalary 
  var type3LimitSalary 
  this.service.getMinumSalaries(resp=>{
    console.log("resp from getminumsalaries: ",resp)
    var respData = JSON.parse(resp)
    type1LimitSalary = respData.PerHourMin
    type2LimitSalary = respData.PerMonthMin
    type3LimitSalary = respData.PerYearMin

  // },err=>{
  //   console.log("err from getminumsalaries: ",err)
  // })
  console.log(this.startdate)
  console.log(this.enddate)
  this.startdate = this.mystartdate + " "+ this.myTime ;
this.enddate = this.myenddate + " "+ this.myendTime
//   myendDate;
// myendTime
// myenddate
console.log("after all start =  :",this.startdate, " ,end: ",this.enddate)
// var current2=Date.parse(new Date().toString());
// console.log("current : ",current2)
// var JobStartTime=Date.parse(this.startdate);
// console.log("JobStartTime : ",JobStartTime)
// if(current2 > JobStartTime){
//   console.log("current2 > started")
//   this.helper.presentToast(this.translate.instant("startTimeGreaterThanCurrent"))
//   return
// }

// else if(JobStartTime > current2){
//   console.log("pass job started > current")
// }


  // console.log("publish :  SelectedDaysIDs : ",this.SelectedDaysIDs  , "numberofhour : " ,this.numberofhour)
  //var newdata = {InvitedJS:this.selecedJS,City:this.city_id,SalaryType:this.jobTypeSelected,WorkingDays:this.SelectedDaysIDs,WorkingHoursStart:this.selectedStratHours,WorkingHoursEnd:this.selectedEndHours};                    
  //console.log("newdata: ",newdata);
  //console.log("this.startdate.split('T')[0] : ",this.startdate.split("T")[0]);
     // console.log("this.enddate.split('T')[0] : ",this.enddate.split("T")[0]);
    //  console.log("this.startdate.split(T)[0]>this.enddate.split(T)[0]: ",this.startdate.split("T")[0]>this.enddate.split("T")[0])
  // this.clickForLocation == true&&
  
  if(this.job && this.category && this.description &&this.startdate&&this.enddate&&this.type&& this.places &&this.places != "0" && this.startdate.split(" ")[0]<=this.enddate.split(" ")[0] && this.city_id){
    console.log("if 1")


    if(! this.natId)
    {
      this.helper.presentToast(this.translate.instant("enternat"))
      return
    }

   

    console.log("jobTypeSelected  : ",this.jobTypeSelected )
    if(this.jobTypeSelected == 1 && ! this.salary){
       
      this.helper.presentToast(this.translate.instant("nopriceperhour"))
      return
    }
    if(this.jobTypeSelected == 2 && ! this.monthlySalary){
       
      this.helper.presentToast(this.translate.instant("nopricepermonth"))
      return
    }
    if(this.jobTypeSelected == 3 && ! this.monthlySalary){
       
      this.helper.presentToast(this.translate.instant("nopriceperyear"))
      return
    }

    if(this.jobTypeSelected == 1 && this.salary < type1LimitSalary){
      // this.helper.presentToast(this.translate.instant("salarymustntlessthanzero"))
      
      this.helper.presentToast(this.translate.instant("salarymustntlessthan")+" "+type1LimitSalary+" "+this.translate.instant("sar"))
      return
    }
    // if(this.jobTypeSelected == 1 && this.salary <= 0){
    //   this.helper.presentToast(this.translate.instant("salarymustntlessthanzero"))
    //   return
    // }
      //month,<=0
    if(this.jobTypeSelected == 2 && this.monthlySalary < type2LimitSalary){
      // this.helper.presentToast(this.translate.instant("salarymustntlessthan800"))
      this.helper.presentToast(this.translate.instant("salarymustntlessthan")+" "+type2LimitSalary+" "+this.translate.instant("sar"))
      return
    }
  
//year
    if(this.jobTypeSelected == 3 && this.monthlySalary < type3LimitSalary){
      // this.helper.presentToast(this.translate.instant("salarymustntlessthan3000"))
      this.helper.presentToast(this.translate.instant("salarymustntlessthan")+" "+type3LimitSalary+" "+this.translate.instant("sar"))
      return
    }

  
if(this.startdate == "undefined undefined" )
  {
    this.helper.presentToast(this.translate.instant("noostartdate"))
    
    
    return
  }

  if(this.enddate == "undefined undefined" )
  {
    this.helper.presentToast(this.translate.instant("noenddate"))
    return
  }

    console.log("this.startdate : ",this.startdate ,"new Date(this.startdate) : ",new Date(this.startdate.split(" ")[0]+ "T"+this.startdate.split(" ")[1]), " : startdate.getTime() : ",new Date(this.startdate.split(" ")[0] + "T"+this.startdate.split(" ")[1]).getTime())
    console.log("this.enddate : ",this.enddate ,"new Date(this.enddate) : ",new Date(this.enddate.split(" ")[0]+ "T"+this.enddate.split(" ")[1]), " : enddate.getTime() : ",new Date(this.enddate.split(" ")[0]+ "T"+this.enddate.split(" ")[1]).getTime())
    var ss =Math.abs(Math.round((new Date(this.startdate.split(" ")[0] + "T"+this.startdate.split(" ")[1]).getTime() - new Date(this.enddate.split(" ")[0] + "T"+this.enddate.split(" ")[1]).getTime()) / 1000));
     
        var h = Math.floor(ss/3600);
//     var timeStart = new Date(this.startdate.split(" ")[0]+ this.startdate.split(" ")[1]).getHours();
// var timeEnd = new Date(this.enddate.split(" ")[0]+"T" +this.enddate.split(" ")[1]).getHours();

// var h =  this.startdate.split(" ")[1] - this.enddate.split(" ")[1]; 

        console.log("h = ",h , " : this.numberofhour = ",this.numberofhour)


        // && this.startdate.split(" ")[0] == this.enddate.split(" ")[0] 
    if(this.jobTypeSelected == 1 &&  h >= this.numberofhour){
      //&& h >= this.numberofhour){ //time , hours 



    // if(this.jobTypeSelected == 1 && (!this.salary || !this.numberofhour)){
    //   console.log("if 2")
    //   if (!this.salary)
    //   this.helper.presentToast(this.translate.instant("nopriceperhour"))
    //   else if (!this.numberofhour)
    //   this.helper.presentToast(this.translate.instant("nonumberofhour"))
    //   return
    // }
    // if(this.jobTypeSelected == 2 && (!this.SelectedDaysIDs )){//|| !this.numberofhour
    //   console.log("if 3")
    //   if(!this.SelectedDaysIDs){
    //     this.helper.presentToast(this.translate.instant("SelectedDaysIDsErr"))
    //     console.log("if 4")
    //   }
      
    //   else if(!this.selecteHours)
    //   {
    //     this.helper.presentToast(this.translate.instant("selecteHoursErr"))
    //     console.log("if 5")
    //   }
    //   else if(!this.SelectedDaysIDs)
    //   {
    //     this.helper.presentToast(this.translate.instant("SelectedDaysIDsErr"))
    //     console.log("if 6")
    //   }

    //   return
    // }



    // if(!this.agefrom1 )
    // {
    //   this.helper.presentToast(this.translate.instant("enterAgeFrom"))
    //   return
    // }else if(!this.ageto1 )
    // {
    //   this.helper.presentToast(this.translate.instant("enterAgeto"))
    //   return
    // }else 
  
    if (this.agefrom1 <16){
      this.helper.presentToast(this.translate.instant("agelessthan16"))
      return
    }else if (this.agefrom1 && !this.ageto1){
      this.helper.presentToast(this.translate.instant("enterAgeto"))
      return
    }else if (this.ageto1 && !this.agefrom1){
      this.helper.presentToast(this.translate.instant("enterAgeFrom"))
      return
    }
    else if(this.agefrom1 > this.ageto1){
      this.helper.presentToast(this.translate.instant("ageFromGraterThanAgeTo"))
      return
    }

    var current2=Date.parse(new Date().toString());
    console.log("current : ",current2)
    var JobStartTime=Date.parse(this.startdate);
    console.log("JobStartTime : ",JobStartTime)
    if(current2 > JobStartTime){
      console.log("current2 > started")
      this.helper.presentToast(this.translate.instant("startTimeGreaterThanCurrent"))
      return
    }
       // int City 
                                          // public string WorkingDays
                                          // string WorkingHoursStart 
                                          // string WorkingHoursEnd 
                                          // string InvitedJS 
                                          // int SalaryType  // 1: By Hour     2: By Month
var newdata = {ageFrom:this.agefrom1,ageTo:this.ageto1, AddressDescription:this.locationdesc,Nationality:this.natId,InvitedJS:this.selecedJS,City:this.city_id,SalaryType:this.jobTypeSelected,WorkingDays:this.SelectedDaysIDs,WorkingHoursStart:this.selectedStratHours,WorkingHoursEnd:this.selectedEndHours,monthsalary:this.monthlySalary};                    
// parseFloat(this.helper.parseArabic(this.numberofhour))
// parseFloat(this.helper.parseArabic(this.salary))
// parseInt(this.helper.parseArabic(this.places))
this.service.companysavejob(newdata,this.comsid,this.category,this.job,this.description,this.salary,this.numberofhour,this.startdate,this.enddate,this.type,this.lat,this.long,this.places,this.reward,(data)=>{


  console.log("data from resp post job: ",data);
  if(JSON.parse(data).result == "-2")
  {
    this.helper.presentToast2(this.translate.instant("noBalance"));
    this.navCtrl.pop();
  }else if(JSON.parse(data).result == "-1000"){
    this.helper.presentToast(this.translate.instant("startmorethanend"));
  }
  else
  {
    this.helper.presentToast(this.translate.instant('jobposted'))
    this.navCtrl.pop();
  }
    
// this.startdate=""
// this.enddate=""
// this.description=""
// this.job=""
// this.city=""
// this.category=""
// this.salary=""
// this.numberofhour=""
// this.type=""
// this.lat="31.98832728"
// this.long="36.8827291"
// this.reward=""
// this.places=""


// this.service.companylistjobcategories((data)=>{
//   let DataParse = JSON.parse(data);
//   this.categorys=DataParse
// console.log(JSON.stringify(DataParse))
// },(data)=>{

// })

},(data)=>{


})


  } // end of check time and hour
else{
  console.log("h != no hours")
  if(this.jobTypeSelected == 1)
  this.helper.presentToast(this.translate.instant("startEndHourDifference"))
  else{

    // if(!this.agefrom1 )
    // {
    //   this.helper.presentToast(this.translate.instant("enterAgeFrom"))
    //   return
    // }else  if(!this.ageto1 )
    // {
    //   this.helper.presentToast(this.translate.instant("enterAgeto"))
    //   return
    // }else
    
    if (this.agefrom1 <16){
      this.helper.presentToast(this.translate.instant("agelessthan16"))
      return
    }else if (this.agefrom1 && !this.ageto1){
      this.helper.presentToast(this.translate.instant("enterAgeto"))
      return
    }else if (this.ageto1 && !this.agefrom1){
      this.helper.presentToast(this.translate.instant("enterAgeFrom"))
      return
    }
    else if(this.agefrom1 > this.ageto1){
      this.helper.presentToast(this.translate.instant("ageFromGraterThanAgeTo"))
      return
    }


    var current2=Date.parse(new Date().toString());
console.log("current : ",current2)
var JobStartTime=Date.parse(this.startdate);
console.log("JobStartTime : ",JobStartTime)
if(current2 > JobStartTime){
  console.log("current2 > started")
  this.helper.presentToast(this.translate.instant("startTimeGreaterThanCurrent"))
  return
}



    var newdata = {ageFrom:this.agefrom1,ageTo:this.ageto1,AddressDescription:this.locationdesc,Nationality:this.natId,InvitedJS:this.selecedJS,City:this.city_id,SalaryType:this.jobTypeSelected,WorkingDays:this.SelectedDaysIDs,WorkingHoursStart:this.selectedStratHours,WorkingHoursEnd:this.selectedEndHours,monthsalary:this.monthlySalary};                    

this.service.companysavejob(newdata,this.comsid,this.category,this.job,this.description,this.salary,this.numberofhour,this.startdate,this.enddate,this.type,this.lat,this.long,this.places,this.reward,(data)=>{


  console.log("data from resp post job: ",data);
  if(JSON.parse(data).result == "-2")
  {
    this.helper.presentToast2(this.translate.instant("noBalance"));
    this.navCtrl.pop();
  }else if(JSON.parse(data).result == "-1000"){
    this.helper.presentToast(this.translate.instant("startmorethanend"));
  }
  else
  {
    this.helper.presentToast(this.translate.instant('jobposted'))
    this.navCtrl.pop();
  }
    


},(data)=>{


})

  }
}


  }else
  {
    console.log("else 1")
     // this.startdate = this.mystartdate + " "+ this.myTime ;
    // this.enddate = this.myenddate + " "+ this.myendTime

    console.log("mystartdate : ",this.mystartdate," , myTime :",this.myTime)
    console.log("myenddate : ",this.myenddate," , myendTime :",this.myendTime)
  


      
    
    if (! this.category){
      this.helper.presentToast(this.translate.instant("nocategory"))
      return
    }
    
    else if(! this.job){
      this.helper.presentToast(this.translate.instant("nojob"))
      return
    }
    
    else if (! this.description){
      this.helper.presentToast(this.translate.instant("nodescription"))
      return
    }
    

else if ( ! this.mystartdate ){
  this.helper.presentToast(this.translate.instant("noostartdate"))
  return
}

else if (!this.myTime){
  this.helper.presentToast(this.translate.instant("noostarttime"))
  return
}

else if ( !this.myenddate){
  this.helper.presentToast(this.translate.instant("noenddate"))
  return
}

else if (!this.myendTime){
  this.helper.presentToast(this.translate.instant("noendtime"))
  return
}
else if (!this.city_id){
  this.helper.presentToast(this.translate.instant("nocity"))
  return
}

    else if(this.jobTypeSelected == 1 && (!this.salary || !this.numberofhour)){
      if (!this.salary)
      this.helper.presentToast(this.translate.instant("nopriceperhour"))
      else if (!this.numberofhour)
      this.helper.presentToast(this.translate.instant("nonumberofhour"))
      return
    }
    else if(this.jobTypeSelected == 2 && (!this.SelectedDaysIDs || !this.numberofhour)){
      if(!this.SelectedDaysIDs)
      this.helper.presentToast(this.translate.instant("SelectedDaysIDsErr"))
      else if(!this.selecteHours)
      this.helper.presentToast(this.translate.instant("selecteHoursErr"))
      else if(!this.SelectedDaysIDs)
      this.helper.presentToast(this.translate.instant("SelectedDaysIDsErr"))
      return
    } else   if(this.jobTypeSelected == 3 && (!this.SelectedDaysIDs || !this.numberofhour)){
      if(!this.SelectedDaysIDs)
      this.helper.presentToast(this.translate.instant("SelectedDaysIDsErr"))
      else if(!this.selecteHours)
      this.helper.presentToast(this.translate.instant("selecteHoursErr"))
      else if(!this.SelectedDaysIDs)
      this.helper.presentToast(this.translate.instant("SelectedDaysIDsErr"))
      return
    }
    

    else if (! this.startdate)
    this.helper.presentToast(this.translate.instant("noostartdate"))
    else if (! this.enddate)
    this.helper.presentToast(this.translate.instant("noenddate"))
    else if(this.startdate.split("T")[0] > this.enddate.split("T")[0])
    {
      console.log("this.startdate.split('T')[0] : ",this.startdate.split("T")[0]);
      console.log("this.enddate.split('T')[0] : ",this.enddate.split("T")[0]);
      this.helper.presentToast(this.translate.instant("startmorethanend"));
    }
    else if (! this.type)
    this.helper.presentToast(this.translate.instant("nogender"))
    else if (this.places == "0" ||  ! this.places)
    this.helper.presentToast(this.translate.instant("noavailabelplaces"))



    // this.startdate = this.mystartdate + " "+ this.myTime ;
    // this.enddate = this.myenddate + " "+ this.myendTime


    // else if (this.clickForLocation == false)
    // this.helper.presentToast(this.translate.instant("nolocation"))
  }
    
},err=>{
  console.log("err from getminumsalaries: ",err)
  this.helper.presentToast(this.translate.instant("ServerError"))
})

}
openmodal()
{
  
}
placeMarker(location) {

  var marker;
 
 if (marker == undefined){
     marker = new google.maps.Marker({
         position: location,
         map: this.map, 
         animation: google.maps.Animation.DROP,
     });
     this.markers.push(marker)
 }
 else{
   this.markers.push(marker)
     marker.setPosition(location);
 }
 
 this.map.setCenter(location);

}
daysPopover(event) {
  let popover = this.popoverCtrl.create('DaysPopoverPage',{days:this.SelectedDaysIDs});
  popover.present({
    ev: event
  });
  popover.onDidDismiss(data => {
    console.log(data);
    if (data != null) {
      this.selectedDays = data
      let DaysIDs = []
      let DaysStrings = []
      for(let x=0;x<data.length;x++){
        DaysStrings.push(data[x].day)
        DaysIDs.push(data[x].value)
      }
      this.SelectedDayStrings = DaysStrings.toString()
      this.SelectedDaysIDs = DaysIDs.toString()
      console.log("this.SelectedDaysIDs : ",this.SelectedDaysIDs)
    }
  })

}
SelectedhourString="";
hoursPopover(event) {
  let popover = this.popoverCtrl.create('HoursPopoverPage',{from:this.selectedStratHours,to:this.selectedEndHours},{cssClass:"hourPopover"});
  popover.present({
    ev: event 
  });
  popover.onDidDismiss(data => {
    console.log(data);
    if (data != null) {
      this.selecteHours =   data.from + " , " + data.to // : 
      this.selectedStratHours = data.from
      this.selectedEndHours = data.to
    }
  this.SelectedhourString = this.selecteHours.toString();
    console.log("this.selecteHours: ",this.selecteHours);
  })
}
inviteFriend(){
  let popover = this.popoverCtrl.create('InvitePopoverPage',{},{cssClass:"invitePopover"});
  popover.present({
    ev: event 
  });
  popover.onDidDismiss(data => {
    console.log(data);
    if (data != null) {
      // this.selecteHours = data
      this.selecedJS = data
     
    }
    console.log("this.selecedJS: ",this.selecedJS);
  })
}
dismiss(){
  // this.navCtrl.parent.select(0);
  this.navCtrl.pop();
}



change(datePicker){
  console.log("date",this.myDate);
  console.log("datePicker",datePicker);
 
  datePicker.open();
  
}
changeTime(){
  console.log("myDate : ",this.myDate)
  console.log("myTime : ",this.myTime)
  this.mystartdate = this.myDate;
  this.myDate = ""
}



changeend(datePicker){
  console.log("myendDate",this.myendDate);
  console.log("end datePicker",datePicker);
 
  datePicker.open();
  
}
changeTimeend(){
  console.log("myendDate : ",this.myendDate)
  console.log("myendTime : ",this.myendTime)
  this.myenddate = this.myendDate;
  this.myendDate = ""
}

numberofhourinput(){
  console.log("numberofhourinput")
  this.numberofhour = this.textArabicNumbersReplacment(this.numberofhour)
}
placesinput(){
  console.log("placesinput")
  this.places = this.textArabicNumbersReplacment(this.places)
}

salaryinput(){
  console.log("salaryinput")
  this.salary = this.textArabicNumbersReplacment(this.salary)
}

monthsalaryinput(){
  console.log("monthsalaryinput")
  this.monthlySalary = this.textArabicNumbersReplacment(this.monthlySalary)
}
ageFromFunc(){
this.agefrom1 = this.textArabicNumbersReplacment(this.agefrom1)
}
ageTOFunc(){
this.ageto1 = this.textArabicNumbersReplacment(this.ageto1)
}

textArabicNumbersReplacment(strText) {
  var strTextFiltered = strText.trim();
  strTextFiltered = strText;
  strTextFiltered = strTextFiltered.replace(/[\٩]/g, '9');
  strTextFiltered = strTextFiltered.replace(/[\٨]/g, '8');
  strTextFiltered = strTextFiltered.replace(/[\٧]/g, '7');
  strTextFiltered = strTextFiltered.replace(/[\٦]/g, '6');
  strTextFiltered = strTextFiltered.replace(/[\٥]/g, '5');
  strTextFiltered = strTextFiltered.replace(/[\٤]/g, '4');
  strTextFiltered = strTextFiltered.replace(/[\٣]/g, '3');
  strTextFiltered = strTextFiltered.replace(/[\٢]/g, '2');
  strTextFiltered = strTextFiltered.replace(/[\١]/g, '1');
  strTextFiltered = strTextFiltered.replace(/[\٠]/g, '0');
  return strTextFiltered;
}

}
