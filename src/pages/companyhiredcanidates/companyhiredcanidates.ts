import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';

/**
 * Generated class for the CompanyhiredcanidatesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-companyhiredcanidates',
  templateUrl: 'companyhiredcanidates.html',
})
export class CompanyhiredcanidatesPage {
  langDirection:any
  constructor(public ViewCtrl:ViewController,public helper:HelperProvider,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.langDirection = this.helper.lang_direction;

    console.log('ionViewDidLoad CompanyhiredcanidatesPage');
  }

}
