import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CompanyhiredcanidatesPage } from './companyhiredcanidates';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';

@NgModule({
  declarations: [
    CompanyhiredcanidatesPage,
  ],
  imports: [
    IonicPageModule.forChild(CompanyhiredcanidatesPage),
    TranslateModule.forChild()
  ],
})
export class CompanyhiredcanidatesPageModule {}
