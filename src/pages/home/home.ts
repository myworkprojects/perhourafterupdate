import { Component } from '@angular/core';
import { NavController ,Platform,Events} from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
import { ServicesProvider } from '../../providers/services/services';
import { TranslateService } from '@ngx-translate/core';
import { CompanypostajobPage } from '../companypostajob/companypostajob';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  langDirection = "";
  hideClass="";
  applyClass ="";
  morertl="";
  newJobsList=[];

  tmpHidden = false;
  refresher;
  jsid;
  latestApplied;
  recommendedJobs
        latestnodata
        
  hideRecommendedTitle

  constructor(public  events:Events, public Platform: Platform,public storage:Storage, public translate: TranslateService,public service:ServicesProvider, public helper:HelperProvider,public navCtrl: NavController) {

    this.tmpHidden = false;
    this.latestnodata = true;

    this.langDirection = this.helper.lang_direction;
    if(this.langDirection == "rtl")
    {
      this.hideClass="hidertl";
      this.applyClass = "applyrtl";
      this.morertl ="left";

    }else{
      this.hideClass="hideltr";
      this.applyClass = "applyltr";
      this.morertl ="right";
    }

  
    this.events.subscribe('refreshAppliedJobs', () => {
console.log("su refreshAppliedJobs ")
this.service.jsgetLatestJobs2(this.jsid,resp=>{
  console.log("su resp from jsgetLatestJobs",resp);
  if(JSON.parse(resp).result == "1"){
    this.latestnodata = true;
    console.log("su JSON.parse(resp).jobsList : ",JSON.parse(resp).jobsList)
    console.log("su JSON.parse(resp).jobsList.slice(0,3) : ",JSON.parse(resp).jobsList.slice(0,3))
    this.latestApplied = JSON.parse(resp).jobsList.slice(0,3);
  }
  else{
    console.log("su else getJsAppliedJobs")
    this.latestApplied = [];
    this.latestnodata = false;
  }

    
if(this.refresher){
this.refresher.complete();
}

},err=>{
  console.log("err from jsgetLatestJobs",err);

if(this.refresher){
this.refresher.complete();
}

})
    });

  }

  ionViewDidLoad(){
    console.log("ionViewDidLoad");
    // this.getLatestRecommendedJobs();
    this.hideRecommendedTitle = true;

    this.service.jsListNewJobs(resp=>{
      console.log("resp from new jobs",resp);
      this.newJobsList= JSON.parse(resp);
      console.log("this.newJobsList",this.newJobsList);
      for(var j=0;j<this.newJobsList.length;j++)
      {
        var dt1 = new Date();
        var dt2 = new Date(this.newJobsList[j].CreationDate);//"2018-11-16T12:50:53.466Z"
        // console.log("dateDiff: ",this.DateDiff(dt1, dt2));
        console.log("this.newJobsList[j].SID",this.newJobsList[j].SID);
        this.newJobsList[j].dateConversion=this.DateDiff(dt1, dt2);
      }

      this.getLatestRecommendedJobs();
      
    },err=>{
      console.log("err from new jobs",err);
      this.getLatestRecommendedJobs();
    });

  }
  ionViewDidEnter(){
    console.log("ionViewWillEnter");
    // this.getLatestRecommendedJobs();

    // this.service.jsListNewJobs(resp=>{
    //   console.log("resp from new jobs",resp);
    //   this.newJobsList= JSON.parse(resp);
    //   console.log("this.newJobsList",this.newJobsList);
    //   for(var j=0;j<this.newJobsList.length;j++)
    //   {
    //     var dt1 = new Date();
    //     var dt2 = new Date(this.newJobsList[j].CreationDate);//"2018-11-16T12:50:53.466Z"
    //     // console.log("dateDiff: ",this.DateDiff(dt1, dt2));
    //     console.log("this.newJobsList[j].SID",this.newJobsList[j].SID);
    //     this.newJobsList[j].dateConversion=this.DateDiff(dt1, dt2);
    //   }
      
    // },err=>{
    //   console.log("err from new jobs",err);
    // });

//    this.service.jsgetLatestJobs2(this.jsid,resp=>{
//     console.log("enter resp from jsgetLatestJobs",resp);
//     if(JSON.parse(resp).result == "1"){
//       this.latestnodata = true;
//       console.log("enter JSON.parse(resp).jobsList : ",JSON.parse(resp).jobsList)
//       console.log("enter JSON.parse(resp).jobsList.slice(0,3) : ",JSON.parse(resp).jobsList.slice(0,3))
//       this.latestApplied = JSON.parse(resp).jobsList.slice(0,3);
//     }
//     else{
//       console.log("enter else getJsAppliedJobs")
//       this.latestApplied = [];
//       this.latestnodata = false;
//     }

      
// if(this.refresher){
//   this.refresher.complete();
// }

//   },err=>{
//     console.log("err from jsgetLatestJobs",err);

// if(this.refresher){
//   this.refresher.complete();
// }

//   })

  }
  applayforJob(item){
    console.log("applayforJob :",item)
    this.navCtrl.push('JobDetailsPage',{'jobDetails':item,'from':"apply"});
  }

  activejobs(){
    this.navCtrl.push('ActiveJobsPage');
  }
  study(){
    this.navCtrl.push('StudyPage');
  }
  experience(){
    this.navCtrl.push('ExperiencePage');
  }
  CandidateDetails(){
    this.navCtrl.push('CandidateDetailsPage',{data:{"JobSeekerSID":1025,"FirstName":"Ali","ShortDescription":"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxyyyyyyyyyyyyyyy","ProfileImage":"defualt.jpg","Mobile":"0123164564564","DateOfBirth":"1988-01-01T00:00:00",jobid:1}});
  }
 
  activationCode(){
    this.navCtrl.setRoot('ActivationCodePage',{"JsSID":"1025",type:"js"}); 

  }
  CompanyCheckInNotificationPage2(){
    this.navCtrl.push('CompanyCheckInNotificationPage',{notificationType:"cancel",jsid:1047,jobId:1});
  }
  openPosteaJob(){
    this.navCtrl.push(CompanypostajobPage);
  }

DateDiff(dt2, dt1) 
{
  //edit dt1
  
  var returnTime;
  var timeDiff = Math.abs(dt2.getTime() - dt1.getTime());
  var diffDays = Math.floor(timeDiff / (1000 * 3600 * 24)); 
  console.log("diff day",diffDays);
console.log("dt1.getTime(): ",dt1.getTime());

  if(diffDays <= 0)
  {
    var ss =Math.abs(Math.round((dt2.getTime() - dt1.getTime()) / 1000));
    // var newss = ss-(2*60*60);
    var newss
    if (this.Platform.is('android')) {
      newss = ss-(2*60*60);
    }else  if (this.Platform.is('ios')) {
      newss = ss
    }

    // var newss = ss;
    console.log("ss: ",ss," newss: ",newss);
    // var h = Math.floor(ss/3600);
    // var m = Math.floor(ss % 3600 /60);
    // var s = Math.floor(ss % 3600 % 60);

    // console.log("h ", h,"m: ",m,"s: ",s);
    var h = Math.floor(newss/3600);
    var m = Math.floor(newss % 3600 /60);
    var s = Math.floor(newss % 3600 % 60);

    console.log("h ", h,"m: ",m,"s: ",s);
    
    // var newh = Math.floor(newss/3600);
    // var newm = Math.floor(newss % 3600 /60);
    // var news = Math.floor(newss % 3600 % 60);
    // console.log("newh ", newh,"newm: ",newm,"news: ",news); 

    var hdisplay = h > 0 ? h + (h == 1 ? this.translate.instant("hour"):this.translate.instant("hours")):"";
    var mdisplay = m > 0 ? m + (m == 1 ? this.translate.instant("min"):this.translate.instant("mins")):"";
    // var sdisplay = s > 0 ? s + (s == 1 ? this.translate.instant("sec"):this.translate.instant("secs")):"";
    // returnTime = s+this.translate.instant("sec")+":" + min++":"+hour+;
  
    if(h>0 && m >0)
    {
      console.log("1");
      returnTime =hdisplay+this.translate.instant(",")+mdisplay;
    }
    else if(h<=0)
    {
      returnTime =mdisplay;
      console.log("2");
    }
    else if (m<=0)
    {
      returnTime = hdisplay;
      console.log("3")
    }
    
   if (h<=0 && m<=0){
      returnTime = this.translate.instant("fewSeconds");
      console.log("4")
    }
    
    return returnTime;
      
  }else{
    if(diffDays == 1)
    returnTime = diffDays+this.translate.instant("day");
    else 
    returnTime = diffDays+this.translate.instant("days");

    return returnTime;
  }   
 }

 seeMoreNewJobs(){
  // this.navCtrl.parent.getByIndex(1).rootParams={from:"newJobs"};
  this.helper.choosetabs = "newJobs";
   this.navCtrl.parent.select(1);
 }

 seeMoreRecommendedJobs(){
  // this.navCtrl.parent.getByIndex(1).rootParams={from:"RecommendedJobs"};
  this.helper.choosetabs = "RecommendedJobs";
  this.helper.recommendedArr = this.recommendedJobs;
  this.navCtrl.parent.select(1);
 }

 doRefresh(ev){
this.refresher = ev;
// this.getLatestRecommendedJobs();
  this.service.jsListNewJobs(resp=>{
    console.log("resp from new jobs",JSON.parse(resp));
    this.newJobsList= JSON.parse(resp);
    console.log("this.newJobsList",this.newJobsList);
    for(var j=0;j<this.newJobsList.length;j++)
    {
      var dt1 = new Date();
      var dt2 = new Date(this.newJobsList[j].CreationDate);//"2018-11-16T12:50:53.466Z"
      // console.log("dateDiff: ",this.DateDiff(dt1, dt2));
      console.log("this.newJobsList[j].SID",this.newJobsList[j].SID);
      this.newJobsList[j].dateConversion=this.DateDiff(dt1, dt2);
      
    }

    if(this.refresher){
      this.refresher.complete();
    }
    
    this.getLatestRecommendedJobs();
  },err=>{
    console.log("err from new jobs",err);
    if(this.refresher){
      this.refresher.complete();
    }
    this.getLatestRecommendedJobs();
  });

 }

 hideJob(item){
  console.log("hide job",item);
  for(var j=0;j<this.newJobsList.length;j++){
    if(item.SID == this.newJobsList[j].SID)
    {
      this.newJobsList.splice(j,1)
    }
  }
  console.log("this.newJobsList",this.newJobsList);
 }

 confirmPass(){
  this.navCtrl.setRoot('ConfirmPasswordPage',{type:"compActivationForget",mobile:"mobile",id:"id"});
 }
 
 getLatestRecommendedJobs(){
   console.log("getLatestRecommendedJobs")
  this.storage.get('js_info').then((val) => {
    console.log("val from get js info",val);
    if(val)
      this.jsid = val.SID;  
   
    //   this.service.jsgetLatestJobs(this.jsid,resp=>{
    //     console.log("resp from jsgetLatestJobs",resp);
    //     if(JSON.parse(resp).result == "1"){
    //       this.latestnodata = true;
    //       this.latestApplied = JSON.parse(resp).jobsList.slice(0,3);
    //     }
    //     else{
    //       this.latestApplied = "";
    //       this.latestnodata = false;
    //     }

          
    // if(this.refresher){
    //   this.refresher.complete();
    // }

    //   },err=>{
    //     console.log("err from jsgetLatestJobs",err);

    // if(this.refresher){
    //   this.refresher.complete();
    // }

    //   })


      this.service.jsgetRecomendedJobs(this.jsid,resp=>{
        console.log("resp from jsgetRecomendedJobs",resp);
        if(JSON.parse(resp).result == "1"){
          console.log("JSON.parse(resp).jobsList.length : ",JSON.parse(resp).jobsList.length);
          if(JSON.parse(resp).jobsList.length > 2)
            this.recommendedJobs = JSON.parse(resp).jobsList.slice(0,2);
          else 
            this.recommendedJobs = JSON.parse(resp).jobsList;

            console.log("this.recommendedJobs : ",this.recommendedJobs);
            for(var j=0;j<this.recommendedJobs.length;j++)
            {
              var dt1 = new Date();
              var dt2 = new Date(this.recommendedJobs[j].CreationDate);//"2018-11-16T12:50:53.466Z"
              // console.log("dateDiff: ",this.DateDiff(dt1, dt2));
              console.log("this.newJobsList[j].SID",this.recommendedJobs[j].SID);
              this.recommendedJobs[j].dateConversion=this.DateDiff(dt1, dt2);
              
            }

            
            
        }
        else  if(JSON.parse(resp).result == "-1"){
          console.log("from else recommendedJobs: ");
          this.recommendedJobs = [];
        }

        console.log("recommendedJobs.length  before  <= 0 condition: ",this.recommendedJobs.length )
          if(this.recommendedJobs.length <= 0)
              this.hideRecommendedTitle = true;
            else
              this.hideRecommendedTitle = false;

    if(this.refresher){
      this.refresher.complete();
    }


   this.service.jsgetLatestJobs(this.jsid,resp=>{
        console.log("resp from jsgetLatestJobs",resp);
        if(JSON.parse(resp).result == "1"){
          this.latestnodata = true;
          console.log("JSON.parse(resp).jobsList : ",JSON.parse(resp).jobsList)
          console.log("JSON.parse(resp).jobsList.slice(0,3) : ",JSON.parse(resp).jobsList.slice(0,3))
          this.latestApplied = JSON.parse(resp).jobsList.slice(0,3);
        }
        else{
        console.log("2 enter else jsgetLatestJobs")
          this.latestApplied = [];
          this.latestnodata = false;
        }

          
    if(this.refresher){
      this.refresher.complete();
    }

      },err=>{
        console.log("err from jsgetLatestJobs",err);

    if(this.refresher){
      this.refresher.complete();
    }

      })


      },err=>{
        console.log("err from jsgetLatestJobs",err);

    if(this.refresher){
      this.refresher.complete();
    }
      })


  }).catch(err=>{
    console.log("catch from get js info",err);

       this.service.jsgetLatestJobs(this.jsid,resp=>{
        console.log("resp from jsgetLatestJobs",resp);
        if(JSON.parse(resp).result == "1"){
          console.log("if getJsAppliedJobs")
          this.latestnodata = true;
          this.latestApplied = JSON.parse(resp).jobsList.slice(0,3);
        }
        else{
          console.log("else getJsAppliedJobs")
          this.latestApplied = [];
          this.latestnodata = false;
        }

          
    if(this.refresher){
      this.refresher.complete();
    }

      },err=>{
        console.log("err from jsgetLatestJobs",err);

    if(this.refresher){
      this.refresher.complete();
    }

      })
    
  });

 }
 latestDetails(item){
   console.log("latestDetails : ",item)
   this.navCtrl.push('JobDetailsPage',{'jobDetails':item,from:"unapply"});
 }

}
