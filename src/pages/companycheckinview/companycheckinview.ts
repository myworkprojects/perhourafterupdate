import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';

/**
 * Generated class for the CompanycheckinviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-companycheckinview',
  templateUrl: 'companycheckinview.html',
})
export class CompanycheckinviewPage {
  langDirection:any
  constructor(public ViewCtrl:ViewController,public helper:HelperProvider,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.langDirection = this.helper.lang_direction;

    console.log('ionViewDidLoad CompanycheckinviewPage');
  }
  dismiss(){
    this.navCtrl.pop();
  }
}
