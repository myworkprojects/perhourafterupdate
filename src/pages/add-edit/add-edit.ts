import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams ,Platform, Content,ActionSheetController} from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ServicesProvider } from '../../providers/services/services';
import { HelperProvider } from '../../providers/helper/helper';
import { TranslateService } from '@ngx-translate/core';
import { File } from '@ionic-native/file';
import { IOSFilePicker } from '@ionic-native/file-picker';
import { FileChooser } from '@ionic-native/file-chooser';
import { Base64 } from '@ionic-native/base64';
import { FilePath } from '@ionic-native/file-path';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Camera, CameraOptions } from '@ionic-native/camera';


@IonicPage()
@Component({
  selector: 'page-add-edit',
  templateUrl: 'add-edit.html',
  providers: [File, FileChooser, IOSFilePicker, Base64, FilePath,Camera]
})
export class AddEditPage {

  private registerForm : FormGroup;
  @ViewChild(Content) content: Content;
  school_compName;
  degree_job;
  address_feild;
  grade="";
  description_jobDescription;
  tillNow=true;
  tillNow_school_jobTxt;
  startdate;
  enddate;
  hideforexperience ;
  hidecrtforexperience;
  submitAttempt = false;
  errors = {
    school_compName:"",
    degree_job:"",
    address_feild: "",
    startdate:"",
    enddate:"",
    description_jobDescription:""
  };
  langDirection = "";
  placeholder = {school_compName:"",degree_job:"", address_feild: "",grade:"",description_jobDescription:"",startDateTxt:"",endDateTxt:"" };
  scaleClass="";

  floating = "";

  from;
  dataForEdit;
  date;
  pageTitle;
  hideEndDateError = true;
  truecase = false;
  stop = false;
  jsid;

  user_cv_name = [];
  cv_ext="";
  cv_data="";
  isChecked = false;
  pathforview;
  idforeditedu;
  linktoview;
  certtxtname = ""

  constructor(  private camera: Camera, public actionSheetCtrl: ActionSheetController, private iab: InAppBrowser,private filePicker: IOSFilePicker, public platform: Platform,
    private file: File, private filePath: FilePath,
    private base64: Base64, private fileChooser: FileChooser,public srv:ServicesProvider, public translate: TranslateService,public helper:HelperProvider,private formBuilder: FormBuilder,public navCtrl: NavController, public navParams: NavParams) {
  
    this.langDirection = this.helper.lang_direction;

    if(this.langDirection == "rtl")
    {
      this.scaleClass="scaleClass";
      this.floating = "floatingrtl";
      console.log("floating : ",this.floating)
    }else {
      this.floating = "floatingltr";
      console.log("floating : ",this.floating)
    }
    

    this.from = this.navParams.get('from');
    this.jsid = this.navParams.get('id');

    if(this.navParams.get("item"))
      this.dataForEdit = this.navParams.get("item")
   

this.date = new Date().toISOString();
this.hideEndDateError = true;
this.truecase = false;
this.isChecked = false;
this.tillNow=true;


    this.registerForm = this.formBuilder.group({
      school_compName :['',Validators.required],
      degree_job:['',Validators.required],
      address_feild: ['',Validators.required],
      grade :['',''],
      description_jobDescription:['',Validators.required],
      startdate:['',Validators.required]
    });

if(this.from == "addexp"){
console.log("addexp");
this.hideforexperience = true;
this.hidecrtforexperience = true;
this.pageTitle = this.translate.instant("addexp");
this.placeholder.school_compName = this.translate.instant("expcompany");
this.placeholder.degree_job = this.translate.instant("expjob");
this.placeholder.address_feild = this.translate.instant("expadd");
this.placeholder.startDateTxt = this.translate.instant("startdate");
this.placeholder.endDateTxt = this.translate.instant("enddate");
this.placeholder.description_jobDescription = this.translate.instant("expdesc");
this.tillNow_school_jobTxt = this.translate.instant("exptillnow");

this.errors.school_compName = this.translate.instant("expcompanyerr");
this.errors.degree_job = this.translate.instant("expjoberr");
this.errors.address_feild = this.translate.instant("expadderr");
this.errors.description_jobDescription = this.translate.instant("expdescerr");
this.errors.startdate = this.translate.instant("noostartdate");
this.errors.enddate = this.translate.instant("noenddate");

}
else if(this.from == "editexp"){
  console.log("editexp");
  this.hideforexperience = true;
  this.hidecrtforexperience = false;
  this.pageTitle = this.translate.instant("editedu");
  this.school_compName = this.dataForEdit.PlaceName;
  this.degree_job = this.dataForEdit.JobTitle;
  this.address_feild = this.dataForEdit.Address;
  this.idforeditedu = this.dataForEdit.SID;
  this.linktoview = this.dataForEdit.CertFile;
  this.cv_data = this.dataForEdit.CertFile;
  // this.grade="";
  
  if(this.dataForEdit.StillWorking == "0")
  {
    this.isChecked = false;
    this.tillNow = false;
    this.enddate =  this.dataForEdit.EndDate;
  } 
 else if(this.dataForEdit.StillWorking == "1"){
  this.isChecked = true;
  this.tillNow = true;
  console.log("isChecked",this.isChecked);
 }
 this.startdate = this.dataForEdit.StartDate.split("T")[0];
 this.description_jobDescription = this.dataForEdit.Description;

 this.placeholder.school_compName = this.translate.instant("expcompany");
 this.placeholder.degree_job = this.translate.instant("expjob");
 this.placeholder.address_feild = this.translate.instant("expadd");
 this.placeholder.startDateTxt = this.translate.instant("startdate");
 this.placeholder.endDateTxt = this.translate.instant("enddate");
 this.placeholder.description_jobDescription = this.translate.instant("expdesc");
 this.tillNow_school_jobTxt = this.translate.instant("exptillnow");
 
 this.errors.school_compName = this.translate.instant("expcompanyerr");
 this.errors.degree_job = this.translate.instant("expjoberr");
 this.errors.address_feild = this.translate.instant("expadderr");
 this.errors.description_jobDescription = this.translate.instant("expdescerr");
 this.errors.startdate = this.translate.instant("noostartdate");
 this.errors.enddate = this.translate.instant("noenddate");


}
else if(this.from == "addedu"){
  console.log("addedu");
  this.hideforexperience = false;
  this.hidecrtforexperience = true;
  this.pageTitle = this.translate.instant("addedu");
this.placeholder.school_compName = this.translate.instant("eduschool");
this.placeholder.degree_job = this.translate.instant("edudegree");
this.placeholder.address_feild = this.translate.instant("edufeild");
this.placeholder.grade = this.translate.instant("edugrade");
this.placeholder.startDateTxt = this.translate.instant("startdate");
this.placeholder.endDateTxt = this.translate.instant("enddate");
this.placeholder.description_jobDescription = this.translate.instant("description");
this.tillNow_school_jobTxt = this.translate.instant("edutillnow");

this.errors.school_compName = this.translate.instant("eduschoolerr");
this.errors.degree_job = this.translate.instant("edudegreeerr");
this.errors.address_feild = this.translate.instant("edufeilderr");
this.errors.description_jobDescription = this.translate.instant("descriptionerr");
this.errors.startdate = this.translate.instant("noostartdate");
this.errors.enddate = this.translate.instant("noenddate");




}
else if(this.from == "editedu"){
  console.log("editedu");
  this.hideforexperience = false;
  this.hidecrtforexperience = false;
  this.pageTitle = this.translate.instant("editedu");
  this.school_compName = this.dataForEdit.SchoolName;
  this.degree_job = this.dataForEdit.ScientificGrade;
  this.address_feild = this.dataForEdit.Branch;
  this.idforeditedu = this.dataForEdit.SID;
  this.linktoview = this.dataForEdit.CertFile;
  this.cv_data = this.dataForEdit.CertFile;
  this.grade=this.dataForEdit.TotalGrade
  
  if(this.dataForEdit.StillLearning == "0")
  {
    this.isChecked = false;
    this.tillNow = false;
    this.enddate =  this.dataForEdit.EndDate;
  } 
 else if(this.dataForEdit.StillLearning == "1"){
  this.isChecked = true;
  this.tillNow = true;
  console.log("isChecked",this.isChecked);
 }
 

  this.startdate = this.dataForEdit.StartDate.split("T")[0];
  this.description_jobDescription = this.dataForEdit.Description;

  this.placeholder.school_compName = this.translate.instant("eduschool");
this.placeholder.degree_job = this.translate.instant("edudegree");
this.placeholder.address_feild = this.translate.instant("edufeild");
this.placeholder.grade = this.translate.instant("edugrade");
this.placeholder.startDateTxt = this.translate.instant("startdate");
this.placeholder.endDateTxt = this.translate.instant("enddate");
this.placeholder.description_jobDescription = this.translate.instant("description");
this.tillNow_school_jobTxt = this.translate.instant("edutillnow");

this.errors.school_compName = this.translate.instant("eduschoolerr");
this.errors.degree_job = this.translate.instant("edudegreeerr");
this.errors.address_feild = this.translate.instant("edufeilderr");
this.errors.description_jobDescription = this.translate.instant("descriptionerr");
this.errors.startdate = this.translate.instant("noostartdate");
this.errors.enddate = this.translate.instant("noenddate");

}
this.certtxtname = ""

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddEditPage');
  }

  save(){
    if(! this.registerForm.valid ){
      this.submitAttempt=true;
      this.content.scrollToTop(3000);
    }else{

      if(this.tillNow == true){
        console.log("tillnow true true case :",this.truecase);
        if(this.startdate.split("T")[0]  == new Date().toISOString().split("T")[0])
        {
          // this.helper.presentToast(this.translate.instant("startequalend"));
          // this.truecase = false;
          this.truecase = true;
          console.log("tillnow true from equal true case :",this.truecase);
        }
        else
        {
          this.truecase = true;
          console.log("tillnow true from equal else true case :",this.truecase);
        }
          


      }else if(this.tillNow == false){
        console.log("tillnow false true case :",this.truecase);
        if(this.enddate )
        {
          console.log("tillnow false with end date true case :",this.truecase);
          if(this.startdate.split("T")[0] > this.enddate.split("T")[0])
          {
            console.log("this.startdate.split('T')[0] : ",this.startdate.split("T")[0]);
            console.log("this.enddate.split('T')[0] : ",this.enddate.split("T")[0]);
            this.helper.presentToast(this.translate.instant("startmorethanend"));
            this.hideEndDateError = true;
            this.truecase = false;
            this.stop = true;
          }else if(this.startdate.split("T")[0] == this.enddate.split("T")[0]){

            this.helper.presentToast(this.translate.instant("startequalend"));
            this.truecase = false;
          this.hideEndDateError = true;
          this.stop = true;
          console.log("tillnow false equal 2 true  case :",this.truecase);
          }else{
            this.truecase = true;
          this.hideEndDateError = true;
          this.stop = false;
          console.log("tillnow false with end else true case :",this.truecase);
          }
        }else if(! this.enddate){
          this.hideEndDateError = false;
          this.truecase = false;
          console.log("tillnow false with out end date true case :",this.truecase);
        }

      }

  //   if(this.startdate && this.enddate)
  //   {
  //     if(this.startdate.split("T")[0] > this.enddate.split("T")[0])
  //     {
  //       console.log("this.startdate.split('T')[0] : ",this.startdate.split("T")[0]);
  //       console.log("this.enddate.split('T')[0] : ",this.enddate.split("T")[0]);
  //       this.helper.presentToast(this.translate.instant("startmorethanend"));
  //       this.hideEndDateError = true;
  //       this.truecase = false;
  //       this.stop = true;
  //     }else if(this.startdate.split("T")[0] == this.enddate.split("T")[0]){
  //       this.helper.presentToast(this.translate.instant("startequalend"));
  //       this.truecase = false;
  //     this.hideEndDateError = true;
  //     this.stop = true;
  //     }else{
  //       this.truecase = true;
  //     this.hideEndDateError = true;
  //     this.stop = false;
  //     }
  //   }
  //    if(this.tillNow == false && !this.enddate)
  //   {
  //     this.truecase = false;
  //     this.hideEndDateError = false;
  //   }
  //   else if(this.tillNow == false && this.enddate && this.stop == false)
  //   {
  //     this.truecase = true;
  //     this.hideEndDateError = true;
  //   }  
  //   else if(this.tillNow == true && !this.enddate)
  //   {
  //     this.truecase = true;
  //   this.hideEndDateError = true;
  //  }
  //   else if(this.tillNow == true && this.enddate && this.stop == false)
  //   {
  //     console.log("check with end date");
  //     this.hideEndDateError = true;
  //     this.enddate = "";
  //     this.truecase = true;
  //   } else if(this.tillNow == false && !this.enddate)
  //   {
  //     this.truecase = false;
  //     this.hideEndDateError = false;
  //   } 
console.log("true case :",this.truecase);
      if(this.truecase == true)
      {
        console.log("save");
        var tillno ;
        if(this.tillNow == false)
         tillno = 0;
        else if(this.tillNow == true)
         tillno = 1;

         if(this.from == "addedu"){
          let data={
            'Seeker' : this.jsid,
            'Study':-1,
            'SchoolName':this.school_compName,
            'ScientificGrade':this.degree_job,
            'TotalGrade':this.grade,
            'EndDate':this.enddate,
            'StartDate':this.startdate,
            'StillLearning':tillno,
            'Description':this.description_jobDescription,
            'CertFile': this.cv_data,
            "FileExtension":this.cv_ext,
            "Branch":this.address_feild
           
          };
  
          this.srv.jsAddStudy(data,"add",resp=>{
            console.log("resp from jsAddStudy",resp);
            if(JSON.parse(resp).OperationResult == "1")
              this.helper.presentToast(this.translate.instant("suadd"));
            else
            this.helper.presentToast(this.translate.instant("ServerError"));

            this.navCtrl.pop();

          },err=>{
            console.log("err from jsAddStudy",err);
            this.helper.presentToast(this.translate.instant("ServerError"));

            this.navCtrl.pop();
          })
  
         }else if (this.from == "editedu"){

          let data={
            'Seeker' : this.jsid,
            'Study':this.idforeditedu,
            'SchoolName':this.school_compName,
            'ScientificGrade':this.degree_job,
            'TotalGrade':this.grade,
            'EndDate':this.enddate,
            'StartDate':this.startdate,
            'StillLearning':tillno,
            'Description':this.description_jobDescription,
            'CertFile': this.cv_data,
            "FileExtension":this.cv_ext,
            "Branch":this.address_feild
           
          };
  
          this.srv.jsAddStudy(data,"edit",resp=>{
            console.log("resp from jsAddStudy",resp);
            if(JSON.parse(resp).OperationResult == "1")
              this.helper.presentToast(this.translate.instant("suedit"));
            else
            this.helper.presentToast(this.translate.instant("ServerError"));

            this.navCtrl.pop();

          },err=>{
            console.log("err from jsAddStudy",err);
            this.helper.presentToast(this.translate.instant("ServerError"));

            this.navCtrl.pop();
          })

         }else if(this.from == "addexp"){
          let data={
            'Seeker' : this.jsid,
            'Experience':-1,
            'PlaceName':this.school_compName,
            'JobTitle':this.degree_job,
            'EndDate':this.enddate,
            'StartDate':this.startdate,
            'StillWorking':tillno,
            'Description':this.description_jobDescription,
            'CertFile': this.cv_data,
            "FileExtension":this.cv_ext,
            "Address":this.address_feild
           
          };


  
          this.srv.jsAddExperience(data,"add",resp=>{
            console.log("resp from jsAddStudy",resp);
            if(JSON.parse(resp).OperationResult == "1")
              this.helper.presentToast(this.translate.instant("suadd"));
            else
            this.helper.presentToast(this.translate.instant("ServerError"));

            this.navCtrl.pop();

          },err=>{
            console.log("err from jsAddStudy",err);
            this.helper.presentToast(this.translate.instant("ServerError"));

            this.navCtrl.pop();
          })
  
         }else if (this.from == "editexp"){

          let data={
            'Seeker' : this.jsid,
            'Experience':this.idforeditedu,
            'PlaceName':this.school_compName,
            'JobTitle':this.degree_job,
            'EndDate':this.enddate,
            'StartDate':this.startdate,
            'StillWorking':tillno,
            'Description':this.description_jobDescription,
            'CertFile': this.cv_data,
            "FileExtension":this.cv_ext,
            "Address":this.address_feild
           
          };
  
          this.srv.jsAddExperience(data,"edit",resp=>{
            console.log("resp from jsAddStudy",resp);
            if(JSON.parse(resp).OperationResult == "1")
              this.helper.presentToast(this.translate.instant("suedit"));
            else
            this.helper.presentToast(this.translate.instant("ServerError"));

            this.navCtrl.pop();

          },err=>{
            console.log("err from jsAddStudy",err);
            this.helper.presentToast(this.translate.instant("ServerError"));

            this.navCtrl.pop();
          })

         }
       

        


      }
      else{
        console.log("end date errors")
        this.content.scrollToTop(3000);
      }
        

    
    }
  

  }




  dismiss(){
    this.navCtrl.pop();
  }

  selectWorkerCertificate(){
    let actionSheet = this.actionSheetCtrl.create({
      title: this.translate.instant("selectcertsource"),
      buttons: [
        {
          text: this.translate.instant("LoadfromFiles"),
          handler: () => {
            this.getCertificates();
          }
        },
        {
          text: this.translate.instant("LoadfromLibrary"),
          handler: () => {
            this.takePictureforworker(this.camera.PictureSourceType.PHOTOLIBRARY,1);
          }
        },

        {
          text: this.translate.instant("UseCamera"),
          handler: () => {
            this.takePictureforworker(this.camera.PictureSourceType.CAMERA,2);
          }
        },
        {
          text: this.translate.instant("cancel"),
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();

  }

  public takePictureforworker(sourceType,typex) {
    // Create options for the Camera Dialog
    var options = {
      quality: 100, //50
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true,
      allowEdit:true,
      targetWidth:200,
      targetHeight:200
    };
    this.camera.getPicture(options).then((imageData: string) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      // this.storage.get("js_info").then((val) => {
       // this.userImageUrl = 'data:image/jpeg;base64,' + imageData
        //this.storage.set("user_image",this.userImageUrl)
        let imgdata = encodeURIComponent(imageData)
        // this.certttxt = this.translate.instant("imageCaptured");
        if(typex == 1 )
        this.certtxtname = this.translate.instant("imageChoosed")
        else if(typex == 2)
        this.certtxtname = this.translate.instant("imageCaptured")
        
        this.cv_ext =  'jpeg'
        this.cv_data = imgdata
        this.hidecrtforexperience = true;
        


      // })
    }, (err) => {
      // Handle error
    });
  }

  getCertificates() {
    // this.hidecrtforexperience = true;
    if (this.platform.is('ios')) {
      this.filePicker.pickFile()
        .then(uri => {
          

          let correctPath = uri.substr(0, uri.lastIndexOf('/') + 1);
          let filename = uri.substr(uri.lastIndexOf('/') + 1)
          this.certtxtname = filename;
          this.hidecrtforexperience = true;
          this.user_cv_name.push(filename)
          let fileExt = filename.split('.').pop();
          this.cv_ext = fileExt
          var cvextuper = this.cv_ext.toUpperCase();
          if (cvextuper == "pdf".toUpperCase() || cvextuper == "docx".toUpperCase() || cvextuper == "doc".toUpperCase() || cvextuper == "JPEG".toUpperCase() || cvextuper == "PNG".toUpperCase() || cvextuper == "JPG".toUpperCase() || cvextuper == "GIF".toUpperCase() || cvextuper == "BMP".toUpperCase()) {
            console.log("from if : ", cvextuper);
            // this.helper.presentToast(this.translate.instant("fileupsu"));
          } else {
            console.log("from else : ", cvextuper);
            this.helper.presentToast(this.translate.instant("fileupnoex"));
            this.cv_data = "";
            this.cv_ext = "";
            this.certtxtname = ""
            this.hidecrtforexperience = false;
            this.user_cv_name = [];
          }

          var vx = "file:///"+correctPath
            this.file.resolveLocalFilesystemUrl(vx).then(fileEntry => {
              console.log("file entry : ",fileEntry)
              fileEntry.getMetadata((metadata) => {
    
                  console.log("worker cert meta data from resolveLocalFilesystemUrl : ",metadata);//metadata.size is the size in bytes
                  // metadata.size: 20761
    // this.vedioSize = (metadata.size / 1024)/1024
                  console.log("(metadata.size / 1024)/1024 : ",(metadata.size / 1024)/1024)
                  if((metadata.size / 1024)/1024 > 1){
                   this.helper.presentToast(this.translate.instant("documentLargerThan1Mega"))
                  //  this.hidecrtforexperience = false;
                   this.cv_data = "";
                           this.cv_ext = "";
                           this.certtxtname = ""
                           
                           this.user_cv_name = [];

                  }else{

          this.file.readAsDataURL("file:///" + correctPath, filename).then((val) => {
            this.pathforview = "file:///" + correctPath;

            this.cv_data = encodeURIComponent(val.split(",")[1]);

            console.log("this.cv_data: ", this.cv_data);
            console.log("this.user_cv_name : ", this.user_cv_name);
            console.log("this.cv_ext : ", this.cv_ext)
        
          }).catch(err => console.log('Error reader' + err));

        }
      });
    });

        }).catch(err => console.log('Error' + err));
    }
    else if (this.platform.is('android')) {
      this.fileChooser.open()
        .then(uri => {
          console.log("uuu" + uri)
          

          this.filePath.resolveNativePath(uri).then((result) => {
            this.base64.encodeFile(result).then((base64File: string) => {
              console.log("base64File " + base64File)
              let fileData = base64File.split(',')[1];
              this.cv_data = encodeURIComponent(fileData);
              console.log("this.cv_data : ", this.cv_data);
              this.filePath.resolveNativePath(uri)
                .then(filePath => {
                  console.log(filePath)
                  this.pathforview = filePath;

                  let filename = filePath.substr(filePath.lastIndexOf('/') + 1)
                  this.certtxtname = filename;
                  this.hidecrtforexperience = true;
                  this.user_cv_name.push(filename)
                  let fileExt = filename.split('.').pop();
                  // this.cv_ext.push(fileExt)
                  this.cv_ext = fileExt;
                  var cvextuper = this.cv_ext.toUpperCase();
                  if (cvextuper == "pdf".toUpperCase() || cvextuper == "docx".toUpperCase() || cvextuper == "doc".toUpperCase() || cvextuper == "JPEG".toUpperCase() || cvextuper == "PNG".toUpperCase() || cvextuper == "JPG".toUpperCase() || cvextuper == "GIF".toUpperCase() || cvextuper == "BMP".toUpperCase()) {
                    console.log("from if : ", cvextuper);
                    // this.helper.presentToast(this.translate.instant("fileupsu"));
                    var vx =filePath
                    this.file.resolveLocalFilesystemUrl(vx).then(fileEntry => {
                      console.log("file entry : ",fileEntry)
                      fileEntry.getMetadata((metadata) => {
            
                          console.log("worker cert meta data from resolveLocalFilesystemUrl : ",metadata);//metadata.size is the size in bytes
                          // metadata.size: 20761
            // this.vedioSize = (metadata.size / 1024)/1024
                          console.log("(metadata.size / 1024)/1024 : ",(metadata.size / 1024)/1024)
                          if((metadata.size / 1024)/1024 > 1){
                           this.helper.presentToast(this.translate.instant("documentLargerThan1Mega"))
                          //  this.hidecrtforexperience = false;
                           this.cv_data = "";
                           this.cv_ext = "";
                           this.certtxtname = ""
                           
                           this.user_cv_name = [];
                          }
                          // else{

                          // }

                          })
                        })
                  } else {
                    console.log("from else : ", cvextuper);
                    this.helper.presentToast(this.translate.instant("fileupnoex"));
                    this.cv_data = "";
                    this.cv_ext = "";
                    this.certtxtname = ""
                    this.hidecrtforexperience = false;
                    this.user_cv_name = [];
                  }

                  console.log("this.user_cv_name : ", this.user_cv_name);
                  console.log("this.cv_ext : ", this.cv_ext)
                 

                })
                .catch(err => console.log(err));
            }, (err) => {
              console.log("base" + err);
            });

          }, (err) => {
            console.log(err);
          })


        })
        .catch(e => console.log(e));
    }
  }

  showcv() {
    if (this.from == "editedu" || this.from == "editexp") {
      // if(this.pathforview){

      //   console.log("if editedu or editexp open pathforview: ",this.pathforview);
      //   window.open(this.pathforview);
      // }else{

      
      if(this.linktoview){
        console.log("if editedu or editexp open ",this.linktoview);
        // window.open(this.linktoview);
        const browser = this.iab.create(this.linktoview,'_system',"location=yes");
      }else {
        console.log("else editedu or editexp open ");
        this.helper.presentToast(this.translate.instant("nocerttoshow"));
      }
   // }

    }
    
    
  }

  tillnowchanged(){
    if(this.tillNow == true)
    {
      this.enddate= "";
      this.hideEndDateError = true;
    }
      
  }

}
