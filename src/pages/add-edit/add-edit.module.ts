import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddEditPage } from './add-edit';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [
    AddEditPage,
  ],
  imports: [
    IonicPageModule.forChild(AddEditPage),
    TranslateModule.forChild(),
  ],
})
export class AddEditPageModule {}
