import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,PopoverController,AlertController } from 'ionic-angular';

import { HelperProvider } from '../../providers/helper/helper';
import { ServicesProvider } from '../../providers/services/services';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';
import { JsProfilePage } from '../js-profile/js-profile';

@IonicPage()
@Component({
  selector: 'page-companyselectedusers',
  templateUrl: 'companyselectedusers.html',
})
export class CompanyselectedusersPage {

  jsShortDescription = "";
  jsFirstName="";
  scaleClass="";
  langDirection;

  jobId;
  jsId;
  pageType;
  hide2Btns;
  jobName;
  jsName;
  txt;
  allselected;
  title;
  companyId

  constructor(public alertCtrl:AlertController,public storage:Storage,public popoverCtrl: PopoverController,public translate: TranslateService,public service:ServicesProvider,public helper:HelperProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.langDirection = this.helper.lang_direction;
    if(this.langDirection == "rtl")
      this.scaleClass="scaleClass";

    this.jobId = this.navParams.get('jobId');
    console.log("job id",this.jobId);
    this.title = this.navParams.get('jobname');


    this.storage.get("js_info").then((val) => {
      this.companyId = val.SID;
    });
    this.service.compGetSelectedcan(this.jobId,resp=>{
      console.log("resp from compGetSelectedcan",resp);
      var respData = JSON.parse(resp);
      if(respData.OperationResult == "-1")
      {
        this.navCtrl.pop();
        this.helper.presentToast(this.translate.instant("noselected"))
        
      }else if(respData.OperationResult == "1"){
        this.allselected = respData.Candidates;
        for(var x=0;x<this.allselected.length;x++){
          // ShortDescription
          if(this.allselected[x].ShortDescription == "1")
            this.allselected[x].ShortDescription2 = this.translate.instant("student")
          else if(this.allselected[x].ShortDescription == "2")
            this.allselected[x].ShortDescription2 = this.translate.instant("employee")
          else if(this.allselected[x].ShortDescription == "3")
            this.allselected[x].ShortDescription2 = this.translate.instant("unemployed")
        }

      }
       
    },err=>{
      console.log("err from compGetSelectedcan",err);
    });
    // this.jsId = this.navParams.get('jsid');
    // this.pageType = this.navParams.get('notificationType');
    //console.log("this.pageType: ",this.pageType);
    // if(this.pageType == "cancel")
    // {
    //   this.hide2Btns = true;
    //   this.txt = this.translate.instant("Cancelled");
    // }
    // else if (this.pageType == "checkout")
    //   {
    //     this.hide2Btns = true;
    //     this.txt = this.translate.instant("Checkout2");
    //   }
    // else if (this.pageType == "checkIn")
    //   {
    //     this.hide2Btns = false;
    //     this.txt = this.translate.instant("Checkin2");
    //   }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CompanyselectedusersPage');
    // this.service.getUserDetails(this.jsId,resp=>{
    //   console.log("resp from getUserDetails",resp);
    //   var respData = JSON.parse(resp);
    //   if(respData.Result == "1")
    //   {
        
    //     this.jsName = respData.details.FirstName;
        
    //   this.jsShortDescription = respData.details.ShortDescription


    //   }else{
    //     console.log("else respData.Result == 1");
    //     this.navCtrl.pop();
    //   }

    // },err=>{
    //   console.log("err from getUserDetails",err);
    // });

    // this.service.getJobDetails(this.jobId,resp=>{
    //   console.log("resp from getJobDetails",resp);
    //   var respData = JSON.parse(resp);
    //   if(respData.Result == "1")
    //   {
    //     if(this.helper.lang_direction == "ltr")
    //     this.jobName = respData.details.JobTitleEn;
    //     else if(this.helper.lang_direction == "rtl")
    //     this.jobName = respData.details.JobTitleAr;
      


    //   }else{
    //     console.log("else respData.Result == 1");
    //     this.navCtrl.pop();
    //   }
    // },err=>{
    //   console.log("err from getJobDetails",err);
    // });

  }

  dismiss(){
    this.navCtrl.pop();
  }
  accept(item){
    this.service.acceptorrejectAfterNotificationJSCheckin(this.jobId,item.JobSeekerSID,1,resp=>{
      //if(JSON.parse(resp).Result ==  "-1")
        //this.helper.presentToast(this.translate.instant("acceptBefore"));
     // else
        this.helper.presentToast(this.translate.instant("acceptsu"));
      this.navCtrl.pop();
    },err=>{
      this.navCtrl.pop();
    });
    
  }
  reject(item){
   
    this.service.acceptorrejectAfterNotificationJSCheckin(this.jobId,item.JobSeekerSID,-1,resp=>{
      this.helper.presentToast(this.translate.instant("rejectsu"))
      this.navCtrl.pop();
    },err=>{
      this.navCtrl.pop();
    });
  }
  refresher;
  doRefresh(ev){
    this.refresher = ev;

    this.service.compGetSelectedcan(this.jobId,resp=>{
      console.log("resp from compGetSelectedcan",resp);
      var respData = JSON.parse(resp);
      if(respData.OperationResult == "-1")
      {
        this.navCtrl.pop();
        this.helper.presentToast(this.translate.instant("noselected"))
        
      }else if(respData.OperationResult == "1"){
        this.allselected = respData.Candidates;
        
        for(var x=0;x<this.allselected.length;x++){
          // ShortDescription
          if(this.allselected[x].ShortDescription == "1")
            this.allselected[x].ShortDescription2 = this.translate.instant("student")
          else if(this.allselected[x].ShortDescription == "2")
            this.allselected[x].ShortDescription2 = this.translate.instant("employee")
          else if(this.allselected[x].ShortDescription == "3")
            this.allselected[x].ShortDescription2 = this.translate.instant("unemployed")
        }

      }

      if(this.refresher){
        this.refresher.complete();
      }
       
    },err=>{
      console.log("err from compGetSelectedcan",err);

      if(this.refresher){
        this.refresher.complete();
      }
    });


  
  }


  showcv(item){
    console.log("item from showcv: ",item)

    this.navCtrl.push(JsProfilePage,{item:item,from:"showProfile"});
    // if(item.CVFile){
    //   console.log("if cvlinkfromstorge");
    //   window.open(item.CVFile);
    // }else{
    //   console.log("else cvlinkfromstorge");
    //   this.helper.presentToast(this.translate.instant("nocvtoshow"));
      
    // }
  }

  rate(item,event){
    console.log("rate item: ",item)
    let popover = this.popoverCtrl.create('RatePopoverPage');
    popover.present({
      ev: event
    });
    popover.onDidDismiss(data => {
      console.log("onDidDismiss data: ",data);
      if (data != null) {
        console.log("data from if : ",data);
        var rateData ={Company:this.companyId,Seeker:item.JobSeekerSID,Job:this.jobId,ReviewGrade:data.rate,ReviewComment:data.notes};

        this.service.comapnyAddRate(rateData,resp=>{
          console.log("resp from comapnyAddRate",resp);
          if(JSON.parse(resp).OperationResult == "1")
            this.helper.presentToast(this.translate.instant("ratedone"))
          else
            this.helper.presentToast(this.translate.instant("ServerError"))
        },err=>{
          console.log("err from comapnyAddRate",err);
        })
      }
    })

  }

  // daysPopover(event) {
  //   let popover = this.popoverCtrl.create('DaysPopoverPage');
  //   popover.present({
  //     ev: event
  //   });
  //   popover.onDidDismiss(data => {
  //     console.log(data);
  //     if (data != null) {
  //       this.selectedDays = data
  //       let DaysIDs = []
  //       let DaysStrings = []
  //       for(let x=0;x<data.length;x++){
  //         DaysStrings.push(data[x].day)
  //         DaysIDs.push(data[x].value)
  //       }
  //       this.SelectedDayStrings = DaysStrings.toString()
  //       this.SelectedDaysIDs = DaysIDs.toString()
  //       console.log("this.SelectedDaysIDs : ",this.SelectedDaysIDs)
  //     }
  //   })
  
  // }
  Absence(item){
    console.log("Absence item : ",item)
//     JobSID: 242
// JobSeekerSID: 1093
let alert = this.alertCtrl.create({
  title: this.translate.instant('absentSure'),
  buttons: [
    {
      text: this.translate.instant('no2'),
      role: 'cancel',
      handler: () => {
        console.log('Cancel clicked');
      }
    },
    {
      text: this.translate.instant('yes2'),
      handler: () => {
        console.log('Agree clicked');
        this.storage.get("js_info").then((val) => {
          this.companyId = val.SID;
          //     JobSID: 242
// JobSeekerSID: 1093

// http://api.perhour-sa.com/api/job/JobSeekerAbsense?job=242&seeker=1093&company=72

var current2=Date.parse(new Date().toString());
console.log("current : ",current2)
var JobStartTime=Date.parse(item.JobStartTime);
console.log("JobStartTime : ",JobStartTime)
if(current2 >= JobStartTime){
  console.log("current >= job start")

            this.service.absent(item.JobSID,item.JobSeekerSID,this.companyId,resp=>{
            console.log("resp from absent :",resp)
            var respData  = JSON.parse(resp)

//             Message: "Done"
// Result: 1

          if(respData.Result == "1")
            this.helper.presentToast( this.translate.instant("reportDoneSu"))
          else
           this.helper.presentToast( this.translate.instant("ServerError"))

          },err=>{
            console.log("err from absent :",err)

          })

}
        
else{
  console.log("current < job started")
  this.helper.presentToast(this.translate.instant("jobnotstarted"))
} 
       
//           this.service.absent(item.JobSID,item.JobSeekerSID,this.companyId,resp=>{
//             console.log("resp from absent :",resp)
//             var respData  = JSON.parse(resp)

// //             Message: "Done"
// // Result: 1

//           if(respData.Result == "1")
//             this.helper.presentToast( this.translate.instant("reportDoneSu"))
//           else
//            this.helper.presentToast( this.translate.instant("ServerError"))

//           },err=>{
//             console.log("err from absent :",err)

//           })


        });

      }
    }
  
]
})
alert.present()


  }

}
