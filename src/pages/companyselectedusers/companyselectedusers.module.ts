import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CompanyselectedusersPage } from './companyselectedusers';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';

@NgModule({
  declarations: [
    CompanyselectedusersPage,
  ],
  imports: [
    IonicPageModule.forChild(CompanyselectedusersPage),
    TranslateModule.forChild()
  ],
})
export class CompanyselectedusersPageModule {}
