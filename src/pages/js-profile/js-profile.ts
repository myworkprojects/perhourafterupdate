import { Component ,ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ActionSheetController, Events } from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
import { Storage } from '@ionic/storage';
import { File ,DirectoryEntry , FileEntry} from '@ionic-native/file';
import { IOSFilePicker } from '@ionic-native/file-picker';
import { FileChooser } from '@ionic-native/file-chooser';
import { Base64 } from '@ionic-native/base64';
import { FilePath } from '@ionic-native/file-path';
import { ServicesProvider } from '../../providers/services/services';
import { TranslateService } from '@ngx-translate/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { StreamingMedia, StreamingVideoOptions } from '@ionic-native/streaming-media';

import { MediaCapture ,MediaFile, CaptureError, CaptureVideoOptions} from '@ionic-native/media-capture';
import { Media,MediaObject } from '@ionic-native/media';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import {DomSanitizer} from '@angular/platform-browser';
import { VideoEditor } from '@ionic-native/video-editor/ngx';

// @IonicPage()
@Component({
  selector: 'page-js-profile',
  templateUrl: 'js-profile.html',
  providers: [File, FileChooser, IOSFilePicker, Base64, FilePath, Camera,MediaCapture,Media,VideoEditor]
})
export class JsProfilePage {

  scaleClass = "";
  langDirection;

  userData;
  job = "UI/UX";
  userImageUrl = "assets/imgs/default-avatar.png";
  FirstName;
  mail;
  mobile;
  user_cv_name = [];
  cv_ext;
  cv_data;
  
  user_vedio_name = [];
  vedio_ext;
  vedio_data;

  jscvlink = "cv link";
  cvuploadedname = "js";
  id;
  cvlinkfromstorge = "";
  
  vediolinkfromstorge = "";
  private logInForm: FormGroup;

  IBANtxt;
  submitAttempt = false;
  errors = {
    IBANtxtErr: ""
  };
  placeholder = { IBANtxt: "" };
  vediourl;
  desc
  rate = 0
  
  @ViewChild('myvideo') myVideo: any;
  myvdeiofilename;
  vediosrc;
  newvediosrc;
  linkforview;

  hideForShowProfile
  dataForToShow
  noofdone
  noofnotappear

  workercertlinkfromstorge = "";
  user_workercert_name = [];
  workercert_ext;
  workercert_data;

  Workingdays
  joinday

  amorpmTxt
  
  
city_id  
interestedJobs
gender
dob
hoursperweek
hoursperweekTXT
workingDaysInWeekend
gendertxt
citytxt


sunamTime = false
sunpmTime = false

monamTime = false
monpmTime = false

tueamTime = false
tuepmTime = false

wedamTime = false
wedpmTime = false

thuamTime = false
thupmTime = false

friamTime = false
fripmTime = false

satamTime = false
satpmTime = false

hideAll
sunHide
monHide
tueHide
wedHide
thuHide
friHide
satHide
workingDaysAndTimes


cvtxt
vediotxt
certttxt
natString
natid

trysource
newvedioLink

videoType

plat

descid
vedioSize
//day number , 0 both . 1 am .2 pm
  constructor(private videoEditor: VideoEditor,private sanitizer: DomSanitizer,private iab: InAppBrowser,private streamingMedia: StreamingMedia,private formBuilder: FormBuilder, public storage: Storage, public platform: Platform, private camera: Camera,
    public helper: HelperProvider, public navCtrl: NavController, public actionSheetCtrl: ActionSheetController,
    public navParams: NavParams, public translate: TranslateService,public events: Events,
    private filePicker: IOSFilePicker
    , private file: File, private filePath: FilePath,
    private base64: Base64, private fileChooser: FileChooser, public service: ServicesProvider,
    private mediaCapture: MediaCapture, private media: Media) {

    //  console.log("12345 = > ",this.service.DateEnncryption("12345"))

    

    this.langDirection = this.helper.lang_direction;
    if (this.langDirection == "rtl")
      this.scaleClass = "scaleClass";


    this.logInForm = this.formBuilder.group({
      IBANtxt: ['', Validators.compose([Validators.required, Validators.pattern("^(SA)([0-9]{22})")])]

    });


    this.placeholder.IBANtxt = this.translate.instant("enterIBAN");

if(this.navParams.get('from') && this.navParams.get('from') == "showProfile"){
  this.hideForShowProfile = true;
  this.dataForToShow  = this.navParams.get('item')

}else{
  this.hideForShowProfile = false;
  this.dataForToShow = ""
}
    
this.hideAll = true
events.subscribe('profileChangedall', (data) => {

//        this.userData.VideoURL = this.videourl;
    console.log("data from profileChangedall sub :",data)
        this.FirstName = data.FirstName;
        this.mail = data.Email; 
        
        this.descid = data.ShortDescription

        if(data.ShortDescription == "1")
          this.desc = this.translate.instant("student")
        else if(data.ShortDescription == "2")
          this.desc = this.translate.instant("employee")
        else if(data.ShortDescription == "3")
          this.desc = this.translate.instant("unemployed")

        // this.desc = data.ShortDescription;        
          this.IBANtxt = data.IBAN;

          if(data.natString){
            this.natid = data.Nationality
            this.natString = data.natString
          }
          else{
            this.natid = ""
            this.natString = ""
          }
        

          this.Workingdays = data.Workingdays;
          this.amorpmTxt = data.amorpmTxt
          this.city_id = data.city_id
          if(data.interestedJobs)
          this.interestedJobs = data.interestedJobs.split(",")
          else
          this.interestedJobs = ""

          this.joinday = data.joinday
          this.gender = data.gender
          console.log("this.gender = data.gender" ,this.gender ," , ", data.gender)
          if(this.gender == "1")
            this.gendertxt = this.translate.instant("male")
          else if(this.gender == "0")
            this.gendertxt = this.translate.instant("female")

          this.dob = data.dob
          this.hoursperweek = data.hoursperweek
          
          this.workingDaysInWeekend = data.workingDaysInWeekend

          if(this.workingDaysInWeekend == "1")
                this.hoursperweekTXT = this.translate.instant("oneday")
              else if(this.workingDaysInWeekend == "2")
                this.hoursperweekTXT = this.translate.instant("twodays")




                //day number , 0 both . 1 am .2 pm
// 1 sunday


this.sunamTime = false
this.sunpmTime = false

this.monamTime = false
this.monpmTime = false

this.tueamTime = false
this.tuepmTime = false

this.wedamTime = false
this.wedpmTime = false

this.thuamTime = false
this.thupmTime = false

this.friamTime = false
this.fripmTime = false

this.satamTime = false
this.satpmTime = false


this.workingDaysAndTimes = data.workingDaysAndTimes
console.log("profileChangedall data.workingDaysAndTimes : ",data.workingDaysAndTimes)

if(data.workingDaysAndTimes){
  this.hideAll = false
  
  if(data.workingDaysAndTimes.length >=1){
    this.hideAll = false
    var tmp
    for(var j=0;j<data.workingDaysAndTimes.length;j++){
      tmp = data.workingDaysAndTimes[j].split("x")
      if(tmp[0] == "1"){
        this.sunHide = false
        if(tmp[1] == "0"){
          this.sunamTime  = true
          this.sunpmTime = true
        }else if(tmp[1] == "1"){
          this.sunamTime  = true
          this.sunpmTime = false
        }else if(tmp[1] == "2"){
          this.sunamTime  = false
          this.sunpmTime = true
        }
      }
      if(tmp[0] == "2"){
        this.monHide = false
        if(tmp[1] == "0"){
          this.monamTime  = true
          this.monpmTime = true
        }else if(tmp[1] == "1"){
          this.monamTime  = true
          this.monpmTime = false
        }else if(tmp[1] == "2"){
          this.monamTime  = false
          this.monpmTime = true
        }
        
      }
      if(tmp[0] == "3"){
        this.tueHide = false
        if(tmp[1] == "0"){
          this.tueamTime  = true
          this.tuepmTime = true
        }else if(tmp[1] == "1"){
          this.tueamTime  = true
          this.tuepmTime = false
        }else if(tmp[1] == "2"){
          this.tueamTime  = false
          this.tuepmTime = true
        }
      }
      if(tmp[0] == "4"){
        this.wedHide = false
        if(tmp[1] == "0"){
          this.wedamTime  = true
          this.wedpmTime = true
        }else if(tmp[1] == "1"){
          this.wedamTime  = true
          this.wedpmTime = false
        }else if(tmp[1] == "2"){
          this.wedamTime  = false
          this.wedpmTime = true
        }
      }
      if(tmp[0] == "5"){
        this.thuHide = false
        if(tmp[1] == "0"){
          this.thuamTime  = true
          this.thupmTime = true
        }else if(tmp[1] == "1"){
          this.thuamTime  = true
          this.thupmTime = false
        }else if(tmp[1] == "2"){
          this.thuamTime  = false
          this.thupmTime = true
        }
      }
      if(tmp[0] == "6"){
        this.friHide = false
        if(tmp[1] == "0"){
          this.friamTime  = true
          this.fripmTime = true
        }else if(tmp[1] == "1"){
          this.friamTime  = true
          this.fripmTime = false
        }else if(tmp[1] == "2"){
          this.friamTime  = false
          this.fripmTime = true
        }
      }
      if(tmp[0] == "7"){
        this.satHide = false
        if(tmp[1] == "0"){
          this.satamTime  = true
          this.satpmTime = true
        }else if(tmp[1] == "1"){
          this.satamTime  = true
          this.satpmTime = false
        }else if(tmp[1] == "2"){
          this.satamTime  = false
          this.satpmTime = true
        }
      }



    }
  }else{
    this.hideAll = true  
  }
}else{
  this.hideAll = true
}

     


 });



  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad JsProfilePage');

    if (this.platform.is('ios')) {
      this.videoType = "video/MOV"
      this.plat = 'ios'
      console.log('this.videoType: ',this.videoType)
    }else if (this.platform.is('android')) {
      this.videoType = "video/mp4"
      this.plat = 'android'
      console.log('this.videoType: ',this.videoType)
    }
    this.cvtxt = this.translate.instant("view")
    this.vediotxt =  this.translate.instant("viewvedio")
    this.certttxt =  this.translate.instant("viewWorkerCertificate")

    this.jscvlink = "cv link";
    this.cvuploadedname = "js";
    if(this.hideForShowProfile == false){
    this.storage.get('js_info').then((val) => {
      console.log("val from get js info from enter profile", val);
      if (val) {
        this.userData = val;
        this.FirstName = val.FirstName;
        // this.job = val.job;
        this.userImageUrl = val.ProfileImage;
        this.id = val.SID;
        this.mail = val.Email;
        this.mobile = val.Mobile;
        // this.desc = val.ShortDescription;

      this.descid = val.ShortDescription
      
      if(val.ShortDescription == "1")
        this.desc = this.translate.instant("student")
      else if(val.ShortDescription == "2")
        this.desc = this.translate.instant("employee")
      else if(val.ShortDescription == "3")
        this.desc = this.translate.instant("unemployed")

        if(val.natString){
          this.natid = val.Nationality
          this.natString = val.natString
        }
        else{
          this.natid = ""
          this.natString = ""
        }

        if (val.CVFile)
          this.cvlinkfromstorge = val.CVFile;
        else
          this.cvlinkfromstorge = "";

        if (val.IBAN)
          this.IBANtxt = val.IBAN;
        else
          this.IBANtxt = "";

        if(val.VideoURL){
          this.vediolinkfromstorge = val.VideoURL;
           this.newvedioLink =  this.sanitizer.bypassSecurityTrustResourceUrl(this.vediolinkfromstorge)
        }
          
        else
          this.vediolinkfromstorge = "";

          if(val.CertFile)
          this.workercertlinkfromstorge = val.CertFile;
        else
          this.workercertlinkfromstorge = "";

          console.log("val.Workingdays : ",val.Workingdays);
          if(val.Workingdays)
            this.Workingdays = val.Workingdays
          else
            this.Workingdays = ""
          console.log("this.Workingdays :",this.Workingdays)

          if(val.joinday)
            this.joinday = val.joinday
          else
            this.joinday = ""

            console.log("joinday ",this.joinday)
        if(val.amorpmTxt)
          this.amorpmTxt = val.amorpmTxt
        else
          this.amorpmTxt = ""
          
          if(val.city_id)
            this.city_id = val.city_id
          else
          this.city_id = ""

          if(val.interestedJobs)
            this.interestedJobs = val.interestedJobs.split(",")
          else
          this.interestedJobs =""

            if(val.gender){
              
              this.gender = val.gender
              console.log("this.gender = val.gender : ",this.gender , " , ",this.gender)
              if(this.gender == "1"){
                this.gendertxt = this.translate.instant("male")
                console.log("gendertxt from 1: ",this.gendertxt)
              } 
              else if(this.gender == "0"){
                this.gendertxt = this.translate.instant("female")  
                console.log("gendertxt from 2: ",this.gendertxt)
              }
                
            }else
              this.gender = ""
            
            if(val.dob)
              this.dob = val.dob
            else
            this.dob = ""

            if(val.hoursperweek){
              this.hoursperweek = val.hoursperweek
             
            }  
            else
              this.hoursperweek = ""

              if(val.workingDaysInWeekend){

                this.workingDaysInWeekend = val.workingDaysInWeekend
                if(this.workingDaysInWeekend == "1")
                this.hoursperweekTXT = this.translate.instant("oneday")
              else if(this.workingDaysInWeekend == "2")
                this.hoursperweekTXT = this.translate.instant("twodays")
              }
                
              else
                this.workingDaysInWeekend = ""




this.workingDaysAndTimes = val.workingDaysAndTimes

console.log("val.workingDaysAndTimes : ",val.workingDaysAndTimes)
                if(val.workingDaysAndTimes){
                  this.hideAll = false
                  console.log("if 1 ")
                  if(val.workingDaysAndTimes.length >=1){
                    this.hideAll = false
                    var tmp
                    for(var j=0;j<val.workingDaysAndTimes.length;j++){
                      tmp = val.workingDaysAndTimes[j].split("x")
                      if(tmp[0] == "1"){
                        this.sunHide = false
                        if(tmp[1] == "0"){
                          this.sunamTime  = true
                          this.sunpmTime = true
                        }else if(tmp[1] == "1"){
                          this.sunamTime  = true
                          this.sunpmTime = false
                        }else if(tmp[1] == "2"){
                          this.sunamTime  = false
                          this.sunpmTime = true
                        }
                      }
                      if(tmp[0] == "2"){
                        this.monHide = false
                        if(tmp[1] == "0"){
                          this.monamTime  = true
                          this.monpmTime = true
                        }else if(tmp[1] == "1"){
                          this.monamTime  = true
                          this.monpmTime = false
                        }else if(tmp[1] == "2"){
                          this.monamTime  = false
                          this.monpmTime = true
                        }
                        
                      }
                      if(tmp[0] == "3"){
                        this.tueHide = false
                        if(tmp[1] == "0"){
                          this.tueamTime  = true
                          this.tuepmTime = true
                        }else if(tmp[1] == "1"){
                          this.tueamTime  = true
                          this.tuepmTime = false
                        }else if(tmp[1] == "2"){
                          this.tueamTime  = false
                          this.tuepmTime = true
                        }
                      }
                      if(tmp[0] == "4"){
                        this.wedHide = false
                        if(tmp[1] == "0"){
                          this.wedamTime  = true
                          this.wedpmTime = true
                        }else if(tmp[1] == "1"){
                          this.wedamTime  = true
                          this.wedpmTime = false
                        }else if(tmp[1] == "2"){
                          this.wedamTime  = false
                          this.wedpmTime = true
                        }
                      }
                      if(tmp[0] == "5"){
                        this.thuHide = false
                        if(tmp[1] == "0"){
                          this.thuamTime  = true
                          this.thupmTime = true
                        }else if(tmp[1] == "1"){
                          this.thuamTime  = true
                          this.thupmTime = false
                        }else if(tmp[1] == "2"){
                          this.thuamTime  = false
                          this.thupmTime = true
                        }
                      }
                      if(tmp[0] == "6"){
                        this.friHide = false
                        if(tmp[1] == "0"){
                          this.friamTime  = true
                          this.fripmTime = true
                        }else if(tmp[1] == "1"){
                          this.friamTime  = true
                          this.fripmTime = false
                        }else if(tmp[1] == "2"){
                          this.friamTime  = false
                          this.fripmTime = true
                        }
                      }
                      if(tmp[0] == "7"){
                        this.satHide = false
                        if(tmp[1] == "0"){
                          this.satamTime  = true
                          this.satpmTime = true
                        }else if(tmp[1] == "1"){
                          this.satamTime  = true
                          this.satpmTime = false
                        }else if(tmp[1] == "2"){
                          this.satamTime  = false
                          this.satpmTime = true
                        }
                      }
                
                
                
                
                
                
                
                    }
                  }else{
                    this.hideAll = true  
                  }
                }else{
                  this.hideAll = true
                }
                

                



      }



    }).catch(err => {
      console.log("catch from get js info", err);

    });

  }else if(this.hideForShowProfile == true){

    this.FirstName = this.dataForToShow.FirstName;
    this.userImageUrl = this.dataForToShow.ProfileImage;
    // this.mail = this.dataForToShow.Email;
    this.mobile = this.dataForToShow.Mobile;
    // this.desc = this.dataForToShow.ShortDescription;

    this.descid = this.dataForToShow.ShortDescription

    if(this.dataForToShow.ShortDescription == "1")
      this.desc = this.translate.instant("student")
    else if(this.dataForToShow.ShortDescription == "2")
      this.desc = this.translate.instant("employee")
    else if(this.dataForToShow.ShortDescription == "3")
      this.desc = this.translate.instant("unemployed")


    this.id = this.dataForToShow.JobSeekerSID;
    if (this.dataForToShow.CVFile)
      this.cvlinkfromstorge = this.dataForToShow.CVFile;
    else
      this.cvlinkfromstorge = "";

    if(this.dataForToShow.VideoURL)
      this.vediourl = this.dataForToShow.VideoURL;
    else
      this.vediourl = "";

      if(this.dataForToShow.ReviewPercentage)
      this.rate = this.dataForToShow.ReviewPercentage;
    else
      this.rate = 0

      if(this.dataForToShow.CompletedJobs)
      this.noofdone = this.dataForToShow.CompletedJobs;
    else
      this.noofdone = 0

      if(this.dataForToShow.Email)
        this.mail =  this.dataForToShow.Email
      else
        this.mail = ""

      if(this.helper.lang_direction == "rtl")
      this.natString = this.dataForToShow.Nationality_Ar
      else if(this.helper.lang_direction == "ltr")
      this.natString = this.dataForToShow.Nationality_En

      if(this.dataForToShow.DateOfBirth && this.dataForToShow.DateOfBirth == "0001-01-01T00:00:00")
      this.dob = ""
      else if (!  this.dataForToShow.DateOfBirth)
      this.dob = ""
      else
        this.dob = this.dataForToShow.DateOfBirth.split("T")[0]

        
        if (this.dataForToShow.VideoURL){
          this.vediolinkfromstorge  = this.dataForToShow.VideoURL;
          this.newvedioLink =  this.sanitizer.bypassSecurityTrustResourceUrl(this.vediolinkfromstorge)
        }
          
        else
          this.vediolinkfromstorge  = "";


          if (this.dataForToShow.CertFile)
            this.workercertlinkfromstorge   = this.dataForToShow.CertFile ;
          else
            this.workercertlinkfromstorge   = "";
            
            
            if(this.dataForToShow.WeekEndWorkingDays == "1")
              this.hoursperweekTXT = this.translate.instant("oneday")
            else if(this.dataForToShow.WeekEndWorkingDays == "2")
              this.hoursperweekTXT = this.translate.instant("twodays")
 
              
              if (this.dataForToShow.WeeklyHoursCount)
                this.hoursperweek   = this.dataForToShow.WeeklyHoursCount ;
              else
                this.hoursperweek   = "";

        if(this.dataForToShow.Gender == "1"){
          this.gendertxt = this.translate.instant("male")
          console.log("gendertxt from 1: ",this.gendertxt)
        } 
        else if(this.dataForToShow.Gender == "0"){
          this.gendertxt = this.translate.instant("female")  
          console.log("gendertxt from 2: ",this.gendertxt)
        }
         

        this.workingDaysAndTimes = this.dataForToShow.DaysAvailability

        if(this.dataForToShow.DaysAvailability){
          this.hideAll = false
          if(this.dataForToShow.DaysAvailability.length >=3){
            this.hideAll = false
            var tmp
            console.log("this.dataForToShow.DaysAvailability : ",this.dataForToShow.DaysAvailability)
            var arr = this.dataForToShow.DaysAvailability.split(",")
            console.log("arr : ",arr)
            for(var j=0;j<arr.length;j++){
              tmp = arr[j].split("x")
              if(tmp[0] == "1"){
                this.sunHide = false
                if(tmp[1] == "0"){
                  this.sunamTime  = true
                  this.sunpmTime = true
                }else if(tmp[1] == "1"){
                  this.sunamTime  = true
                  this.sunpmTime = false
                }else if(tmp[1] == "2"){
                  this.sunamTime  = false
                  this.sunpmTime = true
                }
              }
              if(tmp[0] == "2"){
                this.monHide = false
                if(tmp[1] == "0"){
                  this.monamTime  = true
                  this.monpmTime = true
                }else if(tmp[1] == "1"){
                  this.monamTime  = true
                  this.monpmTime = false
                }else if(tmp[1] == "2"){
                  this.monamTime  = false
                  this.monpmTime = true
                }
                
              }
              if(tmp[0] == "3"){
                this.tueHide = false
                if(tmp[1] == "0"){
                  this.tueamTime  = true
                  this.tuepmTime = true
                }else if(tmp[1] == "1"){
                  this.tueamTime  = true
                  this.tuepmTime = false
                }else if(tmp[1] == "2"){
                  this.tueamTime  = false
                  this.tuepmTime = true
                }
              }
              if(tmp[0] == "4"){
                this.wedHide = false
                if(tmp[1] == "0"){
                  this.wedamTime  = true
                  this.wedpmTime = true
                }else if(tmp[1] == "1"){
                  this.wedamTime  = true
                  this.wedpmTime = false
                }else if(tmp[1] == "2"){
                  this.wedamTime  = false
                  this.wedpmTime = true
                }
              }
              if(tmp[0] == "5"){
                this.thuHide = false
                if(tmp[1] == "0"){
                  this.thuamTime  = true
                  this.thupmTime = true
                }else if(tmp[1] == "1"){
                  this.thuamTime  = true
                  this.thupmTime = false
                }else if(tmp[1] == "2"){
                  this.thuamTime  = false
                  this.thupmTime = true
                }
              }
              if(tmp[0] == "6"){
                this.friHide = false
                if(tmp[1] == "0"){
                  this.friamTime  = true
                  this.fripmTime = true
                }else if(tmp[1] == "1"){
                  this.friamTime  = true
                  this.fripmTime = false
                }else if(tmp[1] == "2"){
                  this.friamTime  = false
                  this.fripmTime = true
                }
              }
              if(tmp[0] == "7"){
                this.satHide = false
                if(tmp[1] == "0"){
                  this.satamTime  = true
                  this.satpmTime = true
                }else if(tmp[1] == "1"){
                  this.satamTime  = true
                  this.satpmTime = false
                }else if(tmp[1] == "2"){
                  this.satamTime  = false
                  this.satpmTime = true
                }
              }
        
        
        
        
        
        
        
            }
          }else{
            this.hideAll = true  
          }
        }else{
          this.hideAll = true
        }
        

        


  }


  }
opengalleryToLoadVedio(){
      // Create options for the Camera Dialog
      var options = {
        quality: 0, //50
        destinationType: this.camera.DestinationType.FILE_URI,
        // encodingType: this.camera.EncodingType.,
        mediaType: this.camera.MediaType.VIDEO,
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        saveToPhotoAlbum: false,
        correctOrientation: true,
       
      };
      this.camera.getPicture(options).then((vediodata: string) => {
        console.log("vediodata : ",vediodata)
// var vx = "file://"+vediodata
var vx = vediodata
        this.file.resolveLocalFilesystemUrl(vx).then(fileEntry => {
          console.log("file entry : ",fileEntry)
          fileEntry.getMetadata((metadata) => {

              console.log("meta data from resolveLocalFilesystemUrl : ",metadata);//metadata.size is the size in bytes
              // metadata.size: 20761
this.vedioSize = (metadata.size / 1024)/1024
              console.log("(metadata.size / 1024)/1024 : ",(metadata.size / 1024)/1024)
              if((metadata.size / 1024)/1024 > 2){
               this.helper.presentToast(this.translate.instant("vediolargerthan2mega"))
               return
              }

          });
          
      });
console.log("after resolveLocalFilesystemUrl ");
        // imageData is either a base64 encoded string or a file URI
        // If it's base64:
         //  this.userImageUrl = 'data:image/jpeg;base64,' + imageData
        //   this.storage.set("user_image",this.userImageUrl)
    
        //   var VideoEditorOptions = {
      //     OptimizeForNetworkUse: {
      //         NO: 0,
      //         YES: 1
      //     },
      //     OutputFileType: {
      //         M4V: 0,
      //         MPEG4: 1,
      //         M4A: 2,
      //         QUICK_TIME: 3
      //     }
      // }
       
        //   this.videoEditor.transcodeVideo({
        //     fileUri: vediodata,
        //     outputFileName: 'output.mp4',
        //     outputFileType: VideoEditorOptions.OutputFileType.MPEG4,
        //   width: 640, // optional, see note below on width and height
        // height: 640,
          
        //   })
        //   .then((fileUri: string) => console.log('video transcode success', fileUri))
        //   .catch((error: any) => console.log('video transcode error', error));
if(this.vedioSize <= 2){
          
          if (this.platform.is('android')) {
            console.log("android vedio data from galley: ",vediodata)
          var vdata = vediodata
          let filename = vdata.substr(vdata.lastIndexOf('/') + 1)
                this.vediotxt = filename;
                this.user_vedio_name.push(filename)
                let fileExt = filename.split('.').pop();
          
          this.convertCapturedVedioToBase64("file://"+vediodata);
          }else  if (this.platform.is('ios')) {
            console.log("ios vedio data from galley: ",vediodata)
            let correctPath = vediodata.substr(0, vediodata.lastIndexOf('/') + 1);
            console.log("correctPath : ",correctPath);
            let filename = vediodata.substr(vediodata.lastIndexOf('/') + 1)
            console.log("filename :",filename)
            this.vediotxt = filename;
            this.user_vedio_name.push(filename)
            let fileExt = filename.split('.').pop();
            this.vedio_ext = fileExt
            var cvextuper = this.vedio_ext.toUpperCase();
            if (cvextuper == "MP4".toUpperCase() || cvextuper == "AVI".toUpperCase() || cvextuper == "FLV".toUpperCase() || cvextuper == "WMV".toUpperCase() || cvextuper == "MOV".toUpperCase() ) {
              console.log("from if : ", cvextuper);
              // this.helper.presentToast(this.translate.instant("fileupsu"));
              // "file://" + 
              this.file.readAsDataURL(correctPath, filename).then((val) => {
                this.vedio_data = encodeURIComponent(val.split(",")[1]);
    
                console.log("this.vedio_data: ", this.vedio_data);
                console.log("this.user_vedio_name : ", this.user_vedio_name);
                console.log("this.vedio_ext : ", this.vedio_ext)
                this.sendVedioToApi();
              
              }).catch(err => console.log('Error reader' + err));
    
            } else {
              console.log("from else : ", cvextuper);
              this.helper.presentToast(this.translate.instant("fileupnoex"));
              this.vedio_data = "";
              this.vedio_ext = "";
              this.vediotxt =  this.translate.instant("viewvedio")
              this.user_vedio_name = [];
            }
          }


        }//
       
      }, (err) => {
        // Handle error
      });
}
  public takePictureforworker(sourceType,typex) {
    // Create options for the Camera Dialog
    var options = {
      quality: 100, //50
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true,
      allowEdit:true,
      targetWidth:200,
      targetHeight:200
    };
    this.camera.getPicture(options).then((imageData: string) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.storage.get("js_info").then((val) => {
       // this.userImageUrl = 'data:image/jpeg;base64,' + imageData
        //this.storage.set("user_image",this.userImageUrl)
        let imgdata = encodeURIComponent(imageData)
        // this.certttxt = this.translate.instant("imageCaptured");
        if(typex == 1 )
        this.certttxt = this.translate.instant("imageChoosed")
        else if(typex == 2)
        this.certttxt = this.translate.instant("imageCaptured")
        
        this.service.jsaddworkercert(val.SID, imgdata, 'jpeg', resp => {
          console.log("resp from compaddcr", resp);
          if (JSON.parse(resp).OperationResult == "1") {
            this.workercertlinkfromstorge = JSON.parse(resp).filePath;

            // this.helper.presentToast(this.translate.instant("fileupsu"));
             this.helper.presentToast(this.translate.instant("certupsu"));
            this.userData.workercertFile = this.workercertlinkfromstorge;
            this.helper.storeJSInfo(this.userData);
          }
          else
            this.helper.presentToast(this.translate.instant("conectionError"));
        }, err => {
          console.log("err from compaddcr", err);
          this.helper.presentToast(this.translate.instant("conectionError"));

        })


      })
    }, (err) => {
      // Handle error
    });
  }
  public takePictureforcv(sourceType,typex) {
    // Create options for the Camera Dialog
    var options = {
      quality: 100, //50
     destinationType: this.camera.DestinationType.DATA_URL,
    //  destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true,
      allowEdit:true,
      targetWidth:200,
      targetHeight:200
    };
    this.camera.getPicture(options).then((imageData: string) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.storage.get("js_info").then((val) => {
       // this.userImageUrl = 'data:image/jpeg;base64,' + imageData
        //this.storage.set("user_image",this.userImageUrl)
        //c
        let imgdata = encodeURIComponent(imageData)
        //c
        // let filename = imageData.substring(imageData.lastIndexOf('/')+1);
        // this.cvtxt = filename
        // let path =  imageData.substring(0,imageData.lastIndexOf('/')+1);
        //      //then use the method reasDataURL  btw. var_picture is ur image variable
        //      this.file.readAsDataURL(path, filename).then(res=> {
        //       console.log("res : ",res);
        //       console.log("res.split(,)[1] : ",res.split(",")[1]);

        //       let imgdata = encodeURIComponent(res.split(",")[1]);

        //       this.service.jsaddcv(val.SID, imgdata, 'jpeg', resp => {
        //         console.log("resp from compaddcr", resp);
        //         if (JSON.parse(resp).OperationResult == "1") {
        //           this.cvlinkfromstorge = JSON.parse(resp).filePath;
      
        //           // this.helper.presentToast(this.translate.instant("fileupsu"));
        //           this.helper.presentToast(this.translate.instant("cvupsu"));
        //           this.userData.CVFile = this.cvlinkfromstorge;
        //           this.helper.storeJSInfo(this.userData);
        //         }
        //         else
        //           this.helper.presentToast(this.translate.instant("conectionError"));
        //       }, err => {
        //         console.log("err from compaddcr", err);
        //         this.helper.presentToast(this.translate.instant("conectionError"));
      
        //       })


        //      }
              
        //      );

//c
        this.service.jsaddcv(val.SID, imgdata, 'jpeg', resp => {
          console.log("resp from compaddcr", resp);
          if (JSON.parse(resp).OperationResult == "1") {
            this.cvlinkfromstorge = JSON.parse(resp).filePath;
            if(typex == 1 )
            this.cvtxt = this.translate.instant("imageChoosed")
            else if(typex == 2)
            this.cvtxt = this.translate.instant("imageCaptured")
            // this.helper.presentToast(this.translate.instant("fileupsu"));
            this.helper.presentToast(this.translate.instant("cvupsu"));
            this.userData.CVFile = this.cvlinkfromstorge;
            this.helper.storeJSInfo(this.userData);
          }
          else
            this.helper.presentToast(this.translate.instant("conectionError"));
        }, err => {
          console.log("err from compaddcr", err);
          this.helper.presentToast(this.translate.instant("conectionError"));

        })
//c

      })
    }, (err) => {
      // Handle error
    });
  }

  public takePicture(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      quality: 100, //50
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true,
      allowEdit:true,
      targetWidth:200,
      targetHeight:200
    };
    this.camera.getPicture(options).then((imageData: string) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.storage.get("js_info").then((val) => {
       // this.userImageUrl = 'data:image/jpeg;base64,' + imageData
        //this.storage.set("user_image",this.userImageUrl)
        let imgdata = encodeURIComponent(imageData)
        this.service.changeJSProfilePic(imgdata, 'jpeg', val.SID, (data) => {
          if (data.OperationResult == "1") {
           this.helper.presentToast(this.translate.instant("profileImgUpdated"))
           this.events.publish("profileImageChanged",data.filePath)

            this.userImageUrl = data.filePath;

            // this.storage.get("js_info").then(val =>{
            //   let jsInfo = val
            //   jsInfo.RegistryFile = this.userImageUrl
            //   this.storage.set("js_info",jsInfo)
            // })

            this.storage.get('js_info').then((val) => {
              console.log("val from get js info", val);
              if (val) {
                var newuserData = val;
           
              newuserData.ProfileImage = this.userImageUrl;
            this.helper.storeJSInfo(newuserData);
              }});

          }
          else {
            this.translate.instant("conectionError")
          }
        }, (data) => this.translate.instant("conectionError"))
      })
    }, (err) => {
      // Handle error
    });
  }
  selectImage() {

    let actionSheet = this.actionSheetCtrl.create({
      title: this.translate.instant("SelectImageSource"),
      buttons: [
        {
          text: this.translate.instant("LoadfromLibrary"),
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: this.translate.instant("UseCamera"),
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: this.translate.instant("cancel"),
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();

  }
  selectWorkerCertificate(){
    let actionSheet = this.actionSheetCtrl.create({
      title: this.translate.instant("selectworkersource"),
      buttons: [
        {
          text: this.translate.instant("LoadfromFiles"),
          handler: () => {
            this.getworkerCertificates();
          }
        },
        {
          text: this.translate.instant("LoadfromLibrary"),
          handler: () => {
            this.takePictureforworker(this.camera.PictureSourceType.PHOTOLIBRARY,1);
          }
        },

        {
          text: this.translate.instant("UseCamera"),
          handler: () => {
            this.takePictureforworker(this.camera.PictureSourceType.CAMERA,2);
          }
        },
        {
          text: this.translate.instant("cancel"),
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();

  }
  selectcv() {

    let actionSheet = this.actionSheetCtrl.create({
      title: this.translate.instant("SelectcvSource"),
      buttons: [
        {
          text: this.translate.instant("LoadfromFiles"),
          handler: () => {
            this.getCertificates();
          }
        },
        {
          text: this.translate.instant("LoadfromLibrary"),
          handler: () => {
            this.takePictureforcv(this.camera.PictureSourceType.PHOTOLIBRARY,1);
          }
        },
        {
          text: this.translate.instant("UseCamera"),
          handler: () => {
            this.takePictureforcv(this.camera.PictureSourceType.CAMERA,2);
          }
        },
        {
          text: this.translate.instant("cancel"),
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();

  }
  ionViewWillEnter() {

    console.log('ionViewWillEnter JsProfilePage');
    if(this.hideForShowProfile == false){
      this.storage.get('js_info').then((val) => {
        console.log("val from get js info from enter profile", val);
        if (val) {
          this.id = val.SID;

          this.service.getUserDetails(this.id,resp=>{
            console.log("resp from getUserDetails",JSON.parse(resp));
            var respData = JSON.parse(resp)
            if(respData.Result == "1"){

             
              if( respData.details.VideoURL){
                this.vediolinkfromstorge =  respData.details.VideoURL;
                this.newvedioLink =  this.sanitizer.bypassSecurityTrustResourceUrl(this.vediolinkfromstorge)
              }
                
              else
                this.vediolinkfromstorge = "";

              this.city_id = respData.details.City_SID;
              if(respData.details.ReviewPercentage)
                this.rate = respData.details.ReviewPercentage;
              else
                this.rate = 0

                if(respData.details.CompletedJobs)
                this.noofdone = respData.details.CompletedJobs;
              else
                this.noofdone = 0


                 

              if(respData.details.PreferedJobCategories){
                console.log("respData.PreferedJobCategories :",respData.details.PreferedJobCategories)
                this.interestedJobs = respData.details.PreferedJobCategories.split(",")
              }
              // Gender
              if(respData.details){
                console.log("if respData.details.Gende")
                this.gender = respData.details.Gender
              
                if(this.gender == "1")
                  this.gendertxt = this.translate.instant("male")
                else if(this.gender == "0")
                  this.gendertxt = this.translate.instant("female")
              }
              


              if(respData.details.CertFile){
                this.workercertlinkfromstorge = respData.details.CertFile
              }


              console.log("from details gender : ",this.gender," : txt ",this.gendertxt,"respData.details.Gender:",respData.details.Gender)
              
              // if(respData.details.Birthdate)
              // this.dob = respData.details.Birthdate

              
              if(respData.details.DateOfBirth && respData.details.DateOfBirth == "0001-01-01T00:00:00")
              this.dob = ""
              else if (!  respData.details.DateOfBirth)
              this.dob = ""
              else
              this.dob = respData.details.DateOfBirth.split("T")[0]

              if(respData.details.WeeklyHoursCount){
                
                this.hoursperweek = respData.details.WeeklyHoursCount
              
              }
                

              if(respData.details.WeekEndWorkingDays){
                this.workingDaysInWeekend = respData.details.WeekEndWorkingDays
                if(this.workingDaysInWeekend == "1")
                this.hoursperweekTXT = this.translate.instant("oneday")
              else if(this.workingDaysInWeekend == "2")
                this.hoursperweekTXT = this.translate.instant("twodays")
              }

              if(this.helper.lang_direction == "rtl"){
                if(respData.details.CityName_Ar)
                this.citytxt = respData.details.CityName_Ar
                
                if(respData.details.Nationality_Ar)
                  this.natString = respData.details.Nationality_Ar

                this.natid = respData.details.NationalitySID
              
              }else  if(this.helper.lang_direction == "ltr")
              {
                if(respData.details.CityName_En)
                this.citytxt = respData.details.CityName_En

                if(respData.details.Nationality_En)
                  this.natString = respData.details.Nationality_En

                  this.natid = respData.details.NationalitySID
              }

              
            
                
            }
              
           console.log(" respData.details.DaysAvailability : ", respData.details.DaysAvailability)
           
           
this.workingDaysAndTimes = respData.details.DaysAvailability
            if(respData.details.DaysAvailability){
              console.log(" respData.details.DaysAvailability.length : ", respData.details.DaysAvailability.length)
              this.hideAll = false
              if(respData.details.DaysAvailability.length >=3){
                this.hideAll = false
                var tmp
                var arr = respData.details.DaysAvailability.split(",")
                console.log("arr from details : ",arr)
                for(var j=0;j<arr.length;j++){
                  tmp = arr[j].split("x")
                  console.log("tmp[]",tmp , " , j :",j)
                  if(tmp[0] == "1"){
                    this.sunHide = false
                    if(tmp[1] == "0"){
                      this.sunamTime  = true
                      this.sunpmTime = true
                    }else if(tmp[1] == "1"){
                      this.sunamTime  = true
                      this.sunpmTime = false
                    }else if(tmp[1] == "2"){
                      this.sunamTime  = false
                      this.sunpmTime = true
                    }
                  }
                  if(tmp[0] == "2"){
                    this.monHide = false
                    if(tmp[1] == "0"){
                      this.monamTime  = true
                      this.monpmTime = true
                    }else if(tmp[1] == "1"){
                      this.monamTime  = true
                      this.monpmTime = false
                    }else if(tmp[1] == "2"){
                      this.monamTime  = false
                      this.monpmTime = true
                    }
                    
                  }
                  if(tmp[0] == "3"){
                    this.tueHide = false
                    if(tmp[1] == "0"){
                      this.tueamTime  = true
                      this.tuepmTime = true
                    }else if(tmp[1] == "1"){
                      this.tueamTime  = true
                      this.tuepmTime = false
                    }else if(tmp[1] == "2"){
                      this.tueamTime  = false
                      this.tuepmTime = true
                    }
                  }
                  if(tmp[0] == "4"){
                    this.wedHide = false
                    if(tmp[1] == "0"){
                      this.wedamTime  = true
                      this.wedpmTime = true
                    }else if(tmp[1] == "1"){
                      this.wedamTime  = true
                      this.wedpmTime = false
                    }else if(tmp[1] == "2"){
                      this.wedamTime  = false
                      this.wedpmTime = true
                    }
                  }
                  if(tmp[0] == "5"){
                    this.thuHide = false
                    if(tmp[1] == "0"){
                      this.thuamTime  = true
                      this.thupmTime = true
                    }else if(tmp[1] == "1"){
                      this.thuamTime  = true
                      this.thupmTime = false
                    }else if(tmp[1] == "2"){
                      this.thuamTime  = false
                      this.thupmTime = true
                    }
                  }
                  if(tmp[0] == "6"){
                    this.friHide = false
                    if(tmp[1] == "0"){
                      this.friamTime  = true
                      this.fripmTime = true
                    }else if(tmp[1] == "1"){
                      this.friamTime  = true
                      this.fripmTime = false
                    }else if(tmp[1] == "2"){
                      this.friamTime  = false
                      this.fripmTime = true
                    }
                  }
                  if(tmp[0] == "7"){
                    this.satHide = false
                    if(tmp[1] == "0"){
                      this.satamTime  = true
                      this.satpmTime = true
                    }else if(tmp[1] == "1"){
                      this.satamTime  = true
                      this.satpmTime = false
                    }else if(tmp[1] == "2"){
                      this.satamTime  = false
                      this.satpmTime = true
                    }
                  }
            
            
            
            
            
            
            
                }
              }else{
                this.hideAll = true  
              }
            }else{
              this.hideAll = true
            }
      
            



          },err=>{
            console.log("err from getUserDetails",err);
          });
        }
          
        })}

    

  //   this.jscvlink = "cv link";
  //   this.cvuploadedname = "js";
  //   if(this.hideForShowProfile == false){
  //   this.storage.get('js_info').then((val) => {
  //     console.log("val from get js info from enter profile", val);
  //     if (val) {
  //       this.userData = val;
  //       this.FirstName = val.FirstName;
  //       // this.job = val.job;
  //       this.userImageUrl = val.ProfileImage;
  //       this.id = val.SID;
  //       this.mail = val.Email;
  //       this.mobile = val.Mobile;
  //       this.desc = val.ShortDescription;

  //       if (val.CVFile)
  //         this.cvlinkfromstorge = val.CVFile;
  //       else
  //         this.cvlinkfromstorge = "";

  //       if (val.IBAN)
  //         this.IBANtxt = val.IBAN;
  //       else
  //         this.IBANtxt = "";

  //       if(val.VideoURL)
  //         this.vediourl = val.VideoURL;
  //       else
  //         this.vediourl = "";
  //     }



  //   }).catch(err => {
  //     console.log("catch from get js info", err);

  //   });

  // }else if(this.hideForShowProfile == true){

  //   this.FirstName = this.dataForToShow.FirstName;
  //   this.userImageUrl = this.dataForToShow.ProfileImage;
  //   // this.mail = this.dataForToShow.Email;
  //   this.mobile = this.dataForToShow.Mobile;
  //   this.desc = this.dataForToShow.ShortDescription;
  //   this.id = this.dataForToShow.JobSeekerSID;
  //   if (this.dataForToShow.CVFile)
  //     this.cvlinkfromstorge = this.dataForToShow.CVFile;
  //   else
  //     this.cvlinkfromstorge = "";

  //   if(this.dataForToShow.VideoURL)
  //     this.vediourl = this.dataForToShow.VideoURL;
  //   else
  //     this.vediourl = "";


  // }

  // this.events.subscribe('profileChangedall', (data) => {

  //   //        this.userData.VideoURL = this.videourl;
  //       console.log("data from profileChangedall sub :",data)
  //           this.FirstName = data.FirstName;
  //           this.mail = data.Email; 
  //           this.desc = data.ShortDescription;        
  //             this.IBANtxt = data.IBAN;
    
    
  //    });

  }

  dismiss() {
    this.navCtrl.pop();
  }
  openProfileImg() {

  }


  study() {
    this.navCtrl.push('StudyPage',{userId:this.id,from:this.hideForShowProfile });
  }
  experience() {
    this.navCtrl.push('ExperiencePage',{userId:this.id,from:this.hideForShowProfile});
  }


  editcr() {
    console.log("edit cr");
    this.getCertificates();
  }
  getCertificates() {
    if (this.platform.is('ios')) {
      this.filePicker.pickFile()
        .then(uri => {
          console.log("ios uri :" ,uri)
          let correctPath = uri.substr(0, uri.lastIndexOf('/') + 1);
          console.log("correctPath : ",correctPath)
          let filename = uri.substr(uri.lastIndexOf('/') + 1)
          console.log("filename : ",filename);
          this.user_cv_name.push(filename)
          this.cvtxt = filename;
          let fileExt = filename.split('.').pop();
          this.cv_ext = fileExt
          var cvextuper = this.cv_ext.toUpperCase();
          if (cvextuper == "pdf".toUpperCase() || cvextuper == "docx".toUpperCase() || cvextuper == "doc".toUpperCase() || cvextuper == "JPEG".toUpperCase() || cvextuper == "PNG".toUpperCase() || cvextuper == "JPG".toUpperCase() || cvextuper == "GIF".toUpperCase() || cvextuper == "BMP".toUpperCase()) {
            console.log("from if : ", cvextuper);
            // this.helper.presentToast(this.translate.instant("fileupsu"));

            var vx = "file:///"+correctPath
            this.file.resolveLocalFilesystemUrl(vx).then(fileEntry => {
              console.log("file entry : ",fileEntry)
              fileEntry.getMetadata((metadata) => {
    
                  console.log("worker cert meta data from resolveLocalFilesystemUrl : ",metadata);//metadata.size is the size in bytes
                  // metadata.size: 20761
    // this.vedioSize = (metadata.size / 1024)/1024
                  console.log("(metadata.size / 1024)/1024 : ",(metadata.size / 1024)/1024)
                  if((metadata.size / 1024)/1024 > 1){
                   this.helper.presentToast(this.translate.instant("documentLargerThan1Mega"))
                   this.cvtxt = this.translate.instant("view")
                   
                  }else{


            this.file.readAsDataURL("file:///" + correctPath, filename).then((val) => {
              this.cv_data = encodeURIComponent(val.split(",")[1]);
  
              console.log("this.cv_data: ", this.cv_data);
              console.log("this.user_cv_name : ", this.user_cv_name);
              console.log("this.cv_ext : ", this.cv_ext)
              this.service.jsaddcv(this.id, this.cv_data, this.cv_ext, resp => {
                console.log("resp from compaddcr", resp);
                if (JSON.parse(resp).OperationResult == "1") {
                  this.cvlinkfromstorge = JSON.parse(resp).filePath;
  
                  this.helper.presentToast(this.translate.instant("cvupsu"));
                  this.userData.CVFile = this.cvlinkfromstorge;
                  this.helper.storeJSInfo(this.userData);
                }
                else
                  this.helper.presentToast(this.translate.instant("conectionError"));
              }, err => {
                console.log("err from compaddcr", err);
                this.helper.presentToast(this.translate.instant("conectionError"));
              })
  
            }).catch(err => console.log('Error reader' + err));


          }
        });
      });

          } else {
            console.log("from else : ", cvextuper);
            this.helper.presentToast(this.translate.instant("fileupnoex"));
            this.cv_data = "";
            this.cv_ext = "";
            this.cvtxt = this.translate.instant("view")
            this.user_cv_name = [];
          }

      
        }).catch(err => console.log('Error' + err));
    }
    else if (this.platform.is('android')) {
      this.fileChooser.open()
        .then(uri => {
          console.log("uuu" + uri)
          this.filePath.resolveNativePath(uri).then((result) => {
            this.base64.encodeFile(result).then((base64File: string) => {
              console.log("base64File " + base64File)
              let fileData = base64File.split(',')[1];
              this.cv_data = encodeURIComponent(fileData);
              console.log("this.cv_data : ", this.cv_data);
              this.filePath.resolveNativePath(uri)
                .then(filePath => {
                  console.log(filePath)
                  let filename = filePath.substr(filePath.lastIndexOf('/') + 1)
                  this.user_cv_name.push(filename)
                  this.cvtxt = filename;
                  let fileExt = filename.split('.').pop();
                  // this.cv_ext.push(fileExt)
                  this.cv_ext = fileExt;
                  var cvextuper = this.cv_ext.toUpperCase();
                  if (cvextuper == "pdf".toUpperCase() || cvextuper == "docx".toUpperCase() || cvextuper == "doc".toUpperCase() || cvextuper == "JPEG".toUpperCase() || cvextuper == "PNG".toUpperCase() || cvextuper == "JPG".toUpperCase() || cvextuper == "GIF".toUpperCase() || cvextuper == "BMP".toUpperCase()) {
                    console.log("from if : ", cvextuper);
                    // this.helper.presentToast(this.translate.instant("fileupsu"));
                    
                    
                    var vx =filePath
                    this.file.resolveLocalFilesystemUrl(vx).then(fileEntry => {
                      console.log("file entry : ",fileEntry)
                      fileEntry.getMetadata((metadata) => {
            
                          console.log("worker cert meta data from resolveLocalFilesystemUrl : ",metadata);//metadata.size is the size in bytes
                          // metadata.size: 20761
            // this.vedioSize = (metadata.size / 1024)/1024
                          console.log("(metadata.size / 1024)/1024 : ",(metadata.size / 1024)/1024)
                          if((metadata.size / 1024)/1024 > 1){
                           this.helper.presentToast(this.translate.instant("documentLargerThan1Mega"))
                           this.cvtxt = this.translate.instant("view")
                          }else{

                    this.service.jsaddcv(this.id, this.cv_data, this.cv_ext, resp => {
                      console.log("resp from compaddcr", resp);
                      if (JSON.parse(resp).OperationResult == "1") {
                        this.cvlinkfromstorge = JSON.parse(resp).filePath;
  
                        this.helper.presentToast(this.translate.instant("cvupsu"));
                        this.userData.CVFile = this.cvlinkfromstorge;
                        this.helper.storeJSInfo(this.userData);
                      }
                      else
                        this.helper.presentToast(this.translate.instant("conectionError"));
                    }, err => {
                      console.log("err from compaddcr", err);
                      this.helper.presentToast(this.translate.instant("conectionError"));
  
                    })


}
    
                  });
                  
              });


                  } else {
                    console.log("from else : ", cvextuper);
                    this.helper.presentToast(this.translate.instant("fileupnoex"));
                    this.cv_data = "";
                    this.cv_ext = "";
                    this.cvtxt = this.translate.instant("view")
                    this.user_cv_name = [];
                  }

                  console.log("this.user_cv_name : ", this.user_cv_name);
                  console.log("this.cv_ext : ", this.cv_ext)
               

                })
                .catch(err => console.log(err));
            }, (err) => {
              console.log("base" + err);
            });

          }, (err) => {
            console.log(err);
          })


        })
        .catch(e => console.log(e));
    }
  }
  getworkerCertificates() {
    if (this.platform.is('ios')) {
      this.filePicker.pickFile()
        .then(uri => {
          let correctPath = uri.substr(0, uri.lastIndexOf('/') + 1);
          let filename = uri.substr(uri.lastIndexOf('/') + 1)
          this.certttxt = filename;
          this.user_workercert_name.push(filename)
          let fileExt = filename.split('.').pop();
          this.workercert_ext = fileExt
          var cvextuper = this.workercert_ext.toUpperCase();
          if (cvextuper == "pdf".toUpperCase() || cvextuper == "docx".toUpperCase() || cvextuper == "doc".toUpperCase() || cvextuper == "JPEG".toUpperCase() || cvextuper == "PNG".toUpperCase() || cvextuper == "JPG".toUpperCase() || cvextuper == "GIF".toUpperCase() || cvextuper == "BMP".toUpperCase()) {
            console.log("from if : ", cvextuper);
            // this.helper.presentToast(this.translate.instant("fileupsu"));
          
            var vx = "file:///"+correctPath
            this.file.resolveLocalFilesystemUrl(vx).then(fileEntry => {
              console.log("file entry : ",fileEntry)
              fileEntry.getMetadata((metadata) => {
    
                  console.log("worker cert meta data from resolveLocalFilesystemUrl : ",metadata);//metadata.size is the size in bytes
                  // metadata.size: 20761
    // this.vedioSize = (metadata.size / 1024)/1024
                  console.log("(metadata.size / 1024)/1024 : ",(metadata.size / 1024)/1024)
                  if((metadata.size / 1024)/1024 > 1){
                   this.helper.presentToast(this.translate.instant("documentLargerThan1Mega"))
                   this.certttxt = this.translate.instant("viewWorkerCertificate")
                  }else{

                  
          
            this.file.readAsDataURL("file:///" + correctPath, filename).then((val) => {
              this.workercert_data = encodeURIComponent(val.split(",")[1]);
  
              console.log("this.workercert_data: ", this.workercert_data);
              console.log("this.user_workercert_name : ", this.user_workercert_name);
              console.log("this.workercert_ext : ", this.workercert_ext)
               this.service.jsaddworkercert(this.id, this.workercert_data, this.workercert_ext, resp => {
                 console.log("resp from compaddcr", resp);
                 if (JSON.parse(resp).OperationResult == "1") {
                   this.workercertlinkfromstorge = JSON.parse(resp).filePath;
  
                  this.helper.presentToast(this.translate.instant("certupsu"));
                   this.userData.workercertFile = this.workercertlinkfromstorge;
                   this.helper.storeJSInfo(this.userData);
                 }
                 else
                   this.helper.presentToast(this.translate.instant("conectionError"));
               }, err => {
                 console.log("err from compaddcr", err);
                 this.helper.presentToast(this.translate.instant("conectionError"));
               })
  
            }).catch(err => console.log('Error reader' + err));


          
          }
    
        });
        
    });


          } else {
            console.log("from else : ", cvextuper);
            this.helper.presentToast(this.translate.instant("fileupnoex"));
            this.workercert_data = "";
            this.workercert_ext = "";
            this.certttxt =  this.translate.instant("viewWorkerCertificate")
            this.user_workercert_name = [];
          }

      
        }).catch(err => console.log('Error' + err));
    }
    else if (this.platform.is('android')) {
      this.fileChooser.open()
        .then(uri => {
          console.log("uuu" + uri)
          this.filePath.resolveNativePath(uri).then((result) => {
            this.base64.encodeFile(result).then((base64File: string) => {
              console.log("base64File " + base64File)
              let fileData = base64File.split(',')[1];
              this.workercert_data = encodeURIComponent(fileData);
              console.log("this.workercert_data : ", this.workercert_data);
              this.filePath.resolveNativePath(uri)
                .then(filePath => {
                  console.log(filePath)
                  let filename = filePath.substr(filePath.lastIndexOf('/') + 1)
                  this.certttxt = filename;
                  this.user_workercert_name.push(filename)
                  let fileExt = filename.split('.').pop();
                  // this.cv_ext.push(fileExt)
                  this.workercert_ext = fileExt;
                  var cvextuper = this.workercert_ext.toUpperCase();
                  if (cvextuper == "pdf".toUpperCase() || cvextuper == "docx".toUpperCase() || cvextuper == "doc".toUpperCase() || cvextuper == "JPEG".toUpperCase() || cvextuper == "PNG".toUpperCase() || cvextuper == "JPG".toUpperCase() || cvextuper == "GIF".toUpperCase() || cvextuper == "BMP".toUpperCase()) {
                    console.log("from if : ", cvextuper);
                    //// this.helper.presentToast(this.translate.instant("fileupsu"));
                

                    var vx =filePath
                    this.file.resolveLocalFilesystemUrl(vx).then(fileEntry => {
                      console.log("file entry : ",fileEntry)
                      fileEntry.getMetadata((metadata) => {
            
                          console.log("worker cert meta data from resolveLocalFilesystemUrl : ",metadata);//metadata.size is the size in bytes
                          // metadata.size: 20761
            // this.vedioSize = (metadata.size / 1024)/1024
                          console.log("(metadata.size / 1024)/1024 : ",(metadata.size / 1024)/1024)
                          if((metadata.size / 1024)/1024 > 1){
                           this.helper.presentToast(this.translate.instant("documentLargerThan1Mega"))
                           this.certttxt = this.translate.instant("viewWorkerCertificate")
                          }else{

                     this.service.jsaddworkercert(this.id, this.workercert_data, this.workercert_ext, resp => {
                       console.log("resp from compaddcr", resp);
                       if (JSON.parse(resp).OperationResult == "1") {
                        this.workercertlinkfromstorge = JSON.parse(resp).filePath;
  
                         this.helper.presentToast(this.translate.instant("certupsu"));
                         this.userData.workercertFile = this.workercertlinkfromstorge;
                         this.helper.storeJSInfo(this.userData);
                       }
                       else
                       this.helper.presentToast(this.translate.instant("conectionError"));
                    }, err => {
                       console.log("err from compaddcr", err);
                       this.helper.presentToast(this.translate.instant("conectionError"));
  
                     })


                    }
    
                  });
                  
              });


                  } else {
                    console.log("from else : ", cvextuper);
                    this.helper.presentToast(this.translate.instant("fileupnoex"));
                    this.workercert_data = "";
                    this.workercert_ext = "";
                    this.certttxt =  this.translate.instant("viewWorkerCertificate")
                    this.user_workercert_name = [];
                  }

                  console.log("this.user_workercert_name : ", this.user_workercert_name);
                  console.log("this.workercert_ext : ", this.workercert_ext)
               

                })
                .catch(err => console.log(err));
            }, (err) => {
              console.log("base" + err);
            });

          }, (err) => {
            console.log(err);
          })


        })
        .catch(e => console.log(e));
    }
  }
  showcv() {
    if (this.cvlinkfromstorge) {
      console.log("if cvlinkfromstorge: ",this.cvlinkfromstorge);
      // window.open(this.cvlinkfromstorge,'_system');
      
      // window.open(this.cvlinkfromstorge,'_blank');
      // ,"location=no"
      const browser = this.iab.create(this.cvlinkfromstorge,'_system',"location=yes");
    } else {
      console.log("else cvlinkfromstorge");
      this.helper.presentToast(this.translate.instant("nocvtoshow"));

    }
  }

  showWorkerCertificate() {
    console.log("this.workercertlinkfromstorge : ",this.workercertlinkfromstorge)
    if (this.workercertlinkfromstorge) {
      console.log("if workercertlinkfromstorge");
      // window.open(this.workercertlinkfromstorge);
      const browser = this.iab.create(this.workercertlinkfromstorge,'_system',"location=yes");
    } else {
      console.log("else workercertlinkfromstorge");
      this.helper.presentToast(this.translate.instant("noworkercerttoshow"));

    }
  }

  IBAN() {
    if (!this.logInForm.valid) {
      this.submitAttempt = true;

      if (this.logInForm.controls["IBANtxt"].errors) {
        if (this.logInForm.controls["IBANtxt"].errors['required'])
          this.errors.IBANtxtErr = this.translate.instant("IBANreq");
        else if (this.logInForm.controls["IBANtxt"].errors['pattern'])
          this.errors.IBANtxtErr = this.translate.instant("ibanpatt");
        // else if (this.mobile.includes('-') || this.mobile.includes('.'))
        //   this.errors.mobileErr = this.translate.instant("invalidPhone");
        else
          console.log("mobile errors:", this.logInForm.controls["mobile"].errors);
      }

    } else {
      this.service.saveIBAN(this.id, this.IBANtxt, resp => {
        console.log("resp from saveIBAN", resp);
        this.helper.presentToast(this.translate.instant("IBANsaved"));
        this.userData.IBAN = this.IBANtxt;
        this.helper.storeJSInfo(this.userData);

      }, err => {
        console.log("err from saveIBAN", err);
      })
    }
  }

  onInputTime() {
    console.log("onInputTime");
    this.IBANtxt = this.IBANtxt.replace(/\s/g, "");
  }

  editProfile(){
    console.log("editProfile");


    var anotherData = {Workingdays:this.Workingdays,amorpmTxt:this.amorpmTxt,city_id:this.city_id,interestedJobs:this.interestedJobs,joinday:this.joinday,gender:this.gender,dob:this.dob,hoursperweek:this.hoursperweek,workingDaysInWeekend:this.workingDaysInWeekend,workingDaysAndTimes:this.workingDaysAndTimes,natid:this.natid}
    console.log("another data",anotherData)
    this.navCtrl.push('EditjsprofilePage',{name:this.FirstName,email:this.mail,iban:this.IBANtxt,videourl:this.vediourl,userid:this.id,desc:this.descid,anotherData:anotherData});
  }

  uploadvideo(){
    console.log("upload video");
  }

  runVedio(link){
    // let options: StreamingVideoOptions = {
    //   successCallback: () => { console.log('Video played') },
    //   errorCallback: (e) => { console.log('Error streaming') },
    //   orientation: 'landscape',
    //   shouldAutoClose: true,
    //   controls: false
    // };
    
    // this.streamingMedia.playVideo('https://www.youtube.com/watch?v=QSIPNhOiMoE', options);
    let options: StreamingVideoOptions = {
      successCallback: () => { console.log('Finished Video') },
      errorCallback: (e) => { console.log('Error: ', e) },
      orientation: 'portrait'
    };
 
    // http://www.sample-videos.com/
    // https://r2---sn-hgn7yn7l.googlevideo.com/videoplayback?itag=18&ipbits=0&signature=1338CB8FFDBEC623D19CAA11F06FD9FDC85B9BAB.2CC86FF4C79740595A8814C1411C0D33327E3F12&source=youtube&clen=9404163&requiressl=yes&key=cms1&mime=video%2Fmp4&ei=x1wWXNfoKIPF1wKhv4_IBQ&id=o-AMcJG7-iVpOiHwX0hcGcKkIz0NrpUSyJrZ0AZ6WtVMQd&fvip=2&lmt=1431924607246831&c=WEB&ip=185.225.17.253&sparams=clen,dur,ei,expire,gir,id,ip,ipbits,itag,lmt,mime,mip,mm,mn,ms,mv,pl,ratebypass,requiressl,source&gir=yes&dur=196.533&expire=1544991015&pl=19&ratebypass=yes&title=Intel+IoT%20--%20What%20Does%20The%20Internet%20of%20Things%20Mean&mip=197.61.192.135&redirect_counter=1&cm2rm=sn-uxaxjvhxbt2u-2nqe7l&fexp=23763603&req_id=1f7037fe50a3ee&cms_redirect=yes&mm=29&mn=sn-hgn7yn7l&ms=rdu&mt=1544969329&mv=m
    this.streamingMedia.playVideo(link, options);
  
  }

  
  captureVideo() {
    let options: CaptureVideoOptions = {
      limit: 1,
      duration: 30,
      quality: 0 
    }
    

    this.mediaCapture.captureVideo(options).then((res: MediaFile[]) => {
      console.log("capture res",res);
      // size: 20761 /1024 => k /1024 => m ,,, / 1024 => g
    console.log("(res[0].size / 1024)/1024 : ",(res[0].size / 1024)/1024)
       if((res[0].size / 1024)/1024 > 2){
        this.helper.presentToast(this.translate.instant("vediolargerthan2mega"))
        return
       }else {
      if (this.platform.is('android')) {
      let capturedFile = res[0];
      let fileName = capturedFile.name;
      this.vediotxt = fileName;
      this.myvdeiofilename = fileName;
      this.vedio_ext = fileName.split('.').pop();
      console.log("file name",fileName);
      let dir = capturedFile['localURL'].split('/');
      dir.pop();
      let fromDirectory = dir.join('/');      
      var toDirectory = this.file.dataDirectory;
      console.log("fromDirectory",fromDirectory);
      console.log("toDirectory",toDirectory);
      
      this.file.copyFile(fromDirectory , fileName , toDirectory , fileName).then((res) => {
        console.log('copyFile res: ', res);
        // this.func(res.nativeURL);
        this.linkforview = res.nativeURL;
        let url = res.nativeURL.replace(/^file:\/\//g, '');
        this.vediosrc = url;
        console.log("this.vediosrc : "+this.vediosrc);

        this.convertCapturedVedioToBase64(capturedFile.fullPath);
       
      

      },err => {
        console.log('copyFile err: ', err);
      });

      }else if (this.platform.is('ios')) {
           let capturedFile = res[0];
           let fileURL = capturedFile.fullPath
           
      this.vediotxt =  capturedFile.name;
      
      this.vedio_ext = capturedFile.name.split('.').pop();
      console.log("capturedFile.name : ",capturedFile.name , " , this.vediotxt :",this.vediotxt," ,vedio_ext :  ",this.vedio_ext)

           let fileName1 = fileURL.substring(fileURL.lastIndexOf('/') + 1);
           let filePath1 = fileURL.substring(0, fileURL.lastIndexOf("/") + 1);

                   let correctPath = fileURL.substr(0, fileURL.lastIndexOf('/') + 1);
                           let filename = fileURL.substr(fileURL.lastIndexOf('/') + 1)
           console.log("filePath1 :",filePath1)
          //  this.file.readAsDataURL(filePath1, fileName1)
                 this.file.readAsDataURL("file:///" + correctPath, filename)
           .then(
            file64 => {
                console.log("vedio base 64 : ",file64); //base64url...
                // this.trysource = file64
                let fileData = file64.split(',')[1];
                this.trysource = "data:video/webm;base64,"+fileData
      this.vedio_data = encodeURIComponent(fileData);
                this.sendVedioToApi();
                //resolve(file64);
            }).catch(err => {
               // reject(err);
               console.log("catch  readAsDataURL : ",err)
          });

           //this.convertCapturedVedioToBase64(capturedFile.fullPath);
      let fileName = capturedFile.name;
      this.vediotxt = fileName;
      this.myvdeiofilename = fileName;
      this.vedio_ext = fileName.split('.').pop();
      console.log("file name",fileName);
      let dir = capturedFile['localURL'].split('/');
      dir.pop();
      let fromDirectory = dir.join('/');      
      var toDirectory = this.file.dataDirectory;
      console.log("fromDirectory",fromDirectory);
      console.log("toDirectory",toDirectory);
        // this.convertCapturedVedioToBase64(fromDirectory+"/"+fileName)
//this.convertCapturedVedioToBase64(toDirectory+fileName)

//        this.file.readAsDataURL("file:///" + correctPath, filename).then((val) => {
// //             this.vedio_data = encodeURIComponent(val.split(",")[1]);}
      // this.file.copyFile(fromDirectory , fileName , toDirectory , fileName).then((res) => {
      //   console.log('copyFile res: ', res);
      //   // this.func(res.nativeURL);
      //   // this.linkforview = res.nativeURL;
      //   // let url = res.nativeURL.replace(/^file:\/\//g, '');
      //   // this.vediosrc = url;
      //   // console.log("this.vediosrc : "+this.vediosrc);

      //   // this.convertCapturedVedioToBase64("file:///"+capturedFile.fullPath);
      //   this.convertCapturedVedioToBase64(res.nativeURL)
       
      

      // },err => {
      //   console.log('copyFile err: ', err);
      // });

      }

//
//  if (this.platform.is('ios')) {
//     this.filePicker.pickFile()
//       .then(uri => {
//         console.log("linkforview: ",uri);
   
//         let correctPath = uri.substr(0, uri.lastIndexOf('/') + 1);
//         let filename = uri.substr(uri.lastIndexOf('/') + 1)
//         this.vediotxt = filename;
//         this.user_vedio_name.push(filename)
//         let fileExt = filename.split('.').pop();
//         this.vedio_ext = fileExt
//         var cvextuper = this.vedio_ext.toUpperCase();
//         if (cvextuper == "MP4".toUpperCase() || cvextuper == "AVI".toUpperCase() || cvextuper == "FLV".toUpperCase() || cvextuper == "WMV".toUpperCase() || cvextuper == "MOV".toUpperCase() ) {
//           console.log("from if : ", cvextuper);
          
//           this.file.readAsDataURL("file:///" + correctPath, filename).then((val) => {
//             this.vedio_data = encodeURIComponent(val.split(",")[1]);

//             console.log("this.vedio_data: ", this.vedio_data);
//             console.log("this.user_vedio_name : ", this.user_vedio_name);
//             console.log("this.vedio_ext : ", this.vedio_ext)
//             this.sendVedioToApi();
            

//           }).catch(err => console.log('Error reader' + err));

//         } else {
//           console.log("from else : ", cvextuper);
//           this.helper.presentToast(this.translate.instant("fileupnoex"));
//           this.vedio_data = "";
//           this.vedio_ext = "";
//           this.vediotxt =  this.translate.instant("viewvedio")
//           this.user_vedio_name = [];
//         }

    
//       }).catch(err => console.log('Error' + err));
//   }
//   else if (this.platform.is('android')) {
//     this.fileChooser.open()
//       .then(uri => {
//         console.log("uuu" + uri)
//         this.filePath.resolveNativePath(uri).then((result) => {
//           this.base64.encodeFile(result).then((base64File: string) => {
//             console.log("base64File " + base64File)
//             this.vediosrc = base64File;
//             let fileData = base64File.split(',')[1];
//           //  this.newvediosrc = fileData;
//             this.vedio_data = encodeURIComponent(fileData);
//             console.log("this.vedio_data : ", this.vedio_data);
//             this.filePath.resolveNativePath(uri)
//               .then(filePath => {
//                 console.log("linkforview: ",filePath);
//                 //this.linkforview = filePath;
//                 let filename = filePath.substr(filePath.lastIndexOf('/') + 1)
//                 this.vediotxt = filename;
//                 this.user_vedio_name.push(filename)
//                 let fileExt = filename.split('.').pop();
//                 // this.cv_ext.push(fileExt)
//                 this.vedio_ext = fileExt;
//                 var cvextuper = this.vedio_ext.toUpperCase();
//                 if (cvextuper == "mp4".toUpperCase() ) {
//                   console.log("from if : ", cvextuper);
//                   this.sendVedioToApi();
                

//                 } else {
//                   console.log("from else : ", cvextuper);
//                   this.helper.presentToast(this.translate.instant("fileupnoex"));
//                   this.vedio_data = "";
//                   this.vedio_ext = "";
//                   this.vediotxt =  this.translate.instant("viewvedio")
//                   this.user_vedio_name = [];
//                 }

//                 console.log("this.user_vedio_name : ", this.user_vedio_name);
//                 console.log("this.vedio_ext : ", this.vedio_ext)
             

//               })
//               .catch(err => console.log(err));
//           }, (err) => {
//             console.log("base" + err);
//           });

//         }, (err) => {
//           console.log(err);
//         })


//       })
//       .catch(e => console.log(e));
//   }




      //


    } //else

          },
    (err: CaptureError) => console.error("CaptureError err : ",JSON.stringify(err)));
  }
 
  convertCapturedVedioToBase64(vediopath){
    console.log("convertCapturedVedioToBase64 : ",vediopath)
    this.base64.encodeFile(vediopath).then((base64File: string) => {
      console.log("base64File " + base64File)
      let fileData = base64File.split(',')[1];
      this.trysource = "data:video/webm;base64,"+fileData
      this.vedio_data = encodeURIComponent(fileData);
      console.log("this.vedio_data : ", this.vedio_data);

      this.sendVedioToApi();


    }, (err) => {
      console.log("base" + err);
    });

  }
  play() {
    // if (this.myvdeiofilename.indexOf('.wav') > -1) {
    //   const audioFile: MediaObject = this.media.create(myFile.localURL);
    //   audioFile.play();
    // } else {
      // let path = this.file.dataDirectory + myFile.name;
      let path = this.file.dataDirectory + this.myvdeiofilename;
      let url = path.replace(/^file:\/\//g, '');
      this.vediosrc = url;
      this.runVedio(url);
      // let video = this.myVideo.nativeElement;
      // alert(this.myVideo.nativeElement);
      // video.src = url;
      // alert("url: "+url)
      // video.play();

    // }
  }

func(link){
  console.log("from func link = ",link);
  // this.filePath.resolveNativePath(link).then((result) => {
    // console.log("result video: ",result);
  this.base64.encodeFile(link).then((base64File: string) => {
    console.log("base64File video: " + base64File)
    this.vediosrc = base64File;
    let fileData = base64File.split(',')[1];
    console.log("file data video: ",fileData);
    // this.vediosrc = encodeURIComponent(fileData);
  });

// });

//   this.file.checkFile(this.file.dataDirectory, "file.mp4").then((correct : boolean) => {
//     if(correct){
//         this.file.readAsDataURL(this.file.dataDirectory,  "file.mp4").then((base64) => {
          
//           this.base64.encodeFile(result).then((base64File: string) => {
//             console.log("base64File " + base64File)
//             let fileData = base64File.split(',')[1];
//             this.vediosrc = encodeURIComponent(fileData);
//           });
//           // this.vediosrc = this.sanitizer.bypassSecurityTrustUrl(base64);
//             // this.bo_html5 = true;
//         }).catch((err) => {
//             console.log("VIDEO :: No se pudo recuperar el video");
//             console.log(err);
//         });
//     } else {
//         console.log("VIDEO :: El video no pudo ser encontrado");
//     }
// }).catch((err) => {
//     console.log("VIDEO :: Ocurrio un error al verificar si el video existe");
//     console.log(err);
// });

}


uploadvediofromgallery() {
  if (this.platform.is('ios')) {
    this.filePicker.pickFile()
      .then(uri => {
        console.log("linkforview: ",uri);
      //  this.linkforview = uri;
        let correctPath = uri.substr(0, uri.lastIndexOf('/') + 1);
        console.log("correctPath : ",correctPath);
        let filename = uri.substr(uri.lastIndexOf('/') + 1)
        console.log("filename :",filename)
        this.vediotxt = filename;
        this.user_vedio_name.push(filename)
        let fileExt = filename.split('.').pop();
        this.vedio_ext = fileExt
        var cvextuper = this.vedio_ext.toUpperCase();
        if (cvextuper == "MP4".toUpperCase() || cvextuper == "AVI".toUpperCase() || cvextuper == "FLV".toUpperCase() || cvextuper == "WMV".toUpperCase() || cvextuper == "MOV".toUpperCase() ) {
          console.log("from if : ", cvextuper);
          // this.helper.presentToast(this.translate.instant("fileupsu"));
          this.file.readAsDataURL("file:///" + correctPath, filename).then((val) => {
            this.vedio_data = encodeURIComponent(val.split(",")[1]);

            console.log("this.vedio_data: ", this.vedio_data);
            console.log("this.user_vedio_name : ", this.user_vedio_name);
            console.log("this.vedio_ext : ", this.vedio_ext)
            this.sendVedioToApi();
            // this.service.jsaddcv(this.id, this.cv_data, this.cv_ext, resp => {
            //   console.log("resp from compaddcr", resp);
            //   if (JSON.parse(resp).OperationResult == "1") {
            //     this.cvlinkfromstorge = JSON.parse(resp).filePath;

            //     this.helper.presentToast(this.translate.instant("fileupsu"));
            //     this.userData.CVFile = this.cvlinkfromstorge;
            //     this.helper.storeJSInfo(this.userData);
            //   }
            //   else
            //     this.helper.presentToast(this.translate.instant("conectionError"));
            // }, err => {
            //   console.log("err from compaddcr", err);
            //   this.helper.presentToast(this.translate.instant("conectionError"));
            // })

          }).catch(err => console.log('Error reader' + err));

        } else {
          console.log("from else : ", cvextuper);
          this.helper.presentToast(this.translate.instant("fileupnoex"));
          this.vedio_data = "";
          this.vedio_ext = "";
          this.vediotxt =  this.translate.instant("viewvedio")
          this.user_vedio_name = [];
        }

    
      }).catch(err => console.log('Error' + err));
  }
  else if (this.platform.is('android')) {
    this.fileChooser.open()
      .then(uri => {
        console.log("uuu" + uri)
        this.filePath.resolveNativePath(uri).then((result) => {
          this.base64.encodeFile(result).then((base64File: string) => {
            console.log("base64File " + base64File)
            this.vediosrc = base64File;
            let fileData = base64File.split(',')[1];
          //  this.newvediosrc = fileData;
            this.vedio_data = encodeURIComponent(fileData);
            console.log("this.vedio_data : ", this.vedio_data);
            this.filePath.resolveNativePath(uri)
              .then(filePath => {
                console.log("linkforview: ",filePath);
                //this.linkforview = filePath;
                let filename = filePath.substr(filePath.lastIndexOf('/') + 1)
                this.vediotxt = filename;
                this.user_vedio_name.push(filename)
                let fileExt = filename.split('.').pop();
                // this.cv_ext.push(fileExt)
                this.vedio_ext = fileExt;
                var cvextuper = this.vedio_ext.toUpperCase();
                // if (cvextuper == "mp4".toUpperCase() ) {
                  if (cvextuper == "MP4".toUpperCase() || cvextuper == "AVI".toUpperCase() || cvextuper == "FLV".toUpperCase() || cvextuper == "WMV".toUpperCase() || cvextuper == "MOV".toUpperCase() ) {
                  console.log("from if : ", cvextuper);
                  this.sendVedioToApi();
                  // this.helper.presentToast(this.translate.instant("fileupsu"));
                  // this.service.jsaddcv(this.id, this.cv_data, this.cv_ext, resp => {
                  //   console.log("resp from compaddcr", resp);
                  //   if (JSON.parse(resp).OperationResult == "1") {
                  //     this.cvlinkfromstorge = JSON.parse(resp).filePath;

                  //     this.helper.presentToast(this.translate.instant("fileupsu"));
                  //     this.userData.CVFile = this.cvlinkfromstorge;
                  //     this.helper.storeJSInfo(this.userData);
                  //   }
                  //   else
                  //     this.helper.presentToast(this.translate.instant("conectionError"));
                  // }, err => {
                  //   console.log("err from compaddcr", err);
                  //   this.helper.presentToast(this.translate.instant("conectionError"));

                  // })

                } else {
                  console.log("from else : ", cvextuper);
                  this.helper.presentToast(this.translate.instant("fileupnoex"));
                  this.vedio_data = "";
                  this.vedio_ext = "";
                  this.vediotxt =  this.translate.instant("viewvedio")
                  this.user_vedio_name = [];
                }

                console.log("this.user_vedio_name : ", this.user_vedio_name);
                console.log("this.vedio_ext : ", this.vedio_ext)
             

              })
              .catch(err => console.log(err));
          }, (err) => {
            console.log("base" + err);
          });

        }, (err) => {
          console.log(err);
        })


      })
      .catch(e => console.log(e));
  }
}

sendVedioToApi(){
  this.service.jsUploadVedio(this.id, this.vedio_data, this.vedio_ext, resp => {
                    console.log("resp from compaddcr", resp);
                    if (JSON.parse(resp).OperationResult == "1") {
                      this.vediolinkfromstorge = JSON.parse(resp).filePath;

                      this.newvedioLink =  this.sanitizer.bypassSecurityTrustResourceUrl(this.vediolinkfromstorge)
                      // this.helper.presentToast(this.translate.instant("fileupsu"));
                       this.helper.presentToast(this.translate.instant("vedioupsu"));
                      this.userData.VideoURL = this.vediolinkfromstorge;
                      this.helper.storeJSInfo(this.userData);
                    }
                    else
                      this.helper.presentToast(this.translate.instant("conectionError"));
                  }, err => {
                    console.log("err from compaddcr", err);
                    this.helper.presentToast(this.translate.instant("conectionError"));

                  })

}
chooseVedioSource(){
let actionSheet = this.actionSheetCtrl.create({
  title: this.translate.instant("SelectVideoSource"),
  buttons: [
    {
      text: this.translate.instant("uploadVedio"),
      handler: () => {
        // this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
        // this.uploadvediofromgallery();
        this.opengalleryToLoadVedio();
      }
    },
    {
      text: this.translate.instant("UseCamera"),
      handler: () => {
        // this.takePicture(this.camera.PictureSourceType.CAMERA);
        this.captureVideo();
      }
    },
    {
      text: this.translate.instant("cancel"),
      role: 'cancel'
    }
  ]
});
actionSheet.present();

}
showvedio(){
  if (this.vediolinkfromstorge) {
    console.log("if vediourl",this.vediolinkfromstorge);
    if (this.platform.is('ios')) {
      console.log("ios vedio ",this.vediolinkfromstorge)
     const browser = this.iab.create(this.vediolinkfromstorge,'_system',"location=yes");
    
    //   let options: StreamingVideoOptions = {
    //   successCallback: () => { console.log('Finished Video') },
    //   errorCallback: (e) => { console.log('Error: ', e) },
    //   orientation: 'portrait'
    // };
 
    // this.streamingMedia.playVideo(this.vediolinkfromstorge, options)

    }else if (this.platform.is('android')) {
      console.log("android vedio ",this.vediolinkfromstorge)
    let options: StreamingVideoOptions = {
      successCallback: () => { console.log('Finished Video') },
      errorCallback: (e) => { console.log('Error: ', e) },
      orientation: 'portrait'
    };
 
    this.streamingMedia.playVideo(this.vediolinkfromstorge, options);
    }


  } else {
    console.log("else vediourl");
    this.helper.presentToast(this.translate.instant("novideotoshow"));

  }

   
  
}


showRate(){
  this.navCtrl.push("ViewRatePage",{id:this.id})
  // SeekerAgeFrom,SeekerAgeTo   int   optional
}


}
