import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,MenuController} from 'ionic-angular';

import { HelperProvider } from '../../providers/helper/helper';
import { ServicesProvider } from '../../providers/services/services';
import { TranslateService } from '@ngx-translate/core';
import { TabsPage } from '../tabs/tabs';
import { Storage } from '@ionic/storage';

// @IonicPage()
@Component({
  selector: 'page-accounts-for-comp',
  templateUrl: 'accounts-for-comp.html',
})
export class AccountsForCompPage {
  scaleClass="";
  langDirection;
  comsid;
  totalBalance;
  refresher;

  constructor( public storage:Storage,public menu:MenuController,public translate: TranslateService,public service:ServicesProvider, public helper:HelperProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.langDirection = this.helper.lang_direction;
    if(this.langDirection == "rtl")
      this.scaleClass="scaleClass";

      // this.storage.get('js_info').then((val)=>{
      //   this.comsid=val.SID
      //   console.log(this.comsid)
      //   this.service.balanceForComp(this.comsid,resp=>{
      //     console.log("resp balanceForComp",resp);
      //     var respData = JSON.parse(resp);
      //     if(respData.Result == "1")
      //       this.totalBalance = respData.TotalBalance;
          

      //   },err=>{
      //     console.log("err balanceForComp",err);
      //   })
      // });
     
      
  }

  openmenu()
  {
    this.menu.open()
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AccountsForCompPage');
  }
  dismiss(){
    this.navCtrl.pop();
    // this.navCtrl.setRoot(TabsPage);
    // this.navCtrl.parent.select(0);
  }

  doRefresh(ev){
    this.refresher = ev;

    this.storage.get('js_info').then((val)=>{
      this.comsid=val.SID
      console.log(this.comsid)
      this.service.balanceForComp(this.comsid,resp=>{
        console.log("resp balanceForComp",resp);
        var respData = JSON.parse(resp);
        if(respData.Result == "1")
          this.totalBalance = respData.TotalBalance;
        
          if(this.refresher){
            this.refresher.complete();
          }


      },err=>{
        console.log("err balanceForComp",err);

        if(this.refresher){
          this.refresher.complete();
        }
        
      })
    });

  }

  ionViewWillEnter(){
    console.log("ionViewWillEnter");

    this.storage.get('js_info').then((val)=>{
      this.comsid=val.SID
      console.log(this.comsid)
      this.service.balanceForComp(this.comsid,resp=>{
        console.log("resp balanceForComp",resp);
        var respData = JSON.parse(resp);
        if(respData.Result == "1")
          this.totalBalance = respData.TotalBalance;
        

      },err=>{
        console.log("err balanceForComp",err);
      })
    });

  }
}
