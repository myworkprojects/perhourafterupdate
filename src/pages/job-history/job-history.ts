import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
import { ServicesProvider } from '../../providers/services/services';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';

// @IonicPage()
@Component({
  selector: 'page-job-history',
  templateUrl: 'job-history.html',
})
export class JobHistoryPage {

  scaleClass="";
  langDirection;
  rate;
  refresher;
  hideNoDataTxt;
  jsid;
  jobhistorydata
  hideCertBtn

  constructor(public translate: TranslateService, public storage:Storage,public srv:ServicesProvider, public helper:HelperProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.langDirection = this.helper.lang_direction;
    if(this.langDirection == "rtl")
      this.scaleClass="scaleClass";
    
      this.storage.get('js_info').then((val) => {
        console.log("val from get js info",val);
        if(val)
          this.jsid = val.SID;  
       
      }).catch(err=>{
        console.log("catch from get js info",err);
        
      });

    this.rate = 3;
    this.hideNoDataTxt = true;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad JobHistoryPage');
  }
  ionViewWillEnter(){
    console.log("ionViewWillEnter JobHistoryPage");
    this.laodJobHistory();
  }
  dismiss(){
    this.navCtrl.pop();
  }

  doRefresh(ev){
    this.refresher = ev;
    this.laodJobHistory();
    
  }

  laodJobHistory(){
    this.storage.get('js_info').then((val) => {
      console.log("val from get js info",val);
      if(val)
        this.jsid = val.SID;  
     
    
    this.srv.jsJobHistory(this.jsid,resp=>{
      console.log("resp from jsJobHistory",resp);
      var respdata = JSON.parse(resp)
      if(respdata.result ==  "-1")
        this.hideNoDataTxt = false;
      else if(respdata.result ==  "1")
        this.hideNoDataTxt = true;

      this.jobhistorydata = respdata.jobsList;
      for(var i=0;i<this.jobhistorydata.length;i++){
        this.jobhistorydata[i].rate =0;
        if(this.jobhistorydata[i].Status == "2"){
          this.jobhistorydata[i].statustxt = this.translate.instant("jobdone")
          this.jobhistorydata[i].hideCertBtn = false;
        }
        else  if(this.jobhistorydata[i].Status == "-2" || this.jobhistorydata[i].Status == "-1"){
          this.jobhistorydata[i].statustxt = this.translate.instant("jobcancel")
          this.jobhistorydata[i].hideCertBtn = true;
        }else  if(this.jobhistorydata[i].Status == "1"){
          this.jobhistorydata[i].statustxt = this.translate.instant("jobnotcompleted")
        }
        else{
          this.jobhistorydata[i].hideCertBtn = true;
          // jobcancel
          // jobtimeout
        }

      }
      if(this.refresher){
        this.refresher.complete();
      }
    },err=>{
      console.log("err from jsJobHistory",err);
      if(this.refresher){
        this.refresher.complete();
      }
    })

  });
  }


}
