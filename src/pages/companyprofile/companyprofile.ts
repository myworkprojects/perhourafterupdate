import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController ,MenuController,Platform, ActionSheetController, Events} from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { HelperProvider } from '../../providers/helper/helper';
import { Storage } from '@ionic/storage';
import { File } from '@ionic-native/file';
import { IOSFilePicker } from '@ionic-native/file-picker';
import { FileChooser } from '@ionic-native/file-chooser';
import { Base64 } from '@ionic-native/base64'
import { FilePath } from '@ionic-native/file-path';
import { ServicesProvider } from '../../providers/services/services';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { InAppBrowser } from '@ionic-native/in-app-browser';
// declare var google:any


//@IonicPage()
@Component({
  selector: 'page-companyprofile',
  templateUrl: 'companyprofile.html',
  providers: [File,FileChooser,IOSFilePicker,Base64,FilePath,Camera]
})
export class CompanyprofilePage {
  map:any
  // @ViewChild('map') mymap:ElementRef
  langDirection:any;
  compName="";
  compEmail="";
  compShortDescription="";
  scaleClass="";
  phone = "";
  compAddress = "Location";
  compCR = "";
  compcrlink = "Commercial Profile Pdf or jpg";
  user_cv_name=[];
  cv_ext;
  cv_data;
  id;
  cvlinkfromstorge="";
  userData;
  compdesc;
  compmobile;
  userImageUrl = "assets/imgs/default-avatar.png"

  comptxt
  city_id
  RegistryNum
  citytxt

  constructor(private iab: InAppBrowser,public platform: Platform, public menu:MenuController,public storage:Storage,private camera: Camera,
    public ViewCtrl:ViewController,public helper:HelperProvider,public translate:TranslateService,public events: Events,
    public navCtrl: NavController, public navParams: NavParams, public actionSheetCtrl: ActionSheetController,
    private filePicker: IOSFilePicker 
    ,private file: File,private filePath: FilePath,
    private base64: Base64, private fileChooser: FileChooser,public service: ServicesProvider
  ) {
    this.compAddress = this.translate.instant("compLocationTxt");
    this.compCR = "CRN NO.";
    this.compcrlink = "Commercial Profile ";

    
    events.subscribe('compprofileChangedall', (data) => {
      console.log("data from compprofileChangedall subscribe :",data);
      this.compName = data.CompanyName;
      this.compEmail = data.Email; 
      this.compShortDescription = data.ShortDescription; 
      this.city_id =  data.CitySID
      this.RegistryNum = data.RegistryNum
      this.citytxt = data.citytxt
  
    });

  }

  openmenu()
  {
    this.menu.open()
  }
  public takePicture(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      quality: 100, //50
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true,
      allowEdit:true,
      targetWidth:200,
      targetHeight:200
    };
    this.camera.getPicture(options).then((imageData: string) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.storage.get("js_info").then((val) => {
       // this.userImageUrl = 'data:image/jpeg;base64,' + imageData
        //this.storage.set("user_image",this.userImageUrl)
        let imgdata = encodeURIComponent(imageData)
        this.service.changeCompanyProfilePic(imgdata, 'jpeg', val.SID, (data) => {
          if (data.OperationResult == "1") {
            this.helper.presentToast(this.translate.instant("profileImgUpdated"))
            this.events.publish("profileImageChanged",data.filePath)
            this.userImageUrl = data.filePath;
            this.storage.get("js_info").then(val =>{
              let jsInfo = val
              jsInfo.CompanyLogo = this.userImageUrl
              this.storage.set("js_info",jsInfo)
            })
          }
          else {
            this.translate.instant("conectionError")
          }
        }, (data) => {this.translate.instant("conectionError")})
      })
    }, (err) => {
      // Handle error
    });
  }
  selectImage() {

    let actionSheet = this.actionSheetCtrl.create({
      title: this.translate.instant("SelectImageSource"),
      buttons: [
        {
          text: this.translate.instant("LoadfromLibrary"),
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: this.translate.instant("UseCamera"),
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: this.translate.instant("cancel"),
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();

  }
  ionViewDidLoad() {
    
    this.langDirection = this.helper.lang_direction;
    if(this.langDirection == "rtl")
    this.scaleClass="scaleClass";

    this.comptxt = this.translate.instant("compview");

    this.storage.get('js_info').then((val) => {
      console.log("val from get js info",val);
      if(val)
      {
        // this.userData = val;
        // this.FirstName = val.FirstName;
        // // this.job = val.job;
        this.city_id =  val.CitySID
        this.RegistryNum = val.RegistryNum
        this.citytxt = val.citytxt
        console.log(" val.RegistryNum : ", val.RegistryNum)
        console.log(" val.citytxt : ", val.citytxt)
        

        this.userImageUrl = val.CompanyLogo;
        // this.mail = val.Email;
        this.compmobile = val.Phone;
        this.userData = val;
        this.id = val.SID;
        this.compName = val.CompanyName;
        this.compEmail = val.Email;
        this.phone = val.Phone;
        this.compCR = val.RegistryNum;
        if(val.ShortDescription)
        this.compShortDescription = val.ShortDescription;
        else
        this.compShortDescription ="";

        if(val.RegistryFile)
        this.cvlinkfromstorge = val.RegistryFile;
      else
        this.cvlinkfromstorge = "";

      }
          
    

    }).catch(err=>{
      console.log("catch from get js info",err);
      
    });

    // const locations =new google.maps.LatLng('31.98832728','36.8827291')
    // let mapOptions = {
    //     center:locations,
    //     zoom:8,

    //     };
    //     this. map =new google.maps.Map(this.mymap.nativeElement,mapOptions)
       
       
      
    console.log('ionViewDidLoad CompanyprofilePage');
  }
  dismiss(){
    this.navCtrl.parent.select(0);
  }
  editlocation(){
    console.log("editlocation");
    this.navCtrl.push('CompanyaddlocationPage',{data:this.id});
  }

  selectcv() {

    let actionSheet = this.actionSheetCtrl.create({
      title: this.translate.instant("SelectrfSource"),
      buttons: [
        {
          text: this.translate.instant("LoadfromFiles"),
          handler: () => {
            this.getCertificates();
          }
        },
        {
          text: this.translate.instant("LoadfromLibrary"),
          handler: () => {
            this.takePictureforcv(this.camera.PictureSourceType.PHOTOLIBRARY,1);
          }
        },

        {
          text: this.translate.instant("UseCamera"),
          handler: () => {
            this.takePictureforcv(this.camera.PictureSourceType.CAMERA,2);
          }
        },
        {
          text: this.translate.instant("cancel"),
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();

  }


  editcr()
  {
    console.log("edit cr");
    this.getCertificates();
  } 
  getCertificates() {
    if (this.platform.is('ios')) {
      this.filePicker.pickFile()
        .then(uri => {
          console.log("ios uri ",uri)
          let correctPath = uri.substr(0, uri.lastIndexOf('/') + 1);
          console.log("correctPath : ",correctPath)
          let filename = uri.substr(uri.lastIndexOf('/') + 1)
          console.log("filename : ",filename)
          this.comptxt = filename
          this.user_cv_name.push(filename)
          let fileExt = filename.split('.').pop();
          this.cv_ext = fileExt
          var cvextuper = this.cv_ext.toUpperCase();
                  if(cvextuper == "pdf".toUpperCase() || cvextuper == "docx".toUpperCase() ||cvextuper == "doc".toUpperCase() || cvextuper == "JPEG".toUpperCase() || 	cvextuper == "PNG".toUpperCase() || cvextuper == "JPG".toUpperCase() || cvextuper == "GIF".toUpperCase() 	|| cvextuper == "BMP".toUpperCase()	)
                  {
                    console.log("from if : ",cvextuper);
                    // this.helper.presentToast(this.translate.instant("fileupsu"));
                  }else{
                    console.log("from else : ",cvextuper);
                    this.helper.presentToast(this.translate.instant("fileupnoex"));
                    this.cv_data = "";
                    this.cv_ext="";
                    this.comptxt = this.translate.instant("compview");
                    this.user_cv_name =[];
                  }


            var vx = "file:///"+correctPath
            this.file.resolveLocalFilesystemUrl(vx).then(fileEntry => {
              console.log("file entry : ",fileEntry)
              fileEntry.getMetadata((metadata) => {
    
                  console.log("worker cert meta data from resolveLocalFilesystemUrl : ",metadata);//metadata.size is the size in bytes
                  // metadata.size: 20761
    // this.vedioSize = (metadata.size / 1024)/1024
                  console.log("(metadata.size / 1024)/1024 : ",(metadata.size / 1024)/1024)
                  if((metadata.size / 1024)/1024 > 1){
                   this.helper.presentToast(this.translate.instant("documentLargerThan1Mega"))
                   this.comptxt = this.translate.instant("compview");
                   
                  }else{

          this.file.readAsDataURL("file:///" + correctPath, filename).then((val) => {
            console.log("val from readAsDataURL :",val)
            this.cv_data =encodeURIComponent(val.split(",")[1]);

            console.log("this.cv_data : ",this.cv_data);
            console.log("this.user_cv_name : ",this.user_cv_name);
            console.log("this.cv_ext : ",this.cv_ext)
            this.service.compaddcr(this.id,this.cv_data,this.cv_ext,resp=>{
              console.log("resp from compaddcr",resp);
              if(JSON.parse(resp).OperationResult == "1")
                    {
                      this.cvlinkfromstorge =  JSON.parse(resp).filePath;

                      this.helper.presentToast(this.translate.instant("fileupsu"));
                      this.userData.RegistryFile = this.cvlinkfromstorge;
                      this.helper.storeJSInfo(this.userData);
                    }
                    else
                      this.helper.presentToast(this.translate.instant("conectionError"));
            },err=>{
              console.log("err from compaddcr",err);
              this.helper.presentToast(this.translate.instant("conectionError"));
            })

          }).catch(err => console.log('Error reader' + err));

        }
      });
    });

        }).catch(err => console.log('Error' + err));
    }
    else if (this.platform.is('android')) {
      this.fileChooser.open()
        .then(uri => {
          console.log("uuu" + uri)
          this.filePath.resolveNativePath(uri).then((result) => {
            this.base64.encodeFile(result).then((base64File: string) => {
              console.log("base64File " + base64File)
              let fileData = base64File.split(',')[1];
              this.cv_data =  encodeURIComponent(fileData);
              console.log("this.cv_data : ",this.cv_data);
              this.filePath.resolveNativePath(uri)
                .then(filePath => {
                  console.log(filePath)
                  let filename = filePath.substr(filePath.lastIndexOf('/') + 1)
                  this.comptxt = filename
                  this.user_cv_name.push(filename)
                  let fileExt = filename.split('.').pop();
                  // this.cv_ext.push(fileExt)
                  this.cv_ext = fileExt;
                  var cvextuper = this.cv_ext.toUpperCase();
                  if(cvextuper == "pdf".toUpperCase() || cvextuper == "docx".toUpperCase() ||cvextuper == "doc".toUpperCase() || cvextuper == "JPEG".toUpperCase() || 	cvextuper == "PNG".toUpperCase() || cvextuper == "JPG".toUpperCase() || cvextuper == "GIF".toUpperCase() 	|| cvextuper == "BMP".toUpperCase()	)
                  {
                    console.log("from if : ",cvextuper);
                    var vx =filePath
                    this.file.resolveLocalFilesystemUrl(vx).then(fileEntry => {
                      console.log("file entry : ",fileEntry)
                      fileEntry.getMetadata((metadata) => {
            
                          console.log("worker cert meta data from resolveLocalFilesystemUrl : ",metadata);//metadata.size is the size in bytes
                          // metadata.size: 20761
            // this.vedioSize = (metadata.size / 1024)/1024
                          console.log("(metadata.size / 1024)/1024 : ",(metadata.size / 1024)/1024)
                          if((metadata.size / 1024)/1024 > 1){
                           this.helper.presentToast(this.translate.instant("documentLargerThan1Mega"))
                           this.comptxt = this.translate.instant("compview");
                          }else{

                    this.service.compaddcr(this.id,this.cv_data,this.cv_ext,resp=>{
                      console.log("resp from compaddcr",resp);
                      if(JSON.parse(resp).OperationResult == "1")
                      {
                        this.cvlinkfromstorge =  JSON.parse(resp).filePath;
  
                        this.helper.presentToast(this.translate.instant("fileupsu"));
                        this.userData.RegistryFile = this.cvlinkfromstorge;
                        this.helper.storeJSInfo(this.userData);
                      }
                      else
                        this.helper.presentToast(this.translate.instant("conectionError"));
                    },err=>{
                      console.log("err from compaddcr",err);
                      this.helper.presentToast(this.translate.instant("conectionError"));
                    })

                  }
                });
              });

                    
                  // this.helper.presentToast(this.translate.instant("fileupsu"));
                  }else{
                    console.log("from else : ",cvextuper);
                    this.helper.presentToast(this.translate.instant("fileupnoex"));
                    this.cv_data = "";
                    this.cv_ext="";
                    this.comptxt = this.translate.instant("compview");
                    this.user_cv_name =[];
                  }

                  console.log("this.user_cv_name : ",this.user_cv_name);
                  console.log("this.cv_ext : ",this.cv_ext)
                  
                })
                .catch(err => console.log(err));
            }, (err) => {
              console.log("base" + err);
            });

          }, (err) => {
            console.log(err);
          })


        })
        .catch(e => console.log(e));
    }
  } 
  showcv(){
    if(this.cvlinkfromstorge){
      console.log("if cvlinkfromstorge");
      // window.open(this.cvlinkfromstorge);
      const browser = this.iab.create(this.cvlinkfromstorge,'_system',"location=yes");
    }else{
      console.log("else cvlinkfromstorge");
      this.helper.presentToast(this.translate.instant("nocr"));
      
    }
  }
  changepass(){
    console.log("changepass ");
    this.navCtrl.push('ChangepassPage',{type:"compchangepass",mobile:"",id:this.id });
  }

  public takePictureforcv(sourceType,typex) {
    // Create options for the Camera Dialog
    var options = {
      quality: 100, //50
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true,
      allowEdit:true,
      targetWidth:200,
      targetHeight:200
    };
    this.camera.getPicture(options).then((imageData: string) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.storage.get("js_info").then((val) => {
       // this.userImageUrl = 'data:image/jpeg;base64,' + imageData
        //this.storage.set("user_image",this.userImageUrl)
        let imgdata = encodeURIComponent(imageData)
        // this.comptxt = this.translate.instant("imageCaptured")
        
        if(typex == 1 )
        this.comptxt = this.translate.instant("imageChoosed")
        else if(typex == 2)
        this.comptxt = this.translate.instant("imageCaptured")

        this.service.compaddcr(val.SID,imgdata,'jpeg',resp=>{
          console.log("resp from compaddcr",resp);
          if(JSON.parse(resp).OperationResult == "1")
          {
            this.cvlinkfromstorge =  JSON.parse(resp).filePath;

            this.helper.presentToast(this.translate.instant("fileupsu"));
            this.userData.RegistryFile = this.cvlinkfromstorge;
            this.helper.storeJSInfo(this.userData);
          }
          else
            this.helper.presentToast(this.translate.instant("conectionError"));
        },err=>{
          console.log("err from compaddcr",err);
          this.helper.presentToast(this.translate.instant("conectionError"));
        })



      })
    }, (err) => {
      // Handle error
    });
  }


  editProfile(){
    console.log("editProfile")

    this.navCtrl.push("CompanyEditProfilePage",{data:{CitySID:this.city_id,RegistryNum:this.RegistryNum,citytxt:this.citytxt,name:this.compName,desc:this.compShortDescription,mail:this.compEmail}})
  }

}
