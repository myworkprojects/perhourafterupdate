import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Platform ,Events} from 'ionic-angular';

import { HelperProvider } from '../../providers/helper/helper';
import { TranslateService } from '@ngx-translate/core';
import { TabsPage } from  '../../pages/tabs/tabs';
import { ServicesProvider } from '../../providers/services/services';
import { Storage } from '@ionic/storage';


@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  scaleClass="";
  langDirection;
  language;
  notificationonoff
  idforcomporjs
  hidechangepassforcompany
  fortype
  
  langId

  constructor(public storage:Storage,public service:ServicesProvider, public events:Events, public platform: Platform,public translate: TranslateService,public helper:HelperProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.langDirection = this.helper.lang_direction;
    if(this.langDirection == "rtl")
    this.scaleClass="scaleClass";
this.langId = "ar"
    if (this.helper.lang_direction == 'ltr') {
      this.langId = "en"
     // this.language = this.translate.instant("English");
     this.language = this.translate.instant("Arabic");
    }else {
      console.log("stroe en lang");
   
  // this.language = this.translate.instant("Arabic");
      
  this.language = this.translate.instant("English");

    }


 
    


    

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');

    this.storage.get('notification_status').then((val)=>{
      if(val && val == "1")
      { 
        console.log("val from notification_status",val)
        this.notificationonoff = true;
      }else  if(val && val == "-1"){
        console.log("val from else if notification_status",val)
        this.notificationonoff = false;
      }else{
        console.log("val from else notification_status",val)
        this.notificationonoff = true;
      }
    }).catch((err)=>{
      console.log("err from notification_status",err)
    })

    
    this.storage.get('js_info').then((val)=>{
      if(val)
      { 
        console.log("val from js_info",val)
        if(val.type == "company")
        {
          this.hidechangepassforcompany = false;
          this.fortype = "company"
        }
        else if(val.type == "js"){
          this.hidechangepassforcompany = true;
          this.fortype = "js"
        }
          
        
      }
    }).catch((err)=>{
      console.log("err from js_info",err)
    })

  }

  changelang(){
    console.log("this.platform.dir(): ",this.platform.dir());
      if (this.helper.lang_direction == 'ltr') {
        console.log("stroe ar lang");
        this.helper.storeLanguage2('ar');
        
        this.platform.setDir('rtl', true);
        console.log("if this.platform.dir():",this.platform.dir())
        
        this.translate.setDefaultLang('ar');  
        this.translate.use('ar');    
        
        this.helper.lang_direction = "rtl"; 
        //this.lang_direction = "rtl";  
        //this.menuSide = "right";

       
       
       
       
        this.langDirection = "rtl";
        
        this.language = this.translate.instant("English");
        // window.location.reload();


        //this.events.publish('changeLang');
        
      }else {
        console.log("stroe en lang");
        this.helper.storeLanguage2('en');
        this.platform.setDir('ltr', true);
        console.log("else this.platform.dir():",this.platform.dir())
        
            this.translate.setDefaultLang('en');
            this.translate.use('en');
           
            this.helper.lang_direction = "ltr";
    //        this.lang_direction = "ltr";
    this.langDirection = "ltr";
    this.language = this.translate.instant("Arabic");
    //  window.location.reload();

    
          //    this.menuSide = "left";
         // this.events.publish('changeLang');
      }
      //this.navCtrl.setRoot(TabsPage);
      
    }
    dismiss(){
      this.navCtrl.pop();
    }

    notificationChanged(){
      console.log("this.notificationonoff: " , this.notificationonoff)
      this.storage.get('js_info').then((val)=>{
        console.log("js_info from storage: " ,JSON.stringify(val))
        if(val)
        { 
          this.idforcomporjs=val.SID
          if(this.notificationonoff == true){
            //notification on
            if(this.fortype == "company"){
              // 1: Enable     & -1:Disable
              this.service.disableOrEnableNotification(this.idforcomporjs,1,resp=>{
                console.log("resp from disableOrEnableNotification",resp);
                if(JSON.parse(resp).Result == "1"){
                  this.helper.presentToast(this.translate.instant("notificationTurnOnSU"))
                  this.helper.storeNotificationStatus("1")
                }else{
                  this.helper.presentToast(this.translate.instant("ServerError"))
                }
              },err=>{
                console.log("err from disableOrEnableNotification",err);
                this.helper.presentToast(this.translate.instant("ServerError"))
              })

            }else if (this.fortype == "js"){
              console.log("js notification")
              this.service.jsdisableOrEnableNotification(this.idforcomporjs,1,resp=>{
                console.log("resp from disableOrEnableNotification",resp);
                if(JSON.parse(resp).Result == "1"){
                  this.helper.presentToast(this.translate.instant("notificationTurnOnSU"))
                  this.helper.storeNotificationStatus("1")
                }else{
                  this.helper.presentToast(this.translate.instant("ServerError"))
                }
              },err=>{
                console.log("err from disableOrEnableNotification",err);
                this.helper.presentToast(this.translate.instant("ServerError"))
              })

            }
            
            
          }else if(this.notificationonoff == false){
            //notification off
            if(this.fortype == "company"){

              this.service.disableOrEnableNotification(this.idforcomporjs,-1,resp=>{
                console.log("resp from disableOrEnableNotification",resp);
                if(JSON.parse(resp).Result == "1"){
                  this.helper.presentToast(this.translate.instant("notificationTurnOffSU"))
                  this.helper.storeNotificationStatus("-1")
                }else{
                  this.helper.presentToast(this.translate.instant("ServerError"))
                }
                
              },err=>{
                console.log("err from disableOrEnableNotification",err);
                this.helper.presentToast(this.translate.instant("ServerError"))
              })

            }else if (this.fortype == "js"){
              console.log("js notification")
              this.service.jsdisableOrEnableNotification(this.idforcomporjs,-1,resp=>{
                console.log("resp from disableOrEnableNotification",resp);
                if(JSON.parse(resp).Result == "1"){
                  this.helper.presentToast(this.translate.instant("notificationTurnOffSU"))
                  this.helper.storeNotificationStatus("-1")
                }else{
                  this.helper.presentToast(this.translate.instant("ServerError"))
                }
                
              },err=>{
                console.log("err from disableOrEnableNotification",err);
                this.helper.presentToast(this.translate.instant("ServerError"))
              })
            }

            //this.helper.storeNotificationStatus(this.notificationonoff)
          }

        }
      });
      
    }

    changepass(){
      console.log("changepass ");
      this.storage.get('js_info').then((val)=>{
        console.log("js_info from storage: " ,JSON.stringify(val))
        if(val)
        {
          this.idforcomporjs=val.SID 
          this.navCtrl.push('ChangepassPage',{type:"compchangepass",mobile:"",id:this.idforcomporjs });
        }
      });
      
    }

    changepassforjs(){
      console.log("changepassforjs ");
      var idforcomporjs;
      this.storage.get('js_info').then((val)=>{
        console.log("js_info from storage: " ,JSON.stringify(val))
        if(val)
        {
          idforcomporjs=val.SID 
          this.navCtrl.push('ChangepassPage',{type:"jschangepass",mobile:"",id:idforcomporjs });
        }
      });
      
    }

    changepassforJSorComp(){
      console.log("changepassforJSorComp")
      if(this.fortype == "company")
      {
        this.changepassforcomp();
      }else if (this.fortype = "js")
        {
          this.changepassforjs();
        }
    }


changepassforcomp(){
  console.log("changepassforcomp ");
  var idforcomporjs;
  this.storage.get('js_info').then((val)=>{
    console.log("js_info from storage: " ,JSON.stringify(val))
    if(val)
    {
      idforcomporjs=val.SID 
      this.navCtrl.push('ChangepassPage',{type:"compchangepass",mobile:"",id:idforcomporjs });
    }
  });
  
}

changelang2(){
  console.log("this.platform.dir(): ",this.platform.dir());
      if (this.langId == 'ar') {
        console.log("stroe ar lang");
        
        
        this.platform.setDir('rtl', true);
        console.log("if this.platform.dir():",this.platform.dir())
        
        this.translate.setDefaultLang('ar');  
        this.translate.use('ar');    
        
        // console.log(" this.translate.use : ", this.translate.use)
        this.helper.lang_direction = "rtl"; 
        //this.lang_direction = "rtl";  
        //this.menuSide = "right";

       
       
       
       
        this.langDirection = "rtl";
        
        this.language = this.translate.instant("English");
        // window.location.reload();


        //this.events.publish('changeLang');
        this.helper.storeLanguage2('ar');
        // this.navCtrl.setRoot(TabsPage)
        // window.location.reload();

      }else {
        console.log("stroe en lang");
        
        this.platform.setDir('ltr', true);
        console.log("else this.platform.dir():",this.platform.dir())
        
            this.translate.setDefaultLang('en');
            this.translate.use('en');
          //  console.log(" this.translate.use : ", this.translate.use())
            this.helper.lang_direction = "ltr";
    //        this.lang_direction = "ltr";
    this.langDirection = "ltr";
    this.language = this.translate.instant("Arabic");
    //  window.location.reload();

    
          //    this.menuSide = "left";
         // this.events.publish('changeLang');
         this.helper.storeLanguage2('en');
        //  this.navCtrl.setRoot(TabsPage)
      }
      console.log("translate : ",this.translate.instant("home"))
      //this.navCtrl.setRoot(TabsPage);
      
    
}
}
