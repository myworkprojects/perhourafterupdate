import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController,Events} from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { HelperProvider } from '../../providers/helper/helper';
import { Storage } from '@ionic/storage';
import { ServicesProvider } from '../../providers/services/services';
import { TranslateService } from '@ngx-translate/core';



@IonicPage()
@Component({
  selector: 'page-js-applied-jobs',
  templateUrl: 'js-applied-jobs.html',
})
export class JsAppliedJobsPage {

  scaleClass="";
  langDirection;
  jsSID;
  sprint1Hidden = true;

  myAppliedJobs;
  refresher;
  hideNoDataTxt = true;

  constructor(public events:Events, public alertCtrl:AlertController, public translate: TranslateService,public service:ServicesProvider,
    public storage:Storage,public helper:HelperProvider,
    public navCtrl: NavController, public navParams: NavParams) {
      this.langDirection = this.helper.lang_direction;
    if(this.langDirection == "rtl")
      this.scaleClass="scaleClass";
    this.sprint1Hidden = true;

    this.hideNoDataTxt = true;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad JsAppliedJobsPage');
    this.storage.get('js_info').then((val) => {
      console.log("val from get js info",val);
      if(val)
        this.jsSID = val.SID;  
      this.getAllAppliedJob();
    }).catch(err=>{
      console.log("catch from get js info",err);
      
    });
  }
  dismiss(){
    this.events.publish('refreshAppliedJobs');
    this.navCtrl.pop();

    // this.navCtrl.setRoot(TabsPage)
  }

  openDetails(item){
    this.navCtrl.push("JobDetailsPage",{'jobDetails':item,'applied':1})
  }
  getAllAppliedJob(){

    this.service.getJsAppliedJobs(this.jsSID,resp=>{
      console.log("resp from getJsAppliedJobs",resp);
       var respData = JSON.parse(resp);
       
       if(respData.result == "1")
       {
         if(respData.jobsList.length == 0)
         {
          this.myAppliedJobs=[];
          this.hideNoDataTxt = false;
          //this.helper.presentToast(this.translate.instant("noapppliedjobs"));
         }
         else{
          this.myAppliedJobs = respData.jobsList;
          this.hideNoDataTxt = true;
         }
         
       }else{
        if(respData.jobsList.length == 0)
        {
          this.myAppliedJobs=[];
          this.hideNoDataTxt = false;
          //this.helper.presentToast(this.translate.instant("noapppliedjobs"));
        }
        

       }
       // var CurrentJobId ;
      // if(respData.WorkStatus == "1")
      // {
      //   CurrentJobId= respData.CurrentJobID;
      //   for(var j=0;j<respData.activeJobs.length;j++){
      //     if(CurrentJobId == respData.activeJobs[j].SID)
      //       respData.activeJobs[j].checkInOutBtnTxt = this.translate.instant("Checkout");
      //     else
      //       respData.activeJobs[j].checkInOutBtnTxt = this.translate.instant("checkIN");
      //   }
      // }else{
      //   for(var j=0;j<respData.activeJobs.length;j++){
      //     respData.activeJobs[j].checkInOutBtnTxt = this.translate.instant("checkIN");
      //   }
      // }
      // this.myAppliedJobs = respData.activeJobs;

      if(this.refresher){
        this.refresher.complete();
      }
      
    },err=>{
      console.log("err from getJsAppliedJobs",err);
      if(this.refresher){
        this.refresher.complete();
      }
    });


  }
  
  unapplyjob(item){
    this.service.jsUnapply(this.jsSID,item.SID,resp=>{
      console.log("resp from jsUnapply ",resp);
      if(JSON.parse(resp).OperationResult == "1")
      {
        this.helper.presentToast(this.translate.instant("unappliedsu"))
        this.getAllAppliedJob();
      }
        
    },err=>{
      console.log("err from jsUnapply ",err);
    });
  }

  cancelconfirm(item)
  {

  let alert = this.alertCtrl.create({
    title: this.translate.instant('cancelappliedJobTitle'),
    buttons: [
      {
        text: this.translate.instant('cancel'),
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: this.translate.instant('done'),
        handler: () => {
       this.unapplyjob(item);

        }
      }
    
  ]
})
alert.present()
  }

  doRefresh(ev){
    this.refresher = ev;
    this.getAllAppliedJob();
  }

}
