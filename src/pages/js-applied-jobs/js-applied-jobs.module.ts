import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { JsAppliedJobsPage } from './js-applied-jobs';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';

@NgModule({
  declarations: [
    JsAppliedJobsPage,
  ],
  imports: [
    IonicPageModule.forChild(JsAppliedJobsPage),
    TranslateModule.forChild()
  ],
})
export class JsAppliedJobsPageModule {}
