import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InvitePopoverPage } from './invite-popover';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    InvitePopoverPage,
  ],
  imports: [
    IonicPageModule.forChild(InvitePopoverPage),
    TranslateModule.forChild(),
  ],
})
export class InvitePopoverPageModule {}
