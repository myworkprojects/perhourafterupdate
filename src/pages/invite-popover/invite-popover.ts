import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
import { ServicesProvider } from '../../providers/services/services';
import { TranslateService } from '@ngx-translate/core';

/**
 * Generated class for the InvitePopoverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-invite-popover',
  templateUrl: 'invite-popover.html',
})
export class InvitePopoverPage {
jsList = []
invitefriends
searchWord
langDirection="";

  constructor(public navCtrl: NavController, public helper:HelperProvider, public navParams: NavParams,
    public service:ServicesProvider, public translate: TranslateService, public viewCtrl:ViewController) {

      this.langDirection = this.helper.lang_direction;
      
      this.invitefriends = this.translate.instant("invitefriends")
  }
  friendChecked(item,ev){
    item.checked = !item.checked
  }
  getFriends(ev: any){
    if (ev.target.value == undefined) {
      ev.target.value = "";
    }
    if(ev.target.value.length >= 3){
      if(navigator.onLine){
        this.jsList = []
        this.service.jsToInvite(ev.target.value, (data)=>{
          console.log(JSON.stringify(data))
          data = JSON.parse(data)
          for(let x=0;x<data.searchResult.length;x++){
            data.searchResult[x].checked = false;
          }
          this.jsList = data.searchResult
        },
        ()=>{this.helper.presentToast(this.translate.instant("ServerError"))})
      }
      else{
        this.helper.presentToast(this.translate.instant("InternetErr"))
      }
    }

  }
  searchjs(ev: any)
  {
    if (ev.target.value == undefined) {
      this.searchWord = "" 
    }else{
      this.searchWord = ev.target.value
    }
  }
  searchFriends(){
    if(navigator.onLine){
      this.jsList = []
      this.service.jsToInvite(this.searchWord, (data)=>{
        console.log("resp from jsToInvite : ",JSON.stringify(data))
        data = JSON.parse(data)
        if(data.Result == "-1"){
          this.helper.presentToast(this.translate.instant("nodata"));
        }else{
          for(let x=0;x<data.searchResult.length;x++){
            data.searchResult[x].checked = false;
          }
          this.jsList = data.searchResult
        }
       
      },
      ()=>{this.helper.presentToast(this.translate.instant("ServerError"))})
    }
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad InvitePopoverPage');
  }
  DismissInvite(){
    let invites = []
    for(let x=0;x<this.jsList.length;x++){
      if(this.jsList[x].checked){
        invites.push(this.jsList[x].SID)
      }
    }
    this.viewCtrl.dismiss(invites.toString())
  }
}
