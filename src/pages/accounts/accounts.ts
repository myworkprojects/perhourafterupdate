import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
import { ServicesProvider } from '../../providers/services/services';
import { TranslateService } from '@ngx-translate/core';
import { TabsPage } from '../tabs/tabs';
import { Storage } from '@ionic/storage';

// @IonicPage()

@Component({
  selector: 'page-accounts',
  templateUrl: 'accounts.html',
})
export class AccountsPage {

  scaleClass="";
  langDirection;
  jssid;
  totalBalance;
  totalworked;
  refresher;
  
  constructor(public storage:Storage,public translate: TranslateService,public service:ServicesProvider, public helper:HelperProvider,
    public navCtrl: NavController, public navParams: NavParams) {
      
      this.langDirection = this.helper.lang_direction;
      if(this.langDirection == "rtl")
        this.scaleClass="scaleClass";

        // this.storage.get('js_info').then((val)=>{
        //   this.jssid=val.SID
        //   console.log(this.jssid)
        //   this.service.balanceForJs(this.jssid,resp=>{
        //     console.log("resp balanceForJs",resp);
        //     var respData = JSON.parse(resp);
        //     if(respData.Result == "1")
        //     {
        //       this.totalBalance = respData.TotalBalance;
        //       this.totalworked =  respData.TotalWorkedTime
        //     }
              
            
  
        //   },err=>{
        //     console.log("err balanceForComp",err);
        //   })
        // });
     
        
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AccountsPage');
  }

  dismiss(){
    this.navCtrl.pop();
    // this.navCtrl.setRoot(TabsPage);

    // this.navCtrl.parent.select(0);
  }
  doRefresh(ev){
    this.refresher = ev;

    this.storage.get('js_info').then((val)=>{
      this.jssid=val.SID
      console.log(this.jssid)
      this.service.balanceForJs(this.jssid,resp=>{
        console.log("resp balanceForJs",resp);
        var respData = JSON.parse(resp);
        if(respData.Result == "1")
        {
          this.totalBalance = respData.TotalBalance;
          this.totalworked =  respData.TotalWorkedTime
        }
          
        
    if(this.refresher){
      this.refresher.complete();
    }

      },err=>{
        console.log("err balanceForComp",err);

    if(this.refresher){
      this.refresher.complete();
    }
      })
    });

  }
  ionViewWillEnter(){
    console.log("ionViewWillEnter");
    this.storage.get('js_info').then((val)=>{
      this.jssid=val.SID
      console.log(this.jssid)
      this.service.balanceForJs(this.jssid,resp=>{
        console.log("resp balanceForJs",resp);
        var respData = JSON.parse(resp);
        if(respData.Result == "1")
        {
          this.totalBalance = respData.TotalBalance;
          this.totalworked =  respData.TotalWorkedTime
        }
          
        

      },err=>{
        console.log("err balanceForComp",err);
      })
    });

  }

}
