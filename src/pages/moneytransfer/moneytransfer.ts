import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ServicesProvider } from '../../providers/services/services';
import { HelperProvider } from '../../providers/helper/helper';
import { Storage } from '@ionic/storage';


@IonicPage()
@Component({
  selector: 'page-moneytransfer',
  templateUrl: 'moneytransfer.html',
})
export class MoneytransferPage {

  langDirection = "";
  scaleClass="";
  refresher;
  hideNoDataTxt;
  from
  comsid
  jssid
  historyData
  compName
  compLogo

  constructor(public storage:Storage,public srv:ServicesProvider,public helper:HelperProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.langDirection = this.helper.lang_direction;

    if(this.langDirection == "rtl")
    this.scaleClass="scaleClass";
    
    this.hideNoDataTxt = true;

    this.from = this.navParams.get('from');

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MoneytransferPage');
    if(this.from == "company")
    {
      this.storage.get('js_info').then((val)=>{
        
        console.log("js_info from storage: " ,JSON.stringify(val))
        if(val)
        { 
          this.comsid=val.SID
          this.compName = val.CompanyName
          this.compLogo = val.CompanyLogo
          this.loadHistoryForCompany();

        }});
      
    }
    else  if(this.from == "js"){
      this.storage.get('js_info').then((val)=>{
        
        console.log("js_info from storage: " ,JSON.stringify(val))
        if(val)
        { 
          this.jssid=val.SID
          this.compName = val.FirstName
          this.compLogo = val.ProfileImage
          this.loadHistoryForJS();

        }});
        
      
        // this.historyData = []
        // this.hideNoDataTxt = false;
    }
  }


  dismiss(){
    this.navCtrl.pop();
  }

  doRefresh(ev){
    this.refresher = ev;
    if(this.from == "company")
      this.loadHistoryForCompany();
    else  if(this.from == "js")
      this.loadHistoryForJS();
  }
  loadHistoryForJS(){
    
    this.srv.jsgetPaymentHistory(this.jssid,resp=>{
      console.log("resp from jsgetPaymentHistory",resp);
      var respData = JSON.parse(resp);
      if(respData.result ==	"-1"){
        this.hideNoDataTxt = false;
        this.historyData = [];
      }
      else{
        this.hideNoDataTxt = true;
        this.historyData = respData.jobsList
        for(var i=0;i<this.historyData.length;i++){
          // this.historyData[i].PaymentDate2 = this.historyData[i].PaymentDate.split("T")[0] + " , "+this.historyData[i].PaymentDate.split("T")[1]
          this.historyData[i].PaymentDate2 = this.historyData[i].PaymentDate.split("T")[0] 
        }
      }

      if(this.refresher){
        this.refresher.complete();
      }

    },err=>{
      console.log("err from jsgetPaymentHistory",err);
      if(this.refresher){
        this.refresher.complete();
      }
    });

  }

  loadHistoryForCompany(){
    this.srv.companyPaymentHistory(this.comsid,resp=>{
      console.log("resp from companyPaymentHistory",resp);
      var respData = JSON.parse(resp);
      if(respData.Result ==	"-1"){
        this.hideNoDataTxt = false;
        this.historyData = [];
      }
      else{
        this.hideNoDataTxt = true;
        this.historyData = respData.List
        for(var i=0;i<this.historyData.length;i++){
          // this.historyData[i].PaymentDate2 = this.historyData[i].PaymentDate.split("T")[0] + " , "+this.historyData[i].PaymentDate.split("T")[1]
          this.historyData[i].PaymentDate2 = this.historyData[i].PaymentDate.split("T")[0] 
        }
      }

      if(this.refresher){
        this.refresher.complete();
      }

    },err=>{
      console.log("err from companyPaymentHistory",err);
      if(this.refresher){
        this.refresher.complete();
      }
    });

  }

}
