import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MoneytransferPage } from './moneytransfer';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    MoneytransferPage,
  ],
  imports: [
    IonicPageModule.forChild(MoneytransferPage),
    TranslateModule.forChild(),
  ],
})
export class MoneytransferPageModule {}
