import { Component ,ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { HelperProvider } from '../../providers/helper/helper';
declare var google;


@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage {

  scaleClass="";
  langDirection;

  lat= 24.774265;//31.035212;
  lng= 46.738586;//31.391137;
  allMarkers = [] ; 
  
  
  // locations=[{lat:31.03622225245989,lng:31.394227406674304},{lat:31.037933,lng:31.381523},{lat:31.035212,lng:31.391137},{lat:31.036058,lng:31.397617},{lat:31.041721,lng:31.390965}];
  locations=[{lat:24.774265,lng:46.738586},{lat:31.037933,lng:31.381523},{lat:31.035212,lng:31.391137},{lat:31.036058,lng:31.397617},{lat:31.041721,lng:31.390965}];

  @ViewChild('map') mapElement;
  map: any;

  constructor(public helper:HelperProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.langDirection = this.helper.lang_direction;
    if(this.langDirection == "rtl")
    this.scaleClass="scaleClass";
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MapPage');
    this.initMap();
  }

  initMap(){

    console.log("init map");
    let latlng = new google.maps.LatLng(this.lat,this.lng);
    var mapOptions={
     center:latlng,
      zoom:15,
      mapTypeId:google.maps.MapTypeId.ROADMAP,
  
    };
    this.map=  new google.maps.Map(this.mapElement.nativeElement,mapOptions);
  

    var markers, i;
    
    for(var j=0;j<this.allMarkers.length;j++)
    {
      this.allMarkers[j].setMap(null);
    }
    for (i = 0; i < this.locations.length; i++) {  

    let marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: new google.maps.LatLng(this.locations[i].lat, this.locations[i].lng),
      icon: { 
        url : 'assets/icon/place.png',
        size: new google.maps.Size(71, 71),
        scaledSize: new google.maps.Size(20, 25) 
      }     
    });
    
   
    
  
      
  }
    
  }


  dismiss(){
    // this.navCtrl.setRoot(TabsPage);
    this.navCtrl.parent.select(0);
  }
}
