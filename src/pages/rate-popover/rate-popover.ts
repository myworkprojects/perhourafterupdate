import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
/**
 * Generated class for the RatePopoverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-rate-popover',
  templateUrl: 'rate-popover.html',
})
export class RatePopoverPage {
  rate
  notes=""
  notesPlaceholder
  savetxt
  msgTxt
  constructor(  public helper:HelperProvider,public viewCtrl:ViewController,public navCtrl: NavController, public navParams: NavParams) {
    if(this.helper.lang_direction == "rtl")
    {
      this.savetxt = "حفظ"
      this.notesPlaceholder = "ملاحظات..."
      this.msgTxt = "اختر تقييم"
    }
    
    else{
      this.savetxt = "Save"
      this.notesPlaceholder = "Notes..."
      this.msgTxt = "Choose rate"
    }
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RatePopoverPage');
  }

Dismissrate(){
  var data = {rate:this.rate,notes:this.notes}
  if(this.rate)
    this.viewCtrl.dismiss(data)
  else
    this.helper.presentToast(this.msgTxt)
    
}

}
