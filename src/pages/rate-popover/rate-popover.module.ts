import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RatePopoverPage } from './rate-popover';
import { Ionic2RatingModule } from "ionic2-rating";

@NgModule({
  declarations: [
    RatePopoverPage,
  ],
  imports: [
    IonicPageModule.forChild(RatePopoverPage),
    Ionic2RatingModule
  ],
})
export class RatePopoverPageModule {}
