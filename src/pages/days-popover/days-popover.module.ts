import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DaysPopoverPage } from './days-popover';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    DaysPopoverPage,
  ],
  imports: [
    IonicPageModule.forChild(DaysPopoverPage),
    TranslateModule.forChild(),
  ],
})
export class DaysPopoverPageModule {}
