import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { HelperProvider } from '../../providers/helper/helper';

/**
 * Generated class for the DaysPopoverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-days-popover',
  templateUrl: 'days-popover.html',
})

export class DaysPopoverPage {
  saturday: Boolean
  sunday: Boolean
  monday: Boolean
  tuesday: Boolean
  Wednesday: Boolean
  Thursday: Boolean
  friday: Boolean
  userdays
  usersdaysArr
  constructor(public navCtrl: NavController, public viewCtrl:ViewController, public navParams: NavParams,
    public helper:HelperProvider, public translate: TranslateService) {
     this.userdays  =  this.navParams.get("days")
     console.log("userdays : ",this.userdays)
     this.usersdaysArr =  this.userdays.split(",")
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DaysPopoverPage');
    for(var i=0;i<this.usersdaysArr.length;i++){
      if(this.usersdaysArr[i] == "1")
        this.saturday = true 
      else if(this.usersdaysArr[i] == "2")
      this.sunday = true 
      else if(this.usersdaysArr[i] == "3")
      this.monday = true 
      else if(this.usersdaysArr[i] == "4")
      this.tuesday = true 
      else if(this.usersdaysArr[i] == "5")
      this.Wednesday = true 
      else if(this.usersdaysArr[i] == "6")
      this.Thursday = true 
      else if(this.usersdaysArr[i] == "7")
      this.friday = true 
    }
  }
  DismissDays(){
    let days = []
    if(this.saturday)
    days.push({day:this.translate.instant('saturday'), value: 1})
    if(this.sunday)
    days.push({day:this.translate.instant('sunday'), value: 2})
    if(this.monday)
    days.push({day:this.translate.instant('monday'), value: 3})
    if(this.tuesday)
    days.push({day:this.translate.instant('tuesday'), value: 4})
    if(this.Wednesday)
    days.push({day:this.translate.instant('Wednesday'), value: 5})
    if(this.Thursday)
    days.push({day:this.translate.instant('Thursday'), value: 6})
    if(this.friday)
    days.push({day:this.translate.instant('friday'), value: 7})

    if(days.length > 0)
    this.viewCtrl.dismiss(days)
    else
    this.helper.presentToast(this.translate.instant("selectDay"))
  }
}
