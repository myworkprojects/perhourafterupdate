import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CompanycandidatedetailsPage } from './companycandidatedetails';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';

@NgModule({
  declarations: [
    CompanycandidatedetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(CompanycandidatedetailsPage),
    TranslateModule.forChild()
  ],
})
export class CompanycandidatedetailsPageModule {}
