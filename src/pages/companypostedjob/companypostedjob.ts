import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController,MenuController,AlertController } from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
import { ServicesProvider } from '../../providers/services/services';
import { Storage } from '@ionic/storage';
import { DatePipe } from '@angular/common';
import { CompanyselectedcandidatesPage } from '../companyselectedcandidates/companyselectedcandidates';
import { TranslateService } from '@ngx-translate/core';
import { CompanypostajobPage } from '../companypostajob/companypostajob';

@Component({
  selector: 'page-companypostedjob',
  templateUrl: 'companypostedjob.html',
})
export class CompanypostedjobPage {
  langDirection:any
  comsid:any
  jobdata:any=[]
  date:any
  disable:any=false;
  scaleClass="";
  hideNoDataTxt = true;
  CompanyLogo
  
  from
  hideback
  constructor( public alertCtrl:AlertController,public menu:MenuController,public translate:TranslateService,public datepipe:DatePipe,public storage:Storage,public service:ServicesProvider,public ViewCtrl:ViewController,public helper:HelperProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.langDirection = this.helper.lang_direction;
    if(this.langDirection == "rtl")
    this.scaleClass="scaleClass";

    this.hideNoDataTxt = true;

    this.hideback = true
    if(this.navParams.get("from") && this.navParams.get("from") == "notification")
    {
      this.hideback = false;
    }else{
      this.hideback = true;
    }

  }



  ionViewWillEnter(){
    console.log("ionViewWillEnter CompanypostedjobPage");
    this.date=this.datepipe.transform(new Date(), 'MM-dd-yyyy')
    console.log(this.date)
    this.langDirection = this.helper.lang_direction;
    this.storage.get('js_info').then((val)=>{
      console.log(JSON.stringify(val))
      if(val)
      {
        this.CompanyLogo = val.CompanyLogo
        this.comsid=val.SID
        console.log(this.comsid)
        this.service.companypostedjob(this.comsid,(data)=>{
          console.log(JSON.stringify(data))

          let Dataparse=JSON.parse(data)
          if(Dataparse.length == 0)
          {
            this.hideNoDataTxt = false;
            //this.helper.presentToast(this.translate.instant("noPostedjobs"))
          }else{
            this.hideNoDataTxt = true;
          this.jobdata=Dataparse
          // this.jobdata.forEach(element => {
           
          // });
          this.jobdata.forEach(element => {
            element.CreationDate= this.datepipe.transform(element.CreationDate.split('T')[0], 'MM-dd-yyyy')
           // element.CreationDate= (this.date - parseInt(element.CreationDate))/ (24 * 3600 * 1000);
          
          
           if(element.Status == "1")
           {
            element. jobbordercolor ="2px solid #2783c3";
            element.hideviewDEtailsForUNposted = false;
           }
          else if(element.Status == "0")
          {
            element. jobbordercolor ="2px solid red";
            element.hideviewDEtailsForUNposted = true;
          }
          
          
          });
          console.log(JSON.stringify(Dataparse))
        }

        },(data)=>{
          this.hideNoDataTxt = true;
        })
      }
  
    })
  }
  ionViewDidLoad() {
    // this.date=this.datepipe.transform(new Date(), 'MM-dd-yyyy')
    // console.log(this.date)
    // this.langDirection = this.helper.lang_direction;
    // this.storage.get('js_info').then((val)=>{
    //   console.log(JSON.stringify(val))
    //   if(val)
    //   {
    //     this.comsid=val.SID
    //     console.log(this.comsid)
    //     this.service.companypostedjob(this.comsid,(data)=>{
    //       console.log(JSON.stringify(data))
    //       let Dataparse=JSON.parse(data)
    //       this.jobdata=Dataparse
    //       this.jobdata.forEach(element => {
           
    //       });
    //       this.jobdata.forEach(element => {
    //         element.CreationDate= this.datepipe.transform(element.CreationDate.split('T')[0], 'MM-dd-yyyy')
    //        // element.CreationDate= (this.date - parseInt(element.CreationDate))/ (24 * 3600 * 1000);
    //       });
    //       console.log(JSON.stringify(Dataparse))

    //     },(data)=>{
    
    //     })
    //   }
  
    // })
   
    console.log('ionViewDidLoad CompanypostedjobPage');
  }
  showdetails(name,id,count)
  {
    if(count>0)
    {
    this.navCtrl.push('CompanyselectedcandidatesPage',{name: name,ID:id})
    }
    else
    {
      this.helper.presentToast(this.translate.instant('nocandidate'))
    }
  }

  dismiss(){
  this.navCtrl.pop();
    // this.navCtrl.parent.select(0);
  }
  openPostedJob(){
    this.navCtrl.push(CompanypostajobPage);
  }
  refresher;

  doRefresh(ev){
  this.refresher = ev;

  

  this.date=this.datepipe.transform(new Date(), 'MM-dd-yyyy')
    console.log(this.date)
    this.langDirection = this.helper.lang_direction;
    this.storage.get('js_info').then((val)=>{
      console.log(JSON.stringify(val))
      if(val)
      {
        this.comsid=val.SID
        console.log(this.comsid)
        this.service.companypostedjob(this.comsid,(data)=>{
          // console.log(JSON.stringify(data))
          let Dataparse=JSON.parse(data)
          this.jobdata=Dataparse
          // this.jobdata.forEach(element => {
           
          // });

          if(Dataparse.length == 0)
          {
            this.hideNoDataTxt = false;
            
          }else{
            this.hideNoDataTxt = true;
          }
          this.jobdata.forEach(element => {
            element.CreationDate= this.datepipe.transform(element.CreationDate.split('T')[0], 'MM-dd-yyyy')
           
            // element.CreationDate= (this.date - parseInt(element.CreationDate))/ (24 * 3600 * 1000);
          
            if(element.Status == "1")
            {
             element. jobbordercolor ="2px solid #2783c3";
             element.hideviewDEtailsForUNposted = false;
            }
           else if(element.Status == "0")
           {
             element. jobbordercolor ="2px solid red";
             element.hideviewDEtailsForUNposted = true;
           }
           
          
          });
          console.log(JSON.stringify(Dataparse))
          if(this.refresher){
            this.refresher.complete();
          }

        },(data)=>{
          if(this.refresher){
            this.refresher.complete();
          }
    
        })
      }
  
    })
   

  }
  openmenu()
  {
    this.menu.open()
  }


  canceluppostedjob(item){
    console.log("item from canceluppostedjob",item);
    let alert = this.alertCtrl.create({
      title: this.translate.instant('cancelJob'),
      buttons: [
        {
          text: this.translate.instant('no2'),
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: this.translate.instant('yes2'),
          handler: () => {
        this.service.compCacelNotPostedJob(this.comsid ,item.SID,resp=>{
          console.log("resp from compCacelNotPostedJob",resp);
          if(JSON.parse(resp).Result == "1")
            this.helper.presentToast(this.translate.instant("jobcanceled"));
          else if( JSON.parse(resp).Result == "-2")
            this.helper.presentToast(this.translate.instant("can'tcancelThisJob"));
          else 
          this.helper.presentToast(this.translate.instant("ServerError"));

          this.doRefresh2();
        },err=>{
          console.log("err from compCacelNotPostedJob",err);
          this.helper.presentToast(this.translate.instant("ServerError"));
          this.doRefresh2();
        })
          }
        }
      
    ]
  })
  alert.present()
  }
  
  doRefresh2(){
   
    this.date=this.datepipe.transform(new Date(), 'MM-dd-yyyy')
    console.log(this.date)
    this.langDirection = this.helper.lang_direction;
    console.log("for refresh2")
    this.storage.get('js_info').then((val)=>{
      console.log(JSON.stringify(val))
      if(val)
      {
        this.comsid=val.SID
        console.log(this.comsid)
        this.service.companypostedjob(this.comsid,(data)=>{
          console.log("for refresh1")
          if(this.refresher){
            this.refresher.complete();
          }
          console.log(JSON.stringify(data))
          let Dataparse=JSON.parse(data)
          if(Dataparse.length == 0)
          {
            this.hideNoDataTxt = false;
            this.jobdata=[]
            //this.helper.presentToast(this.translate.instant("noPostedjobs"))
          }else{
            this.hideNoDataTxt = true;
          this.jobdata=Dataparse
          // this.jobdata.forEach(element => {
           
          // });
          this.jobdata.forEach(element => {
            element.CreationDate= this.datepipe.transform(element.CreationDate.split('T')[0], 'MM-dd-yyyy')
           // element.CreationDate= (this.date - parseInt(element.CreationDate))/ (24 * 3600 * 1000);
          
           if(element.Status == "1")
           {
            element. jobbordercolor ="2px solid #2783c3";
            element.hideviewDEtailsForUNposted = false;
           }
          else if(element.Status == "0")
          {
            element. jobbordercolor ="2px solid red";
            element.hideviewDEtailsForUNposted = true;
          }

          });
          
          console.log(JSON.stringify(Dataparse))
        }

          

        },(data)=>{
          
        })
      }
  
    })
   
    
  }



  details(item){
    console.log("details item: ",item);
      this.navCtrl.push("CompJobDetailsPage",{jobId:item.SID})
    }
    selected(item){
      console.log("selected item: ",item);
      var newjobname = "";
      if(this.langDirection == "rtl")
      newjobname = item.JobTitleAr;
      else if(this.langDirection == "ltr")
      newjobname = item.JobTitleEn;
      this.navCtrl.push("CompanyselectedusersPage",{jobId:item.SID,jobname:newjobname})
    }
    
}
