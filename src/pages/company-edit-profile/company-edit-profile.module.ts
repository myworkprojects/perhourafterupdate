import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CompanyEditProfilePage } from './company-edit-profile';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    CompanyEditProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(CompanyEditProfilePage),
    TranslateModule.forChild(),
  ],
})
export class CompanyEditProfilePageModule {}
