import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,Events} from 'ionic-angular';

import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ServicesProvider } from '../../providers/services/services';
import { HelperProvider } from '../../providers/helper/helper';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-company-edit-profile',
  templateUrl: 'company-edit-profile.html',
})
export class CompanyEditProfilePage {

  profileData

  private registerForm : FormGroup;
  name;
  email;
  
 
  submitAttempt = false;
  errors = {
    nameErr:"",
    emailErr:"",
    descErr:"",
    RegistryNum:""
  };
  langDirection = "";
  placeholder = {name:"",email:"",desc:"",RegistryNum:"" };
  scaleClass="";
  hideemailerr
  userData
  
  desc
  city_id
  RegistryNum
  cities
  registrynotnumber

  constructor(public srv:ServicesProvider, public storage: Storage,public events:Events,
    public translate: TranslateService,public helper:HelperProvider,
    private formBuilder: FormBuilder,public navCtrl: NavController,
    public navParams: NavParams) {
    this.profileData =  this.navParams.get("data")
    
    console.log("profileData : ",this.profileData)

    // this.navCtrl.push("CompanyEditProfilePage",{data:{name:this.compName,desc:this.compShortDescription,mail:this.compEmail}})

    this.name = this.profileData.name
    this.desc = this.profileData.desc
    this.email = this.profileData.mail
    this.city_id = this.profileData.CitySID
    this.RegistryNum = this.profileData.RegistryNum
this.registrynotnumber = ""
    // this.citytxt = this.profileData.citytxt
    
    this.langDirection = this.helper.lang_direction;

    if(this.langDirection == "rtl")
    this.scaleClass="scaleClass";
    
    this.registerForm = this.formBuilder.group({
            name :['',Validators.required],
            email:['', Validators.compose([Validators.required,Validators.email])],
            desc:['',Validators.required],
            // RegistryNum:['', Validators.pattern(/[0-9]/)], //"[0-9]"
            RegistryNum:['',''],
            city_id:['','']
    });

    this.placeholder.name = this.translate.instant("name");
    this.errors.nameErr = this.translate.instant("nameErr");

    // this.placeholder.desc = this.translate.instant("name");
    this.errors.descErr = this.translate.instant("descErr");

    this.placeholder.email = this.translate.instant("email");
    this.hideemailerr = true;
    this.placeholder.RegistryNum = this.translate.instant("RegistryNum")
    this.errors.RegistryNum = this.translate.instant("registryerr")

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CompanyEditProfilePage');
    this.srv.listCities((data)=>{
      data = JSON.parse(data)
      this.cities = data
      console.log(data)
      if(this.profileData.CitySID)
      this.city_id = this.profileData.CitySID

      console.log("city_id: ",this.city_id)

    },
    ()=> this.helper.presentToast(this.translate.instant("ServerError")))
  }

  dismiss(){
    this.navCtrl.pop()
  }

  save(){
    if(! this.registerForm.valid ){
      this.submitAttempt=true;
      console.log("form not valid")
    //  console.log("this.registerForm.controls[RegistryNum].errors : ",this.registerForm.controls["RegistryNum"].errors)
      
    if(! Number(this.RegistryNum))
      this.registrynotnumber = this.translate.instant("registryerr")
    else
      this.registrynotnumber = ""
       
    if(this.registerForm.controls["email"].errors){
        if(this.registerForm.controls["email"].errors['required'])
        {
          this.hideemailerr = false;
          this.errors.emailErr = this.translate.instant("emailErr");
        }
        else if(this.registerForm.controls["email"].errors['email']) //invalidChars 
        {
          this.hideemailerr = false;
          this.errors.emailErr = this.translate.instant("invalidEmailAddress");
        }   
        else
        {
          this.errors.emailErr="";
          this.hideemailerr = true;
          console.log("phone errors:",this.registerForm.controls["email"].errors);
        }

      }
      console.log("invalid from , email : ",this.email)
      if(this.email){
        if(! ( this.email.includes('.com') || this.email.includes('.net')) ) 
        {
          console.log("not contain  .net .com");
          this.errors.emailErr = this.translate.instant("invalidEmailAddress");
          this.hideemailerr = false;
        }else{
          this.errors.emailErr = "";
          this.hideemailerr = true;
        }
      }

    }else{

      if(! ( this.email.includes('.com') || this.email.includes('.net')) ) 
      {
        console.log("not contain  .net .com");
        this.errors.emailErr = this.translate.instant("invalidEmailAddress");
        this.hideemailerr = false;
      }else{
        this.errors.emailErr = "";
        this.hideemailerr = true;

   if(! Number(this.RegistryNum)){
    this.registrynotnumber = this.translate.instant("registryerr")
    return;
   }else{
    this.registrynotnumber = ""
   }
    

        var citytxt = ""
        for(var x=0;x<this.cities.length;x++){
          if(this.cities[x].SID == this.city_id)
          {
            if(this.langDirection == "rtl")
              citytxt = this.cities[x].CityName_Ar
            else if(this.langDirection == "ltr")
              citytxt = this.cities[x].CityName
          }  
        }
        this.storage.get("js_info").then((val) => {
          console.log("val from get js info from edit : ",val)
var data = {
  'compid' : val.SID,
  'name':this.name,
  'Mail':this.email,
  'ShortDescription':this.desc,
  'CitySID':this.city_id,
  'RegistryNum':this.RegistryNum,
  "citytxt":citytxt
 

}
  console.log("saved data : ",data)
this.srv.compEditProfile(data,resp=>{
  console.log("resp from jsEditProfile",resp);
  if(JSON.parse(resp).OperationResult == "1")
  {
    this.storage.get('js_info').then((val) => {
          console.log("val from get js info from edit", val);
      if (val) {
        this.userData = val; 
        this.userData.CompanyName = this.name;
        this.userData.Email = this.email; 
        this.userData.ShortDescription = this.desc;   
        this.userData.CitySID = this.city_id,
        this.userData.RegistryNum=this.RegistryNum,
        this.userData.citytxt = citytxt 
        this.events.publish("profileChanged",this.name)
        this.events.publish("compprofileChangedall",this.userData)
        this.helper.storeJSInfo(this.userData);
        }
      });

    this.helper.presentToast(this.translate.instant("suedit"));
    this.navCtrl.pop();

    
  }else{
    this.helper.presentToast(this.translate.instant("ServerError"));
    this.navCtrl.pop();
  }
    
    

},err=>{
  console.log("err from jsEditProfile",err);
  this.helper.presentToast(this.translate.instant("ServerError"));
})

        
});


      }
      
    }

  }
}
