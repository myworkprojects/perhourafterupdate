import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CompanyselectedcandidatesPage } from './companyselectedcandidates';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';

@NgModule({
  declarations: [
    CompanyselectedcandidatesPage,
  ],
  imports: [
    IonicPageModule.forChild(CompanyselectedcandidatesPage),
    TranslateModule.forChild()
  ],
})
export class CompanyselectedcandidatesPageModule {}
