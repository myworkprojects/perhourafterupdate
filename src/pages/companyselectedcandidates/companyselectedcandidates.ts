import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
import { ServicesProvider } from '../../providers/services/services';
import { TranslateService } from '@ngx-translate/core';
// import { ShortlistPage } from '../shortlist/shortlist';
import { CompanycandidatedetailsPage } from '../companycandidatedetails/companycandidatedetails';
import { JsProfilePage } from '../js-profile/js-profile';

@IonicPage()

@Component({
  selector: 'page-companyselectedcandidates',
  templateUrl: 'companyselectedcandidates.html',
})
export class CompanyselectedcandidatesPage {
  langDirection:any
  name:any
  id:any
  candidates:any
  jobid:any;

  scaleClass="";
  
    hideData ;
  

  constructor(public alertCtrl:AlertController,public translate:TranslateService,public services:ServicesProvider,public ViewCtrl:ViewController,public helper:HelperProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.langDirection = this.helper.lang_direction;
    if(this.langDirection == "rtl")
      this.scaleClass="scaleClass";

  }

  ionViewDidLoad() {
    this.name=this.navParams.get("name")
    this.id=this.navParams.get("ID")
    this.langDirection = this.helper.lang_direction;
    console.log(this.id)
    console.log(this.name)
    this.services.companygetappliedcandidates(this.id,(data)=>{
      let DataParsed=JSON.parse(data)
      this.candidates=DataParsed.Candidates
      console.log(JSON.stringify(this.candidates))
      for(var x=0;x<this.candidates.length;x++){
        // ShortDescription
        if(this.candidates[x].ShortDescription == "1")
          this.candidates[x].ShortDescription2 = this.translate.instant("student")
        else if(this.candidates[x].ShortDescription == "2")
          this.candidates[x].ShortDescription2 = this.translate.instant("employee")
        else if(this.candidates[x].ShortDescription == "3")
          this.candidates[x].ShortDescription2 = this.translate.instant("unemployed")
      }

      if(this.refresher){
        this.refresher.complete();
      }
    },(data)=>{
      if(this.refresher){
        this.refresher.complete();
      }
    })
    console.log('ionViewDidLoad CompanyselectedcandidatesPage');
  }
  dismiss(){
    this.navCtrl.pop();
  }
  hire(id)
  {

  let alert = this.alertCtrl.create({
    title: this.translate.instant('hirecandidate'),
    buttons: [
      {
        text: this.translate.instant('no2'),
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: this.translate.instant('yes2'),
        handler: () => {
       this.confirm(id)
        }
      }
    
  ]
})
alert.present()
  }
  confirm(id)
  {
    
    this.services.companyhirecandidate(id,this.id,(data)=>{
      let DataParsed=JSON.parse(data)
      if (DataParsed.OperationResult == 1) {
        this.helper.presentToast(this.translate.instant('candidateadded'))

      for(var i=0;i<this.candidates.length;i++)
      {
       if(this.candidates[i].JobSeekerSID==id)
       {
       this.candidates.splice(i, 1);
       }
        }
      }
      else if(DataParsed.OperationResult == -2)
      {
        this.helper.presentToast(this.translate.instant('addedbefore'))

      }else if(DataParsed.OperationResult == "-3"){
        this.helper.presentToast(this.translate.instant('ReachMaxNo'))
      }
      console.log(DataParsed)
      this.navCtrl.pop();
    },(data)=>{
      this.navCtrl.pop();
    })
  }
  getmore(item)
  {
   // this.navCtrl.push(ShortlistPage,{name:this.name,ID:id})
   console.log(item)
this.navCtrl.push('CandidateDetailsPage',{data:item,ID:this.id})
  }
  gotoshort(id)
  {
    this.navCtrl.push('ShortlistPage',{name:this.name,ID:this.id})

  }
  refresher;

  doRefresh(ev){
    this.refresher = ev;
    this.services.companygetappliedcandidates(this.id,(data)=>{
      let DataParsed=JSON.parse(data)
      if(DataParsed.Candidates.length == 0)
      {
        this.hideData = false;
      }else{
        this.hideData = true;
      }
      this.candidates=DataParsed.Candidates;
      console.log(JSON.stringify(this.candidates))
      if(this.refresher){
        this.refresher.complete();
      }
    },(data)=>{
      if(this.refresher){
        this.refresher.complete();
      }
    })

  }

  showcv(item){
    console.log("item from showcv: ",item)
    // if(item.CVFile){
    //   console.log("if cvlinkfromstorge");
    //   window.open(item.CVFile);
    // }else{
    //   console.log("else cvlinkfromstorge");
    //   this.helper.presentToast(this.translate.instant("nocvtoshow"));
      
    // }
    this.navCtrl.push(JsProfilePage,{item:item,from:"showProfile"});
  }
}
