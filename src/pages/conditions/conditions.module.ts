import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConditionsPage } from './conditions';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';

@NgModule({
  declarations: [
    ConditionsPage,
  ],
  imports: [
    IonicPageModule.forChild(ConditionsPage),
    TranslateModule.forChild()
  ],
})
export class ConditionsPageModule {}
