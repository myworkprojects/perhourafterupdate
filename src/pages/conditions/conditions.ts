import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { HelperProvider } from '../../providers/helper/helper';

@IonicPage()
@Component({
  selector: 'page-conditions',
  templateUrl: 'conditions.html',
})
export class ConditionsPage {
  scaleClass="";
  langDirection;
  conditionsTxt = "";
  constructor(public helper:HelperProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.conditionsTxt = this.navParams.get('data');
    this.langDirection = this.helper.lang_direction;
    if(this.langDirection == "rtl")
    this.scaleClass="scaleClass";
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConditionsPage');
  }

  dismiss(){
    this.navCtrl.pop();
  }

}
