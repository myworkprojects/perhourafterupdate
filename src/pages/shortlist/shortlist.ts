// import { Component } from '@angular/core';
// import { IonicPage, NavController, NavParams } from 'ionic-angular';

// /**
//  * Generated class for the ShortlistPage page.
//  *
//  * See https://ionicframework.com/docs/components/#navigation for more info on
//  * Ionic pages and navigation.
//  */

// @IonicPage()
// @Component({
//   selector: 'page-shortlist',
//   templateUrl: 'shortlist.html',
// })
// export class ShortlistPage {

//   constructor(public navCtrl: NavController, public navParams: NavParams) {
//   }

//   ionViewDidLoad() {
//     console.log('ionViewDidLoad ShortlistPage');
//   }

// }

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ViewController } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { TranslateService } from '@ngx-translate/core';
import { HelperProvider } from '../../providers/helper/helper';

@IonicPage()
@Component({
  selector: 'page-shortlist',
  templateUrl: 'shortlist.html',
})
export class ShortlistPage {
  name:any
  jobid:any
  langDirection:any
  candidates:any=[];
  scaleClass = "";
  hideemptydata ;
  constructor(public ViewCtrl:ViewController,public alertCtrl:AlertController,public service:ServicesProvider,public translate:TranslateService,public helper:HelperProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.langDirection = this.helper.lang_direction;
    if(this.langDirection == "rtl")
      this.scaleClass="scaleClass";

      this.hideemptydata = true;
  }

  ionViewDidLoad() {
    this.langDirection=this.helper.lang_direction
    this.name=this.navParams.get("name")
    this.jobid=this.navParams.get("ID")
this.service.companygetshortlist(this.jobid,(data)=>{
  let DataParsed=JSON.parse(data)
  console.log(JSON.stringify(DataParsed))
  if(DataParsed.OperationResult == "1")
    this.hideemptydata = true;
  else if (DataParsed.OperationResult == "-1")
    this.hideemptydata = false;

    this.candidates=DataParsed.Candidates;
},(data)=>{

})
    console.log('ionViewDidLoad ShortlistPage');
  }
  hire(id)
  {

  let alert = this.alertCtrl.create({
    title: this.translate.instant('hirecandidate'),
    buttons: [
      {
        text: this.translate.instant('cancel'),
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: this.translate.instant('done'),
        handler: () => {
       this.confirm(id)
        }
      }
    
  ]
})
alert.present()
  }
  confirm(id)
  {
    
    this.service.companyhirecandidate(id,this.jobid,(data)=>{
      let DataParsed=JSON.parse(data)
      if (DataParsed.OperationResult == 1) {
        this.helper.presentToast(this.translate.instant('candidateadded'))

      for(var i=0;i<this.candidates.length;i++)
      {
       if(this.candidates[i].JobSeekerSID==id)
       {
       this.candidates.splice(i, 1);
       }
        }
      }
      else if(DataParsed.OperationResult == -2)
      {
        this.helper.presentToast(this.translate.instant('addedbefore'))

      }
      console.log(DataParsed)
    },(data)=>{

    })
  }
  deleteItem(id)
  {
    let alert = this.alertCtrl.create({
      title: this.translate.instant('deletecandidate'),
      buttons: [
        {
          text: this.translate.instant('cancel'),
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: this.translate.instant('done'),
          handler: () => {
         this.delete(id)
          }
        }
      
    ]
  })
  alert.present()
  }
  delete(id)
  {
    this.service.companyremovefromshortlist(id,this.jobid,(data)=>{
      let DataParsed=JSON.parse(data)
      for(var i=0;i<this.candidates.length;i++)
      {
       if(this.candidates[i].JobSeekerSID==id)
       {
       this.candidates.splice(i, 1);
       }
        }
    },(data)=>{})
      }
      dismiss(){
        this.navCtrl.pop();
      }

      
}
