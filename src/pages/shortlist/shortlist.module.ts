import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShortlistPage } from './shortlist';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
@NgModule({
  declarations: [
    ShortlistPage,
  ],
  imports: [
    IonicPageModule.forChild(ShortlistPage),
    TranslateModule.forChild()
  ],
})
export class ShortlistPageModule {}
