import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
import { ServicesProvider } from '../../providers/services/services';
import { TranslateService } from '@ngx-translate/core';
import { DatePipe } from '@angular/common';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@IonicPage()
@Component({
  selector: 'page-study',
  templateUrl: 'study.html',
})
export class StudyPage {

  scaleClass="";
  langDirection;
  edus;
  hideNoDataTxt;

  jsid;
  refresher;
  hideForShowProfile;
  
  constructor(private iab: InAppBrowser,public datepipe: DatePipe,public translate: TranslateService,public service:ServicesProvider, public helper:HelperProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.langDirection = this.helper.lang_direction;
    if(this.langDirection == "rtl")
    this.scaleClass="scaleClass";

    this.hideNoDataTxt = true;

    this.jsid = this.navParams.get("userId");

    this.hideForShowProfile = this.navParams.get('from');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StudyPage');
   
  }
  ionViewWillEnter(){
    console.log("ionViewWillEnter StudyPage");
    this.loadEdu();
  }

  loadEdu(){
    this.service.jsgetEducation(this.jsid,resp=>{
      console.log("resp from jsgetEducation",resp);
      var respData = JSON.parse(resp);
      if(respData.Result == "-1")
        this.hideNoDataTxt = false;
      else  if(respData.Result == "1")
      {
        this.hideNoDataTxt = true;
        this.edus = respData.list;
        for(var x=0;x<this.edus.length;x++){
         
          if(this.edus[x].StillLearning == "1")
          this.edus[x].date = this.datepipe.transform(this.edus[x].StartDate.split("T")[0], 'dd-MM-yyyy')  + " , " + this.translate.instant("present");
          else if(this.edus[x].StillLearning == "0")
          this.edus[x].date = this.datepipe.transform(this.edus[x].StartDate.split("T")[0], 'dd-MM-yyyy')   + " , " + this.datepipe.transform(this.edus[x].EndDate.split("T")[0], 'dd-MM-yyyy') ;


        }
        
      }


      if(this.refresher){
        this.refresher.complete();
      }

    },err=>{
      console.log("err from jsgetEducation",err);

      if(this.refresher){
        this.refresher.complete();
      }
      
    })

  }
  dismiss(){
    this.navCtrl.pop();
  }
  addedu(){
    console.log("add exp");
    this.navCtrl.push("AddEditPage",{from:'addedu',id:this.jsid});
  }
  editedu(item){
    console.log("edit exp",item);
    this.navCtrl.push("AddEditPage",{from:'editedu',id:this.jsid,item:item});
  }

  doRefresh(ev){
    this.refresher = ev;
    this.loadEdu();
  }

  showCert(CertFile){
    if (CertFile) {
      console.log("if CertFile",CertFile);
      // window.open(CertFile);
      const browser = this.iab.create(CertFile,'_system',"location=yes");
      
    } else {
      console.log("else CertFile");
      this.helper.presentToast(this.translate.instant("nocerttoshow"));

    }
  }
}
