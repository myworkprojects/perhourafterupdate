import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StudyPage } from './study';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';

@NgModule({
  declarations: [
    StudyPage,
  ],
  imports: [
    IonicPageModule.forChild(StudyPage),
    TranslateModule.forChild()
  ],
})
export class StudyPageModule {}
