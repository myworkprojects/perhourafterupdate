import { Component ,ViewChild,ElementRef} from '@angular/core';
import { IonicPage, NavController, NavParams ,Events} from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
import { ServicesProvider } from '../../providers/services/services';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';
import { TabsPage } from '../tabs/tabs';
declare var google;


@IonicPage()
@Component({
  selector: 'page-companyaddlocation',
  templateUrl: 'companyaddlocation.html',
})
export class CompanyaddlocationPage {
  
  @ViewChild('map') mymap:ElementRef;
  map:any
 
  allMarkers = [] ; 
  lat:any
  markers:any=[]
  long:any
  scaleClass="";
  langDirection;
  myLongAddress;
  id="";
  constructor(private storage: Storage,public translate:TranslateService,public service2:ServicesProvider, public events: Events,public helper:HelperProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.langDirection = this.helper.lang_direction;
    if(this.langDirection == "rtl")
      this.scaleClass="scaleClass";

      this.id = this.navParams.get('data');
      this.events.subscribe('detectUserLocation', () => {
        console.log("suscribe detectUserLocation")
        this.initMap();      
      });
      events.subscribe('LocationSaved', () => {
        console.log("location saved subscribe")
        this.dismiss();
      });
//       events.subscribe('callapi', () => {

// this.service2.getaddress(this.lat,this.long).subscribe(
//   resp=>{
//     console.log("resp from get address1",resp);
//      this.myLongAddress =  JSON.parse(JSON.stringify(resp)).results[0].formatted_address;
  
//      this.service2.companyAddLocation(this.id,this.lat,this.long,"",resp=>{
//       console.log("resp from companyaddlocation",resp);
//     },err=>{
//       console.log("err from companyaddlocation",err);
//     });

//   },err=>{
//     console.log("err from get address1",err);
//     this.helper.presentToast(this.translate.instant("conectionError"));
//   }
// );
// });


  }
  locateUser(){
    //this.helper.detectUserLoctaion(); 
    this.helper.geoLoc(()=>{})
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CompanyaddlocationPage');

    this.storage.get('comp_location').then((val) => {
      console.log("val from get comp_location",val);
      if(val){
        console.log("from if val");
        this.helper.lat = val.lat;
        this.helper.lng = val.long;
        this.initMap();
      
      }else{
        console.log("from else val");
        this.initMap();
    //this.helper.detectUserLoctaion(); 
    this.helper.geoLoc(()=>{})
    console.log("after call detectUserLoctaion"); 
    this.events.subscribe('detectUserLocation', () => {
      console.log("suscribe detectUserLocation")
      this.initMap();      
    });
      }
      
    }
    );

    

    // this.lat = this.helper.lat;
    // this.long = this.helper.lng;
  }
  

  initMap(){
    // let page=this
    const locations =new google.maps.LatLng(this.helper.lat,this.helper.lng)
    let mapOptions = {
        center:locations,
        zoom:15,//8
        fullscreenControl:false,
        streetViewControl:false
        // controls: {
        //   myLocationButton: true         
        // }
        // mapTypeControl: true,
        //   mapTypeControlOptions: {
        //     style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
        //     mapTypeIds: ['roadmap', 'terrain']
        //   }
        };
        this. map =new google.maps.Map(this.mymap.nativeElement,mapOptions) 
        var marker = new google.maps.Marker({
          position: locations,
          draggable:true,
          map: this.map
        });
        this.markers.push(marker)
        google.maps.event.addListener(this.map, 'click', (event)=> {
          this.lat=event.latLng.lat()
          this.long=event.latLng.lng()
         
          this.helper.lat =this.lat;
          this.helper.lng = this.long;
          // console.log("this.clickForLocation:",this.clickForLocation)
         

console.log("lat lng: ",this.lat,this.long);
console.log("this : ",this);
console.log("service : ",this.service2);
// this.events.publish('callapi');

// this.service2.getaddress(this.lat,this.long).subscribe(
//   resp=>{
//     console.log("resp from get address1",resp);
//      this.myLongAddress =  JSON.parse(JSON.stringify(resp)).results[0].formatted_address;
  
//      this.service2.companyAddLocation(this.id,this.lat,this.long,"",resp=>{
//       console.log("resp from companyaddlocation",resp);
//     },err=>{
//       console.log("err from companyaddlocation",err);
//     });

//   },err=>{
//     console.log("err from get address1",err);
//     this.helper.presentToast(this.translate.instant("conectionError"));
//   }
// );



          for(var i=0;i<this.markers.length;i++)
          {
            this.markers[i].setMap(null);

          }

          this.placeMarker(event.latLng);
      });

  }

  placeMarker(location) {

    var marker;
   
   if (marker == undefined){
       marker = new google.maps.Marker({
           position: location,
           map: this.map, 
           animation: google.maps.Animation.DROP,
       });
       this.markers.push(marker)
   }
   else{
     this.markers.push(marker)
       marker.setPosition(location);
   }
   
   this.map.setCenter(location);
  
  }
  
  dismiss(){
    // this.navCtrl.setRoot(TabsPage);
    // this.navCtrl.parent.select(3);
    console.log("enter dismiss");
    this.navCtrl.pop();
  }
  Assign(){
    if(this.helper.lat && this.helper.lng){

    
    this.service2.companyAddLocation(this.id,this.helper.lat,this.helper.lng,resp=>{
      console.log("resp from companyaddlocation",resp);
      if(JSON.parse(resp).Result == "1"){
        this.helper.presentToast(this.translate.instant("locsaved"));
        this.helper.storecmplocation({lat:this.lat,long:this.long});
        console.log("after store");
        // this.dismiss();
      }else{
        this.helper.presentToast(this.translate.instant("ServerError"));
        this.dismiss();
      }
    
    },err=>{
      console.log("err from companyaddlocation",err);
    this.helper.presentToast(this.translate.instant("ServerError"));
    this.dismiss();
    });
  }
else
  this.helper.presentToast(this.translate.instant("specifyloc"));

}



}
