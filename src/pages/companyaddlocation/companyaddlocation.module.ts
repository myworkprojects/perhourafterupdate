import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CompanyaddlocationPage } from './companyaddlocation';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';

@NgModule({
  declarations: [
    CompanyaddlocationPage,
  ],
  imports: [
    IonicPageModule.forChild(CompanyaddlocationPage),
    TranslateModule.forChild()
  ],
})
export class CompanyaddlocationPageModule {}
