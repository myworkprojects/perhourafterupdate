import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,MenuController} from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
import { ServicesProvider } from '../../providers/services/services';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';


// @IonicPage()
@Component({
  selector: 'page-company-job-history',
  templateUrl: 'company-job-history.html',
})
export class CompanyJobHistoryPage {


  scaleClass="";
  langDirection;
  compSID
  jobsList
  hideNoDataTxt
  refresher

  constructor(public menu:MenuController,public translate: TranslateService,public storage: Storage,public service:ServicesProvider,public helper:HelperProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.langDirection = this.helper.lang_direction;
    if(this.langDirection == "rtl")
    this.scaleClass="scaleClass";

    this.hideNoDataTxt = true;

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CompanyJobHistoryPage');
    this.loadJobsHistory();
  }


  dismiss(){
    this.navCtrl.pop();
  }

  loadJobsHistory(){
console.log("load jobs history")
    this.storage.get("js_info").then((val) => {
      console.log("val from js info storage",val)
      if(val)
       this.compSID =  val.SID;

      console.log("sid : ",this.compSID)
    

    this.service.companyJobHistory(this.compSID,resp=>{
      console.log("resp from companyJobHistory",resp);
      var respdata = JSON.parse(resp);
      if(respdata.Result ==  "-1")
      this.hideNoDataTxt = false;
    else if(respdata.Result ==  "1")
    {  this.hideNoDataTxt = true;

        this.jobsList = respdata.List
        console.log("jobsList : ",this.jobsList);
        var re = /T/g;
        for(var i=0;i<this.jobsList.length;i++){

          // "jobdone":"Done",
          // "jobcancel":"Cancelled",
          // "jobtimeout":"Time Out",
          // 1 not completeed , 2 done ,-2 canceled
          if(this.jobsList[i].Status == "1")
            this.jobsList[i].txt = this.translate.instant("jobnotcompleted")
          else if(this.jobsList[i].Status == "2")
            this.jobsList[i].txt = this.translate.instant("jobdone")
          else if(this.jobsList[i].Status == "-2" || this.jobsList[i].Status == "-1")
            this.jobsList[i].txt = this.translate.instant("jobcancel")

          this.jobsList[i].StartDate = this.jobsList[i].StartDate.replace(re, " , "); 
          this.jobsList[i].EndDate = this.jobsList[i].EndDate.replace(re, " , "); 
        }
      
      }
        if(this.refresher){
        this.refresher.complete();
        }


    },err=>{
      console.log("err from companyJobHistory",err);
      if(this.refresher){
        this.refresher.complete();
      }

    });

  });


  }


  doRefresh(ev){
    this.refresher = ev;
    console.log("do refresh")
    this.loadJobsHistory();
    
  }
  gotoSelected(item){
    var newjobname
    if(this.langDirection == "rtl")
      newjobname = item.JobTitleAr
    else if (this.langDirection == "ltr")
      newjobname  = item.JobTitleEn
    // this.navCtrl.push("CompanyselectedusersPage",{jobId:item.SID,jobname:newjobname})
    this.navCtrl.push("CompJobDetailsPage",{jobId:item.SID})
  }
  
  openmenu()
  {
    this.menu.open()
  }


}
