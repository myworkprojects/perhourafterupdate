import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Events } from 'ionic-angular';

import { HelperProvider } from '../../providers/helper/helper';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { matchOtherValidator } from '../../validators/passwordValidator';
import { ServicesProvider } from '../../providers/services/services';
import { TabsPage } from '../tabs/tabs';
import { Storage } from '@ionic/storage';


@IonicPage()
@Component({
  selector: 'page-changepass',
  templateUrl: 'changepass.html',
})
export class ChangepassPage {

  scaleClass="";
  langDirection;
  password;
  confirmpassword;
  oldPass

  private registerForm : FormGroup;
  errors = {
    passwordErr:"",
    confirmpasswordErr:"",
    oldPassErr:""
  };

  placeholder = {password:"",confirmPassword:"",oldPass:""};
  submitAttempt = false;
  type = "";
  usermobile="";
  userid = "";

  constructor(public storage: Storage,public events:Events,public service: ServicesProvider,public translate: TranslateService,private formBuilder: FormBuilder,public helper:HelperProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.type  = this.navParams.get('type'); //jsActivationForget , compActivationForget
    this.usermobile  = this.navParams.get('mobile');
    this.userid = this.navParams.get('id');

    this.langDirection = this.helper.lang_direction;
    
    if(this.langDirection == "rtl")
    this.scaleClass="scaleClass";
    
  
    this.registerForm = this.formBuilder.group({
      oldPass:['', Validators.compose([Validators.minLength(4), Validators.required])],
        password: ['', Validators.compose([Validators.minLength(4), Validators.required])],
        confirmpassword: ['', Validators.compose([Validators.minLength(4),  Validators.required, matchOtherValidator('password')])],
       
      });


      this.errors.passwordErr = this.translate.instant("passwordNewErr");
    this.errors.confirmpasswordErr = this.translate.instant("passwordNotConfirmed");
    this.placeholder.password = this.translate.instant("enterNewPassword");
    this.placeholder.confirmPassword = this.translate.instant("enterNewConfirmPass");
    this.placeholder.oldPass = this.translate.instant("enterOldPass")
    this.errors.oldPassErr = this.translate.instant("oldpasserr")

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangepassPage');
  }


  send(){
    console.log("send");
  
  if(! this.registerForm.valid ){
    this.submitAttempt=true;

    if(this.registerForm.controls["oldPass"].errors){
      if(this.registerForm.controls["oldPass"].errors['required'])
        this.errors.oldPassErr = this.translate.instant("oldpasserr");
      else if (this.registerForm.controls["oldPass"].errors['minlength'])
        this.errors.oldPassErr = this.translate.instant("passErr");
      else
        console.log("oldPassErrors:",this.registerForm.controls["oldPass"].errors);
    
    }

    if(this.registerForm.controls["password"].errors){
      if(this.registerForm.controls["password"].errors['required'])
        this.errors.passwordErr = this.translate.instant("passwordNewErr");
      else if (this.registerForm.controls["password"].errors['minlength'])
        this.errors.passwordErr = this.translate.instant("passErr");
      else
        console.log("passErrors:",this.registerForm.controls["password"].errors);
    
    }

    if(this.registerForm.controls["confirmpassword"].errors){
      if(this.registerForm.controls["confirmpassword"].errors['required'])
        this.errors.confirmpasswordErr = this.translate.instant("confirmpasswordnewErr");
      else if (this.registerForm.controls["confirmpassword"].errors['minlength'])
        this.errors.confirmpasswordErr = this.translate.instant("passErr");
      else if(this.registerForm.controls["confirmpassword"].errors['matchOther'])
        this.errors.confirmpasswordErr = this.translate.instant("passwordNotConfirmed");
      else
        console.log("confirmpasswordErrors:",this.registerForm.controls["confirmpassword"].errors);
    
    }
  
  }else{
    

    console.log("confirm pass this.type: ",this.type);
    if(this.type == "jsActivationForget")
    {
      this.service.jsCheckPass(this.userid,this.password,resp=>{
        console.log("resp from jsCheckPass",resp);
        // after login or login data
        // var respData = JSON.parse(resp);
        // respData.type = "js";
        // this.helper.storeJSInfo(respData);
        // this.events.publish('user:userLoginSucceeded', respData);
        // this.navCtrl.setRoot(TabsPage); 

        this.service.jsLogin(this.usermobile,this.password,resp=>{
          console.log("resp from login",resp);
          
          var respData = JSON.parse(resp);
  
          if(respData["SID"])
          {
            respData.type = "js";
            this.helper.storeJSInfo(respData);
            this.events.publish('user:userLoginSucceeded', respData);
            this.navCtrl.setRoot(TabsPage); 
          }
          else 
            this.helper.presentToast(this.translate.instant("invalidData"))
          /*
          {"SID":1013,"FirstName":"ss","MiddleName":"","SirName":"","Email":"ss@ss.ssss","Mobile":"01023456785","ProfileImage":"2002159.jpg","CreationDate":"2018-11-17T10:56:43.473"}
  
           */
  
        },err=>{
          console.log("err from login",err);
        });

      },err=>{
        console.log("err from jsCheckPass",err);
      })
    }else if (this.type == "compActivationForget"){
      this.service.compCheckPass(this.userid, this.password,resp=>{
        console.log("resp from compCheckPass",resp);
        // after login or login data
        // var DataParse = JSON.parse(resp);
        // DataParse.type = "company";
        // this.helper.storeJSInfo(DataParse);
        // this.events.publish('menu',"com");
        // this.events.publish('user:userLoginSucceeded',DataParse);
        // this.navCtrl.setRoot(TabsPage);

        this.service.companyLogin(this.usermobile,this.password,(data)=>{

          console.log(JSON.stringify(data))
          // this.navCtrl.setRoot(TabsPage); 
          let DataParse = JSON.parse(data);
          if(DataParse["SID"])
          {
          DataParse.type = "company";
            this.helper.storeJSInfo(DataParse);
            this.events.publish('menu',"com");
            this.events.publish('user:userLoginSucceeded',DataParse);
            this.navCtrl.setRoot(TabsPage); 
          }else 
            this.helper.presentToast(this.translate.instant("invalidData"))

        },(data)=>{
          console.log(JSON.stringify(data))
  
          
        })


      },err=>{
        console.log("err from compCheckPass ",err);
      })
    }else if (this.type == "compchangepass"){
      console.log("else if type compchangepass");

    var oldPassUserEntered = this.service.DateEnncryption(this.oldPass);
    this.storage.get("perStorePass").then((val) => {
console.log("val from storage : ",val)
if(val){
  // Password
  if(val == this.oldPass)
{
  console.log("pass es equal")
  

  this.service.companyChangepass(this.userid,this.password,resp=>{
    console.log("resp[ from companyChangepass",resp);
    if(JSON.parse(resp).Result == "1")
    {
      this.helper.presentToast(this.translate.instant("passchanged"));
      // this.events.publish('user:userLogedout2');
    }
      
    else
    this.helper.presentToast(this.translate.instant("ServerError"));

  this.dismiss();

  },err=>{
    console.log("err[ from companyChangepass",err);
    this.helper.presentToast(this.translate.instant("ServerError"));
    this.dismiss();
  })



}else{
  console.log("pass es not equal") 
  this.helper.presentToast(this.translate.instant("wrongCurrentPass"))  
  }
}
    });
    
    }else if (this.type == "jschangepass"){
      console.log("else if type jschangepass");

      var oldPassUserEntered = this.service.DateEnncryption(this.oldPass);
      this.storage.get("perStorePass").then((val) => {
  console.log("val from storage : ",val)
  if(val){
  
    if(val== this.oldPass)
  {
    console.log("pass es equal")
    

    
      this.service.jsChangepass(this.userid,this.password,resp=>{
        console.log("resp from jsChangepass",resp);
        if(JSON.parse(resp).Result == "1")
        {
          this.helper.presentToast(this.translate.instant("passchanged"));
          // this.events.publish('user:userLogedout2');
        }
          
        else
        this.helper.presentToast(this.translate.instant("ServerError"));

      this.dismiss();

      },err=>{
        console.log("err from jsChangepass",err);
        this.helper.presentToast(this.translate.instant("ServerError"));
        this.dismiss();
      })


    }else{
      console.log("pass es not equal") 
      this.helper.presentToast(this.translate.instant("wrongCurrentPass"))  
      }
    }
        });

    }






  }




}

dismiss(){
  console.log("dismiss")
  this.navCtrl.pop();
}

}
