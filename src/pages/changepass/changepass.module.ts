import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChangepassPage } from './changepass';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';

@NgModule({
  declarations: [
    ChangepassPage,
  ],
  imports: [
    IonicPageModule.forChild(ChangepassPage),
    TranslateModule.forChild()
  ],
})
export class ChangepassPageModule {}
