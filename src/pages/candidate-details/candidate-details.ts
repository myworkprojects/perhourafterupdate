import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
import { ServicesProvider } from '../../providers/services/services';
import { TranslateService } from '@ngx-translate/core';
import { TabsPage } from '../tabs/tabs';

@IonicPage()
@Component({
  selector: 'page-candidate-details',
  templateUrl: 'candidate-details.html',
})
export class CandidateDetailsPage {

  candidateName ;
  candidatePhoto = "assets/imgs/default-avatar.png";
  candidateDescription ;
  candidateMail = "xx@xx.com";
  candidateMobile ;
  shortList ;
  hire ;
  back ;

  moreData;
  id;
  scaleClass="";
  langDirection;

  constructor(public translate: TranslateService,
    public service:ServicesProvider,
    public helper:HelperProvider,
    public alertCtrl:AlertController,
    public navCtrl: NavController, public navParams: NavParams) {

      this.langDirection = this.helper.lang_direction;
      if(this.langDirection == "rtl")
        this.scaleClass="scaleClass";

      this.hire = this.translate.instant("hire");
      this.shortList = this.translate.instant("shortList");
      this.back = this.translate.instant("back");

      this.moreData =  this.navParams.get('data');
      this.id =  this.navParams.get('ID');

      this.candidateName = this.moreData.FirstName;
      this.candidateDescription = this.moreData.ShortDescription;
      this.candidatePhoto = this.moreData.ProfileImage;
      this.candidateMobile = this.moreData.Mobile;
      


        
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CandidateDetailsPage');
  }
  dismiss(){
    this.navCtrl.pop();
  }
  shortListFunc(){
    this.service.comAddToShortList(this.moreData.JobSeekerSID,this.id,resp=>{
      console.log("resp from comAddToShortList",resp);
      var respData = JSON.parse(resp);

      if(respData.OperationResult =="-2"){
        
        // this.helper.presentToast("Added Before");
        // this.helper.presentToast(this.translate.instant('addedbefore'));
        this.helper.presentToast(this.translate.instant('addedtoshortlistbefore'));
        this.navCtrl.pop();
      }
        
      else if(respData.OperationResult =="1")
      {
        // this.helper.presentToast("Successfully Added");
        this.helper.presentToast(this.translate.instant("Successfully Added"))
        this.navCtrl.pop();
      }
        
      
    },err=>{
      console.log("err from comAddToShortList",err);
    })
  }
  hireFunc(id)
  {

  let alert = this.alertCtrl.create({
    title: this.translate.instant('hirecandidate'),
    buttons: [
      {
        text: this.translate.instant('no2'),
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: this.translate.instant('yes2'),
        handler: () => {
       this.confirm(this.moreData.JobSeekerSID)
        }
      }
    
  ]
})
alert.present()
  }
  confirm(id)
  {
    
    this.service.companyhirecandidate(id,this.id,(data)=>{
      let DataParsed=JSON.parse(data)
      if (DataParsed.OperationResult == 1) {
        this.helper.presentToast(this.translate.instant('candidateadded'))

     
      }
      else if(DataParsed.OperationResult == -2)
      {
        this.helper.presentToast(this.translate.instant('addedbefore'))

      }else if(DataParsed.OperationResult == "-3"){
        this.helper.presentToast(this.translate.instant('ReachMaxNo'))
      }
      console.log(DataParsed);
      // this.navCtrl.pop();
      this.navCtrl.setRoot(TabsPage);
    },(data)=>{
      // this.navCtrl.pop();
      this.navCtrl.setRoot(TabsPage);
    })
  }

  backFunc(){
    this.navCtrl.pop();
  }
}
