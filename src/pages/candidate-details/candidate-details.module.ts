import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CandidateDetailsPage } from './candidate-details';

@NgModule({
  declarations: [
    CandidateDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(CandidateDetailsPage),
  ],
})
export class CandidateDetailsPageModule {}
