import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,Platform,AlertController} from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'page-after-splash',
  templateUrl: 'after-splash.html',
})
export class AfterSplashPage {
  lang_direction="";
  langTxt = "";
  langTxt2="";

  langId2
  arlangChecked=false
  enlangChecked=false

  constructor(public alertCtrl: AlertController,public translate: TranslateService,public helper:HelperProvider,public platform: Platform,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AfterSplashPage');
    this.lang_direction = this.helper.lang_direction;
    if(this.lang_direction == 'ltr')
    {
      this.langTxt = "English";
      this.langTxt2 = "English";
      this.arlangChecked = false
      this.enlangChecked = true
      this.langId2 = 'en'

    }  
    else  if(this.lang_direction == 'rtl')
    {
      this.langTxt = "عربى";
      this.langTxt2 = "عربى";
      this.arlangChecked = true
      this.enlangChecked = false
      this.langId2 = 'ar'
    }  


    }

  jobSeeker(){
    this.navCtrl.push('LoginPage',{type:"jobSeeker"});
  }
  
  company(){
    this.navCtrl.push('LoginPage',{type:"company"});
  }

  changelang(){

    if (this.helper.lang_direction == 'ltr') {
      console.log("stroe ar lang");
      this.helper.storeLanguage('ar');
      this.translate.setDefaultLang('ar');  
      this.translate.use('ar');    
      this.platform.setDir('rtl', true);
      this.helper.lang_direction = "rtl"; 
      this.lang_direction = "rtl"; 
      this.langTxt = "العربية"; 
     
    }else {
      console.log("stroe en lang");
      this.helper.storeLanguage('en');
          this.translate.setDefaultLang('en');
          this.translate.use('en');
          this.platform.setDir('ltr', true);
          this.helper.lang_direction = "ltr";
          this.lang_direction = "ltr";
          this.langTxt = "English";
 
    }

  }


  changelang22(){
console.log("changelang22 : ",this.langId2)
    if (this.langId2 == 'ar') {
      console.log("stroe ar lang");
      this.helper.storeLanguage('ar');
      this.translate.setDefaultLang('ar');  
      this.translate.use('ar');    
      this.platform.setDir('rtl', true);
      this.helper.lang_direction = "rtl"; 
      this.lang_direction = "rtl"; 
      this.langTxt = "عربى"; 
      this.langId2 = 'ar'
     
    }else {
      console.log("stroe en lang");
      this.helper.storeLanguage('en');
          this.translate.setDefaultLang('en');
          this.translate.use('en');
          this.platform.setDir('ltr', true);
          this.helper.lang_direction = "ltr";
          this.lang_direction = "ltr";
          this.langTxt = "English";
          this.langId2 = 'en'
    }

  }

  selectLang(){
    console.log("selectLang")
    
  let alert = this.alertCtrl.create({
    title: this.translate.instant("Language"),
    // message: this.translate.instant(""),
    inputs : [
      {type:'radio',
    label:this.translate.instant("arabic2"),
    value:'ar',
    checked:this.arlangChecked}
  ,
  {type:'radio',
    label:this.translate.instant("english2"),
    value:'en',
    checked:this.enlangChecked}

  ],
    buttons: [
      {
        text: this.translate.instant("cancel"),
        role: 'cancel',
        handler: (data) => {
          console.log('disagree clicked',data);
        }
      },
      {
        text: this.translate.instant("done"),
        handler: (langId) => {
          console.log('agree clicked lang id : ',langId);
          if(langId == "ar"){

            this.helper.storeLanguage('ar');
            this.translate.setDefaultLang('ar');  
            this.translate.use('ar');    
            this.platform.setDir('rtl', true);
            this.helper.lang_direction = "rtl"; 
            this.lang_direction = "rtl"; 
            this.langTxt2 = "عربى"; 
            this.arlangChecked = true
            this.enlangChecked = false

          }else if(langId == "en"){
          
            this.helper.storeLanguage('en');
          this.translate.setDefaultLang('en');
          this.translate.use('en');
          this.platform.setDir('ltr', true);
          this.helper.lang_direction = "ltr";
          this.lang_direction = "ltr";
          this.langTxt2 = "English";
          this.arlangChecked = false
          this.enlangChecked = true
          
          }



        }
         
      }
    ]
  });
  alert.present();
  }


}
