import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
import { ServicesProvider } from '../../providers/services/services';
import { TranslateService } from '@ngx-translate/core';
import { DatePipe } from '@angular/common';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@IonicPage()
@Component({
  selector: 'page-experience',
  templateUrl: 'experience.html',
})
export class ExperiencePage {

  scaleClass="";
  langDirection;
  experiences;
  hideNoDataTxt;

  jsid;
  refresher;
  hideForShowProfile

  constructor(private iab: InAppBrowser,public datepipe: DatePipe,public translate: TranslateService,public service:ServicesProvider,public helper:HelperProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.langDirection = this.helper.lang_direction;
    if(this.langDirection == "rtl")
    this.scaleClass="scaleClass";

    this.hideNoDataTxt = true;
    this.jsid = this.navParams.get("userId");

    this.hideForShowProfile = this.navParams.get('from');

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ExperiencePage');
  }

  ionViewWillEnter(){
    console.log("ionViewWillEnter StudyPage");
    this.loadEdu();
  }

  loadEdu(){
    this.service.jsgetExperience(this.jsid,resp=>{
      console.log("resp from jsgetEducation",resp);
      var respData = JSON.parse(resp);
      if(respData.Result == "-1")
        this.hideNoDataTxt = false;
      else  if(respData.Result == "1")
      {
        this.hideNoDataTxt = true;
        this.experiences = respData.list;
        for(var x=0;x<this.experiences.length;x++){
         
          if(this.experiences[x].StillWorking == "1")
          this.experiences[x].date =  this.datepipe.transform(this.experiences[x].StartDate.split("T")[0], 'dd-MM-yyyy') + " , " + this.translate.instant("present");
          else if(this.experiences[x].StillWorking == "0")
          this.experiences[x].date =  this.datepipe.transform(this.experiences[x].StartDate.split("T")[0], 'dd-MM-yyyy') + " , " + this.datepipe.transform(this.experiences[x].EndDate.split("T")[0], 'dd-MM-yyyy') ;


        }
        
      }


      if(this.refresher){
        this.refresher.complete();
      }

    },err=>{
      console.log("err from jsgetEducation",err);

      if(this.refresher){
        this.refresher.complete();
      }
      
    })

  }


  dismiss(){
    this.navCtrl.pop();
  }
  addexp(){
    console.log("add exp");
    this.navCtrl.push("AddEditPage",{from:'addexp',id:this.jsid});
  }
  editexp(item){
    console.log("edit exp");
    this.navCtrl.push("AddEditPage",{from:'editexp',id:this.jsid,item:item});
  }


  doRefresh(ev){
    this.refresher = ev;
    this.loadEdu();
  }

  showCert(CertFile){
    if (CertFile) {
      console.log("if CertFile",CertFile);
      // window.open(CertFile);
      const browser = this.iab.create(CertFile,'_system',"location=yes");
    } else {
      console.log("else CertFile");
      this.helper.presentToast(this.translate.instant("nocerttoshow"));

    }
  }
  
}
