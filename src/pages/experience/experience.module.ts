import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ExperiencePage } from './experience';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';

@NgModule({
  declarations: [
    ExperiencePage,
  ],
  imports: [
    IonicPageModule.forChild(ExperiencePage),
    TranslateModule.forChild()
  ],
})
export class ExperiencePageModule {}
