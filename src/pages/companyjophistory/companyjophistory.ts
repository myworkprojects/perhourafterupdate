import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { CompanyselectedcandidatesPage } from '../companyselectedcandidates/companyselectedcandidates';
import { HelperProvider } from '../../providers/helper/helper';

/**
 * Generated class for the CompanyjophistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-companyjophistory',
  templateUrl: 'companyjophistory.html',
})
export class CompanyjophistoryPage {
  langDirection:any

  constructor(public ViewCtrl:ViewController,public helper:HelperProvider,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.langDirection = this.helper.lang_direction;

    console.log('ionViewDidLoad CompanyjophistoryPage');
  }
show()
{
this.navCtrl.setRoot(CompanyselectedcandidatesPage)
}
hire()
{
  
}
dismiss(){
  this.navCtrl.pop();
}
}
