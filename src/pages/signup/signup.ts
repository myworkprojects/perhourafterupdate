import { Component ,ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams, Platform ,Content,AlertController} from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { HelperProvider } from '../../providers/helper/helper';
import { TabsPage } from '../tabs/tabs';
import { TranslateService } from '@ngx-translate/core';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { ActionSheetController } from 'ionic-angular'
import { IOSFilePicker } from '@ionic-native/file-picker';
import { FileChooser } from '@ionic-native/file-chooser';
import { Base64 } from '@ionic-native/base64'
import { FilePath } from '@ionic-native/file-path';
import { ServicesProvider } from '../../providers/services/services';
import { matchOtherValidator } from '../../validators/passwordValidator';
import { Jsonp } from '@angular/http/src/http';

@IonicPage()
@Component({
  templateUrl: 'signup.html',
  selector: 'page-signup',
  providers: [Camera,File,FileChooser,IOSFilePicker,Base64,FilePath]
})
export class SignupPage {
  
  private registerForm : FormGroup;
  @ViewChild(Content) content: Content;
  email;
  password;
  name;
  candidateImg;
  confirmpassword;
  exexperiencepe;
  experience;
  mobile;
  submitAttempt = false;
  errors = {
    nameErr:"",
    emailErr:"",
    passwordErr:"",
    confirmpasswordErr:"",
    mobileErr:"",
    experience:""
  };

  langDirection = "";
  user_cv_name=[];
  cv_ext;
  cv_data;
  placeholder = {name:"",email:"",password:"",confirmPassword:"",mobileNumber:"",experience:""};
  scaleClass="";

  hideCV=true;
  var_picture="assets/icon/camera.png";

  termsStatus = false;
  termsError = false;
jstype= false;
hideemailerr = true;
txtname = ""

experienceforjs
appearExpErrText = false
natsArr

  constructor(public alertCtrl:AlertController, public translate: TranslateService,private formBuilder: FormBuilder,public helper:HelperProvider,
    public navCtrl: NavController, public navParams: NavParams,public platform: Platform, private filePicker: IOSFilePicker 
    ,public actionSheetCtrl: ActionSheetController ,private file: File,private filePath: FilePath,
    private base64: Base64, private camera: Camera, private fileChooser: FileChooser, public service: ServicesProvider) {

    this.langDirection = this.helper.lang_direction;
    console.log("js",this.navParams.get('jobSeeker'));
    
    this.appearExpErrText = false
    if(this.navParams.get('jobSeeker') == true)
    {
      this.jstype = true;

      this.hideCV = false;
      this.appearExpErrText = false
    } 
    else{
      this.hideCV = true;
      this.jstype = false;
    }
      

    console.log("hide",this.hideCV);

    if(this.langDirection == "rtl")
    this.scaleClass="scaleClass";
    
    this.registerForm = this.formBuilder.group({
      //email:['', Validators.compose([Validators.required,Validators.email])],
   //   email:['', Validators.compose([Validators.required,Validators.pattern("^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$")])],
   
   email:['', Validators.compose([Validators.required,Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)])],
   password: ['', Validators.compose([Validators.minLength(4), Validators.required])],
      confirmpassword: ['', Validators.compose([Validators.minLength(4),  Validators.required, matchOtherValidator('password')])],
      name :['',Validators.required],
      mobile: ['', Validators.compose([Validators.required,Validators.pattern("[0-9]{9,10}")])],//"^(966)([0-9]{10})"
      // experience:['' , Validators.required],
      // experienceforjs:['','']

    });
    if(this.jstype == true)
    {
      this.errors.nameErr = this.translate.instant("nameErr");
      // this.errors.experience = this.translate.instant("expErr");
      this.errors.experience = this.translate.instant("jobtitleErr");
      this.placeholder.experience = this.translate.instant("jobtitletxt");
    }
    else{
      this.errors.nameErr = this.translate.instant("comperr");
      this.errors.experience = this.translate.instant("descErr");
    }
    

    this.errors.emailErr = this.translate.instant("emailErr");
    this.hideemailerr = true;
    this.errors.passwordErr = this.translate.instant("passwordErr");
    this.errors.confirmpasswordErr = this.translate.instant("passwordNotConfirmed");
    this.errors.mobileErr = this.translate.instant("mobileErr");
    this.hideemailerr = true;


    this.natsArr=[{SID:1,Nationality:this.translate.instant("student")},{SID:2,Nationality:this.translate.instant("employee")},{SID:3,Nationality:this.translate.instant("unemployed")}]
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');

    this.txtname = ""

    if(this.jstype == true)
      this.placeholder.name = this.translate.instant("enterName");
    else 
      this.placeholder.name = this.translate.instant("comptitle");

    this.placeholder.email = this.translate.instant("enterEmail");
    this.placeholder.password = this.translate.instant("enterPassword");
    this.placeholder.confirmPassword = this.translate.instant("enterConfirmPass");
    this.placeholder.mobileNumber = this.translate.instant("enterMobileNumber");
  }

register(){
    console.log("register");
  //   if(this.mobile.charAt(0) == "0" || this.mobile.charAt(0) == "٠")
  // {
  //   this.mobile =  this.mobile.substr(1);
  //   this.mobile = "966"+this.mobile;
  //   console.log("from if =0 this.mobile: ",this.mobile);
  // }else{
  //   this.mobile = "966"+this.mobile;
  //   console.log("from else =0 this.mobile: ",this.mobile);
  // }
    
if(! this.registerForm.valid ){
  this.submitAttempt=true;
  this.content.scrollToTop(3000);
  
  if(this.registerForm.controls["password"].errors){
    if(this.registerForm.controls["password"].errors['required'])
      this.errors.passwordErr = this.translate.instant("passwordErr");
    else if (this.registerForm.controls["password"].errors['minlength'])
      this.errors.passwordErr = this.translate.instant("passErr");
    else
      console.log("passErrors:",this.registerForm.controls["password"].errors);
  
  }
  console.log(this.registerForm.controls["mobile"].errors);
  console.log("confirmpassword",this.registerForm.controls["confirmpassword"].errors);
  if(this.registerForm.controls["confirmpassword"].errors){
    if(this.registerForm.controls["confirmpassword"].errors['required'])
      this.errors.confirmpasswordErr = this.translate.instant("confirmpasswordErr");
    else if (this.registerForm.controls["confirmpassword"].errors['minlength'])
      this.errors.confirmpasswordErr = this.translate.instant("passErr");
    else if(this.registerForm.controls["confirmpassword"].errors['matchOther'])
      this.errors.confirmpasswordErr = this.translate.instant("passwordNotConfirmed");
    else
      console.log("confirmpasswordErrors:",this.registerForm.controls["confirmpassword"].errors);
  
  }


  if(this.registerForm.controls["mobile"].errors){
    if(this.registerForm.controls["mobile"].errors['required'])  
      this.errors.mobileErr = this.translate.instant("mobileErr");
    else if(this.registerForm.controls["mobile"].errors['pattern']) 
      this.errors.mobileErr = this.translate.instant("phoneErr");
    // else if (this.mobile.includes('-') || this.mobile.includes('.'))
    //   this.errors.mobileErr = this.translate.instant("invalidPhone");
    else
      console.log("mobile errors:",this.registerForm.controls["mobile"].errors);
  }
  console.log("email err",this.registerForm.controls["email"].errors);
//console.log("email",this.email,"this.email.includes('.com'): ",this.email.includes('.com'));

//______ 
// if(this.registerForm.controls["email"].errors){
  //   if(this.registerForm.controls["email"].errors['required'])
  //   {
  //     this.hideemailerr = false;
  //     this.errors.emailErr = this.translate.instant("emailErr");
  //   }
      
  //   else if(this.registerForm.controls["email"].errors['email']) //invalidChars 
  //   {
  //     this.hideemailerr = false;
  //     this.errors.emailErr = this.translate.instant("invalidEmailAddress");
  //   }   
  //   else
  //   {
  //     this.errors.emailErr="";
  //     this.hideemailerr = true;
  //     console.log("phone errors:",this.registerForm.controls["email"].errors);
  //   }
      
  // }else{
  // if(! ( this.email.includes('.com') || this.email.includes('.net')) ) 
  //   {
  //     console.log("not contain  .net .com");
  //     this.errors.emailErr = this.translate.instant("invalidEmailAddress");
  //     this.hideemailerr = false;
  //   }else{
  //     this.errors.emailErr = "";
  //     this.hideemailerr = true;
  //   }

  // }
//_____________
console.log("email errors:",this.registerForm.controls["email"].errors);
if(this.registerForm.controls["email"].errors){
  if(this.registerForm.controls["email"].errors['required'])
  {
    this.errors.emailErr = this.translate.instant("emailErr");
  }else if(this.registerForm.controls["email"].errors['pattern']) {
    this.errors.emailErr = this.translate.instant("invalidEmailAddress");
  }else{
    console.log("email errors:",this.registerForm.controls["email"].errors);
  }
}

}
else{
  if(this.termsStatus == false)
    this.helper.presentToast(this.translate.instant('checkAgreement'))
  if(this.termsStatus)
  {

    console.log("jstype : ",this.jstype , " experience : ",this.experience)
    if(this.jstype == false && !this.experience){
      this.appearExpErrText = true
      console.log("if appearexperrtxt : ",this.appearExpErrText)
      return
    }else{
      this.appearExpErrText = false
      console.log("else appearexperrtxt : ",this.appearExpErrText)
    }
      
    console.log("x.charAt(0)", this.mobile.charAt(0))
    console.log("substr: ",this.mobile.substr(1))
  console.log("966+mobile","966"+this.mobile)


  if(this.mobile.charAt(0) == "0" || this.mobile.charAt(0) == "٠")
  {
    this.mobile =  this.mobile.substr(1);
    //this.mobile = "966"+this.mobile;
    console.log("from if =0 this.mobile: ",this.mobile);
  }else{
    //this.mobile = "966"+this.mobile;
    console.log("from else =0 this.mobile: ",this.mobile);
  }

  // if(! ( this.email.includes('.com') || this.email.includes('.net')) ) 
  //   {
  //     console.log("not contain  .net .com");
  //     this.errors.emailErr = this.translate.instant("invalidEmailAddress");
  //     this.hideemailerr = false;
  //     this.content.scrollToTop(3000);
  //   }else{

      this.errors.emailErr = ""
      this.hideemailerr = true;

    if(this.jstype == true)
    {
       
    if(navigator.onLine){
      let userData = {"name":this.name , 
        "email": this.email,"pass":this.password,
         "profileImg": "2002159.jpg", 
         "mobile":this.mobile,
         "experience": this.experience,
         "cv":this.cv_data,
         "cvext":this.cv_ext }
      this.service.jsRegister(userData,resp=>{
        console.log("register resp",resp);
        var respData = JSON.parse( resp);
        console.log("respData.OperationResult",respData["OperationResult"],": ");
        if(respData["OperationResult"] == "-3")
          this.helper.presentToast(this.translate.instant("mobileReg"));
        else if ( respData["OperationResult"] == "-2")
          this.helper.presentToast(this.translate.instant("mailReg"));
        else 
        {
          this.helper.presentToast(this.translate.instant("doneReg"));
          this.navCtrl.setRoot('ActivationCodePage',{"JsSID":respData["OperationResult"],userdata:userData,type:"js"}); //respData["OperationResult"],1013
        }

      },err=>{
        console.log("register err",err);
      })
    } 
    }else{
      let userData = {"name":this.name , 
        "email": this.email,"pass":this.password,
         "profileImg": "123.png", 
         "mobile":this.mobile,
         "experience": this.experience,
         "cv":this.cv_data,
         "cvext":this.cv_ext  }

      this.service.comregister(this.name,this.email,this.mobile,'1',"123.png",this.password,this.experience,this.cv_data,this.cv_ext,(data)=>{
     
        
        console.log(JSON.stringify(data))
        console.log("encriptionData",data);
        let DataParse = JSON.parse(data);
        console.log("parse data",DataParse);

        if(DataParse["OperationResult"] == "-3")
        this.helper.presentToast(this.translate.instant("mobileReg"));
      else if ( DataParse["OperationResult"] == "-2")
        this.helper.presentToast(this.translate.instant("mailReg"));
      else 
      {
        this.helper.presentToast(this.translate.instant("doneReg"));
        this.navCtrl.setRoot('ActivationCodePage',{"JsSID":DataParse["OperationResult"],userdata:userData,type:"company"}); //respData["OperationResult"],1013
      }

        // if(DataParse.Message=="Mail Already Registered")  
        //   this.helper.presentToast(this.translate.instant('register'));
        // else if(DataParse.Message.includes('Successfully')){

        //   // this.service.companyLogin(this.email,this.password,(data)=>{
        //   //   let DataParse = JSON.parse(data);
        //   //     this.helper.storeJSInfo(DataParse);
    
        //   // },(data)=>{
        //   //   console.log(JSON.stringify(data))
        //   // })
          
        // this.navCtrl.setRoot('ActivationCodePage',{"JsSID":DataParse.OperationResult,type:"company"}); 
        // }
  
      },(data)=>{
        console.log(JSON.stringify(data))
  
      })

    }
   

  //}//end of emial else
  }  


  }
      
  }
  attach(){
    console.log("attach");
    this.getCertificates();

  }

  selectregistry(){
    let actionSheet = this.actionSheetCtrl.create({
      title: this.translate.instant("SelectrfSource"),
      buttons: [
        {
          text: this.translate.instant("LoadfromLibrary"),
          handler: () => {
            this.getCertificates();
          }
        },
        {
          text: this.translate.instant("UseCamera"),
          handler: () => {
            this.takePictureforcv(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: this.translate.instant("cancel"),
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();

  }

  selectcv() {

    let actionSheet = this.actionSheetCtrl.create({
      title: this.translate.instant("SelectcvSource"),
      buttons: [
        {
          text: this.translate.instant("LoadfromLibrary"),
          handler: () => {
            this.getCertificates();
          }
        },
        {
          text: this.translate.instant("UseCamera"),
          handler: () => {
            this.takePictureforcv(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: this.translate.instant("cancel"),
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();

  }


  public takePictureforcv(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      quality: 80, //50
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true,
      allowEdit:true,
      targetWidth:200,
      targetHeight:200
    };
    this.camera.getPicture(options).then((imageData: string) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      // this.storage.get("js_info").then((val) => {

       // this.userImageUrl = 'data:image/jpeg;base64,' + imageData
        //this.storage.set("user_image",this.userImageUrl)
        let imgdata = encodeURIComponent(imageData)
        this.txtname = this.translate.instant("imageCaptured")

        this.cv_ext = 'jpeg';
        this.cv_data = imgdata
       



      // })
    }, (err) => {
      // Handle error
    });
  }
  
  
  takePicture(sourceType){
     // Create options for the Camera Dialog,FILE_URI DATA_URL
     var options = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };
    this.camera.getPicture(options).then((imageData) => {
      
      this.var_picture = 'data:image/jpeg;base64,' + imageData;
    //   console.log("imageData",imageData);
    //   this.var_picture = imageData;
    //   let filename = imageData.substring(imageData.lastIndexOf('/')+1);
    //   console.log("image filename",filename);
    // let path =  imageData.substring(0,imageData.lastIndexOf('/')+1);
    // console.log("image path",filename);
    // // this.myimageName =  
    //      //then use the method reasDataURL  btw. var_picture is ur image variable

    // this.file.readAsDataURL(imageData, filename).then(res=>{
    //   console.log("readAsDataURL");
    //    this.var_picture = res;
      
    // }   );


    //   this.filePath.resolveNativePath(imageData)
    //   .then(filePath => {
    //     console.log("img data",filePath)
    //     let filename = filePath.substr(filePath.lastIndexOf('/') + 1)
    //     this.user_cv_name.push(filename)
    //     let fileExt = filename.split('.').pop();
    //     this.cv_ext.push(fileExt)
    //     console.log("this.user_cv_name",this.user_cv_name);
    //     console.log("this.cv_ext",this.cv_ext)
    //   })
    //   .catch(err => console.log(err));
    //   this.candidateImg = encodeURIComponent(imageData);
    //   console.log("this.candidateImg",this.candidateImg);
    //   this.filePath.resolveNativePath(this.candidateImg)
    //             .then(filePath => {
    //               console.log("img",filePath)
    //               let filename = filePath.substr(filePath.lastIndexOf('/') + 1)
    //               this.user_cv_name.push(filename)
    //               let fileExt = filename.split('.').pop();
    //               this.cv_ext.push(fileExt)
    //               console.log("this.user_cv_name",this.user_cv_name);
    //               console.log("this.cv_ext",this.cv_ext)
    //             })
    //             .catch(err => console.log(err));
      
    // }, (err) => {
    //   // Handle error
     });
  }
  presentActionSheet() {

    let actionSheet = this.actionSheetCtrl.create({
      title: this.translate.instant("SelectImageSource"),
      buttons: [
        {
          text: this.translate.instant("LoadfromLibrary"),
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: this.translate.instant("UseCamera"),
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        }
        // ,
        // {
        //   text: this.translate.instant("canceltxt"),
        //   role: 'cancel'
        // }
      ]
    });
    actionSheet.present();
  }
 getCertificates() {
    if (this.platform.is('ios')) {
      this.filePicker.pickFile()
        .then(uri => {
          let correctPath = uri.substr(0, uri.lastIndexOf('/') + 1);
          let filename = uri.substr(uri.lastIndexOf('/') + 1)
          this.txtname = filename
          this.user_cv_name.push(filename)
          let fileExt = filename.split('.').pop();
          this.cv_ext = fileExt
          var cvextuper = this.cv_ext.toUpperCase();
                  if(cvextuper == "pdf".toUpperCase() || cvextuper == "docx".toUpperCase() ||cvextuper == "doc".toUpperCase() || cvextuper == "JPEG".toUpperCase() || 	cvextuper == "PNG".toUpperCase() || cvextuper == "JPG".toUpperCase() || cvextuper == "GIF".toUpperCase() 	|| cvextuper == "BMP".toUpperCase()	)
                  {
                    console.log("from if : ",cvextuper);
                    // this.helper.presentToast(this.translate.instant("fileupsu"));
                  }else{
                    console.log("from else : ",cvextuper);
                    this.helper.presentToast(this.translate.instant("fileupnoex"));
                    this.cv_data = "";
                    this.cv_ext="";
                    this.txtname = ""
                    this.user_cv_name =[];
                  }

          this.file.readAsDataURL("file:///" + correctPath, filename).then((val) => {
            this.cv_data =encodeURIComponent(val.split(",")[1])
          }).catch(err => console.log('Error reader' + err));
        }).catch(err => console.log('Error' + err));
    }
    else if (this.platform.is('android')) {
      this.fileChooser.open()
        .then(uri => {
          console.log("uuu" + uri)
          this.filePath.resolveNativePath(uri).then((result) => {
            this.base64.encodeFile(result).then((base64File: string) => {
              console.log("base64File " + base64File)
              let fileData = base64File.split(',')[1];
              this.cv_data =  encodeURIComponent(fileData);
              console.log("this.cv_data : ",this.cv_data);
              this.filePath.resolveNativePath(uri)
                .then(filePath => {
                  console.log(filePath)
                  let filename = filePath.substr(filePath.lastIndexOf('/') + 1)
                  this.txtname = filename
                  this.user_cv_name.push(filename)
                  let fileExt = filename.split('.').pop();
                  // this.cv_ext.push(fileExt)
                  this.cv_ext = fileExt;
                  var cvextuper = this.cv_ext.toUpperCase();
                  if(cvextuper == "pdf".toUpperCase() || cvextuper == "docx".toUpperCase() ||cvextuper == "doc".toUpperCase() || cvextuper == "JPEG".toUpperCase() || 	cvextuper == "PNG".toUpperCase() || cvextuper == "JPG".toUpperCase() || cvextuper == "GIF".toUpperCase() 	|| cvextuper == "BMP".toUpperCase()	)
                  {
                    console.log("from if : ",cvextuper);
                  // this.helper.presentToast(this.translate.instant("fileupsu"));
                  }else{
                    console.log("from else : ",cvextuper);
                    this.helper.presentToast(this.translate.instant("fileupnoex"));
                    this.cv_data = "";
                    this.cv_ext="";
                    this.txtname = ""
                    this.user_cv_name =[];
                  }

                  console.log("this.user_cv_name : ",this.user_cv_name);
                  console.log("this.cv_ext : ",this.cv_ext)
                })
                .catch(err => console.log(err));
            }, (err) => {
              console.log("base" + err);
            });

          }, (err) => {
            console.log(err);
          })


        })
        .catch(e => console.log(e));
    }
  }
  dismiss(){
    this.navCtrl.pop();
  }

  showConditions(){
    console.log("showConditions: ");
    this.service.getLicense(resp=>{
      console.log("getLicense resp",resp);
      var respData = JSON.parse(resp);
      var msgTxt = "" ;
      if(this.helper.lang_direction == "rtl")
       msgTxt =  respData.LicenseAgreement_Ar;
      else if(this.helper.lang_direction == "ltr")
        msgTxt =  respData.LicenseAgreement_En;

      // this.presentAlert(msgTxt);
      this.navCtrl.push('ConditionsPage',{data:msgTxt});
    },err=>{
      console.log("getLicense err",err);
    })

  }
  presentAlert(txt) {
    let alert = this.alertCtrl.create({
      title: this.translate.instant("consitions"),
      subTitle:txt,
      buttons: ["ok"]
    });
    alert.present();
  }

  changeTxt(){
    console.log("mobile before replacement  ...",this.mobile);
    this.mobile = this.textArabicNumbersReplacment(this.mobile);
    console.log("phone after replacement: ",this.mobile); 
  
  }

  textArabicNumbersReplacment(strText) {
    var strTextFiltered = strText.trim();
    strTextFiltered = strText;
    strTextFiltered = strTextFiltered.replace(/[\٩]/g, '9');
    strTextFiltered = strTextFiltered.replace(/[\٨]/g, '8');
    strTextFiltered = strTextFiltered.replace(/[\٧]/g, '7');
    strTextFiltered = strTextFiltered.replace(/[\٦]/g, '6');
    strTextFiltered = strTextFiltered.replace(/[\٥]/g, '5');
    strTextFiltered = strTextFiltered.replace(/[\٤]/g, '4');
    strTextFiltered = strTextFiltered.replace(/[\٣]/g, '3');
    strTextFiltered = strTextFiltered.replace(/[\٢]/g, '2');
    strTextFiltered = strTextFiltered.replace(/[\١]/g, '1');
    strTextFiltered = strTextFiltered.replace(/[\٠]/g, '0');
    return strTextFiltered;
  }

}
