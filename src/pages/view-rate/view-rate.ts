// import { Component } from '@angular/core';
// import { IonicPage, NavController, NavParams } from 'ionic-angular';



// @IonicPage()
// @Component({
//   selector: 'page-view-rate',
//   templateUrl: 'view-rate.html',
// })
// export class ViewRatePage {

//   constructor(public navCtrl: NavController, public navParams: NavParams) {
//   }

//   ionViewDidLoad() {
//     console.log('ionViewDidLoad ViewRatePage');
//   }

// }
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { HelperProvider } from '../../providers/helper/helper';
import { Storage } from '@ionic/storage';
import { ServicesProvider } from '../../providers/services/services'


@IonicPage()
@Component({
  selector: 'page-view-rate',
  templateUrl: 'view-rate.html',
})
export class ViewRatePage {

  scaleClass="";
  langDirection;
  refresher
  raetesArr=[]
  hideNoData

  id

  constructor(public srv:ServicesProvider, public storage: Storage,public helper:HelperProvider,public navCtrl: NavController, public navParams: NavParams) {
   
    this.langDirection = this.helper.lang_direction;
    if(this.langDirection == "rtl")
      this.scaleClass="scaleClass";

      this.hideNoData = true

      this.id =  this.navParams.get("id")
      console.log("rate id : ",this.id)
//       ReviewDate: "2019-04-01T10:40:27.113"
// ReviewGrade: 5
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewRatePage');
    this.getJSRate()
  }

  dismiss(){
    this.navCtrl.pop()
  }

  doRefresh(ev){
    this.refresher = ev;
    // this.raetesArr = []
    this.getJSRate()
    
    

  }

  getJSRate(){

    // this.storage.get('js_info').then((val) => {
    //   console.log("val from get js info from enter profile", val);
    //   if (val){
    //     this.id = val.SID;

        this.srv.getJSRates(this.id,res=>{
          console.log("resp from getJSRates",res)
          var respData = JSON.parse(res)
          if(respData.result == "-1"){
            this.hideNoData = false
            this.raetesArr = []
          }else{
            this.hideNoData = true
            this.raetesArr = respData.reviewDetails


for(var x=0;x<this.raetesArr.length;x++){

if(this.raetesArr[x].ReviewDate){


            var newMsgDate = new Date(this.raetesArr[x].ReviewDate.split(".")[0]+".000Z") 
     
        console.log("newMsgDate : ",newMsgDate)
this.raetesArr[x].ReviewDate = newMsgDate.toLocaleString()     
          console.log("this.raetesArr[x].ReviewDate : ",this.raetesArr[x].ReviewDate)

          var userLang = navigator.language.split('-')[0];
          console.log("navigator.language",navigator.language);
          console.log("userlang",userLang);
  
       
  
          if (userLang == 'ar') {

            
            var PMReplace = /م/gi; 
            var AMReplace = /ص/gi; 
          // var str = "Apples are round, and apples are juicy.";
          
          if(this.helper.lang_direction == "ltr"){
            this.raetesArr[x].ReviewDate  =this.raetesArr[x].ReviewDate.replace(PMReplace, "PM"); 
            this.raetesArr[x].ReviewDate  = this.raetesArr[x].ReviewDate.replace(AMReplace, "AM"); 
            console.log("from ltr : ",this.raetesArr[x].ReviewDate )
            
          }
  
          var firstPart1 = this.raetesArr[x].ReviewDate.split(" ")[0]
          console.log("firstPart1 : ",firstPart1)
  var firstPart =  firstPart1.split("/")[0]+" / "+firstPart1.split("/")[1]+" / "+firstPart1.split("/")[2]
  console.log("firstPart : ",firstPart)
  var secondPart = this.raetesArr[x].ReviewDate.split(" ")[1]
  console.log("secondPart : ",secondPart)
  var period  = this.raetesArr[x].ReviewDate.split(" ")[2]
  console.log("period : ",period)
  
  var time = secondPart.split(":")[0] + ":"+ secondPart.split(":")[1]
  console.log("time : ",time)
  
  this.raetesArr[x].ReviewDate = firstPart+" , "+time+" "+period
  console.log("this.raetesArr[x].ReviewDate : ",this.raetesArr[x].ReviewDate)

          }else{
            var PMReplace = /PM/gi; 
            var AMReplace = /AM/gi; 
          // var str = "Apples are round, and apples are juicy.";
          
          if(this.helper.lang_direction == "rtl"){
            this.raetesArr[x].ReviewDate  = this.raetesArr[x].ReviewDate.replace(PMReplace, "م"); 
            this.raetesArr[x].ReviewDate  = this.raetesArr[x].ReviewDate.replace(AMReplace, "ص"); 
            console.log("from rtl : ",this.raetesArr[x].ReviewDate )
            
          }
  
          var firstPart1 = this.raetesArr[x].ReviewDate.split(",")[0]
          console.log("firstPart1 : ",firstPart1)
  var firstPart =  firstPart1.split("/")[1]+"/"+firstPart1.split("/")[0]+"/"+firstPart1.split("/")[2]
  console.log("firstPart : ",firstPart)
  var secondPart = this.raetesArr[x].ReviewDate.split(",")[1]
  console.log("secondPart : ",secondPart)
  var period  = secondPart.split(" ")[2]
  console.log("period : ",period)
  
  var time = secondPart.split(" ")[1].split(":")[0] + ":"+ secondPart.split(" ")[1].split(":")[1]
  console.log("time : ",time)
  
  this.raetesArr[x].ReviewDate = firstPart+" , "+time+" "+period
  console.log("this.raetesArr[x].ReviewDate: ",this.raetesArr[x].ReviewDate)

          }
         


        }

        }


          }

          if(this.refresher){
            this.refresher.complete();
          }
    
        },err=>{
          console.log("err from getJSRates",err)

          if(this.refresher){
            this.refresher.complete();
          }
        })
    



      // }
     
        
      // });
    

  }



}