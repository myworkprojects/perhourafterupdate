// import { NgModule } from '@angular/core';
// import { IonicPageModule } from 'ionic-angular';
// import { ViewRatePage } from './view-rate';

// @NgModule({
//   declarations: [
//     ViewRatePage,
//   ],
//   imports: [
//     IonicPageModule.forChild(ViewRatePage),
//   ],
// })
// export class ViewRatePageModule {}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewRatePage } from './view-rate';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { Ionic2RatingModule } from 'ionic2-rating';


@NgModule({
  declarations: [
    ViewRatePage,
  ],
  imports: [
    IonicPageModule.forChild(ViewRatePage),
    TranslateModule.forChild(),
    Ionic2RatingModule 
  ],
})
export class ViewRatePageModule {}