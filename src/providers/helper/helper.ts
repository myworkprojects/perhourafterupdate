import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastController ,AlertController,Events, Platform} from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Diagnostic } from '@ionic-native/diagnostic';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Geolocation } from '@ionic-native/geolocation';
import { TranslateService } from '@ngx-translate/core';


@Injectable()
export class HelperProvider {

  public lang_direction = "";

//   id="net.ITRoots.perHOURjobs"

// id="net.perhourjobs.perHOURjobs"

//   Bugs Fixing
// Enhance upload files
// Performance Enhancement

  //first link
 //public jsServiceUrl = "http://apiperhour.itrootsdemos2.com/api/";
  // second link
   public jsServiceUrl = "http://api.perhour-sa.com/api/";


  public registration;
  public device_type;
  public lat = 24.774265;
  public lng = 46.738586;
  public key =  "AIzaSyC2QnyjAeavsSLmTnsNPXeAVj5nlcWOOiU";
  public choosetabs = "";
  recommendedArr;
// platfom android project.properities 11.6.2 => 11.8.0, 25.+ => 26.+
  constructor(private geolocation: Geolocation, public diagnostic: Diagnostic,
    public locationAccuracy: LocationAccuracy,public alertCtrl: AlertController,
    public translate: TranslateService,public events: Events,
    private storage: Storage, private platform: Platform,
    public toastCtrl: ToastController,public http: HttpClient) {
    console.log('Hello HelperProvider Provider');
  }

  public presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      // position: 'bottom',
      position: 'middle',
      cssClass: this.lang_direction
    });
    toast.present();
  }

  public presentToast2(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 30000,
      // position: 'bottom',
      position: 'middle',
      cssClass: this.lang_direction
    });
    toast.present();
  }

  storeJSInfo(info){
    console.log("store this data from helper: ",info)
    this.storage.set('js_info', info);
  }

  storeNotificationStatus(status){
    console.log("storeNotificationStatus data from helper: ",status)
    this.storage.set('notification_status', status).then(resp=>{
      console.log("store notification_status")
    });
  }

  // storeJSInfo2(info){
  //   this.storage.set('js_info', info).then(resp=>{
     
  //   });
  // }

  parseArabic(str) {
    return ( str.replace(/[٠١٢٣٤٥٦٧٨٩]/g, (d)=> {
        return d.charCodeAt(0) - 1632; // Convert Arabic numbers
    }));
}

  storeLanguage(userlang){
    console.log("storeLanguage",userlang);
    this.storage.set('language', userlang).then(resp=>{
      console.log("resp set('language',: ",resp)
    });
  }

    storeLanguage2(userlang){
    console.log("storeLanguage",userlang);
    this.storage.set('language', userlang).then(resp=>{
      console.log("resp set('language',: ",resp);
      window.location.reload();
    });
  }

  storePass(pass){
    console.log("storePass",pass);
    this.storage.set('perStorePass', pass).then(resp=>{
      console.log("resp from storePass : ",resp)
    });
  }
detectUserLoctaion(){
    this.diagnostic.isGpsLocationEnabled().then(
      a=>{
        
        console.log("from gps opened resp",a);
        if(a)        
          this.getUserLocation();
        else
          this.presentConfirm();          
        
      }
    ).catch(
      a=>{
        console.log("from gps opened err",a);
        this.presentToast("catch isGpsLocationEnabled");
        // this.diagnostic.switchToLocationSettings();
      }
    );
      
  }

  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: this.translate.instant("accessLocation"),
      message: this.translate.instant("msgAccessLocation"),
      buttons: [
        {
          text: this.translate.instant("disagree"),
          role: 'cancel',
          handler: () => {
            console.log('disagree clicked');
          }
        },
        {
          text: this.translate.instant("agree"),
          handler: () => {
            console.log('agree clicked');
            this.diagnostic.switchToLocationSettings();
            this.getUserLocation();
          }
        }
      ]
    });
    alert.present();
  }

  getUserLocation(){
    
    let GPSoptions = {timeout: 20000,enableHighAccuracy: true, maximumAge: 3600};
    this.geolocation.getCurrentPosition(GPSoptions).then((resp) => {
      this.lat = resp.coords.latitude;
      this.lng = resp.coords.longitude;
      console.log("get user location from helper: ",this.lat," , ",this.lng);
      this.events.publish('detectUserLocation');
      
    }).catch((error) => {
      console.log('Error getting location (catch): ', error);
      this.presentToast(this.translate.instant("AccessLocationFailed"));
      this.lat = 24.774265;
      this.lng = 46.738586;
      
      
    });

}



storecmplocation(loc){
  console.log("enter storecmplocation");
  this.storage.set('comp_location', loc).then(val=>{
    console.log("then from storecmplocation");
    this.events.publish('LocationSaved');
  }).catch(err=>{
    console.log("catch from storecmplocation");
  });

}

geoLoc(success) {

  let LocationAuthorizedsuccessCallback = (isAvailable) => {
    //console.log('Is available? ' + isAvailable);
    if (isAvailable) {
      
      this.GPSOpened(success);
    }
    else {
      this.requestOpenGPS(success);
    }
  };
  let LocationAuthorizederrorCallback = (e) => console.error(e);
  this.diagnostic.isLocationAvailable().then(LocationAuthorizedsuccessCallback).catch(LocationAuthorizederrorCallback);

}
GPSOpened(success) {
  let optionsLoc = {}
  optionsLoc = { timeout: 20000, enableHighAccuracy: true, maximumAge: 3600 };



  this.geolocation.getCurrentPosition(optionsLoc).then((resp) => {
    this.lat = resp.coords.latitude;
    this.lng = resp.coords.longitude;
    this.events.publish('detectUserLocation');
    let data = {
      lat: resp.coords.latitude,
      long: resp.coords.longitude
    }
    success(data);
  },
  err => {
    console.log("getCurrentPosition error "+err)
    success("-1");
  }
  ).catch((error) => {
    success("-1");
  });


}
requestOpenGPS(success) {
  if (this.platform.is('ios')) {
    this.diagnostic.getLocationAuthorizationStatus().then(status => {
      if (status == this.diagnostic.permissionStatus.NOT_REQUESTED) {
        //console.log('Permission not requested')
       // console.log('6')
        this.diagnostic.requestLocationAuthorization().then(status => {
          if (status == this.diagnostic.permissionStatus.NOT_REQUESTED) {
          
              let alert = this.alertCtrl.create({
                title: this.translate.instant("detetLoc"),
                message: this.translate.instant("giveGPSPermission"),
                buttons: [
                  {
                    text: this.translate.instant("cancel"),
                    role: 'cancel',
                    handler: () => {
                      console.log('Cancel clicked');
                      success("-1")
                    }
                  },
                  {
                    text: this.translate.instant("openSettings"),
                    handler: () => {
                      console.log("open sett");
                      this.diagnostic.switchToSettings();

                    }
                  }
                ]
              });
              alert.present();
            
          }
          else if (status == this.diagnostic.permissionStatus.DENIED) {
            
              
              let alert = this.alertCtrl.create({
                title: this.translate.instant("detetLoc"),
                message: this.translate.instant("giveGPSPermission"),
                buttons: [
                  {
                    text: this.translate.instant("cancel"),
                    role: 'cancel',
                    handler: () => {
                      console.log('Cancel clicked');
                      success("-1")
                    }
                  },
                  {
                    text: this.translate.instant("openSettings"),
                    handler: () => {
                      console.log("open sett");
                      this.diagnostic.switchToSettings();

                    }
                  }
                ]
              });
              alert.present();
            
          }
          else if (status == this.diagnostic.permissionStatus.GRANTED) {
            this.GPSOpened(success);
          }
          else if (status == this.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE) {
            this.GPSOpened(success);
          }
        })
      }
      else if (status == this.diagnostic.permissionStatus.DENIED) {
        
          let alert = this.alertCtrl.create({
            title: this.translate.instant("detetLoc"),
            message: this.translate.instant("giveGPSPermission"),
            buttons: [
              {
                text: this.translate.instant("cancel"),
                role: 'cancel',
                handler: () => {
                  console.log('Cancel clicked');
                  success("-1")
                }
              },
              {
                text: this.translate.instant("openSettings"),
                handler: () => {
                  console.log("open sett");
                  this.diagnostic.switchToSettings();

                }
              }
            ]
          });
          alert.present();
        
      }
      else if (status == this.diagnostic.permissionStatus.GRANTED) {
        this.GPSOpened(success);
      }
      else if (status == this.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE) {
        this.GPSOpened(success);
      }
    });
  }
  else {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {

      if (canRequest) {
        this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
          () => {
            console.log('Request successful')

            this.geoLoc(success);
          },
          error => {
            console.log('Error requesting location permissions', error);

            // let toast = this.toastCtrl.create({
            //   message: this.translate.instant("gpsWithHigh"),
            //   duration: 3000,
            //   position: 'bottom',
            //   cssClass: 'errorTeast'
            // });
            
              let alert = this.alertCtrl.create({
                title: this.translate.instant("detetLoc"),
                message: this.translate.instant("giveGPSPermission"),
                buttons: [
                  {
                    text: this.translate.instant("cancel"),
                    role: 'cancel',
                    handler: () => {
                      console.log('Cancel clicked');
                      success("-1")
                    }
                  },
                  {
                    text: this.translate.instant("openSettings"),
                    handler: () => {
                      console.log("open sett");
                      this.diagnostic.switchToLocationSettings();

                    }
                  }
                ]
              });
              alert.present();
            
          }
        );
      }

      else {
          let alert = this.alertCtrl.create({
            title: this.translate.instant("detetLoc"),
            message: this.translate.instant("giveGPSPermission"),
            buttons: [
              {
                text: this.translate.instant("cancel"),
                role: 'cancel',
                handler: () => {
                  console.log('Cancel clicked');
                  success("-1")
                }
              },
              {
                text: this.translate.instant("openSettings"),
                handler: () => {
                  console.log("open sett");
                  this.diagnostic.switchToSettings();

                }
              }
            ]
          });
          alert.present();
       
      }
    });
  }

}

}
