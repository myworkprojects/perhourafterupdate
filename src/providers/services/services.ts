import { HttpClient ,HttpHeaders, HttpParams} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HelperProvider } from '../helper/helper';
import { LoadingController } from 'ionic-angular';
import { passwordValidator, matchOtherValidator,emailValidator } from '../../validators/passwordValidator';
import CryptoJS from 'crypto-js';

@Injectable()
export class ServicesProvider {

  // serviceUrl :any = "http://apiperhour.itrootsdemos2.com";
   serviceUrl :any = "http://api.perhour-sa.com";
  
  constructor(public loading:LoadingController,public http: HttpClient,public helper:HelperProvider) {
    console.log('Hello ServicesProvider Provider');
  }

  jsRegister(userData,successCallback, failureCallback) {
    var newtoken = this.getTokenFormat();
    let loading = this.loading.create({
     
    }); 
    var encryptPass = this.DateEnncryption(userData.pass);

    let params={
      'FirstName' : userData.name,
      'MiddleName':"",
      'SirName':"",
      'Email':userData.email,
      'Password':encryptPass,
      'CountrySID':-1,
      'CitySID':-1,
      'ProfileImage':userData.profileImg,
      'DateOfBirth':"",
      'MobileNumber':"966"+userData.mobile,
      "ShortDescription":userData.experience,
      "CVFile":userData.cv,
      "FileExtension":userData.cvext
    }
    loading.present()
    let headers = new HttpHeaders();
   console.log(JSON.stringify(params))
  headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
  let serviceUrl = this.helper.jsServiceUrl + 'JS/Register'

      this.http.post(serviceUrl,JSON.stringify(params), { headers: headers})
      
        .subscribe(
          data => {
            loading.dismiss()
            successCallback(JSON.stringify(data))
          },
          err => {
            loading.dismiss()
            failureCallback(err)
          }
        )
    
}

jsActivationCode(JsSID,Code,successCallback,failureCallback){
  // 'activate?JsSID=1005&Code=123123'
  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({
     
  }); 
  
  loading.present()
  let headers = new HttpHeaders();
 
headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'JS/activate?JsSID='+JsSID+'&Code='+Code;
this.http.get(serviceUrl, { headers: headers})
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )
  
}
jsLogin(email,pass,successCallback,failureCallback){
 // http://apiperhour.itrootsdemos2.com/api/JS/check_login?Email=ali@itroots.net&Password=123123
 var newtoken = this.getTokenFormat();
  let loading = this.loading.create({
     
  }); 
  
  loading.present();
  let headers = new HttpHeaders();
  var encryptPass = this.DateEnncryption(pass);

  let params={
    'RegistrationID' : this.helper.registration,
    'Email':"966"+email,
    'OSType':this.helper.device_type,
    'Password':encryptPass


  }

 
headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
// let serviceUrl = this.helper.jsServiceUrl + 'JS/check_login?Email='+email+'&Password='+pass+'&RegistrationID='+this.helper.registration+'&OSType='+this.helper.device_type;
let serviceUrl = this.helper.jsServiceUrl + 'JS/check_login';
    // this.http.get(serviceUrl, { headers: headers})
     this.http.post(serviceUrl, JSON.stringify(params),{ headers: headers})
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )
}
jsListNewJobs(successCallback,failureCallback){
  //http://apiperhour.itrootsdemos2.com/api/job/latest
  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({
     
  }); 
  
  loading.present()
  let headers = new HttpHeaders();
 
headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'job/latest';

    this.http.get(serviceUrl, { headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )
}

jsApplayForJob(jsId,jobId,successCallback,failureCallback){
  //http://apiperhour.itrootsdemos2.com/api/job/candidateapply?candidate=2&job=1
  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({
     
  }); 
  
  loading.present()
  let headers = new HttpHeaders();
 
headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'job/candidateapply?candidate='+jsId+'&job='+jobId;

    this.http.get(serviceUrl, { headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )
}

jsListAllJobs(successCallback,failureCallback){
 // http://apiperhour.itrootsdemos2.com/api/job/all
 var newtoken = this.getTokenFormat();

 let loading = this.loading.create({
     
}); 

loading.present()
let headers = new HttpHeaders();

headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'job/all';

  this.http.get(serviceUrl, { headers: headers})
  
    .subscribe(
      data => {
        loading.dismiss()
        successCallback(JSON.stringify(data))
      },
      err => {
        loading.dismiss()
        failureCallback(err)
      }
    )

}
changeCompanyProfilePic(profile_pic,profile_pic_ext,SID, successCallback, failureCallback) {
    
  let loader = this.loading.create({
    content: "",
  });
   loader.present();
  let headers = new HttpHeaders();
  var newtoken = this.getTokenFormat();
  let parameter = new HttpParams().set('RegistryFile',profile_pic).set('FileExtension',profile_pic_ext).set('SID',SID)
  headers = headers.set('Content-Type', 'application/x-www-form-urlencoded').set('token',newtoken);
  let serviceUrl = this.helper.jsServiceUrl +'Company/updateprofileimage';
  this.http.post(serviceUrl,parameter,{headers: headers })
  
   
   .subscribe(
    data => {
      loader.dismiss().catch(() => console.log('ERROR CATCH: LoadingController dismiss'));
            console.log(JSON.stringify(data))
            successCallback(data)
    },
    err => {
      loader.dismiss().catch(() => console.log('ERROR CATCH: LoadingController dismiss'));
      failureCallback("-2")
    }
  )
}
changeJSProfilePic(profile_pic,profile_pic_ext,SID, successCallback, failureCallback) {
    
  let loader = this.loading.create({
    content: "",
  });
   loader.present();
   var newtoken = this.getTokenFormat();
  let headers = new HttpHeaders();
  let parameter = new HttpParams().set('CVFile',profile_pic).set('FileExtension',profile_pic_ext).set('SID',SID)
  headers = headers.set('Content-Type', 'application/x-www-form-urlencoded').set('token',newtoken);
  let serviceUrl = this.helper.jsServiceUrl +'JS/updateprofileimage';
  this.http.post(serviceUrl,parameter,{headers: headers })
  
   
   .subscribe(
    data => {
      loader.dismiss().catch(() => console.log('ERROR CATCH: LoadingController dismiss'));
            console.log(JSON.stringify(data))
            successCallback(data)
    },
    err => {
      loader.dismiss().catch(() => console.log('ERROR CATCH: LoadingController dismiss'));
      failureCallback("-2")
    }
  )
}

jsGetActiveJobs(JsSID,successCallback,failureCallback){
  // http://apiperhour.itrootsdemos2.com/api/Js/getactivejobs?JsSID=1
  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({
     
  }); 
  
  loading.present()
  let headers = new HttpHeaders();
  
  headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
  let serviceUrl = this.helper.jsServiceUrl + 'Js/getactivejobs?JsSID='+JsSID;
  
    this.http.get(serviceUrl, { headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )


}
listCities(successCallback,failureCallback){
  // http://apiperhour.itrootsdemos2.com/api/Js/getactivejobs?JsSID=1
  var newtoken = this.getTokenFormat();
  // let loading = this.loading.create({
     
  // }); 
  
  // loading.present()
  let headers = new HttpHeaders();
  
  headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
  let serviceUrl = this.helper.jsServiceUrl + 'lookups/listcities?country=-1 ';
  
    this.http.get(serviceUrl, { headers: headers})
    
      .subscribe(
        data => {
          //loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
        //  loading.dismiss()
          failureCallback(err)
        }
      )


}
jsToInvite(text,successCallback,failureCallback){
  // http://apiperhour.itrootsdemos2.com/api/Js/getactivejobs?JsSID=1
  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({
     
  }); 
  
  loading.present()
  let headers = new HttpHeaders();
  
  headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
  let serviceUrl = this.helper.jsServiceUrl + 'Js/search?searchText='+text;
  
    this.http.get(serviceUrl, { headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )


}
jsCheckInOut(JsSID,jobSID,type,successCallback,failureCallback){
// http://apiperhour.itrootsdemos2.com/api/Js/checkinout?js=1&job=3&type=1
var newtoken = this.getTokenFormat();
let loading = this.loading.create({   
}); 

loading.present()
let headers = new HttpHeaders();

headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'Js/checkinout?js='+JsSID+'&job='+jobSID+'&type='+type;

  this.http.get(serviceUrl, { headers: headers})
  
    .subscribe(
      data => {
        loading.dismiss()
        successCallback(JSON.stringify(data))
      },
      err => {
        loading.dismiss()
        failureCallback(err)
      }
    )
}

// comAddToShortList(candidate,job,successCallback,failureCallback){
//   let loading = this.loading.create({   
//   }); 
  
//   loading.present()
//   let headers = new HttpHeaders();
  
//   headers = headers.set('accept','application/json').set('content-type','application/json');
//   let serviceUrl = this.helper.jsServiceUrl + 'job/addcandidatetoshortlist?candidate='+candidate+'&job='+job;
  
//     this.http.get(serviceUrl, { headers: headers})
    
//       .subscribe(
//         data => {
//           loading.dismiss()
//           successCallback(JSON.stringify(data))
//         },
//         err => {
//           loading.dismiss()
//           failureCallback(err)
//         }
//       )

// }

comregister(name,email,phone,cityid,profile,pass,exp,cv,cvext,successCallback, failureCallback) {
  var newtoken = this.getTokenFormat();
    
  let loading = this.loading.create({
   
  }); 
  var encryptPass = this.DateEnncryption(pass);
  
  let params={
    'Name' : name,
    'Email':email,
    'Phone':"966"+phone,
    'Password':encryptPass,
    'CitySID':cityid,
    'Logo':profile,
    'ShortDescription':exp,
    "RegistryFile":cv,
    "FileExtension":cvext


  }
  loading.present()
  let headers = new HttpHeaders();
 console.log(JSON.stringify(params))
headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.serviceUrl + '/api/Company/register'

    this.http.post(serviceUrl,JSON.stringify(params), { headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )
  
    }
    comverification(comsid,verification,successCallback, failureCallback) {
      var newtoken = this.getTokenFormat();
      let loading = this.loading.create({
       
      }); 
    console.log(verification)
    console.log(comsid)
      loading.present()
      let headers = new HttpHeaders();
    headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
    let serviceUrl = this.serviceUrl + '/api/Company/activate?ComSID='+comsid +'&VerificationCode='+verification

        this.http.get(serviceUrl, { headers: headers})
        
          .subscribe(
            data => {
              loading.dismiss()
              successCallback(JSON.stringify(data))
            },
            err => {
              loading.dismiss()
              failureCallback(err)
            }
          )
      
        }

      //  companyLogin(email,pass,successCallback, failureCallback) {

      //     let loading = this.loading.create({
           
      //     }); 
          
      //     loading.present()
      //     let headers = new HttpHeaders();
         
      //   headers = headers.set('accept','application/json').set('content-type','application/json');
      //   let serviceUrl = this.serviceUrl + '/api/Company/check_login?Email=' +email+'&Password='+pass;
    
      //       this.http.get(serviceUrl, { headers: headers})
            
      //         .subscribe(
      //           data => {
      //             loading.dismiss()
      //             successCallback(JSON.stringify(data))
      //           },
      //           err => {
      //             loading.dismiss()
      //             failureCallback(err)
      //           }
      //         )
          
      //       } 


            companyLogin(email,pass,successCallback, failureCallback) {
              var newtoken = this.getTokenFormat();
              let loading = this.loading.create({
              }); 
              loading.present()
              let headers = new HttpHeaders();
              var encryptPass = this.DateEnncryption(pass);
              let params={
                'RegistrationID' : this.helper.registration,
                'Email':"966"+email,
                'OSType':this.helper.device_type,
                'Password':encryptPass
            
            
              }
             
            headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
            // let serviceUrl = this.serviceUrl + '/api/Company/check_login?Email=' +email+'&Password='+pass+'&RegistrationID='+this.helper.registration+'&OSType='+this.helper.device_type;
            let serviceUrl = this.serviceUrl + '/api/Company/check_login';
                this.http.post(serviceUrl, JSON.stringify(params),{ headers: headers})
                
                  .subscribe(
                    data => {
                      loading.dismiss()
                      successCallback(JSON.stringify(data))
                    },
                    err => {
                      loading.dismiss()
                      failureCallback(err)
                    }
                  )
              
                } 
    
                // companylistjobcategories(successCallback, failureCallback) {
                //   let loading = this.loading.create({
                //   });  
                //   loading.present()
                //   let headers = new HttpHeaders();
                // headers = headers.set('accept','application/json').set('content-type','application/json');
                // let serviceUrl = this.serviceUrl + '/api/lookups/listjobcategories';
                //     this.http.get(serviceUrl, { headers: headers})
                //       .subscribe(
                //         data => {
                //           loading.dismiss()
                //           successCallback(JSON.stringify(data))
                //         },
                //         err => {
                //           loading.dismiss()
                //           failureCallback(err)
                //         }
                //       )
                //     } 
                    // companylistjobtitles(jobid,successCallback, failureCallback) {
                    //   let loading = this.loading.create({
                    //   }); 
                      
                    //   loading.present()
                    //   let headers = new HttpHeaders();
                     
                    // headers = headers.set('accept','application/json').set('content-type','application/json');
                    // let serviceUrl = this.serviceUrl + '/api/lookups/listjobtitles?jobCat='+jobid;
                
                    //     this.http.get(serviceUrl, { headers: headers})
                        
                    //       .subscribe(
                    //         data => {
                    //           loading.dismiss()
                    //           successCallback(JSON.stringify(data))
                    //         },
                    //         err => {
                    //           loading.dismiss()
                    //           failureCallback(err)
                    //         }
                    //       )
                      
                    //     } 
                        // companysavejob(comsid,jobcatsid,jobtitsid,jobdes,PricePerHour,HourNumber,StartDate,EndDate,Gender,Latitude,Longitude,NumberOfPositions,TrophiesOffered,successCallback, failureCallback) {
                        //   let loading = this.loading.create({
                        //   }); 
                        //   let params={
                        //     'CompanySID' : comsid,
                        //     'JobCategorySID':jobcatsid,
                        //     'JobTitleSID':jobtitsid,
                        //     'JobDescription':jobdes,
                        //     'PricePerHour':PricePerHour,
                        //     'HourNumber':HourNumber,
                        //     'StartDate':StartDate,
                        //     'EndDate':EndDate,
                        //     'Gender':Gender,
                        //     'Latitude':Latitude,
                        //     "Longitude":Longitude,
                        //     'NumberOfPositions':NumberOfPositions,
                        //     "TrophiesOffered":TrophiesOffered
                        //   }
                        //   console.log(JSON.stringify(params))
                        //   loading.present()
                        //   let headers = new HttpHeaders();
                         
                        // headers = headers.set('accept','application/json').set('content-type','application/json');
                        // let serviceUrl = this.serviceUrl + '/api/job/postnew';
                    
                        //     this.http.post(serviceUrl,params, { headers: headers})
                            
                        //       .subscribe(
                        //         data => {
                        //           loading.dismiss()
                        //           successCallback(JSON.stringify(data))
                        //         },
                        //         err => {
                        //           loading.dismiss()
                        //           failureCallback(err)
                        //         }
                        //       )
                          
                        //     } 
                            // companypostedjob(comsid,successCallback, failureCallback) {
                            //   let loading = this.loading.create({
                            //   }); 
                            
                            //   loading.present()
                            //   let headers = new HttpHeaders();
                             
                            // headers = headers.set('accept','application/json').set('content-type','application/json');
                            // let serviceUrl = this.serviceUrl + '/api/Company/getpostedjobs?company='+comsid;
                            //     this.http.get(serviceUrl, { headers: headers})
                                
                            //       .subscribe(
                            //         data => {
                            //           loading.dismiss()
                            //           successCallback(JSON.stringify(data))
                            //         },
                            //         err => {
                            //           loading.dismiss()
                            //           failureCallback(err)
                            //         }
                            //       )
                              
                            //     }

                                companylistjobcategories(successCallback, failureCallback) {
                                  var newtoken = this.getTokenFormat();
                                  let loading = this.loading.create({
                                  });  
                                  loading.present()
                                  let headers = new HttpHeaders();
                                headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
                                let serviceUrl = this.serviceUrl + '/api/lookups/listjobcategories';
                                    this.http.get(serviceUrl, { headers: headers})
                                      .subscribe(
                                        data => {
                                          loading.dismiss()
                                          successCallback(JSON.stringify(data))
                                        },
                                        err => {
                                          loading.dismiss()
                                          failureCallback(err)
                                        }
                                      )
                                    } 
                                    companylistjobtitles(jobid,successCallback, failureCallback) {
                                      var newtoken = this.getTokenFormat();
                                      let loading = this.loading.create({
                                      }); 
                                      
                                      loading.present()
                                      let headers = new HttpHeaders();
                                     
                                    headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
                                    let serviceUrl = this.serviceUrl + '/api/lookups/listjobtitles?jobCat='+jobid;
                                
                                        this.http.get(serviceUrl, { headers: headers})
                                        
                                          .subscribe(
                                            data => {
                                              loading.dismiss()
                                              successCallback(JSON.stringify(data))
                                            },
                                            err => {
                                              loading.dismiss()
                                              failureCallback(err)
                                            }
                                          )
                                      
                                        } 
                                        companysavejob(newdata,comsid,jobcatsid,jobtitsid,jobdes,PricePerHour,HourNumber,StartDate,EndDate,Gender,Latitude,Longitude,NumberOfPositions,TrophiesOffered,successCallback, failureCallback) {
                                          
                                          // int City 
                                          // public string WorkingDays
                                          // string WorkingHoursStart 
                                          // string WorkingHoursEnd 
                                          // string InvitedJS 
                                          // int SalaryType  // 1: By Hour     2: By Month

                                          // ageFrom:this.agefrom1,ageTo:this.ageto1,
                                          
                                          var newtoken = this.getTokenFormat();
                                          let loading = this.loading.create({
                                          }); 
                                          let params={
                                            'CompanySID' : comsid,
                                            'JobCategorySID':jobcatsid,
                                            'JobTitleSID':jobtitsid,
                                            'JobDescription':jobdes,
                                            'PricePerHour':PricePerHour,
                                            'HourNumber':HourNumber,
                                            'StartDate':StartDate,
                                            'EndDate':EndDate,
                                            'Gender':Gender,
                                            'Latitude':Latitude,
                                            "Longitude":Longitude,
                                            'NumberOfPositions':NumberOfPositions,
                                            "TrophiesOffered":TrophiesOffered,
                                            "City":newdata.City,
                                            "WorkingDays":newdata.WorkingDays,
                                            "WorkingHoursStart":newdata.WorkingHoursStart, 
                                            "WorkingHoursEnd":newdata.WorkingHoursEnd, 
                                            "InvitedJS":newdata.InvitedJS, 
                                            "SalaryType":newdata.SalaryType,
                                            "MonthlySalary":newdata.monthsalary ,
                                            "Nationality":newdata.Nationality,
                                            "AddressDescription":newdata.AddressDescription,
                                            "SeekerAgeFrom":newdata.ageFrom,
                                            "SeekerAgeTo":newdata.ageTo


                                          }
                                          console.log(JSON.stringify(params))
                                          loading.present()
                                          let headers = new HttpHeaders();
                                         
                                        headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
                                        let serviceUrl = this.serviceUrl + '/api/job/postnew';
                                    
                                            this.http.post(serviceUrl,params, { headers: headers})
                                            
                                              .subscribe(
                                                data => {
                                                  loading.dismiss()
                                                  successCallback(JSON.stringify(data))
                                                },
                                                err => {
                                                  loading.dismiss()
                                                  failureCallback(err)
                                                }
                                              )
                                          
                                            } 
                                             companypostedjob(comsid,successCallback, failureCallback) {
                                              var newtoken = this.getTokenFormat();
                                          let loading = this.loading.create({
                                          }); 
                                        
                                          loading.present()
                                          let headers = new HttpHeaders();
                                         
                                        headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
                                        let serviceUrl = this.serviceUrl + '/api/Company/getpostedjobs?company='+comsid;
                                            this.http.get(serviceUrl, { headers: headers})
                                            
                                              .subscribe(
                                                data => {
                                                  loading.dismiss()
                                                  successCallback(JSON.stringify(data))
                                                },
                                                err => {
                                                  loading.dismiss()
                                                  failureCallback(err)
                                                }
                                              )
                                          
                                            } 
                                            companygetappliedcandidates(comsid,successCallback, failureCallback) {
                                              var newtoken = this.getTokenFormat();
                                              let loading = this.loading.create({
                                              }); 
                                            
                                              loading.present()
                                              let headers = new HttpHeaders();
                                             
                                            headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
                                            let serviceUrl = this.serviceUrl + '/api/job/getappliedcandidates?job='+comsid;
                                                this.http.get(serviceUrl, { headers: headers})
                                                
                                                  .subscribe(
                                                    data => {
                                                      loading.dismiss()
                                                      successCallback(JSON.stringify(data))
                                                    },
                                                    err => {
                                                      loading.dismiss()
                                                      failureCallback(err)
                                                    }
                                                  )
                                              
                                                } 
                                                companyhirecandidate(candidate,comsid,successCallback, failureCallback) {
                                                  var newtoken = this.getTokenFormat();
                                                  let loading = this.loading.create({
                                                  }); 
                                                
                                                  loading.present()
                                                  let headers = new HttpHeaders();
                                                 
                                                headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
                                                let serviceUrl = this.serviceUrl + '/api/job/candidatehire?candidate='+candidate+'&job='+comsid;
                                                    this.http.get(serviceUrl, { headers: headers})
                                                    
                                                      .subscribe(
                                                        data => {
                                                          loading.dismiss()
                                                          successCallback(JSON.stringify(data))
                                                        },
                                                        err => {
                                                          loading.dismiss()
                                                          failureCallback(err)
                                                        }
                                                      )
                                                  
                                                    } 
                                                    companygetshortlist(comsid,successCallback, failureCallback) {
                                                      var newtoken = this.getTokenFormat();
                                                      let loading = this.loading.create({
                                                      }); 
                                                    
                                                      loading.present()
                                                      let headers = new HttpHeaders();
                                                     
                                                    headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
                                                    let serviceUrl = this.serviceUrl + '/api/job/getshortlistcandidates?job='+comsid;
                                                        this.http.get(serviceUrl, { headers: headers})
                                                        
                                                          .subscribe(
                                                            data => {
                                                              loading.dismiss()
                                                              successCallback(JSON.stringify(data))
                                                            },
                                                            err => {
                                                              loading.dismiss()
                                                              failureCallback(err)
                                                            }
                                                          )
                                                      
                                                        } 
                                                        companyremovefromshortlist(candidate,comsid,successCallback, failureCallback) {
                                                          var newtoken = this.getTokenFormat();
                                                          let loading = this.loading.create({
                                                          }); 
                                                        
                                                          loading.present()
                                                          let headers = new HttpHeaders();
                                                         
                                                        headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
                                                        let serviceUrl = this.serviceUrl + '/api/job/removecandidatefromshortlist?candidate='+candidate+'&job='+comsid;
                                                            this.http.get(serviceUrl, { headers: headers})
                                                            
                                                              .subscribe(
                                                                data => {
                                                                  loading.dismiss()
                                                                  successCallback(JSON.stringify(data))
                                                                },
                                                                err => {
                                                                  loading.dismiss()
                                                                  failureCallback(err)
                                                                }
                                                              )
                                                          
                                                            } 
                                                            comAddToShortList(candidate,job,successCallback,failureCallback){
                                                              var newtoken = this.getTokenFormat();
                                                              let loading = this.loading.create({   
                                                              }); 
                                                              
                                                              loading.present()
                                                              let headers = new HttpHeaders();
                                                              
                                                              headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
                                                              let serviceUrl = this.helper.jsServiceUrl + 'job/addcandidatetoshortlist?candidate='+candidate+'&job='+job;
                                                              
                                                                this.http.get(serviceUrl, { headers: headers})
                                                                
                                                                  .subscribe(
                                                                    data => {
                                                                      loading.dismiss()
                                                                      successCallback(JSON.stringify(data))
                                                                    },
                                                                    err => {
                                                                      loading.dismiss()
                                                                      failureCallback(err)
                                                                    }
                                                                  )
                                                            
                                                            }
                                                    
                                                
                
                                                          
resendActivation(jsId,successCallback,failureCallback){
  // api/Js/resendactivation?JsSID=1
  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({   
  }); 
  
  loading.present()
  let headers = new HttpHeaders();
  
  headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
  let serviceUrl = this.helper.jsServiceUrl + 'Js/resendactivation?JsSID='+jsId;
  
    this.http.get(serviceUrl, { headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )

}
comresendActivation(jsId,successCallback,failureCallback){
  var newtoken = this.getTokenFormat();
  //api/Company/resendactivation?comp=1
  let loading = this.loading.create({   
  }); 
  
  loading.present()
  let headers = new HttpHeaders();
  
  headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
  let serviceUrl = this.helper.jsServiceUrl + 'Company/resendactivation?comp='+jsId;
  
    this.http.get(serviceUrl, { headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )

}
DateEnncryption(str){ 
  var key = CryptoJS.enc.Utf8.parse('8080808080808080');
      var iv = CryptoJS.enc.Utf8.parse('8080808080808080');
  var encryptedToken = CryptoJS.AES.encrypt(
        CryptoJS.enc.Utf8.parse(str), key, {
          keySize: 128 / 8,
          iv: iv,
          mode: CryptoJS.mode.CBC,
          padding: CryptoJS.pad.Pkcs7
        }).toString()
      console.log("encryptedToken : " + encryptedToken);
      return encryptedToken
}
cancelActiveJob(jsSID,jobSID,successCallback,failureCallback){
  // api/Js/jobcancel?seeker=1&job=1
  //api/Js/jobcancel?seeker=1&job=1
  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({
     
  }); 
  
  loading.present()
  let headers = new HttpHeaders();
 
headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'Js/jobcancel?seeker='+jsSID+'&job='+jobSID;
this.http.get(serviceUrl, { headers: headers})
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )
}

balanceForComp(compSid,successCallback,failureCallback){
  // api/Company/getmybalance?comp=1
  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({   
  }); 
  
  loading.present()
  let headers = new HttpHeaders();
  
  headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
  let serviceUrl = this.helper.jsServiceUrl + 'Company/getmybalance?comp='+compSid;
  
    this.http.get(serviceUrl, { headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )

}
balanceForJs(jsSid,successCallback,failureCallback){
  // api/Js/getmybalance?seeker=1
  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({   
  }); 
  
  loading.present()
  let headers = new HttpHeaders();
  
  headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
  let serviceUrl = this.helper.jsServiceUrl + 'Js/getmybalance?seeker='+jsSid;
  
    this.http.get(serviceUrl, { headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )

}

getJsAppliedJobs(jsSid,successCallback,failureCallback){
  // api/Js/getappliedjobs?JsSID=1
  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({   
  }); 
  
  loading.present()
  let headers = new HttpHeaders();
  
  headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
  let serviceUrl = this.helper.jsServiceUrl + 'Js/getappliedjobs?JsSID='+jsSid;
  
    this.http.get(serviceUrl, { headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )

}
jsUnapply(jsSID,jobSID,successCallback,failureCallback){
  // api/job/candidateunapply?candidate=1&job=1
  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({   
  }); 
  
  loading.present()
  let headers = new HttpHeaders();
  
  headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
  let serviceUrl = this.helper.jsServiceUrl + 'job/candidateunapply?candidate='+jsSID+'&job='+jobSID;
  
    this.http.get(serviceUrl, { headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )

}
acceptorrejectAfterNotificationJSCheckin(jobId,jsId,acOrRe,successCallback,failureCallback){
  var newtoken = this.getTokenFormat();

//api/Company/checkinacceptreject
//AcceptOrReject = 1: Accept, -1: Reject


let loading = this.loading.create({
     
}); 

let params={
  'Job' : jobId,
  'JobSeeker':jsId,
  'AcceptOrReject':acOrRe
}
loading.present()
let headers = new HttpHeaders();
console.log(JSON.stringify(params))
headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'Company/checkinacceptreject';

  this.http.post(serviceUrl,JSON.stringify(params), { headers: headers})
  
    .subscribe(
      data => {
        loading.dismiss()
        successCallback(JSON.stringify(data))
      },
      err => {
        loading.dismiss()
        failureCallback(err)
      }
    )
    

}
getJobDetails(jobId,successCallback,failureCallback){
  // api/job/getdetails?job=1
  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({
     
  }); 
  
  loading.present()
  let headers = new HttpHeaders();
 
headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'job/getdetails?job='+jobId;
this.http.get(serviceUrl, { headers: headers})
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )
}
getUserDetails(jsid,successCallback,failureCallback){
  // api/Js/getdetails?seeker=1
  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({
     
  }); 
  
  loading.present()
  let headers = new HttpHeaders();
 
headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'Js/getdetails?seeker='+jsid;
this.http.get(serviceUrl, { headers: headers})
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )


}

getLicense(successCallback,failureCallback){
  // api/License/get
  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({
     
  }); 
  
  loading.present()
  let headers = new HttpHeaders();
 
headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'License/get';
this.http.get(serviceUrl, { headers: headers})
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )

}

jsActivationCodeForForget(JsSID,Code,successCallback,failureCallback){
  // api/Js/validateverificationcode?seeker=1&code=2345
  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({
     
  }); 
  
  loading.present()
  let headers = new HttpHeaders();
 
headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'Js/validateverificationcode?seeker='+JsSID+'&code='+Code;
this.http.get(serviceUrl, { headers: headers})
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )
  
}

compActivationCodeForForget(JsSID,Code,successCallback,failureCallback){
  // api/Company/validateverificationcode?comp=1&code=2345
  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({
     
  }); 
  
  loading.present()
  let headers = new HttpHeaders();
 
headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'Company/validateverificationcode?comp='+JsSID+'&code='+Code;
this.http.get(serviceUrl, { headers: headers})
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )
  
}
jsCheckPass(jsid,pass,successCallback,failureCallback){
  // api/Js/changepassword?seeker=1&newPassword=sdgwdgs
  var newtoken = this.getTokenFormat();
  var encryptPass = this.DateEnncryption(pass);
  let loading = this.loading.create({
     
  }); 
  
  loading.present()
  let headers = new HttpHeaders();
 
headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'Js/changepassword?seeker='+jsid+'&newPassword='+encryptPass;
this.http.get(serviceUrl, { headers: headers})
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )

}

compCheckPass(jsid,pass,successCallback,failureCallback){
  // api/Company/changepassword?comp=1&newPassword=sdgwdgs
  var newtoken = this.getTokenFormat();
  var encryptPass = this.DateEnncryption(pass);

  let loading = this.loading.create({
     
  }); 
  
  loading.present()
  let headers = new HttpHeaders();
 
headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'Company/changepassword?comp='+jsid+'&newPassword='+encryptPass;
this.http.get(serviceUrl, { headers: headers})
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )

}
jsForgetPassMobile(mobile,successCallback,failureCallback){
  // api/Js/forgetpassword?mobileNum=01231236456
  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({
     
  }); 
  
  loading.present()
  let headers = new HttpHeaders();
 
headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'Js/forgetpassword?mobileNum='+'966'+mobile;
this.http.get(serviceUrl, { headers: headers})
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )

}
compForgetPassMobile(mobile,successCallback,failureCallback){
 // api/Company/forgetpassword?mobileNum=01231236456
 var newtoken = this.getTokenFormat();
  let loading = this.loading.create({
     
  }); 
  
  loading.present()
  let headers = new HttpHeaders();
 
headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'Company/forgetpassword?mobileNum='+'966'+mobile;
this.http.get(serviceUrl, { headers: headers})
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )

}

jsresendActivationForForget(jsId,successCallback,failureCallback){
  var newtoken = this.getTokenFormat();
  //api/Company/resendactivation?comp=1
  let loading = this.loading.create({   
  }); 
  
  loading.present()
  let headers = new HttpHeaders();
  
  headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
  let serviceUrl = this.helper.jsServiceUrl + 'Js/resendactivation?JsSID='+jsId;
  
    this.http.get(serviceUrl, { headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )

}
compresendActivationForForget(jsId,successCallback,failureCallback){
  
  //api/Company/resendactivation?comp=1
  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({   
  }); 
  
  loading.present()
  let headers = new HttpHeaders();
  
  headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
  let serviceUrl = this.helper.jsServiceUrl + 'Company/resendactivation?comp='+jsId;
  
    this.http.get(serviceUrl, { headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )

}


getTokenFormat(){

  //YYYY*10*MM*10*dd*PerHour
//2018*10*12*10*04*PerHour

// var currentDate  = new Date();

// var year = currentDate.getUTCFullYear();
// var month = ("0" + (currentDate.getMonth() + 1)).slice(-2);
// var day = ("0" + currentDate.getDate()).slice(-2);
// var token = year+"*10*"+month+"*10*"+day+"*PerHour";

var currentDate  = new Date().toISOString();
// 2018-12-06
var year = currentDate.split("-")[0];
var month = currentDate.split("-")[1];
var day = currentDate.split("-")[2].split("T")[0];
var token = year+"*10*"+month+"*10*"+day+"*PerHour";
console.log("token : ",token);
var encrypttoken = this.DateEnncryption(token);
console.log("encrypt token : ",encrypttoken);
return encrypttoken;
}

companyAddLocation(id,lat,lon,successCallback,failureCallback){
  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({
   
  }); 
  // let params={
  //   'SID' : lat,
  //   'RegistryFile':lon,
  //   'FileExtension':add
  // }
  loading.present()
  let headers = new HttpHeaders();
//  console.log(JSON.stringify(params)
headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'Company/updatelocation?comp='+id+'&lat='+lat+'&longitude='+lon;

    this.http.get(serviceUrl, { headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )
}


getaddress(lat,lng){
    
  var url = "https:maps.googleapis.com/maps/api/geocode/json?address="+lat+","+lng+"&key="+this.helper.key;
  console.log("google api url ",url);
  return this.http.get(url);
}

compaddcr(id,cv,ext,successCallback,failureCallback){
  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({
   
  }); 
  let params={
    'SID' : id,
    'RegistryFile':cv,
    'FileExtension':ext
  }
  loading.present()
  let headers = new HttpHeaders();
 console.log(JSON.stringify(params))
headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'Company/updateregistryfile';

    this.http.post(serviceUrl,JSON.stringify(params), { headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )
}

jsaddcv(id,cv,ext,successCallback,failureCallback){

  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({
   
  }); 
  let params={
    'SID' : id,
    'CVFile':cv,
    'FileExtension':ext
  }
  loading.present()
  let headers = new HttpHeaders();
 console.log(JSON.stringify(params))
headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'JS/updatecv';

    this.http.post(serviceUrl,JSON.stringify(params), { headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )

}
jsaddworkercert(id,cv,ext,successCallback,failureCallback){
//api/JS/updatecert
  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({
   
  }); 
  let params={
    'SID' : id,
    'CVFile':cv,
    'FileExtension':ext
  }
  loading.present()
  let headers = new HttpHeaders();
 console.log(JSON.stringify(params))
headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'JS/updatecert';

    this.http.post(serviceUrl,JSON.stringify(params), { headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )

}

companyChangepass(id,pass,successCallback,failureCallback){
  // Company/changepassword?comp=1&newPassword=124124

  var newtoken = this.getTokenFormat();
  var encryptPass = this.DateEnncryption(pass);
  let loading = this.loading.create({
   
  }); 
  loading.present()
  let headers = new HttpHeaders();

headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'Company/changepassword?comp='+id+'&newPassword='+encryptPass;

    this.http.get(serviceUrl, { headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )


}

compCacelNotPostedJob(compid,jobid,successCallback,failureCallback){
  // api/Company/canceljob?comp=1&job=1

  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({
   
  }); 
  loading.present()
  let headers = new HttpHeaders();

headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'Company/canceljob?comp='+compid+'&job='+jobid;

    this.http.get(serviceUrl, { headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )

}

saveIBAN(id,IBAN,successCallback,failureCallback){
  // /Js/updateIBAN?seeker=1&IBAN=SA1212345678912345678912
  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({
   
  }); 
  loading.present()
  let headers = new HttpHeaders();

headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'Js/updateIBAN?seeker='+id+'&IBAN='+IBAN;

    this.http.get(serviceUrl, { headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )


}


getjobdetailsforcomp(jobid,successCallback,failureCallback){
  // api/job/getdetails?job=1
  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({
   
  }); 
  loading.present()
  let headers = new HttpHeaders();

headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'job/getdetails?job='+jobid;

    this.http.get(serviceUrl, { headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )


}
compGetSelectedcan(jobid,successCallback,failureCallback){
  // job/getselectedcandidates?job=1
  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({
   
  }); 
  loading.present()
  let headers = new HttpHeaders();

headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'job/getselectedcandidates?job='+jobid;

    this.http.get(serviceUrl, { headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )

}


jsgetEducation(jsSID,successCallback,failureCallback){
  // api/Js/getstudies?seeker=1
  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({
   
  }); 
  loading.present()
  let headers = new HttpHeaders();

headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'Js/getstudies?seeker='+jsSID;

    this.http.get(serviceUrl, { headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )

}

jsAddStudy(userData,submitType,successCallback,failureCallback){

//   api/JS/addstudy
// {
//         int Seeker,
//         int Study ,
//         string SchoolName,
//         string ScientificGrade,
//         string Branch,
//         string TotalGrade,
//         Date StartDate,
//         Date EndDate,
//         int StillLearning (1: true, 0 False),
//         string Description,
//         string CertFile,
//         string FileExtension
// }

var newtoken = this.getTokenFormat();
let loading = this.loading.create({
 
}); 

let params={
  'Seeker' : userData.Seeker,
  'Study':userData.Study,
  'SchoolName':userData.SchoolName,
  'ScientificGrade':userData.ScientificGrade,
  'TotalGrade':userData.TotalGrade,
  'EndDate':userData.EndDate,
  'StartDate':userData.StartDate,
  'StillLearning':userData.StillLearning,
  'Description':userData.Description,
  'CertFile':userData.CertFile,
  "FileExtension":userData.FileExtension,
  "Branch":userData.Branch
}
loading.present()
let headers = new HttpHeaders();
console.log(JSON.stringify(params))
headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);

var serviceUrl ;
if(submitType == "add")
serviceUrl = this.helper.jsServiceUrl + 'JS/addstudy';
else if(submitType == "edit")
serviceUrl = this.helper.jsServiceUrl + 'JS/updatestudy';

  this.http.post(serviceUrl,JSON.stringify(params), { headers: headers})
  
    .subscribe(
      data => {
        loading.dismiss()
        successCallback(JSON.stringify(data))
      },
      err => {
        loading.dismiss()
        failureCallback(err)
      }
    )

}


jsgetExperience(jsSID,successCallback,failureCallback){
  // api/Js/getexperiences?seeker=
  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({
   
  }); 
  loading.present()
  let headers = new HttpHeaders();

headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'Js/getexperiences?seeker='+jsSID;

    this.http.get(serviceUrl, { headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )

}

jsAddExperience(userData,submitType,successCallback,failureCallback){

//   api/JS/addexperience
// {
//   int Seeker,
//   int Experience,
//   string PlaceName,
//   string JobTitle,
//   string Address,
//   DateTime StartDate,
//   DateTime EndDate,
//   int StillWorking,
//   string Description,
//   string CertFile,
//   string FileExtension 
// }

var newtoken = this.getTokenFormat();
let loading = this.loading.create({
 
}); 

let params={
  'Seeker' : userData.Seeker,
  'Experience':userData.Experience,
  'PlaceName':userData.PlaceName,
  'JobTitle':userData.JobTitle,
  'Address':userData.Address,
  'EndDate':userData.EndDate,
  'StartDate':userData.StartDate,
  'StillWorking':userData.StillWorking,
  'Description':userData.Description,
  'CertFile':userData.CertFile,
  "FileExtension":userData.FileExtension
}
loading.present()
let headers = new HttpHeaders();
console.log(JSON.stringify(params))
headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
// let serviceUrl = this.helper.jsServiceUrl + 'JS/addexperience';
var serviceUrl ;
if(submitType == "add")
serviceUrl = this.helper.jsServiceUrl + 'JS/addexperience';
else if(submitType == "edit")
serviceUrl = this.helper.jsServiceUrl + 'JS/updateexperience';

  this.http.post(serviceUrl,JSON.stringify(params), { headers: headers})
  
    .subscribe(
      data => {
        loading.dismiss()
        successCallback(JSON.stringify(data))
      },
      err => {
        loading.dismiss()
        failureCallback(err)
      }
    )

}
compEditProfile(userData,successCallback,failureCallback){
  // api/Company/profileupdate
  // {
  //   int CompanySID;
  //   string CompanyName;
  //   int CitySID;
  //   string ShortDescription;
  //   string RegistryNum;
  //   string Lat;
  //   string Long;
  //   }

var newtoken = this.getTokenFormat();
let loading = this.loading.create({
}); 
let params={
  'CompanySID' : userData.compid,
  'CompanyName':userData.name,
  // 'Mail':userData.Mail,
  'ShortDescription':userData.ShortDescription,
  'CitySID':userData.CitySID,
  'RegistryNum':userData.RegistryNum
  
}
loading.present()
let headers = new HttpHeaders();
console.log(JSON.stringify(params))
headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'Company/profileupdate';
this.http.post(serviceUrl,JSON.stringify(params), { headers: headers})
.subscribe(
      data => {
        loading.dismiss()
        successCallback(JSON.stringify(data))
      },
      err => {
        loading.dismiss()
        failureCallback(err)
      }
    )

}

jsEditProfile(userData,successCallback,failureCallback){
  // api/JS/updateprofile
  // {
  //         int Seeker,
  //         string FullName,
  //         string Mail,
  //         string IBAN,
  //         string VideoURL
  // }
  // {
  //   int City 
  //           string preferedJobCategories
  //           int WeeklyHoursCount
  //           int WeekEndWorkingDays 
  //           Date Birthdate 
  //           int Gender //1: Male, 2: Female
  //   }

var newtoken = this.getTokenFormat();
let loading = this.loading.create({
}); 
let params={
  'Seeker' : userData.Seeker,
  'FullName':userData.FullName,
  'Mail':userData.Mail,
  'IBAN':userData.IBAN,
  'VideoURL':userData.VideoURL,
  'JobTitle':userData.ShortDescription,
  'City':userData.city_id, 
  'preferedJobCategories':userData.interestedJobs,
  'WeeklyHoursCount':userData.WeeklyHoursCount,
  'WeekEndWorkingDays':userData.workingDaysInWeekend, 
  'Birthdate':userData.dob, 
  'Gender':userData.gender,
  "workingDaysAndTimes":userData.workingDaysAndTimes,
  "Nationality":userData.Nationality
}

loading.present()
let headers = new HttpHeaders();
console.log(JSON.stringify(params))
headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'JS/updateprofile';
this.http.post(serviceUrl,JSON.stringify(params), { headers: headers})
.subscribe(
      data => {
        loading.dismiss()
        successCallback(JSON.stringify(data))
      },
      err => {
        loading.dismiss()
        failureCallback(err)
      }
    )

}

jsJobHistory(jssid,successCallback,failureCallback){
  // api/Js/getjobshistory?seeker=1 
  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({
     
  }); 
  
  loading.present()
  let headers = new HttpHeaders();
 
headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'Js/getjobshistory?seeker='+jssid;
this.http.get(serviceUrl, { headers: headers})
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )
}


jsgetLatestJobs(jssid,successCallback,failureCallback)
{  // Js/getappliedjobslatest?seeker=
  var newtoken = this.getTokenFormat();
  // let loading = this.loading.create({
     
  // }); 
  
  // loading.present()
  let headers = new HttpHeaders();
 
headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'Js/getappliedjobslatest?seeker='+jssid;
this.http.get(serviceUrl, { headers: headers})
      .subscribe(
        data => {
          // loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          // loading.dismiss()
          failureCallback(err)
        }
      )

}
jsgetLatestJobs2(jssid,successCallback,failureCallback)
{  // Js/getappliedjobslatest?seeker=
  var newtoken = this.getTokenFormat();
  // let loading = this.loading.create({
     
  // }); 
  
  // loading.present()
  let headers = new HttpHeaders();
 
headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'Js/getappliedjobslatest?seeker='+jssid;
this.http.get(serviceUrl, { headers: headers})
      .subscribe(
        data => {
          // loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          // loading.dismiss()
          failureCallback(err)
        }
      )

}
jsgetRecomendedJobs(jssid,successCallback,failureCallback){
  // api/Js/getrecommendedjobs?seeker=1 
  var newtoken = this.getTokenFormat();
  // let loading = this.loading.create({
     
  // }); 
  
  // loading.present()
  let headers = new HttpHeaders();
 
headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'Js/getrecommendedjobs?seeker='+jssid;
this.http.get(serviceUrl, { headers: headers})
      .subscribe(
        data => {
          // loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          // loading.dismiss()
          failureCallback(err)
        }
      )

}

jsnearestJobs(lat,lng,successCallback,failureCallback){
  // Js/getnearestjobs?currentLat=30.4377182&currentLong=31.0334848
  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({
     
  }); 
  
  loading.present()
  let headers = new HttpHeaders();
 
headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'Js/getnearestjobs?currentLat='+lat+'&currentLong='+lng;
this.http.get(serviceUrl, { headers: headers})
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )
}

companyJobHistory(compSID,successCallback,failureCallback){
  // Company/getjobshistory?comp=
  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({
     
  }); 
  
  loading.present()
  let headers = new HttpHeaders();
 
headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'Company/getjobshistory?comp='+compSID;
this.http.get(serviceUrl, { headers: headers})
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )
}

comapnyAddRate(userData,successCallback,failureCallback){
  // Company/addreview
//   {
//     int Company 
//     int Seeker 
//     int Job 
//     int ReviewGrade 
//     string ReviewComment 
// }
var newtoken = this.getTokenFormat();
    let loading = this.loading.create({
    }); 
    let params={
      'Company' : userData.Company,
      'Seeker':userData.Seeker,
      'Job':userData.Job,
      "ReviewGrade":userData.ReviewGrade,
      "ReviewComment":userData.ReviewComment
    }
    loading.present()
    let headers = new HttpHeaders();
   console.log(JSON.stringify(params))
  headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
  let serviceUrl = this.helper.jsServiceUrl + 'Company/addreview'
  this.http.post(serviceUrl,JSON.stringify(params), { headers: headers})
        .subscribe(
          data => {
            loading.dismiss()
            successCallback(JSON.stringify(data))
          },
          err => {
            loading.dismiss()
            failureCallback(err)
          }
        )

}

companyStatistics(compSID,successCallback,failureCallback){
  // api/Company/getstatistics?comp=1
  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({
     
  }); 
  
  loading.present()
  let headers = new HttpHeaders();
 
headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'Company/getstatistics?comp='+compSID;
this.http.get(serviceUrl, { headers: headers})
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )

}
companyPaymentHistory(compSID,successCallback,failureCallback){
  // api/Company/getpaymenthistory?comp=1
  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({
     
  }); 
  
  loading.present()
  let headers = new HttpHeaders();
 
headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'Company/getpaymenthistory?comp='+compSID;
this.http.get(serviceUrl, { headers: headers})
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )


}

jsgetPaymentHistory(jssid,successCallback,failureCallback){
  // api/Js/getpaymenthistory?seeker=1    
  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({
     
  }); 
  
  loading.present()
  let headers = new HttpHeaders();
 
headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'Js/getpaymenthistory?seeker='+jssid;
this.http.get(serviceUrl, { headers: headers})
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )
}


jsUploadVedio(id,vedio,ext,successCallback,failureCallback){
//   HttpPost
// api/JS/updatevideo

  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({
   
  }); 
  let params={
    'SID' : id,
    'CVFile':vedio,
    'FileExtension':ext
  }
  loading.present()
  let headers = new HttpHeaders();
 console.log(JSON.stringify(params))
headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'JS/updatevideo';

    this.http.post(serviceUrl,JSON.stringify(params), { headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )

}

jsLateOrOntime(jssid,jobsid,lateOrOntime,successCallback,failureCallback){
 // api/Js/jobontimeorlate?seeker=1&job=1&ontimeOrLate=1
//ontimeOrLate:    1: Ontime           2: Late
var newtoken = this.getTokenFormat();
let loading = this.loading.create({
   
}); 

loading.present()
let headers = new HttpHeaders();

headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'Js/jobontimeorlate?seeker='+jssid+'&job='+jobsid+'&ontimeOrLate='+lateOrOntime;
this.http.get(serviceUrl, { headers: headers})
    .subscribe(
      data => {
        loading.dismiss()
        successCallback(JSON.stringify(data))
      },
      err => {
        loading.dismiss()
        failureCallback(err)
      }
    )

}

disableOrEnableNotification(compid,status,successCallback,failureCallback){
  // api/Company/updatenotificationstatus?comp=1&status=1
  // 1: Enable     & -1:Disable
  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({
     
  }); 
  
  loading.present()
  let headers = new HttpHeaders();
 
headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'Company/updatenotificationstatus?comp='+compid+'&status='+status;

    this.http.get(serviceUrl, { headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )
}

requestExperienceCertificate(jssid,jobcat,successCallback,failureCallback){
  //api/Js/requestexperiencecert?seeker=1&jobCategory=1
  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({
     
  }); 
  
  loading.present()
  let headers = new HttpHeaders();
 
headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'Js/requestexperiencecert?seeker='+jssid+'&jobCategory='+jobcat;

    this.http.get(serviceUrl, { headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )
}

jsdisableOrEnableNotification(jssid,status,successCallback,failureCallback){
  //api/Js/updatenotificationstatus?seeker=1&status=1
  // 1: Enable     & -1:Disable
  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({
     
  }); 
  
  loading.present()
  let headers = new HttpHeaders();
 
headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'Js/updatenotificationstatus?seeker='+jssid+'&status='+status;

    this.http.get(serviceUrl, { headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )
}

jsChangepass(id,pass,successCallback,failureCallback){
 // api/Js/changepassword?seeker=1&newPassword=124124

  var newtoken = this.getTokenFormat();
  var encryptPass = this.DateEnncryption(pass);
  let loading = this.loading.create({
   
  }); 
  loading.present()
  let headers = new HttpHeaders();

headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'Js/changepassword?seeker='+id+'&newPassword='+encryptPass;

    this.http.get(serviceUrl, { headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )


}


listNationalities(type,successCallback,failureCallback){
  // api/lookups/listnationalities
  // api/lookups/listnationalities?fromProfile=1
  var newtoken = this.getTokenFormat();
  
  let loading = this.loading.create({
   
  }); 
  loading.present()
  let headers = new HttpHeaders();

headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'lookups/listnationalities?fromProfile='+type;

    this.http.get(serviceUrl, { headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )


}










getJSRates(seekerid,successCallback,failureCallback){
  
  // api/Js/getreviewdetails?seeker=1
  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({
   
  }); 
  loading.present()
  let headers = new HttpHeaders();

headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'Js/getreviewdetails?seeker='+seekerid;

    this.http.get(serviceUrl, { headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )


}



absent(jobId,seekerid,compid,successCallback,failureCallback){
  
  // api/job/JobSeekerAbsense?job=1&seeker=1&company=1
  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({
   
  }); 
  loading.present()
  let headers = new HttpHeaders();

headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'job/JobSeekerAbsense?job='+jobId+'&seeker='+seekerid+'&company='+compid;

    this.http.get(serviceUrl, { headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )


}






searchApi(searchTxt,successCallback,failureCallback){
  // api/job/search?searchText=test
  var newtoken = this.getTokenFormat();
  let loading = this.loading.create({
   
  }); 
  loading.present()
  let headers = new HttpHeaders();

headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'job/search?searchText='+searchTxt;

    this.http.get(serviceUrl, { headers: headers})
    
      .subscribe(
        data => {
          loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          loading.dismiss()
          failureCallback(err)
        }
      )
}



getMinumSalaries(successCallback,failureCallback){
  //api/lookups/getsettings
  var newtoken = this.getTokenFormat();
  // let loading = this.loading.create({
   
  // }); 
  // loading.present()
  let headers = new HttpHeaders();

headers = headers.set('accept','application/json').set('content-type','application/json').set('token',newtoken);
let serviceUrl = this.helper.jsServiceUrl + 'lookups/getsettings';

    this.http.get(serviceUrl, { headers: headers})
    
      .subscribe(
        data => {
          // loading.dismiss()
          successCallback(JSON.stringify(data))
        },
        err => {
          // loading.dismiss()
          failureCallback(err)
        }
      )
}

}
